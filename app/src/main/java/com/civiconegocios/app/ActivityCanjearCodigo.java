package com.civiconegocios.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.OfertasRedimidasVO;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class ActivityCanjearCodigo extends BaseActivity implements
        LoaderManager.LoaderCallbacks<String> {

    private static final String VERIFYING_CODE_TITLE = "verifying_code_title";
    private static final String VERIFYING_CODE_SUBTITLE_1 = "verifying_code_subtitle_1";
    private static final String VERIFYING_CODE_SUBTITLE_2 = "verifying_code_subtitle_2";
    Toolbar defaultToolbar;
    GifImageView givImageView;
    Utils util = new Utils();
    TextView txtTituloCargando;
    TextView txtTituloEnProceso;
    TextView txtParrafoProcesoUno;
    TextView txtParrafoProcesoDos;
    TextView txtParrafoProcesoTres;
    com.google.android.gms.analytics.Tracker mTracker;
    String value;
    OfertasRedimidasVO ofertaRedimida;
    User user;
    String urlServicio = "/brand/";
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cargando_cobro_activity);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("KEY");
        }

        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        givImageView = findViewById(R.id.imgGif);

        txtTituloCargando = findViewById(R.id.txtTituloCargando);
        txtTituloEnProceso = findViewById(R.id.txtTituloEnProceso);
        txtParrafoProcesoUno = findViewById(R.id.txtParrafoProcesoUno);
        txtParrafoProcesoDos = findViewById(R.id.txtParrafoProcesoDos);
        txtParrafoProcesoTres = findViewById(R.id.txtParrafoProcesoTres);

        txtTituloEnProceso.setText(trackingOps.getmFirebaseRemoteConfig().getString(VERIFYING_CODE_TITLE));
        txtParrafoProcesoUno.setText(trackingOps.getmFirebaseRemoteConfig().getString(VERIFYING_CODE_SUBTITLE_1));
        txtParrafoProcesoDos.setText(trackingOps.getmFirebaseRemoteConfig().getString(VERIFYING_CODE_SUBTITLE_2));

        try {
            GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.cargando);
            givImageView.setImageDrawable(gifFromResource);

        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject manJson = new JSONObject();
        try {
            manJson.put("coupon_id", value);
            if (util.haveInternet(getApplicationContext())) {
                makeRequestOperation();
            } else {
                util.mensajeNoInternet(getApplicationContext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        toggleProgressDialog(false);
        if (data == null) {
            util.alertError500(this);
        } else {
            try {
                JSONObject catObj = new JSONObject(data);

                if (!catObj.isNull("offer_coupon")) {
                    JSONObject offer_coupon = catObj.getJSONObject("offer_coupon");
                    JSONArray arrayComent = offer_coupon.getJSONArray("comments");
                    String firstComment = "";
                    String secondsComment = "";
                    if (arrayComent.length() > 0) {
                        if (arrayComent.length() > 1) {
                            firstComment = arrayComent.getJSONObject(0).getString("text");
                            secondsComment = arrayComent.getJSONObject(1).getString("text");
                        } else {
                            firstComment = arrayComent.getJSONObject(0).getString("text");
                        }
                    }

                    ofertaRedimida = new OfertasRedimidasVO(offer_coupon.getString("id"), offer_coupon.getString("status"),
                            offer_coupon.getString("redemption_date"), offer_coupon.getString("offer_name"), offer_coupon.getInt("price") + "", offer_coupon.getInt("discount") + "",
                            offer_coupon.getString("user_name"), offer_coupon.getString("document_type"), offer_coupon.getString("document_number"), "", offer_coupon.getString("code"),
                            offer_coupon.getString("user_email"), firstComment, secondsComment);

                    finish();
                    Intent i = new Intent(this, ActivityRespuestaCanjeoCodigo.class);
                    i.putExtra("KEY", "valido");
                    startActivity(i);
                } else {
                    finish();
                    Intent i = new Intent(this, ActivityRespuestaCanjeoCodigo.class);
                    i.putExtra("KEY", catObj.getString("error"));
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                util.alertError500(this);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }

    public void makeRequestOperation() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {

            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            body.put("coupon_id", value);

            queryBundle.putString(Constants.OPERATION_TYPE, "PUT");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + urlServicio + user.getBrand().getId() + "/offer_coupons/redeem");
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_BODY, body.toString());


            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(0);

            if (loader == null) {
                loaderManager.initLoader(0, queryBundle, this);
            } else {
                loaderManager.restartLoader(0, queryBundle, this);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

}

