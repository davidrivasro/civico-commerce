package com.civiconegocios.app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.civiconegocios.app.R;
import com.civiconegocios.app.models.Message;
import com.civiconegocios.app.models.Response;
import com.civiconegocios.app.models.User;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.realm.RealmList;

public class MessageDetailExpandableListAdapter extends BaseExpandableListAdapter {

    String date = "";
    private Context context;
    private RealmList<Response> responses;
    private User user;
    private Message message;

    public MessageDetailExpandableListAdapter(Context context, RealmList<Response> responses, User user, Message message) {
        this.context = context;
        this.responses = responses;
        this.user = user;
        this.message = message;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.responses.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        Response response = (Response) getChild(groupPosition, 0);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.message_response_adapter, null);
        }

        TextView senderNameTextView = convertView.findViewById(R.id.senderNameTextView);
        TextView senderEmailTextView = convertView.findViewById(R.id.senderEmailTextView);
        TextView toNameTextView = convertView.findViewById(R.id.toNameTextView);
        if (response.getUser_id().equals("null")) {
            senderNameTextView.setText(user.getBrand().getName());
            senderEmailTextView.setText("de " + user.getEmail());
            toNameTextView.setText("para " + message.getName() + " " + message.getLastname());
        } else {
            senderNameTextView.setText("");
            senderEmailTextView.setText("");
            toNameTextView.setText("");
        }

        TextView messageTextView = convertView.findViewById(R.id.messageTextView);

        try {
            DateFormat dateTimeInstance = SimpleDateFormat.getDateTimeInstance();
            DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            Date firTermsDate = readFormat.parse(response.getCreated_at());
            TextView messageDateTextView = convertView.findViewById(R.id.messageDateTextView);
            messageDateTextView.setText(dateTimeInstance.format(firTermsDate));
            String decoded = new String(response.getText().getBytes("iso-8859-1"), "iso-8859-1");
            messageTextView.setText(decoded);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.responses.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.responses.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Response response = (Response) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.snap_message_response_adapter, null);
        }

        if (isExpanded) {
            convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, 10));
        } else {
            convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
            convertView.setVisibility(View.VISIBLE);
        }

        String text;

        if (response.getUser_id().equals("null")) {
            text = user.getBrand().getName() + ".— " + response.getText();
        } else {
            text = " " + ".— " + response.getText();
        }

        Spannable spannable = new SpannableString(text);

        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#59000000")), text.length() - response.getText().length(), text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView snapCreatedAtTextView = convertView.findViewById(R.id.snapCreatedAtTextView);
        TextView snapResponseTextView = convertView.findViewById(R.id.snapResponseTextView);
        snapResponseTextView.setText(spannable, TextView.BufferType.SPANNABLE);

        try {
            DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            Date firTermsDate = readFormat.parse(response.getCreated_at());
            long timeInMilliseconds = firTermsDate.getTime();

            date = DateUtils.getRelativeTimeSpanString(
                    timeInMilliseconds,
                    System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS,
                    DateUtils.FORMAT_ABBREV_ALL).toString();
            snapCreatedAtTextView.setText(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
