package com.civiconegocios.app.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.civiconegocios.app.ActivityEstadoCodigoDetalle;
import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.R;
import com.civiconegocios.app.VO.OfertasRedimidasVO;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.List;

public class AdapterItemOfertaRedimida extends
        RecyclerView.Adapter<AdapterItemOfertaRedimida.ViewHolder> {

    Utils util = new Utils();
    private List<OfertasRedimidasVO> mDeals;
    private Context mContext;
    private TrackingOps mtrackingOps;
    private User user;

    // Pass in the contact array into the constructor
    public AdapterItemOfertaRedimida(Context context, List<OfertasRedimidasVO> deals, TrackingOps trackingOps) {
        mDeals = deals;
        mContext = context;
        mtrackingOps = trackingOps;
        user = mtrackingOps.getUser();
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    @Override
    public AdapterItemOfertaRedimida.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View transactionView = inflater.inflate(R.layout.item_oferta_filtrada, parent, false);
        return new AdapterItemOfertaRedimida.ViewHolder(transactionView);
    }

    @Override
    public void onBindViewHolder(AdapterItemOfertaRedimida.ViewHolder holder, int position) {
        OfertasRedimidasVO transaction = mDeals.get(position);

        TextView nombreOfertaRedimidaTxt = holder.txtNombreOfertaRedimida;
        nombreOfertaRedimidaTxt.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(), R.drawable.ic_comercio_usuarios), null, null, null);
        nombreOfertaRedimidaTxt.setText(" " + transaction.getUser_name());

        TextView fechaOfertaRedimidaTxt = holder.txtFechaOfertaRedimida;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
        String date = format.format(util.convertStringDate(transaction.getRedemption_date()));
        fechaOfertaRedimidaTxt.setText(date);

        TextView detalleOfertaTxt = holder.txtDetalleOferta;
        detalleOfertaTxt.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(), R.drawable.ic_comercio_porcentaje), null, null, null);
        detalleOfertaTxt.setText(" " + transaction.getOffer_name());
    }

    @Override
    public int getItemCount() {
        return mDeals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtNombreOfertaRedimida, txtFechaOfertaRedimida,
                txtDetalleOferta;

        public ViewHolder(View itemView) {
            super(itemView);

            txtNombreOfertaRedimida = itemView.findViewById(R.id.txtNombreOfertaRedimida);
            txtFechaOfertaRedimida = itemView.findViewById(R.id.txtFechaOfertaRedimida);
            txtDetalleOferta = itemView.findViewById(R.id.txtDetalleOferta);
            itemView.setOnClickListener(this);
        }

        // Handles the row being being clicked
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) {
                OfertasRedimidasVO transaction = mDeals.get(position);

                mtrackingOps = new TrackingOps();
                mtrackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Ofertas redimidas", transaction.getOffer_name() + " | " + user.getCustomer().getName());

                Intent i = new Intent(getContext(), ActivityEstadoCodigoDetalle.class);
                i.putExtra("Object", transaction);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(i);
            }
        }
    }
}
