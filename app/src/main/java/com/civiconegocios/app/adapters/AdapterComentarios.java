package com.civiconegocios.app.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.civiconegocios.app.R;
import com.civiconegocios.app.VO.ComentariosVO;

import java.util.List;

public class AdapterComentarios extends
        RecyclerView.Adapter<AdapterComentarios.ViewHolder> {

    private List<ComentariosVO> mComments;
    private Context mContext;

    public AdapterComentarios(Context context, List<ComentariosVO> comments) {
        mComments = comments;
        mContext = context;
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public AdapterComentarios.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_comentarios, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterComentarios.ViewHolder holder, int position) {
        ComentariosVO comment = mComments.get(position);

        TextView commentTxt = holder.commentTextView;
        commentTxt.setText(comment.getText());

        TextView dateTxt = holder.dateTextView;
        dateTxt.setText(comment.getDate());
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView commentTextView;
        public TextView dateTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            commentTextView = itemView.findViewById(R.id.txtComentario);
            dateTextView = itemView.findViewById(R.id.fechaComentario);
        }
    }
}
