package com.civiconegocios.app.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.civiconegocios.app.ActivityDetalleBalance;
import com.civiconegocios.app.R;
import com.civiconegocios.app.VO.BalanceVO;

import java.util.List;

public class AdapterBalance extends
        RecyclerView.Adapter<AdapterBalance.ViewHolder> {

    private List<BalanceVO> mTransactions;
    private Context mContext;

    // Pass in the contact array into the constructor
    public AdapterBalance(Context context, List<BalanceVO> transactions) {
        mTransactions = transactions;
        mContext = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    @Override
    public AdapterBalance.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View transactionView = inflater.inflate(R.layout.item_ultimas_transferencias, parent, false);
        return new AdapterBalance.ViewHolder(transactionView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BalanceVO transaction = mTransactions.get(position);

        TextView transactionAmountTxt = holder.txtAmountBalance;
        transactionAmountTxt.setText("$" + transaction.getAmount());

        TextView transactionDateTxt = holder.txtFechaBalance;
        transactionDateTxt.setText(transaction.getUpdated_at().substring(0, 10));
    }

    @Override
    public int getItemCount() {
        return mTransactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtAmountBalance, txtFechaBalance;

        public ViewHolder(View itemView) {
            super(itemView);

            txtAmountBalance = itemView.findViewById(R.id.txtAmountBalance);
            txtFechaBalance = itemView.findViewById(R.id.txtFechaBalance);
            itemView.setOnClickListener(this);
        }

        // Handles the row being being clicked
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) {
                BalanceVO transaction = mTransactions.get(position);
                Intent i = new Intent(getContext(), ActivityDetalleBalance.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("fechaTotal", transaction.getUpdated_at());
                i.putExtra("valorTotal", transaction.getAmount());
                i.putExtra("reteIva", transaction.getReteiva());
                i.putExtra("reteRenta", transaction.getReterenta());
                i.putExtra("reteIca", transaction.getReteica());
                i.putExtra("valorTranferido", transaction.getTotal_transfer());
                i.putExtra("otrosDescuentos", transaction.getDiscounts());
                getContext().startActivity(i);
            }
        }

    }

}
