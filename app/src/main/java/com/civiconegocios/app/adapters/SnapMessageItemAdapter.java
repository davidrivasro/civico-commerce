package com.civiconegocios.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.civiconegocios.app.MessageDetailActivity;
import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.R;
import com.civiconegocios.app.models.Message;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.realm.RealmResults;

import static com.pushwoosh.internal.platform.AndroidPlatformModule.getApplicationContext;

public class SnapMessageItemAdapter extends
        RecyclerView.Adapter<SnapMessageItemAdapter.TheViewHolder> {

    Utils util = new Utils();
    private RealmResults<Message> messages;
    private Context mContext;

    public SnapMessageItemAdapter(Context context, RealmResults<Message> messages) {
        this.mContext = context;
        this.messages = messages;
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public TheViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.snap_message_item_adapter, parent, false);
        return new TheViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TheViewHolder holder, int position) {
        Message message = messages.get(position);

        holder.typeTextView.setText(message.getType());

        String text = message.getName() + " " + message.getLastname() + ".— " + message.getMessage();

        Spannable spannable = new SpannableString(text);

        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#59000000")), (message.getName() + message.getLastname()).length() + 2, (message.getName() + message.getLastname() + message.getMessage()).length() + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        holder.senderNameTextView.setText(spannable, TextView.BufferType.SPANNABLE);


        if (message.getStatus().equals("unread")) {
            holder.typeTextView.setTypeface(util.OpenSansBoldFont(getApplicationContext()));
            holder.createdAtTextView.setTypeface(util.OpenSansBoldFont(getApplicationContext()));
            holder.createdAtTextView.setTextColor(Color.parseColor("#003F95"));
        } else {
            holder.typeTextView.setTypeface(util.fuenteParrafos(getApplicationContext()));
            holder.createdAtTextView.setTypeface(util.fuenteParrafos(getApplicationContext()));
            holder.createdAtTextView.setTextColor(Color.parseColor("#232333"));
        }

        if (message.getRelated()) {
            holder.refferedTextView.setVisibility(View.VISIBLE);
        } else {
            holder.refferedTextView.setVisibility(View.GONE);
        }

        try {
            DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            Date firTermsDate = readFormat.parse(message.getCreated_at());
            long timeInMilliseconds = firTermsDate.getTime();

            holder.createdAtTextView.setText(
                    DateUtils.getRelativeTimeSpanString(
                            timeInMilliseconds,
                            System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS,
                            DateUtils.FORMAT_ABBREV_ALL).toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class TheViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView typeTextView, senderNameTextView,
                createdAtTextView, refferedTextView;

        public TheViewHolder(View itemView) {
            super(itemView);

            typeTextView = itemView.findViewById(R.id.typeTextView);
            senderNameTextView = itemView.findViewById(R.id.senderNameTextView);
            createdAtTextView = itemView.findViewById(R.id.createdAtTextView);
            refferedTextView = itemView.findViewById(R.id.tv_referido);
            itemView.setOnClickListener(this);
        }

        // Handles the row being being clicked
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) {
                Message message = messages.get(position);

                TrackingOps mtrackingOps = new TrackingOps();
                mtrackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Centro de mensajes", message.getMessage() + " | " + message.getName());

                Intent i = new Intent(getContext(), MessageDetailActivity.class);

                i.putExtra("id", message.getId());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(i);
            }
        }
    }


}
