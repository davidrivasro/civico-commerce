package com.civiconegocios.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.civiconegocios.app.R;
import com.civiconegocios.app.VO.SubMenuVO;
import com.civiconegocios.app.utils.Utils;

import java.util.List;

public class AdapterGridInicio extends ArrayAdapter<SubMenuVO> {

    Context context;
    Utils util = new Utils();
    private LayoutInflater inflate;
    private List<SubMenuVO> listSolcitudes;

    public AdapterGridInicio(Context context, List<SubMenuVO> listSolcitudes) {
        super(context, R.layout.itemgridinicio, listSolcitudes);
        inflate = LayoutInflater.from(context);
        this.listSolcitudes = listSolcitudes;
        this.context = context;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflate.inflate(R.layout.itemgridinicio, parent, false);
        TextView txtHorarioConfiguracion = convertView.findViewById(R.id.txtNumero);
        txtHorarioConfiguracion.setTypeface(util.fuenteCalculadora(context));
        if (getItem(position).getNumero().equals("C")) {
            txtHorarioConfiguracion.setTextColor(context.getResources().getColor(R.color.textoRojo));
        }
        if (getItem(position).getNumero().equals("DEL")) {
            txtHorarioConfiguracion.setTextColor(context.getResources().getColor(R.color.textoGris));
        }
        //final ImageView imgBtnCheck = (ImageView) convertView.findViewById(R.id.itemImgSubMenuCom);
        //int imgRecurso = context.getResources().getIdentifier(getItem(position).getNombreImagen(), "drawable", context.getPackageName());
        txtHorarioConfiguracion.setText(getItem(position).getNumero());
        return convertView;
    }
}