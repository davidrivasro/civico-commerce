package com.civiconegocios.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.civiconegocios.app.ActivityDetalleOferta;
import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.R;
import com.civiconegocios.app.VO.OfertasVO;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterItemOfertas extends
        RecyclerView.Adapter<AdapterItemOfertas.ViewHolder> {

    private static final String BTN_DEAL = "btn_deal";
    TextView txtOfertaMisOfertas;
    private List<OfertasVO> mDeals;
    private Context mContext;
    private TrackingOps mtrackingOps;
    private User user;


    public AdapterItemOfertas(Context context, List<OfertasVO> deals, TrackingOps trackingOps) {
        mDeals = deals;
        mContext = context;
        mtrackingOps = trackingOps;
        user = mtrackingOps.getUser();
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public AdapterItemOfertas.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_list_ofertas, parent, false);

        return new AdapterItemOfertas.ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(AdapterItemOfertas.ViewHolder holder, int position) {
        OfertasVO deal = mDeals.get(position);

        ImageView dealImage = holder.imgOferta;
        if (!deal.getImage().isEmpty()) {
            Picasso.with(getContext()).load(deal.getImage()).into(dealImage);
        }

        TextView title = holder.txtTituloOferta;
        title.setText(deal.getName());

        TextView finishTxt = holder.txtOfertaAcaba;
        finishTxt.setText(deal.getName());
        finishTxt.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(getContext(), R.drawable.ic_comercio_terminarojo), null, null, null);
        finishTxt.setText(" " + deal.getEnd_date_text());

        if (deal.getSoon_expiration().equals("true")) {
            finishTxt.setTextColor(getContext().getResources().getColor(R.color.textoRojo));
            Drawable img = AppCompatResources.getDrawable(getContext(), R.drawable.ic_comercio_terminarojo);
            finishTxt.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        } else {
            finishTxt.setTextColor(getContext().getResources().getColor(R.color.textoGris));
            Drawable img = AppCompatResources.getDrawable(getContext(), R.drawable.ic_comercio_termina);
            finishTxt.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        }
    }

    @Override
    public int getItemCount() {
        return mDeals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imgOferta;
        public TextView txtTituloOferta, txtOfertaAcaba;

        public ViewHolder(View itemView) {
            super(itemView);

            imgOferta = itemView.findViewById(R.id.imgOferta);
            txtTituloOferta = itemView.findViewById(R.id.txtTituloOferta);
            txtOfertaAcaba = itemView.findViewById(R.id.txtOfertaAcaba);
            itemView.setOnClickListener(this);
        }

        // Handles the row being being clicked
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) {
                mtrackingOps = new TrackingOps();
                mtrackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Lista de ofertas", mDeals.get(position).getName() + " - " + user.getCustomer().getName());
                Intent i = new Intent(getContext(), ActivityDetalleOferta.class);
                i.putExtra("Object", mDeals.get(position));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(i);
            }
        }

    }

}
