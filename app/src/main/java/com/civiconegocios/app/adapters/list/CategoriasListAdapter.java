package com.civiconegocios.app.adapters.list;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.R;
import com.civiconegocios.app.VO.QuestionsVO;
import com.civiconegocios.app.utils.UlTagHandler;
import com.civiconegocios.app.utils.Utils;

import java.util.List;


public class CategoriasListAdapter extends RecyclerView.Adapter<CategoriasListAdapter.CategoriasViewHolder> {

    private static final String REQUEST_COPY_INVOICE = "request_copy_invoice";
    private final String TAG = CategoriasListAdapter.class.getSimpleName();
    TrackingOps trackingOps;
    private List<QuestionsVO> categoriasCursor;
    private Context context;

    @Override
    public CategoriasViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        context = parent.getContext();
        int layoutIdForListItem = R.layout.categoria_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new CategoriasViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoriasViewHolder holder, int position) {
        if (categoriasCursor != null) {
            holder.tv_pregunta.setTypeface((new Utils()).fuenteParrafos(context));
            holder.tv_respuesta.setTypeface((new Utils()).fuenteHeader(context));
            holder.tv_pregunta.setText(categoriasCursor.get(position).getQuestion());
            holder.tv_respuesta.setText(Html.fromHtml(categoriasCursor.get(position).getAnswer(), null, new UlTagHandler()));
        }
    }

    @Override
    public int getItemCount() {
        if (categoriasCursor == null) return 0;
        return categoriasCursor.size();
    }

    public void setCategoriasCursor(List<QuestionsVO> data) {
        categoriasCursor = data;
        notifyDataSetChanged();
    }


    class CategoriasViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_pregunta;
        private TextView tv_respuesta;
        private ImageView myImageView;

        private CategoriasViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            trackingOps = new TrackingOps();

            tv_pregunta = itemView.findViewById(R.id.tv_pregunta);
            tv_pregunta.setText(trackingOps.getmFirebaseRemoteConfig().getString(REQUEST_COPY_INVOICE));
            tv_respuesta = itemView.findViewById(R.id.tv_respuesta);
            myImageView = itemView.findViewById(R.id.iv_flecha);
        }

        @Override
        public void onClick(View v) {
            boolean visible = categoriasCursor.get(getAdapterPosition()).isVisible();
            if (!visible) {
                categoriasCursor.get(getAdapterPosition()).setVisible(true);
                tv_respuesta.setVisibility(View.VISIBLE);
                Drawable ic_flecha_abajo = VectorDrawableCompat.create(context.getResources(), R.drawable.ic_flechaabajo, context.getTheme());
                myImageView.setImageDrawable(getRotateDrawable(ic_flecha_abajo));

            } else {
                categoriasCursor.get(getAdapterPosition()).setVisible(false);
                tv_respuesta.setVisibility(View.GONE);
                myImageView.setImageDrawable(VectorDrawableCompat.create(context.getResources(), R.drawable.ic_flechaabajo, context.getTheme()));
            }
        }

        private Drawable getRotateDrawable(final Drawable d) {
            final Drawable[] arD = {d};
            return new LayerDrawable(arD) {
                @Override
                public void draw(final Canvas canvas) {
                    canvas.save();
                    canvas.rotate((float) 180, d.getBounds().width() / 2, d.getBounds().height() / 2);
                    super.draw(canvas);
                    canvas.restore();
                }
            };
        }
    }

}
