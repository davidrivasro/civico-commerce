package com.civiconegocios.app.adapters.list;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.civiconegocios.app.R;
import com.civiconegocios.app.VO.CategoriasVO;
import com.civiconegocios.app.utils.Utils;

import java.util.ArrayList;

public class PreguntasFrecuentesListAdapter extends RecyclerView.Adapter<PreguntasFrecuentesListAdapter.PreguntasFrecuentesViewHolder> {

    private final PreguntasFrecuentesListener onClickListener;
    private ArrayList<CategoriasVO> preguntasFrecuentesCursor;
    private Context context;

    public PreguntasFrecuentesListAdapter(PreguntasFrecuentesListener listener) {
        onClickListener = listener;
    }

    @Override
    public PreguntasFrecuentesListAdapter.PreguntasFrecuentesViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        context = parent.getContext();
        int layoutIdForListItem = R.layout.preguntas_frecuentes_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new PreguntasFrecuentesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PreguntasFrecuentesListAdapter.PreguntasFrecuentesViewHolder holder, int position) {
        if (preguntasFrecuentesCursor != null) {
            holder.categoriaName.setTypeface((new Utils()).fuenteParrafos(context));
            holder.categoriaName.setText(preguntasFrecuentesCursor.get(position).getTitle());
        }
    }

    @Override
    public int getItemCount() {
        if (preguntasFrecuentesCursor == null) return 0;
        return preguntasFrecuentesCursor.size();
    }

    public void setPreguntasFrecuentesCursor(ArrayList<CategoriasVO> data) {
        preguntasFrecuentesCursor = data;
        notifyDataSetChanged();
    }

    public CategoriasVO getCategoria(long clickedItemIndex) {
        return this.preguntasFrecuentesCursor.get((int) clickedItemIndex);
    }

    public interface PreguntasFrecuentesListener {
        void onListItemClick(long clickedItemIndex);
    }

    class PreguntasFrecuentesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView categoriaName;

        private PreguntasFrecuentesViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            categoriaName = itemView.findViewById(R.id.tv_categorias);
        }

        @Override
        public void onClick(View v) {
            onClickListener.onListItemClick(getAdapterPosition());
        }
    }
}
