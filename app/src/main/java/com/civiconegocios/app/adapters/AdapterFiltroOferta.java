package com.civiconegocios.app.adapters;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.civiconegocios.app.R;
import com.civiconegocios.app.VO.FiltroOfertasVO;

import java.util.List;

public class AdapterFiltroOferta extends
        RecyclerView.Adapter<AdapterFiltroOferta.ViewHolder> {

    private List<FiltroOfertasVO> mFilters;
    private Context mContext;

    // Pass in the contact array into the constructor
    public AdapterFiltroOferta(Context context, List<FiltroOfertasVO> filters) {
        mFilters = filters;
        mContext = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    @Override
    public AdapterFiltroOferta.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View transactionView = inflater.inflate(R.layout.item_listo_filtro, parent, false);
        return new AdapterFiltroOferta.ViewHolder(transactionView);
    }

    @Override
    public void onBindViewHolder(AdapterFiltroOferta.ViewHolder holder, int position) {
        FiltroOfertasVO transaction = mFilters.get(position);

        AppCompatImageView imgCheckFiltro = holder.imgCheckFiltro;
        if (transaction.isCheckFiltro()) {
            imgCheckFiltro.setImageResource(R.drawable.ic_btn_checkon);
        } else {
            imgCheckFiltro.setImageResource(R.drawable.ic_btn_check);
        }

        TextView txtFiltro = holder.txtFiltro;
        txtFiltro.setText(transaction.getName());

    }

    @Override
    public int getItemCount() {
        return mFilters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public AppCompatImageView imgCheckFiltro;
        public TextView txtFiltro;

        public ViewHolder(View itemView) {
            super(itemView);

            imgCheckFiltro = itemView.findViewById(R.id.imgCheckFiltro);
            txtFiltro = itemView.findViewById(R.id.txtFiltro);
            itemView.setOnClickListener(this);
        }

        // Handles the row being being clicked
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) {
                FiltroOfertasVO transaction = mFilters.get(position);

                if (transaction.isCheckFiltro()) {
                    transaction.setCheckFiltro(false);
                } else {
                    transaction.setCheckFiltro(true);
                }
                notifyDataSetChanged();
            }
        }
    }

}
