package com.civiconegocios.app;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;

import static com.pushwoosh.internal.platform.AndroidPlatformModule.getApplicationContext;

public class MessageArchivedDialog extends AppCompatDialog implements View.OnClickListener {

    private static final String SENDED_DEAL_TITLE = "archived_message_title";
    private static final String SENDED_DEAL_TEXT = "archived_message_text";
    MessageDetailActivity activity;
    ImageView closeDialog;
    TextView dealActionButton, sendedDealTitle, sendedDealText;
    TrackingOps trackingOps;

    public MessageArchivedDialog(MessageDetailActivity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.message_archived_dialog);

        trackingOps = (TrackingOps) getApplicationContext();

        sendedDealTitle = findViewById(R.id.sendedDealTitle);
        sendedDealTitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(SENDED_DEAL_TITLE));

        sendedDealText = findViewById(R.id.sendedDealText);
        sendedDealText.setText(trackingOps.getmFirebaseRemoteConfig().getString(SENDED_DEAL_TEXT));

        closeDialog = findViewById(R.id.closeDialog);
        closeDialog.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.closeDialog:
                dismiss();
                this.activity.finish();
                break;
            default:
                break;
        }
    }
}
