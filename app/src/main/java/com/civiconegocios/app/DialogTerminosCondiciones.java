package com.civiconegocios.app;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;

import static com.pushwoosh.internal.platform.AndroidPlatformModule.getApplicationContext;

public class DialogTerminosCondiciones extends Dialog implements View.OnClickListener {


    private static final String LBL_TERMINOS_INICIAL = "lbl_terminos_inicial";
    private static final String LBL_TO_ACCEPT = "lbl_to_accept";
    private static final String LBL_THE = "lbl_the";
    private static final String LBL_AND_THE = "lbl_and_the";
    private static final String LBL_TERMS = "lbl_terms";
    private static final String LBL_CONDITIONS_PAY = "lbl_conditions_pay";
    private static final String LBL_AS_WELL_AS_THE = "lbl_as_well_as_the";
    private static final String LBL_POLITICS = "lbl_politics";
    private static final String LBL_TREATMENT_PERSONAL_DATA = "lbl_treatment_personal_data";
    private static final String LBL_CHANGE = "lbl_change";
    private static final String TOOLBAR_MENU_ABOUT_CIVICO = "toolbar_menu_about_civico";
    private static final String URL_TERMS_CONDITIONS = "url_terms_conditions";
    private static final String ABOUT_CIVICO_PAY = "about_civico_pay";
    private static final String URL_TERMS_CONDITIONS_CIVIC_PAY = "url_terms_conditions_civic_pay";
    private static final String PRIVACY_POLICIES = "privacy_policies";
    private static final String URL_PRIVACY_POLICIES = "url_privacy_policies";

    LinearLayout lnBtnCambiarDatos;
    TextView textThe, textTermsConditions, textAndThe, textTermsConditionsPay, textTermsConditionsPay2, textAsWellAsThe,
            textPolitics, textTreatmentPersonalData, textChange, lnBtnAceptar;
    ImageView imagenTerminos;
    inicioActivity c;

    TrackingOps trackingOps;
    User user;

    public DialogTerminosCondiciones(inicioActivity a) {
        super(a);

        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_terminos_condiciones);

        user = ((TrackingOps) getApplicationContext()).getUser();
        trackingOps = (TrackingOps) getApplicationContext();

        imagenTerminos = findViewById(R.id.imagenTerminos);

        Bitmap mbitmap = ((BitmapDrawable) ((TrackingOps) getApplicationContext()).getResources().getDrawable(R.drawable.terminos_cambiados)).getBitmap();
        Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
        Canvas canvas = new Canvas(imageRounded);
        Paint mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 16, 16, mpaint);

        Rect clipRect = new Rect(0, 16, mbitmap.getWidth(), mbitmap.getHeight());
        mpaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        canvas.drawRect(clipRect, mpaint);

        mbitmap.recycle();

        imagenTerminos.setImageBitmap(imageRounded);

        //imagenTerminos.setImageResource(R.drawable.terminos_cambiados);
        textThe = findViewById(R.id.textThe);
        textThe.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_THE));
        textTermsConditions = findViewById(R.id.textTermsConditions);
        textTermsConditions.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
        textTermsConditions.setOnClickListener(this);
        textAndThe = findViewById(R.id.textAndThe);
        textAndThe.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_AND_THE));
        textTermsConditionsPay = findViewById(R.id.textTermsConditionsPay);
        textTermsConditionsPay.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMS));
        textTermsConditionsPay.setOnClickListener(this);
        textTermsConditionsPay2 = findViewById(R.id.textTermsConditionsPay2);
        textTermsConditionsPay2.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CONDITIONS_PAY));
        textTermsConditionsPay2.setOnClickListener(this);
        textAsWellAsThe = findViewById(R.id.textAsWellAsThe);
        textAsWellAsThe.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_AS_WELL_AS_THE));
        textPolitics = findViewById(R.id.textPolitics);
        textPolitics.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_POLITICS));
        textPolitics.setOnClickListener(this);
        textTreatmentPersonalData = findViewById(R.id.textTreatmentPersonalData);
        textTreatmentPersonalData.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TREATMENT_PERSONAL_DATA));
        textTreatmentPersonalData.setOnClickListener(this);
        textChange = findViewById(R.id.textChange);
        textChange.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CHANGE));
        lnBtnAceptar = findViewById(R.id.lnBtnAceptar);
        lnBtnAceptar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TO_ACCEPT).toUpperCase());
        lnBtnCambiarDatos = findViewById(R.id.lnBtnCambiarDatos);
        lnBtnCambiarDatos.setOnClickListener(this);

    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.lnBtnCambiarDatos:
                this.c.makeTermsAcceptanceUpdateRequest();
                dismiss();
                break;
            case R.id.textTermsConditions:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_ABOUT_CIVICO), trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_TERMS_CONDITIONS) + user.getCity() + "&layout=false");
                break;
            case R.id.textTermsConditionsPay:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_TERMS_CONDITIONS_CIVIC_PAY) + user.getCity() + "&layout=false");
                break;
            case R.id.textTermsConditionsPay2:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_TERMS_CONDITIONS_CIVIC_PAY) + user.getCity() + "&layout=false");
                break;
            case R.id.textPolitics:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(PRIVACY_POLICIES));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_PRIVACY_POLICIES) + user.getCity() + "&layout=false");
                break;
            case R.id.textTreatmentPersonalData:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(PRIVACY_POLICIES));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_PRIVACY_POLICIES) + user.getCity() + "&layout=false");
                break;
            default:
                break;
        }

    }

    public void goToWebView(String url) {
        Intent detalle = new Intent(getApplicationContext(), WebSobreCivicoActivity.class);
        detalle.putExtra("KEY", url);
        c.startActivity(detalle);
    }


}
