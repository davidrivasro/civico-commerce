package com.civiconegocios.app;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.ParseException;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.FiltroOfertasVO;
import com.civiconegocios.app.adapters.AdapterFiltroOferta;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class ActivityAplicarFiltros extends AppCompatActivity implements View.OnClickListener {

    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String LBL_BTN_APPLY = "lbl_btn_apply";
    private static final String BTN_DEAL = "btn_deal";
    private static final String DATE = "date";
    private static final String AVAILABILITY = "availability";
    private static final String FROM = "from";
    private static final String TO = "to";
    private static final String AVAILABLE_CODE = "available_code";
    private static final String REDEEMED_DEAL = "redeemed_deal";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    //    UI
    Toolbar defaultToolbar;
    RecyclerView lista_opciones_filtro;
    AdapterFiltroOferta adapter;
    TextView btnCodigoDisponible;
    AppCompatTextView btnOfertaRedimida;
    LinearLayout btnDesdeFiltro;
    LinearLayout btnHastaFiltro;
    TextView txtDesdeFiltro;
    TextView txtHastaFiltro;
    String txtCodDis = "";
    String txtOfer = "";
    boolean desdeBol = false;
    boolean hastaBol = false;
    TextView tv_aplicar_filtros;
    TextView txtAtrasAplicarFiltros;
    TextView tv_oferta;
    TextView textOpenSansRegular;
    TextView textOpenSansRegular3;
    TextView lblDesde;
    TextView lblHasta;
    Drawable imgCheck, imgCheckOn;
    Utils util = new Utils();
    String page = "";
    String limit = "";
    String startDate = "";
    String endDate = "";
    String status = "";
    String filteredNames = "";
    String dealsIds = "";
    ArrayList<FiltroOfertasVO> filterList;
    TrackingOps trackingOps;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            arg2 = arg2 + 1;

            if (hastaBol) {
                if (arg2 >= 1 && arg2 <= 9) { // mes esta entre el 1 y el 9
                    if (arg3 >= 1 && arg3 <= 9) { // dia esta entre 1 y 9
                        endDate = arg1 + "-0" + arg2 + "-0" + arg3;
                    } else {// demas dias 9 al 31 o 30
                        endDate = arg1 + "-0" + arg2 + "-" + arg3;
                    }

                } else {// demas meses 10 al 12
                    if (arg3 >= 1 && arg3 <= 9) {  // dia esta entre 1 y 9
                        endDate = arg1 + "-" + arg2 + "-0" + arg3;
                    } else {// demas dias 9 al 31 o 30
                        endDate = arg1 + "-" + arg2 + "-" + arg3;
                    }

                }

                txtHastaFiltro.setText(StringToDate(arg1 + "-" + arg2 + "-" + arg3));
            } else if (desdeBol) {
                if (arg2 >= 1 && arg2 <= 9) {
                    if (arg3 >= 1 && arg3 <= 9) {
                        startDate = arg1 + "-0" + arg2 + "-0" + arg3;
                    } else {
                        startDate = arg1 + "-0" + arg2 + "-" + arg3;
                    }

                } else {
                    if (arg3 >= 1 && arg3 <= 9) {
                        startDate = arg1 + "-" + arg2 + "-0" + arg3;
                    } else {
                        startDate = arg1 + "-" + arg2 + "-" + arg3;
                    }
                }

                txtDesdeFiltro.setText(StringToDate(arg1 + "-" + arg2 + "-" + arg3));
            }
        }
    };

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aplicar_filtros);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            filterList = extras.getParcelableArrayList("filterList");
        }

        trackingOps = (TrackingOps) getApplication();

        defaultToolbar = findViewById(R.id.toolbar);
        defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        setSupportActionBar(defaultToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        lista_opciones_filtro = findViewById(R.id.lista_opciones_filtro);
        adapter = new AdapterFiltroOferta(getApplicationContext(), filterList);
        lista_opciones_filtro.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        lista_opciones_filtro.setLayoutManager(linearLayoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        lista_opciones_filtro.addItemDecoration(itemDecorator);
        lista_opciones_filtro.setHasFixedSize(true);

        tv_aplicar_filtros = findViewById(R.id.tv_aplicar_filtros);
        tv_aplicar_filtros.setOnClickListener(this);
        tv_aplicar_filtros.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_APPLY).toUpperCase());

        tv_oferta = findViewById(R.id.tv_oferta);
        tv_oferta.setText(trackingOps.getmFirebaseRemoteConfig().getString(BTN_DEAL));

        textOpenSansRegular = findViewById(R.id.tv_fecha);
        textOpenSansRegular.setText(trackingOps.getmFirebaseRemoteConfig().getString(DATE));

        textOpenSansRegular3 = findViewById(R.id.tv_disponibilidad);
        textOpenSansRegular3.setText(trackingOps.getmFirebaseRemoteConfig().getString(AVAILABILITY));

        lblDesde = findViewById(R.id.tv_desde);
        lblDesde.setText(trackingOps.getmFirebaseRemoteConfig().getString(FROM));

        lblHasta = findViewById(R.id.tv_hasta);
        lblHasta.setText(trackingOps.getmFirebaseRemoteConfig().getString(TO));


        txtAtrasAplicarFiltros = findViewById(R.id.tv_atras_aplicar_filtros);
        txtAtrasAplicarFiltros.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);
        txtAtrasAplicarFiltros.setOnClickListener(this);
        txtAtrasAplicarFiltros.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Double vet = size.x / 1.5;
        txtAtrasAplicarFiltros.setWidth(vet.intValue());


        btnDesdeFiltro = findViewById(R.id.btn_desde_filtro);
        btnHastaFiltro = findViewById(R.id.btn_hasta_filtro);

        txtDesdeFiltro = findViewById(R.id.tv_desde_filtro);
        txtHastaFiltro = findViewById(R.id.tv_hasta_filtro);


        btnCodigoDisponible = findViewById(R.id.btn_codigo_disponible);
        btnCodigoDisponible.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(AVAILABLE_CODE));

        btnOfertaRedimida = findViewById(R.id.btn_oferta_redimida);
        btnOfertaRedimida.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(REDEEMED_DEAL));

        btnCodigoDisponible.setOnClickListener(this);
        btnOfertaRedimida.setOnClickListener(this);
        btnDesdeFiltro.setOnClickListener(this);
        btnHastaFiltro.setOnClickListener(this);


        imgCheck = AppCompatResources.getDrawable(this, R.drawable.ic_btn_check);
        imgCheckOn = AppCompatResources.getDrawable(this, R.drawable.ic_btn_checkon);
        btnCodigoDisponible.setCompoundDrawablesWithIntrinsicBounds(imgCheck, null, null, null);
        btnCodigoDisponible.setTypeface(util.fuenteHeader(getApplicationContext()));
        btnOfertaRedimida.setCompoundDrawablesWithIntrinsicBounds(imgCheck, null, null, null);
        btnOfertaRedimida.setTypeface(util.fuenteHeader(getApplicationContext()));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_codigo_disponible:
                if (txtCodDis.equals("")) {
                    txtCodDis = "acquirable";
                    btnCodigoDisponible.setCompoundDrawablesWithIntrinsicBounds(imgCheckOn, null, null, null);
                } else {
                    txtCodDis = "";
                    btnCodigoDisponible.setCompoundDrawablesWithIntrinsicBounds(imgCheck, null, null, null);
                }
                break;
            case R.id.btn_oferta_redimida:
                if (txtOfer.equals("")) {
                    txtOfer = "redeemed";
                    btnOfertaRedimida.setCompoundDrawablesWithIntrinsicBounds(imgCheckOn, null, null, null);
                } else {
                    txtOfer = "";
                    btnOfertaRedimida.setCompoundDrawablesWithIntrinsicBounds(imgCheck, null, null, null);
                }
                break;
            case R.id.btn_desde_filtro:
                showDialog(999);
                desdeBol = true;
                hastaBol = false;
                break;
            case R.id.btn_hasta_filtro:
                showDialog(999);
                desdeBol = false;
                hastaBol = true;
                break;
            case R.id.tv_aplicar_filtros:
                page = "1";
                limit = "10";
                if (txtOfer.equals("") && txtCodDis.equals("")) {
                    status = "";
                } else if (txtOfer.equals("") && !txtCodDis.equals("")) {
                    status = txtCodDis;
                } else if (!txtOfer.equals("") && txtCodDis.equals("")) {
                    status = txtOfer;
                } else if (!txtOfer.equals("") && !txtCodDis.equals("")) {
                    status = txtCodDis + "," + txtOfer;
                }
                obtenerCaracteristicas();
                break;
            case R.id.tv_atras_aplicar_filtros:
                page = "1";
                limit = "10";

                finish();
                break;
            default:
                break;
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            Calendar calendar = Calendar.getInstance();
            int thisYear = calendar.get(Calendar.YEAR);
            int thisMonth = calendar.get(Calendar.MONTH);
            int thisDay = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(this, myDateListener, thisYear, thisMonth + 1, thisDay);
        }
        return null;
    }

    public String StringToDate(String fecha) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try {
            date = format.parse(fecha);

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return DateToString(date);
    }

    public String DateToString(java.util.Date date) {

        SimpleDateFormat dateformat = new SimpleDateFormat("EE yyyy-MM-dd");
        String datetime = "";
        try {
            datetime = dateformat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return datetime;
    }

    public void obtenerCaracteristicas() {
        String caracteristicasFiltradas = "";
        String nombreCaracteristicasFiltradas = "";

        // categorias
        for (int a = 0; a < filterList.size(); a++) {
            if (filterList.get(a).isCheckFiltro()) {
                caracteristicasFiltradas += filterList.get(a).getId() + ",";
                nombreCaracteristicasFiltradas += filterList.get(a).getName() + ",";
            }
        }

        if (!caracteristicasFiltradas.equals("")) {
            dealsIds = caracteristicasFiltradas.substring(0, caracteristicasFiltradas.length() - 1).replace(" ", "");
            filteredNames = nombreCaracteristicasFiltradas.substring(0, nombreCaracteristicasFiltradas.length() - 1).replace(",", "");

        } else {
            dealsIds = "";
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("page", page);
        returnIntent.putExtra("limit", limit);
        returnIntent.putExtra("startDate", startDate);
        returnIntent.putExtra("endDate", endDate);
        returnIntent.putExtra("status", status);
        returnIntent.putExtra("filteredNames", filteredNames);
        returnIntent.putExtra("dealsIds", dealsIds);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
