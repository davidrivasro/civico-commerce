package com.civiconegocios.app;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.util.Log;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.perf.metrics.AddTrace;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.onesignal.OneSignal;
import com.pushwoosh.Pushwoosh;
import com.pushwoosh.exception.PushwooshException;
import com.pushwoosh.exception.RegisterForPushNotificationsException;
import com.pushwoosh.function.Callback;
import com.pushwoosh.function.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<String> {

    private static final String TAG = SplashActivity.class.getSimpleName();
    public static String APP_VERSION;
    private final int SIGN_IN_LOADER = 10;
    private final int MESSAGES_DATA_LOADER = 13;
    TrackingOps trackingOps;
    String signInEndpoint = "/oauth/token";
    Utils util = new Utils();
    User user;

    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        user = ((TrackingOps) this.getApplication()).getUser();
        String city = user != null && user.isValid() ? user.getCity() : "Bogotá";
        if (user != null && !user.isValid()) {
            user = null;
        }

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.IS_DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        long cacheExpiration = 3600;
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            mFirebaseRemoteConfig.activateFetched();
                        }
                    }
                });

        if (!BuildConfig.IS_DEBUG)
            Fabric.with(this, new Crashlytics());

        trackingOps = (TrackingOps) getApplication();
        Uri data = getIntent().getData();
        if (data != null) {
            String campaignData = data.toString();
            if (campaignData.contains("utm_campaign")) {
                trackingOps.registerScreenName("SplashActivity | Cívico PAY Comercios | " + city);
            }
        }

        try {
            JSONObject tags = new JSONObject();
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            tags.put("app_version", pInfo.versionCode);
            OneSignal.sendTags(tags);
        } catch (Exception e) {
            Log.i(TAG, "Exception: " + e.getMessage());
        }

        Pushwoosh.getInstance().registerForPushNotifications(new Callback<String, RegisterForPushNotificationsException>() {
            @Override
            public void process(@NonNull Result<String, RegisterForPushNotificationsException> result) {
                if (result.isSuccess()) {
                    String token = result.getData();
                } else {
                    PushwooshException exception = result.getException();
                }
            }
        });

        Pushwoosh.getInstance().getPushToken();
        try {
            String userId = Pushwoosh.getInstance().getHwid();
            Crashlytics.setUserIdentifier(userId);
        } catch (IllegalStateException ignore) {
        }

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            if (pInfo != null) APP_VERSION = String.valueOf(pInfo.versionCode);
        } catch (PackageManager.NameNotFoundException ignored) {
        }

        if (user == null) {
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            finish();
        } else {
            try {
                String type = getIntent().getExtras().getString("typePush");
                if (type == null) startNewActivity(true);
                else handlerPushWooshMessage(type);
            } catch (Exception e) {
                if (user == null) makeSigninRequest();
                else startNewActivity(true);
            }
        }
    }

    private void handlerPushWooshMessage(String type) {
        if (type != null) {
            switch (type) {
                case "transactions":
                    Intent transactions = new Intent(getApplicationContext(), MisPagosActivity.class);
                    startActivity(transactions);
                    finish();
                    break;

                case "balance":
                    Intent balance = new Intent(getApplicationContext(), BalanceActivity.class);
                    startActivity(balance);
                    finish();
                    break;

                case "new_offer":
                    Intent new_offer = new Intent(getApplicationContext(), CreateDealActivity.class);
                    startActivity(new_offer);
                    finish();
                    break;

                case "deals_redeemed":
                    Intent deals_redeemed = new Intent(getApplicationContext(), ActivityOfertasRedimidas.class);
                    startActivity(deals_redeemed);
                    finish();
                    break;

                case "message_center":
                    Intent message_center = new Intent(getApplicationContext(), InboxActivity.class);
                    if (getIntent().getExtras().getString("id_message") != null) {
                        if (!getIntent().getExtras().getString("id_message").equals("")) {
                            message_center.putExtra("id_message", getIntent().getExtras().getString("id_message"));
                        }
                    }
                    startActivity(message_center);
                    finish();
                    break;

                case "profile":
                    Intent profile = new Intent(getApplicationContext(), PerfilActivity.class);
                    startActivity(profile);
                    finish();
                    break;

                case "payment_update":
                    trackingOps.setJsonProcesoPago(getIntent().getExtras().getString(Pushwoosh.PUSH_RECEIVE_EVENT));
                    Intent detalle = new Intent(getApplicationContext(), inicioActivity.class);
                    detalle.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(detalle);
                    finish();
                    break;

                default:
                    startNewActivity(user == null);
                    break;
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void startNewActivity(boolean result) {
        if (result) {
            if (user.getAcceptingPayments()) {
                Intent detalle = new Intent(getApplicationContext(), inicioActivity.class);
                detalle.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(detalle);
                finish();
            } else {
                Intent detalle = new Intent(getApplicationContext(), ActivityListarOfertas.class);
                detalle.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(detalle);
                finish();
            }
        } else {
            Intent detalle = new Intent(SplashActivity.this, LoginActivity.class);
            if (user != null && user.getAcceptingPayments())
                detalle.putExtra("SHOW_PAY", false);
            else detalle.putExtra("SHOW_PAY", true);
            startActivity(detalle);
            finish();
        }
    }

    public void makeMessagesDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, "/business/customers/places/" + user.getBrand().getId() + "/messages?page=1&limit=20&auth_token=" + user.getAccessToken());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader loader = loaderManager.getLoader(MESSAGES_DATA_LOADER);

            if (loader == null) {
                loaderManager.initLoader(MESSAGES_DATA_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(MESSAGES_DATA_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (Exception e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void makeSigninRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            body.put("grant_type", "password");
            body.put("client_id", Constants.CLIENT_ID);
            body.put("client_secret", Constants.CLIENT_SECRET);
            body.put("username", user.getEmail());
            body.put("password", util.decode(user.getPassword()));

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.OAuth_URL + signInEndpoint);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(SIGN_IN_LOADER);

            if (loader == null) {
                loaderManager.initLoader(SIGN_IN_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(SIGN_IN_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (Exception e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        try {
            switch (loader.getId()) {
                case SIGN_IN_LOADER:
                    try {
                        JSONObject catObj = new JSONObject(data);
                        String accesToken = catObj.getString("access_token");
                        if (accesToken.length() > 0) {
                            makeMessagesDataRequest();
                        } else {
                            startNewActivity(false);
                        }
                    } catch (Exception error) {
                        Log.i(TAG, "Exception : " + error.getMessage());
                        startNewActivity(user == null);
                    }
                    break;

                case MESSAGES_DATA_LOADER:
                    try {
                        JSONObject dataObject = new JSONObject(data);
                        JSONObject metadata = dataObject.getJSONObject("metadata");
                        trackingOps.setUnreadMessagesCount(metadata.getInt("total_unread_messages"));
                        startNewActivity(true);
                    } catch (Exception error) {
                        Log.i(TAG, "Exception : " + error.getMessage());
                        startNewActivity(true);
                    }
                    break;

                default:
                    startNewActivity(user == null);
                    break;
            }
        } catch (Exception error) {
            error.printStackTrace();
            startNewActivity(user == null);
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        long cacheExpiration = 3600;

        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        Log.i(TAG, "cacheExpiration: " + cacheExpiration);

        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            mFirebaseRemoteConfig.activateFetched();
                        }
                    }
                });
    }
}
