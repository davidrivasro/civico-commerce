package com.civiconegocios.app;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.core.content.ContextCompat;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.ComentariosVO;
import com.civiconegocios.app.adapters.AdapterComentarios;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ActivityComentarios extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<String> {

    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String COMMENTS = "comments";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    //    UI
    Toolbar defaultToolbar;
    TextView txtAtrasComentarios;
    RecyclerView listComentarios;
    TextView toolbar_title;
    String value;
    Utils util = new Utils();
    List<ComentariosVO> comentariosArray;
    AdapterComentarios adapter;
    LinearLayout lnNoComentarios;
    User user;
    String urlServicio = "/brand/";
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);

        trackingOps = (TrackingOps) getApplicationContext();

        defaultToolbar = findViewById(R.id.toolbar);
        defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        setSupportActionBar(defaultToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        user = ((TrackingOps) getApplication()).getUser();

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(COMMENTS));

        txtAtrasComentarios = findViewById(R.id.txtAtrasComentarios);
        txtAtrasComentarios.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        txtAtrasComentarios.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);
        txtAtrasComentarios.setOnClickListener(this);

        lnNoComentarios = findViewById(R.id.lnNoComentarios);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("KEY");
        }

        listComentarios = findViewById(R.id.listComentarios);
        comentariosArray = new ArrayList<>();
        adapter = new AdapterComentarios(this, comentariosArray);
        listComentarios.setAdapter(adapter);
        listComentarios.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(AppCompatResources.getDrawable(this, R.drawable.transparent_divider));
        listComentarios.addItemDecoration(itemDecorator);

        if (util.haveInternet(getApplicationContext())) {
            makeRequestOperation();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtAtrasComentarios:
                finish();
                break;
            default:
                break;
        }
    }

    public void mostrarNoResultados() {
        listComentarios.setVisibility(View.GONE);
        lnNoComentarios.setVisibility(View.VISIBLE);
    }

    public void makeRequestOperation() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, urlServicio + user.getBrand().getId() + "/offer_coupons/" + value + "/comments");
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(0);

            if (loader == null) {
                loaderManager.initLoader(0, queryBundle, this);
            } else {
                loaderManager.restartLoader(0, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        toggleProgressDialog(false);
        if (data == null) {
            util.alertErrorTimeOut(this);
        } else {
            try {
                JSONObject catObj = new JSONObject(data);
                List<ComentariosVO> hoyList = new ArrayList<>();
                JSONArray arrayOffers = catObj.getJSONArray("comments");
                if (arrayOffers.length() > 0) {
                    for (int i = 0; i < arrayOffers.length(); i++) {
                        ComentariosVO categoriasInfo = new ComentariosVO();
                        JSONObject teamObj = arrayOffers.getJSONObject(i);
                        categoriasInfo.setId(teamObj.getString("comment_id"));
                        categoriasInfo.setText(teamObj.getString("text"));
                        categoriasInfo.setDate(teamObj.getString("date"));
                        categoriasInfo.setUser(teamObj.getString("user"));
                        hoyList.add(categoriasInfo);
                    }
                    comentariosArray.addAll(hoyList);
                    adapter.notifyDataSetChanged();
                } else {
                    mostrarNoResultados();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }
}
