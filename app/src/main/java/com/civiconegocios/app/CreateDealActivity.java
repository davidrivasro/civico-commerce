package com.civiconegocios.app;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.core.content.ContextCompat;
import androidx.loader.content.Loader;
import androidx.appcompat.content.res.AppCompatResources;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.dialogs.SendedDealDialog;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CreateDealActivity extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<String> {

    private final String TAG = CreateDealActivity.class.getSimpleName();

    private static final String NEW_DEAL = "new_deal";
    private static final String NEW_DEAL_TITLE = "new_deal_title";
    private static final String NEW_DEAL_TITLE_HINT = "new_deal_title_hint";
    private static final String NEW_DEAL_START_DATE_TITLE = "new_deal_start_date_title";
    private static final String NEW_DEAL_END_DATE_TITLE = "new_deal_end_date_title";
    private static final String NEW_DEAL_INITIAL_PRICE_TITLE = "new_deal_initial_price_title";
    private static final String NEW_DEAL_INITIAL_PRICE_HINT = "new_deal_initial_price_hint";
    private static final String NEW_DEAL_DISCOUNT_PRICE_TITLE = "new_deal_discount_price_title";
    private static final String NEW_DEAL_DISCOUNT_PRICE_HINT = "new_deal_discount_price_hint";
    private static final String NEW_DEAL_DESCRIPTION_TITLE = "new_deal_description_title";
    private static final String NEW_DEAL_DESCRIPTION_HINT = "new_deal_description_hint";
    private static final String NEW_DEAL_CONDITIONS_TITLE = "new_deal_conditions_title";
    private static final String NEW_DEAL_CONDITIONS_HINT = "new_deal_conditions_hint";
    private static final String NEW_DEAL_ACTION = "new_deal_action";
    private static final String INCOMPLETE_INFORMATION = "incomplete_information";
    private static final String INCOMPLETE_INFORMATION_SUBTITLE = "incomplete_information_subtitle";
    private static final String DEAL_PRICE_ERROR = "deal_price_error";
    private static final String ERROR = "error";
    private static final String DEAL_DATES_ERROR = "deal_dates_error";
    private final int SEND_DATA = 10;
    TrackingOps trackingOps;
    TextView toolbarTitle, dealNameLabel, dealStartDateLabel, dealStartText,
            dealEndDateLabel, dealEndText, dealInitialPriceLabel, dealDiscountPriceLabel,
            dealDescriptionLabel, dealConditionsLabel, dealActionButton;
    EditText dealNameEditText, dealInitialPriceText, dealDiscountPriceText, dealDescriptionText,
            dealConditionsText;
    String initialDate, finalDate;
    Calendar selectedInitialDate = Calendar.getInstance();
    Calendar selectedFinalDate = Calendar.getInstance();
    Utils util = new Utils();
    Boolean isInitialDate;
    Boolean isShowingDialog = false;
    User user;
    private String initialPrice = "";
    private String discountPrice = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_deal);

        initDrawer();

        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL));

        dealNameLabel = findViewById(R.id.dealNameLabel);
        dealNameLabel.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_TITLE));
        dealNameLabel.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_porcentaje), null, null, null);

        dealNameEditText = findViewById(R.id.dealNameEditText);
        dealNameEditText.setHint(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_TITLE_HINT));
        dealNameEditText.setTypeface(util.fuenteHeader(getApplicationContext()));

        dealStartDateLabel = findViewById(R.id.dealStartDateLabel);
        dealStartDateLabel.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_START_DATE_TITLE));
        dealStartDateLabel.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_calendar), null, null, null);

        dealStartText = findViewById(R.id.dealStartText);
        dealStartText.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_START_DATE_TITLE));
        dealStartText.setOnClickListener(this);

        dealEndDateLabel = findViewById(R.id.dealEndDateLabel);
        dealEndDateLabel.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_END_DATE_TITLE));
        dealEndDateLabel.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_vigencia), null, null, null);

        dealEndText = findViewById(R.id.dealEndText);
        dealEndText.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_END_DATE_TITLE));
        dealEndText.setOnClickListener(this);

        dealInitialPriceLabel = findViewById(R.id.dealInitialPriceLabel);
        dealInitialPriceLabel.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_INITIAL_PRICE_TITLE));
        dealInitialPriceLabel.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_price), null, null, null);

        dealInitialPriceText = findViewById(R.id.dealInitialPriceText);
        dealInitialPriceText.setHint(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_INITIAL_PRICE_HINT));
        dealInitialPriceText.setTypeface(util.fuenteHeader(getApplicationContext()));
        dealInitialPriceText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(initialPrice) && !s.toString().isEmpty()) {
                    dealInitialPriceText.removeTextChangedListener(this);
                    initialPrice = s.toString().replace("$", "");
                    initialPrice = miles(initialPrice).replace(",", ".");
                    dealInitialPriceText.setText("$" + initialPrice);
                    dealInitialPriceText.setSelection(initialPrice.length() + 1);

                    dealInitialPriceText.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dealDiscountPriceLabel = findViewById(R.id.dealDiscountPriceLabel);
        dealDiscountPriceLabel.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_DISCOUNT_PRICE_TITLE));
        dealDiscountPriceLabel.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_descuento), null, null, null);

        dealDiscountPriceText = findViewById(R.id.dealDiscountPriceText);
        dealDiscountPriceText.setHint(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_DISCOUNT_PRICE_HINT));
        dealDiscountPriceText.setTypeface(util.fuenteHeader(getApplicationContext()));
        dealDiscountPriceText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(discountPrice) && !s.toString().isEmpty()) {
                    dealDiscountPriceText.removeTextChangedListener(this);
                    discountPrice = s.toString().replace("$", "");
                    discountPrice = miles(discountPrice).replace(",", ".");
                    dealDiscountPriceText.setText("$" + discountPrice);
                    dealDiscountPriceText.setSelection(discountPrice.length() + 1);

                    dealDiscountPriceText.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dealDescriptionLabel = findViewById(R.id.dealDescriptionLabel);
        dealDescriptionLabel.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_DESCRIPTION_TITLE));
        dealDescriptionLabel.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_black_check), null, null, null);

        dealDescriptionText = findViewById(R.id.dealDescriptionText);
        dealDescriptionText.setHint(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_DESCRIPTION_HINT));
        dealDescriptionText.setTypeface(util.fuenteHeader(getApplicationContext()));

        dealConditionsLabel = findViewById(R.id.dealConditionsLabel);
        dealConditionsLabel.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_CONDITIONS_TITLE));
        dealConditionsLabel.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_conditions), null, null, null);

        dealConditionsText = findViewById(R.id.dealConditionsText);
        dealConditionsText.setHint(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_CONDITIONS_HINT));
        dealConditionsText.setTypeface(util.fuenteHeader(getApplicationContext()));

        dealActionButton = findViewById(R.id.dealActionButton);
        dealActionButton.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_ACTION));
        dealActionButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dealStartText:
                isInitialDate = true;
                showDialog(1);
                break;
            case R.id.dealEndText:
                isInitialDate = false;
                showDialog(1);
                break;
            case R.id.dealActionButton:

                if (dealNameEditText.getText().toString().matches("") || dealDescriptionText.getText().toString().matches("") ||
                        dealConditionsText.getText().toString().matches("") || initialDate == null || initialDate.isEmpty() || finalDate == null ||
                        finalDate.isEmpty()) {
                    util.presentDialogWith(CreateDealActivity.this, trackingOps.getmFirebaseRemoteConfig().getString(INCOMPLETE_INFORMATION), trackingOps.getmFirebaseRemoteConfig().getString(INCOMPLETE_INFORMATION_SUBTITLE));
                } else {
                    if (initialPrice != null && discountPrice != null &&
                            !initialPrice.isEmpty() && !discountPrice.isEmpty()) {
                        int dealInitial = Integer.parseInt(initialPrice.replace(".", ""));
                        int dealDiscount = Integer.parseInt(discountPrice.replace(".", ""));
                        if (dealInitial < dealDiscount) {
                            setRedBorder(dealDiscountPriceText);
                            setRedBorder(dealInitialPriceText);
                            util.presentDialogWith(CreateDealActivity.this, trackingOps.getmFirebaseRemoteConfig().getString(INCOMPLETE_INFORMATION), trackingOps.getmFirebaseRemoteConfig().getString(DEAL_PRICE_ERROR));
                        } else {
                            makeDataRequest();
                        }
                    } else {
                        makeDataRequest();
                    }
                }
                if (dealNameEditText.getText().toString().matches("")) {
                    setRedBorder(dealNameEditText);
                }
                if (dealDescriptionText.getText().toString().matches("")) {
                    setRedBorder(dealDescriptionText);
                }
                if (dealConditionsText.getText().toString().matches("")) {
                    setRedBorder(dealConditionsText);
                }
                if (initialDate == null || initialDate.isEmpty()) {
                    setRedBorder(dealStartText);
                }
                if (finalDate == null || finalDate.isEmpty()) {
                    setRedBorder(dealEndText);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar calendar = Calendar.getInstance();

        int thisYear = calendar.get(Calendar.YEAR);
        int thisMonth = calendar.get(Calendar.MONTH);
        int thisDay = calendar.get(Calendar.DAY_OF_MONTH);

        if (id == 1) {

            return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    if (isInitialDate) {
                        initialDate = day + "/" + month + "/" + year;
                        final Calendar c = Calendar.getInstance();
                        c.set(year, month, day);
                        selectedInitialDate = c;
                    } else {
                        finalDate = day + "/" + month + "/" + year;
                        final Calendar c = Calendar.getInstance();
                        c.set(year, month, day);
                        selectedFinalDate = c;
                    }
                    showDialog(2);
                }
            }, thisYear, thisMonth, thisDay);

        } else if (id == 2) {
            return new TimePickerDialog(CreateDealActivity.this,
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                            Time tme = new Time(hourOfDay, minute, 0);
                            Format formatter;
                            formatter = new SimpleDateFormat("hh:mm");

                            if (isInitialDate) {
                                selectedInitialDate.set(selectedInitialDate.get(Calendar.YEAR), selectedInitialDate.get(Calendar.MONTH), selectedInitialDate.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
                                initialDate = selectedInitialDate.get(Calendar.YEAR) + "/" + (selectedInitialDate.get(Calendar.MONTH) + 1) + "/" + selectedInitialDate.get(Calendar.DAY_OF_MONTH) + " " + formatter.format(tme);
                                if (finalDate != null && !finalDate.isEmpty() && selectedFinalDate != null) {
                                    if (selectedInitialDate.after(selectedFinalDate) || selectedInitialDate.equals(selectedFinalDate)) {
                                        util.presentDialogWith(CreateDealActivity.this, trackingOps.getmFirebaseRemoteConfig().getString(ERROR), trackingOps.getmFirebaseRemoteConfig().getString(DEAL_DATES_ERROR));
                                        selectedInitialDate = null;
                                        initialDate = "";
                                        selectedInitialDate = Calendar.getInstance();
                                    } else {
                                        dealStartText.setText(initialDate);
                                        dealStartText.setTextColor(ContextCompat.getColor(CreateDealActivity.this, R.color.textoNegro));
                                    }
                                } else {
                                    dealStartText.setText(initialDate);
                                    dealStartText.setTextColor(ContextCompat.getColor(CreateDealActivity.this, R.color.textoNegro));
                                }
                            } else {

                                selectedFinalDate.set(selectedFinalDate.get(Calendar.YEAR), selectedFinalDate.get(Calendar.MONTH), selectedFinalDate.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
                                finalDate = selectedFinalDate.get(Calendar.YEAR) + "/" + (selectedFinalDate.get(Calendar.MONTH) + 1) + "/" + selectedFinalDate.get(Calendar.DAY_OF_MONTH) + " " + formatter.format(tme);
                                if ((initialDate != null && !initialDate.isEmpty() && selectedFinalDate.before(selectedInitialDate)) || (initialDate != null && !initialDate.isEmpty() && selectedFinalDate.equals(selectedInitialDate))) {
                                    presentDialogWith(trackingOps.getmFirebaseRemoteConfig().getString(ERROR), trackingOps.getmFirebaseRemoteConfig().getString(DEAL_DATES_ERROR));
                                } else {
                                    dealEndText.setText(finalDate);
                                    dealEndText.setTextColor(ContextCompat.getColor(CreateDealActivity.this, R.color.textoNegro));
                                }
                            }


                        }
                    }, 8, 00, false);
        }
        return null;
    }

    public void presentDialogWith(String title, String message) {
        if (!isShowingDialog) {
            isShowingDialog = true;
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder
                    .setMessage(message)
                    .setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            isShowingDialog = false;
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    public void setRedBorder(View view) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.field_with_red_border));
        } else {
            view.setBackground(ContextCompat.getDrawable(this, R.drawable.field_with_red_border));
        }
    }

    public void setWhiteBg(View view) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.field_white_background));
        } else {
            view.setBackground(ContextCompat.getDrawable(this, R.drawable.field_white_background));
        }
    }

    public void cleanAll() {
        dealNameEditText.setText("");
        dealDescriptionText.setText("");
        dealConditionsText.setText("");
        initialDate = "";
        finalDate = "";
        dealStartText.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_START_DATE_TITLE));
        dealEndText.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEW_DEAL_END_DATE_TITLE));
        dealInitialPriceText.getText().clear();
        dealDiscountPriceText.getText().clear();
        initialPrice = "";
        discountPrice = "";

        setWhiteBg(dealNameEditText);
        setWhiteBg(dealDescriptionText);
        setWhiteBg(dealConditionsText);
        setWhiteBg(dealStartText);
        setWhiteBg(dealEndText);

        dealNameEditText.requestFocus();
    }

    public void makeDataRequest() {
        try {
            Bundle queryBundle = new Bundle();
            JSONObject body = new JSONObject();


            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            JSONObject offer = new JSONObject();
            offer.put("name", util.convertStringToUTF8(dealNameEditText.getText().toString()));
            offer.put("start_date", initialDate.replace("/", "-"));
            offer.put("end_date", finalDate.replace("/", "-"));
            offer.put("price", initialPrice);
            offer.put("discount", discountPrice);
            offer.put("information", util.convertStringToUTF8(dealDescriptionText.getText().toString()));
            offer.put("to_known", util.convertStringToUTF8(dealConditionsText.getText().toString()));
            body.put("offer", offer);
            body.put("brand_id", user.getBrand().getId());

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + "/business/brands/offers?auth_token=" + user.getAccessToken());

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(SEND_DATA);

            if (loader == null) {
                loaderManager.initLoader(SEND_DATA, queryBundle, CreateDealActivity.this);
            } else {
                loaderManager.restartLoader(SEND_DATA, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
            toggleProgressDialog(false);
        }
    }


    @Override
    public Loader<String> onCreateLoader(int id, @Nullable Bundle args) {
        toggleProgressDialog(true);
        switch (id) {
            case SEND_DATA:
                return new RequestDefaultFactory(this, args);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<String> loader, String data) {
        toggleProgressDialog(false);
        switch (loader.getId()) {
            case SEND_DATA:
                if (data == null) {
                    util.alertError500(this);
                } else {
                    SendedDealDialog sendedDealDialog = new SendedDealDialog(this);
                    sendedDealDialog.show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<String> loader) {
        toggleProgressDialog(false);
    }

    public String miles(String no) {
        if (no.length() == 0) {
            return "";
        }
        no = no.replace(".", "");
        no = no.replace(",", "");
        BigDecimal sPrice = new BigDecimal(no);
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(',');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String str = formatter.format(sPrice.longValue());
        return str;
    }
}
