package com.civiconegocios.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.FiltroOfertasVO;
import com.civiconegocios.app.VO.OfertasRedimidasVO;
import com.civiconegocios.app.adapters.AdapterItemOfertaRedimida;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RedeemedDealsDataSerializer;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.EndlessRecyclerViewScrollListener;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class ActivityOfertasRedimidas extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks {

    private static final String TOOLBAR_MENU_REDEEMED_DEALS = "toolbar_menu_redeemed_deals";
    private static final String NOT_RESULTS_FOUNDED = "not_results_founded";
    private static final String FILTER = "filter";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    //    Loaders IDS
    private final int DATA_REQUEST_LOADER = 10;
    private final int DATA_SERIALIZER_LOADER = 11;


    public List<OfertasRedimidasVO> dealsList;
    public ArrayList<FiltroOfertasVO> filterList;


    //    UI
    Toolbar defaultToolbar;
    RecyclerView lvOfertasRedimidas;
    AdapterItemOfertaRedimida adapter;
    Utils util = new Utils();
    LinearLayout btnFiltrarOfertas;
    TextView txtFiltradoRedimidas, redeemedDealsFilterIcon, toolbar_title, dealsNotFoundedTxt;
    LinearLayout lnNoResultadosRedimidas;
    int pageConsult = 1;
    //    Ops
    String urlServicio = "/brand/";
    String complemento, dealsData;
    String pageNumber = "1";
    String limitNumber = "10";
    String startDate = "";
    String endDate = "";
    String status = "";
    String filteredNames = "";
    String dealsIds = "";
    User user;
    private int preLast;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ofertas_redimidas);

        initDrawer();
        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_REDEEMED_DEALS));

        txtFiltradoRedimidas = findViewById(R.id.txtFiltradoRedimidas);
        txtFiltradoRedimidas.setOnClickListener(this);
        txtFiltradoRedimidas.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this, R.drawable.ic_comercio_cerrar), null);

        redeemedDealsFilterIcon = findViewById(R.id.redeemedDealsFilterIcon);
        redeemedDealsFilterIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(FILTER).toUpperCase());
        redeemedDealsFilterIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_filtrar), null, null, null);

        btnFiltrarOfertas = findViewById(R.id.btnFiltrarOfertas);
        btnFiltrarOfertas.setOnClickListener(this);

        lnNoResultadosRedimidas = findViewById(R.id.lnNoResultadosRedimidas);

        dealsNotFoundedTxt = findViewById(R.id.dealsNotFoundedTxt);
        dealsNotFoundedTxt.setText(trackingOps.getmFirebaseRemoteConfig().getString(NOT_RESULTS_FOUNDED));

        filterList = new ArrayList<>();

        lvOfertasRedimidas = findViewById(R.id.lvOfertasRedimidas);
        dealsList = new ArrayList<>();
        adapter = new AdapterItemOfertaRedimida(getApplicationContext(), dealsList, trackingOps);
        lvOfertasRedimidas.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        lvOfertasRedimidas.setLayoutManager(linearLayoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        lvOfertasRedimidas.addItemDecoration(itemDecorator);
        lvOfertasRedimidas.setHasFixedSize(true);


        if (util.haveInternet(getApplicationContext())) {
            complemento = "?page=" + pageNumber + "&limit=" + limitNumber;
            makeDataRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                pageConsult = Integer.parseInt(pageNumber);
                pageConsult = pageConsult + 1;
                pageNumber = pageConsult + "";
                complemento = "?page=" + pageNumber + "&limit=" + limitNumber;
                if (!startDate.equals("")) {
                    complemento = complemento + "&start_date=" + startDate;
                    btnFiltrarOfertas.setVisibility(View.GONE);
                }
                if (!endDate.equals("")) {
                    complemento = complemento + "&end_date=" + endDate;
                    btnFiltrarOfertas.setVisibility(View.GONE);
                }
                if (!dealsIds.equals("")) {
                    complemento = complemento + "&offers_ids[]=" + dealsIds;
                    btnFiltrarOfertas.setVisibility(View.GONE);
                }
                if (!status.equals("")) {
                    complemento = complemento + "&statuses[]=" + status;
                    btnFiltrarOfertas.setVisibility(View.GONE);
                }
                makeDataRequest();
            }
        };

        lvOfertasRedimidas.addOnScrollListener(scrollListener);

        trackingOps.registerScreenName(" Ofertas redimidas | " + user.getCustomer().getName() + " | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFiltrarOfertas:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_REDEEMED_DEALS), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(FILTER));
                Intent i = new Intent(getApplicationContext(), ActivityAplicarFiltros.class);
                i.putParcelableArrayListExtra("filterList", filterList);
                startActivityForResult(i, 1);
                break;
            case R.id.txtFiltradoRedimidas:
                pageNumber = "1";
                limitNumber = "10";
                startDate = "";
                endDate = "";
                status = "";
                filteredNames = "";
                dealsIds = "";
                txtFiltradoRedimidas.setVisibility(View.GONE);
                txtFiltradoRedimidas.setText("");
                btnFiltrarOfertas.setVisibility(View.VISIBLE);
                consultaLimpiaFiltros();
                break;
            default:
                break;
        }
    }

    public void consultaLimpiaFiltros() {
        if (util.haveInternet(getApplicationContext())) {
            lnNoResultadosRedimidas.setVisibility(View.GONE);
            complemento = "?page=" + pageNumber + "&limit=" + limitNumber;
            if (!startDate.equals("")) {
                complemento = complemento + "&start_date=" + startDate;
                txtFiltradoRedimidas.setVisibility(View.VISIBLE);
                txtFiltradoRedimidas.setText(txtFiltradoRedimidas.getText().toString() + " " + startDate);
                btnFiltrarOfertas.setVisibility(View.GONE);
            }
            if (!endDate.equals("")) {
                complemento = complemento + "&end_date=" + endDate;
                txtFiltradoRedimidas.setVisibility(View.VISIBLE);
                txtFiltradoRedimidas.setText(txtFiltradoRedimidas.getText().toString() + " " + endDate);
                btnFiltrarOfertas.setVisibility(View.GONE);
            }
            if (!dealsIds.equals("")) {
                complemento = complemento + "&offers_ids[]=" + dealsIds;
                txtFiltradoRedimidas.setVisibility(View.VISIBLE);
                txtFiltradoRedimidas.setText(txtFiltradoRedimidas.getText().toString() + " " + filteredNames);
                btnFiltrarOfertas.setVisibility(View.GONE);
            }
            if (!status.equals("")) {
                complemento = complemento + "&statuses[]=" + status;
                txtFiltradoRedimidas.setVisibility(View.VISIBLE);
                txtFiltradoRedimidas.setText(txtFiltradoRedimidas.getText().toString() + " " + status.replace(",", " ").replace("acquirable", "Código disponible").replace("redeemed", "Oferta redimida"));
                btnFiltrarOfertas.setVisibility(View.GONE);
            }
            //      arrayFiltro.clear();
            dealsList.clear();
            makeDataRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }
    }

    public void recibirFiltroOfertas(ArrayList<FiltroOfertasVO> hoyList) {
        filterList = hoyList;
        lvOfertasRedimidas.setVisibility(View.VISIBLE);
        lnNoResultadosRedimidas.setVisibility(View.GONE);
    }

    public void esconderFiltro() {
        if (dealsList.size() == 0) {
            btnFiltrarOfertas.setVisibility(View.GONE);
            lvOfertasRedimidas.setVisibility(View.GONE);
            lnNoResultadosRedimidas.setVisibility(View.VISIBLE);
        } else {
            pageNumber = "1";
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                pageNumber = data.getStringExtra("page");
                limitNumber = data.getStringExtra("limit");
                startDate = data.getStringExtra("startDate");
                endDate = data.getStringExtra("endDate");
                status = data.getStringExtra("status");
                filteredNames = data.getStringExtra("filteredNames");
                dealsIds = data.getStringExtra("dealsIds");

                if (util.haveInternet(getApplicationContext())) {
                    complemento = "?page=" + pageNumber + "&limit=" + limitNumber;
                    if (!startDate.equals("")) {
                        complemento = complemento + "&start_date=" + startDate;
                        txtFiltradoRedimidas.setVisibility(View.VISIBLE);
                        txtFiltradoRedimidas.setText(txtFiltradoRedimidas.getText().toString() + " " + startDate);
                        btnFiltrarOfertas.setVisibility(View.GONE);
                    }
                    if (!endDate.equals("")) {
                        complemento = complemento + "&end_date=" + endDate;
                        txtFiltradoRedimidas.setVisibility(View.VISIBLE);
                        txtFiltradoRedimidas.setText(txtFiltradoRedimidas.getText().toString() + " " + endDate);
                        btnFiltrarOfertas.setVisibility(View.GONE);
                    }
                    if (!dealsIds.equals("")) {
                        complemento = complemento + "&offers_ids[]=" + dealsIds;
                        txtFiltradoRedimidas.setVisibility(View.VISIBLE);
                        txtFiltradoRedimidas.setText(txtFiltradoRedimidas.getText().toString() + " " + filteredNames);
                        btnFiltrarOfertas.setVisibility(View.GONE);
                    }
                    if (!status.equals("")) {
                        complemento = complemento + "&statuses[]=" + status;
                        txtFiltradoRedimidas.setVisibility(View.VISIBLE);
                        txtFiltradoRedimidas.setText(txtFiltradoRedimidas.getText().toString() + " " + status.replace(",", " ").replace("acquirable", "Código disponible").replace("redeemed", "Oferta redimida"));
                        btnFiltrarOfertas.setVisibility(View.GONE);
                    }
                    dealsList.clear();
                    makeDataRequest();
                } else {
                    util.mensajeNoInternet(getApplicationContext());
                }
            }
        }
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, urlServicio + user.getBrand().getId() + "/offer_coupons" + complemento);
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader loader = loaderManager.getLoader(DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(DATA_REQUEST_LOADER, queryBundle, ActivityOfertasRedimidas.this);
            } else {
                loaderManager.restartLoader(DATA_REQUEST_LOADER, queryBundle, ActivityOfertasRedimidas.this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        switch (id) {
            case DATA_REQUEST_LOADER:
                return new RequestDefaultFactory(this, args);
            case DATA_SERIALIZER_LOADER:
                RedeemedDealsDataSerializer redeemedDealsDataSerializer = new RedeemedDealsDataSerializer(this);
                redeemedDealsDataSerializer.setData(this.dealsData);
                redeemedDealsDataSerializer.setArrayFiltro(filterList);
                redeemedDealsDataSerializer.setListBalanceTemp(dealsList);
                return redeemedDealsDataSerializer;
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        toggleProgressDialog(false);
        switch (loader.getId()) {
            case DATA_REQUEST_LOADER:
                this.dealsData = String.valueOf(data);
                if (getSupportLoaderManager().getLoader(DATA_SERIALIZER_LOADER) == null) {
                    getSupportLoaderManager().initLoader(DATA_SERIALIZER_LOADER, null, this).forceLoad();
                } else {
                    getSupportLoaderManager().restartLoader(DATA_SERIALIZER_LOADER, null, this).forceLoad();
                }
                break;
            case DATA_SERIALIZER_LOADER:
                ArrayList<List<?>> items = (ArrayList<List<?>>) data;
                if (items.get(0).size() > 0 || items.get(1).size() > 0) {
                    recibirFiltroOfertas(filterList);
                    adapter.notifyDataSetChanged();
                } else {
                    esconderFiltro();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        toggleProgressDialog(false);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && trackingOps.getReloadData()) {
            trackingOps.setReloadData(false);
            if (util.haveInternet(getApplicationContext())) {
                filterList = new ArrayList<>();
                pageNumber = "1";
                complemento = "?page=" + pageNumber + "&limit=" + limitNumber;
                dealsList.clear();
                adapter.notifyDataSetChanged();
                makeDataRequest();
            } else {
                util.mensajeNoInternet(getApplicationContext());
            }
        }
    }
}