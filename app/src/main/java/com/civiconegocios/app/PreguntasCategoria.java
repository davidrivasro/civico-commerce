package com.civiconegocios.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.CategoriasVO;
import com.civiconegocios.app.adapters.list.CategoriasListAdapter;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

public class PreguntasCategoria extends AppCompatActivity {

    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String BILLING = "billing";
    private final String TAG = PreguntasCategoria.class.getSimpleName();
    TextView toolbar_title;
    TextView tv_title_categoria;
    TrackingOps trackingOps;
    private CategoriasListAdapter pfListAdapter;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preguntas_categoria);

        trackingOps = (TrackingOps) getApplicationContext();

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_izquierda_toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        toolbar_title.setTypeface((new Utils()).fuenteHeader(this));

        tv_title_categoria = findViewById(R.id.tv_title_categoria);
        tv_title_categoria.setText(trackingOps.getmFirebaseRemoteConfig().getString(BILLING));
        tv_title_categoria.setTypeface((new Utils()).fuenteParrafos(this));

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        CategoriasVO categoria = extras.getParcelable("Categoria");
        ((TextView) findViewById(R.id.tv_title_categoria)).setText(categoria.getTitle());

        RecyclerView recyclerView = findViewById(R.id.list_preguntas_categorias);
        recyclerView.setHasFixedSize(true);
        pfListAdapter = new CategoriasListAdapter();
        pfListAdapter.setCategoriasCursor(categoria.getQuestions());
        recyclerView.setAdapter(pfListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
