package com.civiconegocios.app.dialogs;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.civiconegocios.app.CreateDealActivity;
import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.R;

import static com.pushwoosh.internal.platform.AndroidPlatformModule.getApplicationContext;

/**
 * Created by nivelics on 3/26/18.
 */

public class SendedDealDialog extends AppCompatDialog implements View.OnClickListener {

    private static final String SENDED_DEAL_TITLE = "sended_deal_title";
    private static final String SENDED_DEAL_TEXT = "sended_deal_text";
    private static final String SENDED_DEAL_ACTION = "sended_deal_action";
    CreateDealActivity activity;
    ImageView closeDialog;
    TextView dealActionButton, sendedDealTitle, sendedDealText;
    TrackingOps trackingOps;

    public SendedDealDialog(CreateDealActivity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sended_deal_dialog);

        trackingOps = (TrackingOps) getApplicationContext();

        sendedDealTitle = findViewById(R.id.sendedDealTitle);
        sendedDealTitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(SENDED_DEAL_TITLE));

        sendedDealText = findViewById(R.id.sendedDealText);
        sendedDealText.setText(trackingOps.getmFirebaseRemoteConfig().getString(SENDED_DEAL_TEXT));

        closeDialog = findViewById(R.id.closeDialog);
        closeDialog.setOnClickListener(this);

        dealActionButton = findViewById(R.id.dealActionButton);
        dealActionButton.setText(trackingOps.getmFirebaseRemoteConfig().getString(SENDED_DEAL_ACTION));
        dealActionButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.closeDialog:
                dismiss();
                break;
            case R.id.dealActionButton:
                this.activity.cleanAll();
                dismiss();
                break;
            default:
                break;
        }
    }
}
