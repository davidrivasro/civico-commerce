package com.civiconegocios.app;

import android.app.ProgressDialog;
import android.app.job.JobScheduler;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;

import com.google.android.material.navigation.NavigationView;

import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.Message;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.CustomTypefaceSpan;
import com.civiconegocios.app.utils.Utils;

import io.realm.Realm;
import io.realm.Sort;

public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final String TAG = BaseActivity.class.getSimpleName();

    private static final String PROCESS_DIALOG_MESSAGE = "process_dialog_message";
    private static final String URL_ACADEMY_CIVICO = "url_academy_civico";
    private static final String NEW_SALE = "new_sale";
    private static final String TOOLBAR_MENU_TRANSACTIONS = "toolbar_menu_transactions";
    private static final String TOOLBAR_MENU_BALANCE = "toolbar_menu_balance";
    private static final String TOOLBAR_MENU_DEALS = "toolbar_menu_deals";
    private static final String TOOLBAR_MENU_NEW_DEAL = "new_deal";
    private static final String TOOLBAR_MENU_REDEEMED_DEALS = "toolbar_menu_redeemed_deals";
    private static final String TOOLBAR_MENU_VERIFY_CODE = "toolbar_menu_verify_code";
    private static final String TOOLBAR_MENU_INBOX = "toolbar_menu_inbox";
    private static final String CIVICO_ACADEMY = "civico_academy";
    private static final String PROFILE = "profile";
    private static final String ENCUESTA_MEXICO = "encuesta_mexico_url";
    private static final String ABOUT_CIVICO_PAY = "about_civico_pay";
    private static final String TOOLBAR_MENU_FAQ = "toolbar_menu_faq";
    private static final int JOB_ID = 0;
    ProgressDialog progressDialog;
    Toolbar defaultToolbar;
    DrawerLayout drawer;
    Utils util = new Utils();
    MenuItem goBackToolbar;
    TrackingOps trackingOps;
    TextView unreadMessagesTextView;
    Realm realm;
    private JobScheduler mScheduler;


    public void initDrawer() {
        trackingOps = (TrackingOps) getApplication();
        realm = ((TrackingOps) this.getApplication()).getRealm();
        if (findViewById(R.id.toolbar) != null) {
            defaultToolbar = findViewById(R.id.toolbar);
            defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
            setSupportActionBar(defaultToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            User user = trackingOps.getUser();

            if (findViewById(R.id.drawer_layout) != null) {
                drawer = findViewById(R.id.drawer_layout);
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                        this, drawer, defaultToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                drawer.addDrawerListener(toggle);
                toggle.syncState();

                NavigationView navigationView = findViewById(R.id.nav_view);
                navigationView.setNavigationItemSelectedListener(this);
                navigationView.setItemIconTintList(AppCompatResources.getColorStateList(this, R.color.blanco));

                Menu menu = navigationView.getMenu();

                for (int i = 0; i < menu.size(); i++) {
                    MenuItem mi = menu.getItem(i);

                    if (mi.getGroupId() == R.id.civicoPayContainer && user.getAcceptingPayments()) {
                        mi.setVisible(true);
                    }

                    if (mi.getGroupId() == R.id.civicoDealsContainer && user.getBrand() != null && !user.getBrand().getId().equals("")) {
                        mi.setVisible(true);
                    }

                    if (mi.getGroupId() == R.id.myBussinessContainer && user.getBrand() != null && !user.getBrand().getId().equals("")) {
                        mi.setVisible(true);
                    }

                    if (mi.getItemId() == R.id.nav_encuesta) {
                        if (trackingOps.getmFirebaseRemoteConfig().getString(ENCUESTA_MEXICO).isEmpty() ||
                                trackingOps.getmFirebaseRemoteConfig().getString(ENCUESTA_MEXICO).equalsIgnoreCase("")) {
                            mi.setVisible(false);
                        } else if (!user.getCity().equalsIgnoreCase("mexico") && !user.getCity().equalsIgnoreCase("cdmx") &&
                                !user.getCity().equalsIgnoreCase("méxico")) {
                            mi.setVisible(false);
                        }
                    }

                    SubMenu subMenu = mi.getSubMenu();
                    if (subMenu != null && subMenu.size() > 0) {
                        for (int j = 0; j < subMenu.size(); j++) {
                            MenuItem subMenuItem = subMenu.getItem(j);
                            applyFontToMenuItem(subMenuItem, false);
                        }
                    }

                    if (mi.hasSubMenu()) {
                        applyFontToMenuItem(mi, true);
                    } else {
                        if (mi.getGroupId() == R.id.nav_civico_academy_group)
                            applyFontToMenuItem(mi, true);
                        else applyFontToMenuItem(mi, false);
                    }
                }

                Integer unreadMessages = trackingOps.getUnreadMessagesCount();

                unreadMessagesTextView = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                        findItem(R.id.nav_inbox));
                unreadMessagesTextView.setSingleLine(true);
                unreadMessagesTextView.setText(String.valueOf(unreadMessages));
                unreadMessagesTextView.setGravity(Gravity.CENTER_VERTICAL);
//                Drawable whiteCircle = ContextCompat.getDrawable(this, R.drawable.ic_white_circle);
//                whiteCircle.setBounds(0,10,0,10);
//                unreadMessagesTextView.setBackgroundDrawable(whiteCircle);

                unreadMessagesTextView.setTypeface(null, Typeface.BOLD);
                unreadMessagesTextView.setTextColor(Color.parseColor("#ffffff"));

                if (unreadMessages < 1) {
                    unreadMessagesTextView.setVisibility(View.GONE);
                }
            }
        }

        //
        // scheduleJob();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.default_toolbar, menu);
        goBackToolbar = menu.findItem(R.id.go_back_toolbar);
//        util.toolbarOrderItems(defaultToolbar);
        return super.onCreateOptionsMenu(menu);
    }

    public void updateUnreadMessagesBadge() {
        Integer unreadMessages = realm.where(Message.class)
                .equalTo("status", "unread")
                .sort("created_at", Sort.DESCENDING)
                .findAll().size();
        unreadMessagesTextView.setText(String.valueOf(unreadMessages));
        if (unreadMessages < 1) {
            unreadMessagesTextView.setVisibility(View.GONE);
        } else {
            unreadMessagesTextView.setVisibility(View.VISIBLE);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_new_sale:
                if (!this.getClass().getSimpleName().equals("inicioActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(NEW_SALE));
                    Intent tabcrear = new Intent(this, inicioActivity.class);
                    tabcrear.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    this.startActivity(tabcrear);
                    this.overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_transactions:
                if (!this.getClass().getSimpleName().equals("MisPagosActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_TRANSACTIONS));
                    Intent intent = new Intent(this, MisPagosActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_balance:
                if (!this.getClass().getSimpleName().equals("BalanceActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BALANCE));
                    Intent intent = new Intent(this, BalanceActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_deals_list:
                if (!this.getClass().getSimpleName().equals("ActivityListarOfertas")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_DEALS));
                    Intent intent = new Intent(this, ActivityListarOfertas.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_new_deal:
                if (!this.getClass().getSimpleName().equals("CreateDealActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_NEW_DEAL));
                    Intent intent = new Intent(this, CreateDealActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_redeemed_deals:
                if (!this.getClass().getSimpleName().equals("ActivityOfertasRedimidas")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_REDEEMED_DEALS));
                    Intent intent = new Intent(this, ActivityOfertasRedimidas.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_verify_code:
                if (!this.getClass().getSimpleName().equals("ActivityVerficarCodigo")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_VERIFY_CODE));
                    Intent intent = new Intent(this, ActivityVerficarCodigo.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_inbox:
                if (!this.getClass().getSimpleName().equals("InboxActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_INBOX));
                    Intent intent = new Intent(this, InboxActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_academy:
                if (!this.getClass().getSimpleName().equals("WebSobreCivicoActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(CIVICO_ACADEMY));
                    Intent intent = new Intent(this, WebSobreCivicoActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("KEY", trackingOps.getmFirebaseRemoteConfig().getString(URL_ACADEMY_CIVICO));
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_encuesta:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", "Encuesta");
                Intent intentCuestionario = new Intent(this, WebSobreCivicoActivity.class);
                intentCuestionario.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentCuestionario.putExtra("KEY", trackingOps.getmFirebaseRemoteConfig().getString(ENCUESTA_MEXICO));
                startActivity(intentCuestionario);
                overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                break;
            case R.id.nav_profile:
                if (!this.getClass().getSimpleName().equals("PerfilActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(PROFILE));
                    Intent intent = new Intent(this, PerfilActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_about:
                if (!this.getClass().getSimpleName().equals("SobreCivicoActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY));
                    Intent intent = new Intent(this, SobreCivicoActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
            case R.id.nav_faq:
                if (!this.getClass().getSimpleName().equals("PreguntasFrecuentesActivity")) {
                    trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Menú", trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_FAQ));
                    Intent intent = new Intent(this, PreguntasFrecuentesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
                }
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void toggleProgressDialog(Boolean show) {
        if (show) {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(PROCESS_DIALOG_MESSAGE));
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
        } else {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private void applyFontToMenuItem(MenuItem mi, Boolean isBold) {
        Typeface font = util.fuenteMenuLatBotones(this);
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        int end = mNewTitle.length();
        if (isBold) {
            mNewTitle.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mNewTitle.setSpan(new RelativeSizeSpan(1.15f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mNewTitle.setSpan(new ForegroundColorSpan(Color.WHITE), 0, end, 0);
        mi.setTitle(mNewTitle);
    }


    private void scheduleJob() {
        /*mScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);

        ComponentName serviceName = new ComponentName(getPackageName(),NotificationJobService.class.getName());
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, serviceName)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setRequiresDeviceIdle(true)
                .setRequiresCharging(true);

        builder.setPeriodic(3000);

        JobInfo myJobInfo = builder.build();
        mScheduler.schedule(myJobInfo);*/

    }
}