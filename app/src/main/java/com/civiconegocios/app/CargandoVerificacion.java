package com.civiconegocios.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.OfertasRedimidasVO;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by brayancamilovillateleon on 21/06/17.
 */

public class CargandoVerificacion extends BaseActivity implements LoaderManager.LoaderCallbacks<String> {

    private static final String VERIFYING_CODE_TITLE = "verifying_code_title";
    private static final String VERIFYING_CODE_SUBTITLE_1 = "verifying_code_subtitle_1";
    private static final String VERIFYING_CODE_SUBTITLE_2 = "verifying_code_subtitle_2";
    GifImageView givImageView;
    Utils util = new Utils();
    TextView txtTituloCargando;
    TextView txtTituloEnProceso;
    TextView txtParrafoProcesoUno;
    TextView txtParrafoProcesoDos;
    TextView txtParrafoProcesoTres;
    String value;
    OfertasRedimidasVO ofertaRedimida;
    String urlServicio = "/brand/";
    User user;
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cargando_cobro_activity);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("KEY");
        }
        user = ((TrackingOps) getApplication()).getUser();
        givImageView = findViewById(R.id.imgGif);
        trackingOps = (TrackingOps) getApplication();

        txtTituloCargando = findViewById(R.id.txtTituloCargando);
        txtTituloEnProceso = findViewById(R.id.txtTituloEnProceso);
        txtParrafoProcesoUno = findViewById(R.id.txtParrafoProcesoUno);
        txtParrafoProcesoDos = findViewById(R.id.txtParrafoProcesoDos);
        txtParrafoProcesoTres = findViewById(R.id.txtParrafoProcesoTres);

        txtTituloEnProceso.setText(trackingOps.getmFirebaseRemoteConfig().getString(VERIFYING_CODE_TITLE));
        txtParrafoProcesoUno.setText(trackingOps.getmFirebaseRemoteConfig().getString(VERIFYING_CODE_SUBTITLE_1));
        txtParrafoProcesoDos.setText(trackingOps.getmFirebaseRemoteConfig().getString(VERIFYING_CODE_SUBTITLE_2));

        try {
            GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.cargando);
            givImageView.setImageDrawable(gifFromResource);

        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject manJson = new JSONObject();
        try {
            manJson.put("code", value);
            if (util.haveInternet(getApplicationContext())) {
                makeDataRequest();
            } else {
                util.mensajeNoInternet(getApplicationContext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            body.put("code", value);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + urlServicio + user.getBrand().getId() + "/offer_coupons/validate");

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(0);

            if (loader == null) {
                loaderManager.initLoader(0, queryBundle, this);
            } else {
                loaderManager.restartLoader(0, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        toggleProgressDialog(false);
        if (data == null) {
            util.alertErrorTimeOut(this);
        } else {
            try {
                JSONObject catObj = new JSONObject(data);
                JSONObject offer_coupon = catObj.getJSONObject("offer_coupon");
                String comentarioUno = "";
                String comentarioDos = "";
                JSONArray arrayComent = offer_coupon.getJSONArray("comments");
                if (arrayComent.length() > 0) {
                    if (arrayComent.length() > 1) {
                        comentarioUno = arrayComent.getJSONObject(0).getString("text");
                        comentarioDos = arrayComent.getJSONObject(1).getString("text");
                    } else {
                        comentarioUno = arrayComent.getJSONObject(0).getString("text");
                    }
                }

                OfertasRedimidasVO RedeemedDeal = new OfertasRedimidasVO(offer_coupon.getString("id"), offer_coupon.getString("status"),
                        offer_coupon.getString("redemption_date"), offer_coupon.getString("offer_name"), offer_coupon.getInt("price") + "", offer_coupon.getInt("discount") + "",
                        offer_coupon.getString("user_name"), offer_coupon.getString("document_type"), offer_coupon.getString("document_number"), offer_coupon.getString("mobile_phone"), offer_coupon.getString("code"),
                        offer_coupon.getString("user_email"), comentarioUno, comentarioDos);

                finish();
                Intent i = new Intent(this, ActivityEstadoCodigoDetalle.class);
                i.putExtra("needsValidation", true);
                i.putExtra("Object", RedeemedDeal);
                startActivity(i);
            } catch (JSONException e) {
                e.printStackTrace();
                finish();
                Intent i = new Intent(this, ActivityRespuestaCanjeoCodigo.class);
                i.putExtra("KEY", "invalido");
                startActivity(i);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }
}
