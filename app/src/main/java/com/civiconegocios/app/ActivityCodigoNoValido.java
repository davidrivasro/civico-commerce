package com.civiconegocios.app;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class ActivityCodigoNoValido extends AppCompatActivity {

    private static final String PERMISSION_PHONE_RATIONALE = "permission_phone_rationale";
    private static final String BUTTON_ALLOW = "button_allow";
    private static final String BUTTON_DENY = "button_deny";
    private static final String PERMISSION_CALL_DENIED = "permission_call_denied";
    private static final String PERMISSION_CALL_NEVERASK = "permission_call_neverask";
    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String CODE_IS_NOT_VALID = "code_is_not_valid";
    private static final String CODE_ALREADY_REDEEMED = "code_already_redeemed";
    private static final String QUESTIONS_ABOUT_CODE = "questions_about_code";
    private static final String LBL_BTN_COMUNICATE_WITH_US = "lbl_btn_comunicate_with_us";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    Toolbar defaultToolbar;
    TextView txtBtnComunicateNoValido, txtAtrasNoValido, textOpenSansLight2, textOpenSansLight4, textOpenSansLight5;
    String value = "";
    Utils util = new Utils();
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_valido);

        trackingOps = (TrackingOps) getApplication();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("KEY");
        }

        defaultToolbar = findViewById(R.id.toolbar);
        defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        setSupportActionBar(defaultToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtAtrasNoValido = findViewById(R.id.txtAtrasNoValido);
        txtAtrasNoValido.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        txtAtrasNoValido.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);

        textOpenSansLight2 = findViewById(R.id.textOpenSansLight2);
        textOpenSansLight2.setText(trackingOps.getmFirebaseRemoteConfig().getString(CODE_IS_NOT_VALID));


        textOpenSansLight4 = findViewById(R.id.textOpenSansLight4);
        textOpenSansLight4.setText(trackingOps.getmFirebaseRemoteConfig().getString(CODE_ALREADY_REDEEMED));

        textOpenSansLight5 = findViewById(R.id.textOpenSansLight5);
        textOpenSansLight5.setText(trackingOps.getmFirebaseRemoteConfig().getString(QUESTIONS_ABOUT_CODE));


        txtBtnComunicateNoValido = findViewById(R.id.txtBtnComunicateNoValido);
        txtBtnComunicateNoValido.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_COMUNICATE_WITH_US));
        txtBtnComunicateNoValido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (util.mostrarPopUp()) {
                    ActivityCodigoNoValidoPermissionsDispatcher.callWithPermissionCheck(ActivityCodigoNoValido.this, "2 36 31 11");
                } else {
                    DialogComunicate cdd = new DialogComunicate(ActivityCodigoNoValido.this);
                    cdd.show();
                }
            }
        });
    }

    @NeedsPermission(android.Manifest.permission.CALL_PHONE)
    public void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + "031" + phone));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ActivityCodigoNoValidoPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(android.Manifest.permission.CALL_PHONE)
    void showRationaleForRegistarToken(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_PHONE_RATIONALE))
                .setPositiveButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_ALLOW), (dialog, button) -> request.proceed())
                .setNegativeButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_DENY), (dialog, button) -> request.cancel())
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(android.Manifest.permission.CALL_PHONE)
    void showDeniedForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_DENIED), Toast.LENGTH_SHORT).show();
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(android.Manifest.permission.CALL_PHONE)
    void showNeverAskForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_NEVERASK), Toast.LENGTH_SHORT).show();
    }
}
