package com.civiconegocios.app;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.OfertasVO;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class ActivityDetalleOferta extends AppCompatActivity implements View.OnClickListener {

    private static final String PERMISSION_PHONE_RATIONALE = "permission_phone_rationale";
    private static final String BUTTON_ALLOW = "button_allow";
    private static final String BUTTON_DENY = "button_deny";
    private static final String PERMISSION_CALL_DENIED = "permission_call_denied";
    private static final String PERMISSION_CALL_NEVERASK = "permission_call_neverask";
    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String BTN_DEAL = "btn_deal";
    private static final String COUPONS = "coupons";
    private static final String REDEMPTIONS = "redemptions";
    private static final String USERS_CLAIMED_OFFER = "users_claimed_offer";
    private static final String VALIDITY = "validity";
    private static final String PRICE = "price";
    private static final String DISCOUNT = "discount";
    private static final String OFFER_CLOSE_EXPIRE = "offer_close_expire";
    private static final String OFFER_INCLUDES = "offer_includes";
    private static final String TERMS_OFFER = "terms_offer";
    private static final String NEED_MODIFY_OFFER = "need_modify_offer";
    private static final String LBL_BTN_COMUNICATE_WITH_US = "lbl_btn_comunicate_with_us";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    //    UI
    Toolbar defaultToolbar;
    ImageView imgOfertaDetalle;
    TextView txtCantCupones;
    TextView txtCantRedenciones;
    TextView txtUsuReclamaron;
    TextView txtVigenciaDetalle;
    TextView txtPrecioDetalle;
    TextView txtDescuentoDetalle;
    TextView txtIncluyeDetalla;
    TextView txtCondicionesDetalle;
    TextView txtComunicateDetalleOferta;
    TextView txtAtrasDetalleOferta;
    TextView txtNombreTotalOferta;
    TextView txtOfertaMisOfertas;
    TextView textOpenSansRegular4;
    TextView textOpenSansRegular5;
    TextView textOpenSansLight;
    TextView txtRojoMensaje, dealDetailCouponIcon, dealDetailDiscountIcon,
            dealDetailPriceIcon, dealDetailRedention, dealDetailClaimIcon,
            dealDetailTrustIcon;
    OfertasVO ofertsVo;
    Utils util = new Utils();
    //    Ops
    TrackingOps trackingOps;
    User user;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_oferta);

        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ofertsVo = (OfertasVO) extras.getParcelable("Object");
        }

        defaultToolbar = findViewById(R.id.toolbar);
        defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        setSupportActionBar(defaultToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtOfertaMisOfertas = findViewById(R.id.txtOfertaMisOfertas);
        txtOfertaMisOfertas.setText(trackingOps.getmFirebaseRemoteConfig().getString(BTN_DEAL));
        imgOfertaDetalle = findViewById(R.id.imgOfertaDetalle);
        txtNombreTotalOferta = findViewById(R.id.txtNombreTotalOferta);
        txtCantCupones = findViewById(R.id.txtCantCupones);
        txtCantRedenciones = findViewById(R.id.txtCantRedenciones);
        txtUsuReclamaron = findViewById(R.id.txtUsuReclamaron);
        txtVigenciaDetalle = findViewById(R.id.txtVigenciaDetalle);
        txtPrecioDetalle = findViewById(R.id.txtPrecioDetalle);
        txtDescuentoDetalle = findViewById(R.id.txtDescuentoDetalle);
        txtIncluyeDetalla = findViewById(R.id.txtIncluyeDetalla);
        txtIncluyeDetalla.setText(trackingOps.getmFirebaseRemoteConfig().getString(OFFER_INCLUDES));
        txtCondicionesDetalle = findViewById(R.id.txtCondicionesDetalle);
        txtCondicionesDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(TERMS_OFFER));
        txtComunicateDetalleOferta = findViewById(R.id.txtComunicateDetalleOferta);
        txtComunicateDetalleOferta.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_COMUNICATE_WITH_US));
        txtAtrasDetalleOferta = findViewById(R.id.txtAtrasDetalleOferta);
        txtAtrasDetalleOferta.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        txtRojoMensaje = findViewById(R.id.txtRojoMensaje);
        txtRojoMensaje.setText(trackingOps.getmFirebaseRemoteConfig().getString(OFFER_CLOSE_EXPIRE));
        dealDetailCouponIcon = findViewById(R.id.dealDetailCouponIcon);
        dealDetailCouponIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(COUPONS));
        dealDetailDiscountIcon = findViewById(R.id.dealDetailDiscountIcon);
        dealDetailDiscountIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(DISCOUNT));
        dealDetailPriceIcon = findViewById(R.id.dealDetailPriceIcon);
        dealDetailPriceIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(PRICE));
        dealDetailRedention = findViewById(R.id.dealDetailRedention);
        dealDetailRedention.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(REDEMPTIONS));
        dealDetailClaimIcon = findViewById(R.id.dealDetailClaimIcon);
        dealDetailClaimIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(USERS_CLAIMED_OFFER));
        dealDetailTrustIcon = findViewById(R.id.dealDetailTrustIcon);
        dealDetailTrustIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(VALIDITY));
        textOpenSansRegular4 = findViewById(R.id.textOpenSansRegular4);
        textOpenSansRegular4.setText(trackingOps.getmFirebaseRemoteConfig().getString(OFFER_INCLUDES));
        textOpenSansRegular5 = findViewById(R.id.textOpenSansRegular5);
        textOpenSansRegular5.setText(trackingOps.getmFirebaseRemoteConfig().getString(TERMS_OFFER));
        textOpenSansLight = findViewById(R.id.textOpenSansLight);
        textOpenSansLight.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEED_MODIFY_OFFER));

        if (!ofertsVo.getImage().isEmpty())
            Picasso.with(this).load(ofertsVo.getImage()).into(imgOfertaDetalle);

        txtNombreTotalOferta.setText(ofertsVo.getName());
        txtCantCupones.setText(ofertsVo.getTotal_coupons());
        txtCantRedenciones.setText(ofertsVo.getAvailable_coupons());
        txtUsuReclamaron.setText(ofertsVo.getRedeem_coupons());
        txtVigenciaDetalle.setText("Desde: " + ofertsVo.getStart_date() + "\n" + "Hasta: " + ofertsVo.getEnd_date());
        txtPrecioDetalle.setText("$" + miles(ofertsVo.getPrice()));
        txtDescuentoDetalle.setText("$" + miles(ofertsVo.getDiscount()));

        dealDetailCouponIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_cupones), null, null, null);
        dealDetailDiscountIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_descuento), null, null, null);
        dealDetailPriceIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_precio), null, null, null);
        dealDetailRedention.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_redenciones), null, null, null);
        dealDetailClaimIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_usuarios), null, null, null);
        dealDetailTrustIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_vigencia), null, null, null);
        txtAtrasDetalleOferta.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);

        String[] parts = ofertsVo.getInformation().split("SEPARADOR");
        String incluye = "";
        for (int a = 0; a < parts.length; a++) {
            if (a == parts.length - 1) {
                incluye += "· " + parts[a];
            } else {
                incluye += "· " + parts[a] + "\n\n";
            }
        }

        txtIncluyeDetalla.setText(incluye);

        String[] parts1 = ofertsVo.getConditions().split("SEPARADOR");
        String incluye1 = "";
        for (int a = 0; a < parts1.length; a++) {
            if (a == parts1.length - 1) {
                incluye1 += "· " + parts1[a];
            } else {
                incluye1 += "· " + parts1[a] + "\n\n";
            }

        }
        txtCondicionesDetalle.setText(incluye1);
        txtComunicateDetalleOferta.setOnClickListener(this);
        txtAtrasDetalleOferta.setOnClickListener(this);
        if (ofertsVo.getSoon_expiration().equals("true")) {
            txtRojoMensaje.setVisibility(View.VISIBLE);
        } else {
            txtRojoMensaje.setVisibility(View.GONE);

        }

        trackingOps.registerScreenName(ofertsVo.getName() + " | " + user.getCustomer().getName() + " | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtComunicateDetalleOferta:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Comunicate con nosotros ", "Detalle oferta"); // ojo revisar si va en cobrar o en siguiente
                if (util.mostrarPopUp()) {
                    ActivityDetalleOfertaPermissionsDispatcher.callWithPermissionCheck(this, "2 36 31 11");
                } else {
                    DialogComunicate cdd = new DialogComunicate(this);
                    cdd.show();
                }
                break;
            case R.id.txtAtrasDetalleOferta:
                finish();
                break;
            default:
                break;
        }
    }

    @NeedsPermission(android.Manifest.permission.CALL_PHONE)
    public void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + "031" + phone));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ActivityDetalleOfertaPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(android.Manifest.permission.CALL_PHONE)
    void showRationaleForRegistarToken(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_PHONE_RATIONALE))
                .setPositiveButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_ALLOW), (dialog, button) -> request.proceed())
                .setNegativeButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_DENY), (dialog, button) -> request.cancel())
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(android.Manifest.permission.CALL_PHONE)
    void showDeniedForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_DENIED), Toast.LENGTH_SHORT).show();
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(android.Manifest.permission.CALL_PHONE)
    void showNeverAskForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_NEVERASK), Toast.LENGTH_SHORT).show();
    }

    public String miles(String no) {
        no = no.replace(".", "");
        no = no.replace(",", "");
        BigDecimal sPrice = new BigDecimal(no);
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(',');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String str = formatter.format(sPrice.longValue());
        return str;
    }
}
