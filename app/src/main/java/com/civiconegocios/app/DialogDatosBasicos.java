package com.civiconegocios.app;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.utils.Utils;

import static com.pushwoosh.internal.platform.AndroidPlatformModule.getApplicationContext;

public class DialogDatosBasicos extends Dialog implements
        View.OnClickListener {

    private static final String CAMPOS_VACIOS = "campos_vacios";
    private static final String LBL_BASIC_DATA = "lbl_basic_data";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String MAIL = "mail";
    private static final String LBL_BTN_SAVE_CHANGES = "lbl_btn_save_changes";
    PerfilActivity c;
    LinearLayout lnBtnCambiarDatos;
    EditText editNombreDatos;
    EditText editApellidosDatos;
    EditText editCorreoDatos;
    String nombre;
    String apellido;
    String correo;
    TextView txtTitDatosTitulo;
    TextView txtTitDatosNombre;
    TextView txtTitDatosApellido;
    TextView txtTitDatosCorreo;
    TextView txtlnBtnCambiarDatos;
    Utils util = new Utils();
    TrackingOps trackingOps;


    public DialogDatosBasicos(PerfilActivity a, String nombre, String apellido, String correo) {
        super(a);

        this.c = a;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_datos_basicos);

        trackingOps = (TrackingOps) getApplicationContext();

        editNombreDatos = findViewById(R.id.editNombreDatos);
        editNombreDatos.setHint(trackingOps.getmFirebaseRemoteConfig().getString(FIRST_NAME));
        editApellidosDatos = findViewById(R.id.editApellidosDatos);
        editApellidosDatos.setHint(trackingOps.getmFirebaseRemoteConfig().getString(LAST_NAME));
        editCorreoDatos = findViewById(R.id.editCorreoDatos);
        lnBtnCambiarDatos = findViewById(R.id.lnBtnCambiarDatos);
        lnBtnCambiarDatos.setOnClickListener(this);
        editNombreDatos.setText(nombre);
        editApellidosDatos.setText(apellido);
        editCorreoDatos.setText(correo);

        txtTitDatosTitulo = findViewById(R.id.txtTitDatosTitulo);
        txtTitDatosTitulo.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BASIC_DATA));
        txtTitDatosNombre = findViewById(R.id.txtTitDatosNombre);
        txtTitDatosNombre.setText(trackingOps.getmFirebaseRemoteConfig().getString(FIRST_NAME));
        txtTitDatosApellido = findViewById(R.id.txtTitDatosApellido);
        txtTitDatosApellido.setText(trackingOps.getmFirebaseRemoteConfig().getString(LAST_NAME));
        txtTitDatosCorreo = findViewById(R.id.txtTitDatosCorreo);
        txtTitDatosCorreo.setText(trackingOps.getmFirebaseRemoteConfig().getString(MAIL));
        txtlnBtnCambiarDatos = findViewById(R.id.txtlnBtnCambiarDatos);
        txtlnBtnCambiarDatos.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_SAVE_CHANGES));

        txtTitDatosTitulo.setTypeface(util.fuenteSubTituHint(c));
        txtTitDatosNombre.setTypeface(util.fuenteSubTituHint(c));
        txtTitDatosApellido.setTypeface(util.fuenteSubTituHint(c));
        txtTitDatosCorreo.setTypeface(util.fuenteSubTituHint(c));
        txtlnBtnCambiarDatos.setTypeface(util.fuenteMenuLatBotones(c));

        editNombreDatos.setTypeface(util.fuenteSubTituHint(c));
        editApellidosDatos.setTypeface(util.fuenteSubTituHint(c));
        editCorreoDatos.setTypeface(util.fuenteSubTituHint(c));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnBtnCambiarDatos:
                if (editNombreDatos.getText().toString().equals("") || editApellidosDatos.getText().toString().equals("") || editCorreoDatos.getText().toString().equals("")) {
                    Toast.makeText(c, trackingOps.getmFirebaseRemoteConfig().getString(CAMPOS_VACIOS),
                            Toast.LENGTH_SHORT).show();
                } else {
                    c.lanzarAsynkEditar(editNombreDatos.getText().toString(), editApellidosDatos.getText().toString(), editCorreoDatos.getText().toString());
                    dismiss();
                }
                break;
            default:
                break;
        }

    }
}
