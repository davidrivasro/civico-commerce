package com.civiconegocios.app;

import android.app.Activity;
import android.net.ParseException;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.TextOpenSannSemiBold;
import com.civiconegocios.app.utils.TextOpenSansRegular;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

public class ActivityDetalleTransaccion extends AppCompatActivity {

    private static final String TRANSACTION_NO = "transaction_no";
    private static final String BASE_VALUE = "base_value";
    private static final String TAX = "tax";
    private static final String CONSUMPTION_TAX = "consumption_tax";
    private static final String TIP = "tip";
    private static final String TOTAL_VALUE = "total_value";
    private static final String USER_CODE = "user_code";
    private static final String APPROVAL_NUMBER = "approval_number";
    String fechaTotal = "";
    String hora;
    String status;
    String title; // valor total
    String pin;
    String taxAmount; // valor iva
    String numAprobacion;
    String transId;
    String tip; // valor base
    String iac; // valor base
    String valorBaseStr, origin; // valor base
    String reason;
    String description;

    Utils util = new Utils();
    ImageView lnPagosDetTrans;
    TextView txtDiaFechaTransaccion;
    TextView txtHoraTransaccion;
    TextView txtValorTransaccion;
    TextView txtPinTransaccion;
    com.google.android.gms.analytics.Tracker mTracker;
    // nuevos textview
    TextView txtTitNumAprobacionDetalle;
    TextView txtNumAprobacionDetalle;
    TextView txtTituloValorBaseDetalle;
    TextView txtValorBaseDetalle;
    TextView txtTituloValorIvaDetalle;
    TextView txtValorIvaDetalle;
    TextView txtTitPinTransaccion;
    TextView txtTitNumTransaccion;
    TextView txtNumTransaccion;
    TextView txtTituloValorTotalDetalle;
    TextView txtTituloImpoconsumoDetalle;
    TextView txtTituloPropinaDetalle;

    TextOpenSansRegular tv_texto_comentarios, tv_texto_descripcion;
    TextOpenSannSemiBold txtValorImpoconsumoDetalle;
    TextOpenSannSemiBold txtValorPropinaDetalle;
    RelativeLayout relativeLayout9, relativeLayout10, relativeLayout11, relativeLayout12;
    LinearLayout userCodeContainer;
    ConstraintLayout constraintLayout3, constraintLayout2;

    //    Ops
    TrackingOps trackingOps;
    User user;

    public static String capitalize(String s) {
        if (s == null) return null;
        if (s.length() == 1) {
            return s.toUpperCase();
        }
        if (s.length() > 1) {
            return s.substring(0, 1).toUpperCase() + s.substring(1);
        }
        return "";
    }

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_transaccion);

        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_flecha_atras_azul);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            // if (!extras.getString("fechaTotal").equals("2016")){
            fechaTotal = StringToDate(extras.getString("fechaTotal"));
            //  }

            hora = extras.getString("hora");
            status = extras.getString("status");
            title = extras.getString("monto"); // valor total
            pin = extras.getString("pin");

            taxAmount = extras.getString("taxAmount"); // valor iva
            numAprobacion = extras.getString("numAprobacion");
            transId = extras.getString("transId");
            valorBaseStr = extras.getString("valorBase"); // valor base
            iac = extras.getString("iac"); // valor base
            tip = extras.getString("tip"); // valor base
            origin = extras.getString("origin");
            reason = extras.getString("reason");
            description = extras.getString("description");
        }

        String[] separated = hora.split(" ");

        lnPagosDetTrans = findViewById(R.id.lnPagosDetTrans);

        txtDiaFechaTransaccion = findViewById(R.id.txtDiaFechaTransaccion);
        txtDiaFechaTransaccion.setText(capitalize(fechaTotal.replace(" ", ", ")));
        txtHoraTransaccion = findViewById(R.id.txtHoraTransaccion);
        txtHoraTransaccion.setText(separated[1] + " " + separated[2]);
        txtValorTransaccion = findViewById(R.id.txtValorTransaccion);

        txtPinTransaccion = findViewById(R.id.txtPinTransaccion);
        txtTituloValorTotalDetalle = findViewById(R.id.txtTituloValorTotalDetalle);
        txtTituloValorTotalDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE));

        txtValorImpoconsumoDetalle = findViewById(R.id.txtValorImpoconsumoDetalle);
        txtValorPropinaDetalle = findViewById(R.id.txtValorPropinaDetalle);

        relativeLayout9 = findViewById(R.id.relativeLayout9);
        relativeLayout10 = findViewById(R.id.relativeLayout10);
        relativeLayout11 = findViewById(R.id.relativeLayout11);
        relativeLayout12 = findViewById(R.id.relativeLayout12);
        userCodeContainer = findViewById(R.id.userCodeContainer);

        tv_texto_comentarios = findViewById(R.id.tv_texto_comentarios);
        constraintLayout3 = findViewById(R.id.constraintLayout3);

        if (reason != null && !reason.isEmpty()) {
            tv_texto_comentarios.setText(reason);
        } else {
            constraintLayout3.setVisibility(View.GONE);
        }

        tv_texto_descripcion = findViewById(R.id.tv_texto_descripcion);
        constraintLayout2 = findViewById(R.id.constraintLayout2);
        if (description != null && !description.isEmpty()) {
            tv_texto_descripcion.setText(description);
        } else {
            constraintLayout2.setVisibility(View.GONE);
        }


        if (origin != null && origin.equals("mpos")) {
            relativeLayout9.setVisibility(View.GONE);
            relativeLayout10.setVisibility(View.GONE);
            relativeLayout11.setVisibility(View.GONE);
            relativeLayout12.setVisibility(View.GONE);
        }

      /*  txtDiaFechaTransaccion.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtHoraTransaccion.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtValorTransaccion.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtPinTransaccion.setTypeface(util.fuenteParrafos(getApplicationContext()));*/

        if (pin == null) {
            userCodeContainer.setVisibility(View.GONE);
        } else {
            txtPinTransaccion.setText("** *** " + pin);
        }

        txtTitNumAprobacionDetalle = findViewById(R.id.txtTitNumAprobacionDetalle);
        txtTitNumAprobacionDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(APPROVAL_NUMBER));
        txtNumAprobacionDetalle = findViewById(R.id.txtNumAprobacionDetalle);
        txtTituloValorBaseDetalle = findViewById(R.id.txtTituloValorBaseDetalle);
        txtTituloValorBaseDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(BASE_VALUE));
        txtValorBaseDetalle = findViewById(R.id.txtValorBaseDetalle);
        txtTituloValorIvaDetalle = findViewById(R.id.txtTituloValorIvaDetalle);
        txtTituloValorIvaDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(TAX));
        txtValorIvaDetalle = findViewById(R.id.txtValorIvaDetalle);
        txtTituloImpoconsumoDetalle = findViewById(R.id.txtTituloImpoconsumoDetalle);
        txtTituloImpoconsumoDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONSUMPTION_TAX));
        txtTituloPropinaDetalle = findViewById(R.id.txtTituloPropinaDetalle);
        txtTituloPropinaDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(TIP));
        txtTitPinTransaccion = findViewById(R.id.txtTitPinTransaccion);
        txtTitPinTransaccion.setText(trackingOps.getmFirebaseRemoteConfig().getString(USER_CODE));
        txtTitNumTransaccion = findViewById(R.id.txtTitNumTransaccion);
        txtTitNumTransaccion.setText(trackingOps.getmFirebaseRemoteConfig().getString(TRANSACTION_NO));
        txtNumTransaccion = findViewById(R.id.txtNumTransaccion);

        switch (status) {
            case "0":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_espera);
                trackingOps.registerScreenName("Mis transacciones | Esperando aprobación | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "1":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_espera);
                trackingOps.registerScreenName("Mis transacciones | Esperando aprobación | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "2":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_aprobado);
                txtTitNumAprobacionDetalle.setVisibility(View.VISIBLE);
                txtNumAprobacionDetalle.setVisibility(View.VISIBLE);
                trackingOps.registerScreenName("Mis transacciones | Aprobado | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "3":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_cancelado);
                trackingOps.registerScreenName("Mis transacciones | Cancelado | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "4":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_cancelado);
                trackingOps.registerScreenName("Mis transacciones | Cancelado | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "5":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_cancelado);
                trackingOps.registerScreenName("Mis transacciones | Cancelado | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "6":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_cancelado);
                trackingOps.registerScreenName("Mis transacciones | Cancelado | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "7":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_aprobado);
                txtTitNumAprobacionDetalle.setVisibility(View.VISIBLE);
                txtNumAprobacionDetalle.setVisibility(View.VISIBLE);
                trackingOps.registerScreenName("Mis transacciones | Aprobado | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "8":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_cancelado);
                trackingOps.registerScreenName("Mis transacciones | Cancelado | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;

            case "9":
                lnPagosDetTrans.setImageResource(R.drawable.pagos_cancelado);
                trackingOps.registerScreenName("Mis transacciones | Cancelado | Cívico PAY Comercios | " + user.getAnalyticsCity());
                break;
        }

      /*  txtTitNumTransaccion.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtNumTransaccion.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtTituloValorTotalDetalle.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtTitNumAprobacionDetalle.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtNumAprobacionDetalle.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtTituloValorBaseDetalle.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtValorBaseDetalle.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtTituloValorIvaDetalle.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtValorIvaDetalle.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtTitPinTransaccion.setTypeface(util.fuenteParrafos(getApplicationContext()));*/

        txtValorBaseDetalle.setText("$ " + miles(valorBaseStr + "").replaceAll(",", "."));
        txtNumAprobacionDetalle.setText(numAprobacion);
        txtNumTransaccion.setText(transId);
        txtValorIvaDetalle.setText("$ " + miles(taxAmount + "").replaceAll(",", "."));

        txtValorTransaccion.setText("$ " + miles(title).replaceAll(",", "."));

        txtValorImpoconsumoDetalle.setText("$ " + iac);
        txtValorPropinaDetalle.setText("$ " + tip);

       /* int titleInt = Integer.parseInt(title);
        int taxAmountInt = Integer.parseInt(taxAmount);
        int valorTotalFinal = titleInt+taxAmountInt;

        txtValorTransaccion.setText("$ "+miles(valorTotalFinal+"").replaceAll(",","."));*/
    }

    public String miles(String no) {
        String newString = no.replaceAll(",", "");
        //String newString = "1000000";
        BigDecimal sPrice = new BigDecimal(newString);
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(',');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String str = formatter.format(sPrice.longValue());
        return str;
    }

    public String StringToDate(String fecha) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try {
            date = format.parse(fecha);

        } catch (ParseException e) {

            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return DateToString(date);
    }

    public String DateToString(java.util.Date date) {

        SimpleDateFormat dateformat = new SimpleDateFormat("EEEE yyyy/MM/dd");
        String datetime = "";
        try {
            datetime = dateformat.format(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return datetime;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
