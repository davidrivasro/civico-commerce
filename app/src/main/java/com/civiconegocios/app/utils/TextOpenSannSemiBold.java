package com.civiconegocios.app.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextOpenSannSemiBold extends TextView {

    Utils utils = new Utils();

    public TextOpenSannSemiBold(Context context) {
        super(context);
        this.setTypeface(utils.fuenteCalculadora(context));
    }

    public TextOpenSannSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(utils.fuenteCalculadora(context));
    }

    public TextOpenSannSemiBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(utils.fuenteCalculadora(context));
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
