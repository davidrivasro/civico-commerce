package com.civiconegocios.app.utils;

import android.content.Context;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.Brand;
import com.civiconegocios.app.models.Customer;
import com.civiconegocios.app.models.User;
import com.pushwoosh.Pushwoosh;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;

public class UserDataSerializer {

    private final String TAG = UserDataSerializer.class.getSimpleName();

    private Realm realm;


    public JSONObject processData(String data, String accessToken, String password, Context context) throws IOException {
        JSONObject output = new JSONObject();
        try {
            String nameComercio;
            JSONObject catObj = new JSONObject(data);
            JSONObject profile = catObj.getJSONObject("profile");
            JSONObject brand = catObj.getJSONObject("brand");
            JSONObject customer_info = catObj.getJSONObject("customer_info");

            realm = ((TrackingOps) context.getApplicationContext()).getRealm();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    try {
                        final User user = new User();
                        user.setId(profile.getString("id"));
                        user.setEmail(catObj.getString("email"));
                        user.setPassword(password);
                        user.setAccessToken(accessToken);
                        user.setAvatar(profile.getString("avatar"));
                        user.setFirstName(profile.getString("first_name"));
                        user.setLastName(profile.getString("last_name"));
                        user.setCity(profile.getString("city"));

                        user.setHasPassword(catObj.getBoolean("has_password?"));
                        user.setReferralCode(catObj.getString("referral_code"));
                        user.setAcceptingPayments(catObj.getBoolean("accept_payments"));
                        user.setPushToken(Pushwoosh.getInstance().getPushToken());

                        if (catObj.get("mpos_enable") instanceof Boolean) {
                            user.setMposEnabled(catObj.getBoolean("mpos_enable"));
                        } else {
                            user.setMposEnabled(false);
                        }

                        user.setMinAmount(catObj.getInt("min_amount"));
                        user.setMaxAmount(catObj.getInt("max_amount"));

                        switch (profile.getString("city")) {
                            case "mexico":
                                user.setAnalyticsCity("CDMX");
                                break;
                            case "bogota":
                                user.setAnalyticsCity("Bogotá");
                                break;
                            case "santiago":
                                user.setAnalyticsCity("Santiago");
                            default:
                                user.setAnalyticsCity(profile.getString("city"));
                                break;
                        }

                        if (brand.length() > 0) {
                            Brand brandObject = realm.createObject(Brand.class);
                            brandObject.setId(brand.getString("id"));
                            brandObject.setName(brand.getString("name"));
                            user.setBrand(brandObject);
                        }

                        if (customer_info.length() > 0) {
                            Customer customer = realm.createObject(Customer.class);
                            customer.setName(customer_info.getString("name"));
                            if (!customer_info.getString("address").equals("")) {
                                customer.setAddress(customer_info.getString("address"));
                            } else {
                                customer.setAddress("-");
                            }
                            customer.setId(customer_info.getString("id"));
                            customer.setSlug(customer_info.getString("slug"));
                            user.setCustomer(customer);
                        }

                        realm.copyToRealmOrUpdate(user);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            String id = profile.getString("id");
            String avatar = profile.getString("avatar");
            String first_name = profile.getString("first_name");
            String last_name = profile.getString("last_name");
            String city = profile.getString("city");


            String name = customer_info.getString("name");
            nameComercio = name;
            String address;

            if (!customer_info.getString("address").equals("")) {
                address = customer_info.getString("address");
            } else {
                address = "-";
            }
            try {
                output.put("datosUser", id + ",," + avatar + ",," + first_name + ",," + last_name + ",," + city + ",," + name + ",," + address);
                output.put("nameComercio", nameComercio);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return output;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }

    public String processPasswordUpdateData(String data, User user, Context context) throws IOException {
        String output = "";
        try {

            realm = ((TrackingOps) context.getApplicationContext()).getRealm();

            JSONObject catObj = new JSONObject(data);

            JSONObject perfil = catObj.getJSONObject("profile");
            String id = perfil.getString("id");
            String avatar = perfil.getString("avatar");
            String first_name = perfil.getString("first_name");
            String last_name = perfil.getString("last_name");
            String city = perfil.getString("city");
            JSONObject customer_info = catObj.getJSONObject("customer_info");
            String name = customer_info.getString("name");
            String address = customer_info.getString("address");

            output = id + ",," + avatar + ",," + first_name + ",," + last_name + ",," + city + ",," + name + ",," + address;

            realm.beginTransaction();
            user.setFirstName(first_name);
            user.setLastName(last_name);
            user.setAvatar(avatar);
            user.setCity(city);
            user.getCustomer().setName(name);
            user.getCustomer().setAddress(address);
            if (city.equals("CDMX")) {
                user.setAnalyticsCity("CDMX");
            } else {
                user.setAnalyticsCity(city);
            }
            realm.commitTransaction();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return output;
    }
}
