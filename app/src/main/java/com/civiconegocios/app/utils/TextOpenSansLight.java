package com.civiconegocios.app.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextOpenSansLight extends TextView {

    Utils utils = new Utils();

    public TextOpenSansLight(Context context) {
        super(context);
        this.setTypeface(utils.fuenteHeader(context));
    }

    public TextOpenSansLight(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(utils.fuenteHeader(context));
    }

    public TextOpenSansLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(utils.fuenteHeader(context));
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
