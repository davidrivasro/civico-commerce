package com.civiconegocios.app.utils;

import com.civiconegocios.app.VO.CategoriasVO;
import com.civiconegocios.app.VO.QuestionsVO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonUtils {

    private final String TAG = JsonUtils.class.getSimpleName();

    public ArrayList<CategoriasVO> getPreguntasFrecuentes(String response) throws JSONException {

        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonTopics = jsonObject.getJSONArray("topics");
        ArrayList<CategoriasVO> categoriasList = new ArrayList<>();

        for (int i = 0; i < jsonTopics.length(); i++) {
            JSONObject categoriaObject = jsonTopics.getJSONObject(i);

            CategoriasVO categorias = new CategoriasVO();
            categorias.setTitle(categoriaObject.getString("title"));

            JSONArray questionsJsonArray = categoriaObject.getJSONArray("questions");
            for (int j = 0; j < questionsJsonArray.length(); j++) {
                JSONObject questionObject = questionsJsonArray.getJSONObject(j);
                QuestionsVO questions = new QuestionsVO();
                questions.setQuestion(questionObject.getString("question"));
                questions.setAnswer(questionObject.getString("answer"));
                categorias.setQuestions(questions);
            }

            categoriasList.add(categorias);
        }

        return categoriasList;
    }
}
