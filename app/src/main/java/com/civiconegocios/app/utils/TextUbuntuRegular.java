package com.civiconegocios.app.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextUbuntuRegular extends TextView {

    Utils utils = new Utils();

    public TextUbuntuRegular(Context context) {
        super(context);
        this.setTypeface(utils.fuenteMenuLatBotones(context));
    }

    public TextUbuntuRegular(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(utils.fuenteMenuLatBotones(context));
    }

    public TextUbuntuRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(utils.fuenteMenuLatBotones(context));
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
