package com.civiconegocios.app.utils;


public class Constants {

    public static final String CRASHLYTICS_CATEGORY = "Civico Pay Comercios";
    public static final String CLIENT_ID = "89249bacf4ddd0c525e4cd51b29f5377966e513df490a1b36e66172a60b1b8ca";
    public static final String CLIENT_SECRET = "e83fa36749527f87ac3a867544216482cbf0a2d71a2a99d60357d13def2abb7f";

    //    Request Extras
    public static final String OPERATION_TYPE = "operation_type";
    public static final String OPERATION_CITY = "city";
    public static final String OPERATION_BODY = "body";
    public static final String OPERATION_PARAMETERS = "parameters";
    public static final String OPERATION_URL = "operation_url";
    public static final String OPERATION_ENDPOINT = "operation_endpoint";
}

