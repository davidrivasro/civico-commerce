package com.civiconegocios.app.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextOpenSansRegular extends TextView {

    Utils utils = new Utils();

    public TextOpenSansRegular(Context context) {
        super(context);
        this.setTypeface(utils.fuenteParrafos(context));
    }

    public TextOpenSansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(utils.fuenteParrafos(context));
    }

    public TextOpenSansRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(utils.fuenteParrafos(context));
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
