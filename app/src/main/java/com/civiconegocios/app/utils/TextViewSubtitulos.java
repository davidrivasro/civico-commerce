package com.civiconegocios.app.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewSubtitulos extends TextView {

    Utils utils = new Utils();

    public TextViewSubtitulos(Context context) {
        super(context);
        this.setTypeface(utils.fuenteSubTituHint(context));
    }

    public TextViewSubtitulos(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setTypeface(utils.fuenteSubTituHint(context));
    }

    public TextViewSubtitulos(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(utils.fuenteSubTituHint(context));
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
