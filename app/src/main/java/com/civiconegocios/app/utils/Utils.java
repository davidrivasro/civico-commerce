package com.civiconegocios.app.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;

import com.civiconegocios.app.R;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public Utils() {

    }

    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean haveInternet(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        return !(info == null || !info.isConnected());
    }

    public void mensajeNoInternet(Context context) {
        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }

    public Typeface fuenteMenuLatBotones(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "Ubuntu-Regular.ttf");
    }

    public Typeface fuenteSubTituHint(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "Ubuntu-Light.ttf");
    }

    public Typeface fuenteHeader(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");
    }

    public Typeface fuenteParrafos(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");
    }

    public Typeface fuenteCalculadora(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "OpenSans-Semibold.ttf");
    }

    public Typeface OpenSansBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "OpenSans-Bold.ttf");
    }

    public void alertErrorContrasena(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        // set title
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.title_alert_generic));
        // set dialog message
        alertDialogBuilder
                .setMessage(context.getResources().getString(R.string.alert_error_contrasena))
                .setCancelable(false)
                .setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void alertError500(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        // set title
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.title_alert_generic));
        // set dialog message
        alertDialogBuilder
                .setMessage(context.getResources().getString(R.string.alert_500))
                .setCancelable(false)
                .setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void presentDialogWith(Context context, String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        // set title
        alertDialogBuilder.setTitle(title);
        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void alertErrorTimeOut(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        // set title
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.title_alert_generic));
        // set dialog message
        alertDialogBuilder
                .setMessage(context.getResources().getString(R.string.alert_conexion_lenta))
                .setCancelable(false)
                .setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public boolean mostrarPopUp() {
        int hours = new Time(System.currentTimeMillis()).getHours();
        return hours > 9 && hours < 18;
    }


    public void toolbarOrderItems(Toolbar toolbar) {
        final CharSequence originalTitle = toolbar.getTitle();

        toolbar.setTitle("title");

        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);


            if (view instanceof ActionMenuView) {
                ActionMenuView actionMenuView = (ActionMenuView) view;

                if (actionMenuView.getId() == -1) {
                    Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
                    actionMenuView.setLayoutParams(params);
                }
            }
        }
        toolbar.setTitle(originalTitle);
    }

    public String encode(String access_token_str) throws UnsupportedEncodingException {
        // Sending side
        byte[] data = access_token_str.getBytes("UTF-8");
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public String decode(String access_token_str) throws UnsupportedEncodingException {
        // Sending side
        byte[] data = Base64.decode(access_token_str, Base64.DEFAULT);
        return new String(data, "UTF-8");
    }

    public Date convertStringDate(String dateString) {
        String[] parts = dateString.split("T");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(parts[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public String StringToDate(String fecha) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try {
            date = format.parse(fecha);
        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return DateToString(date);
    }

    public String DateToString(java.util.Date date) {

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        String datetime = "";
        try {
            datetime = dateformat.format(date);
        } catch (android.net.ParseException e) {
            e.printStackTrace();
        }

        return datetime;
    }

    public String miles(String no) {
        String newString = no.replaceAll(",", "");
        //String newString = "1000000";
        BigDecimal sPrice = new BigDecimal(newString);
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(',');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String str = formatter.format(sPrice.longValue());
        return str;
    }

    public String convertStringToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

}
