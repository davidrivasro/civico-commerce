package com.civiconegocios.app.VO;

import android.os.Parcel;
import android.os.Parcelable;

public class OfertasRedimidasVO implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public OfertasRedimidasVO createFromParcel(Parcel in) {
            return new OfertasRedimidasVO(in);
        }

        public OfertasRedimidasVO[] newArray(int size) {
            return new OfertasRedimidasVO[size];
        }
    };

    String id = "";
    String status = "";
    String redemption_date = "";
    String offer_name = "";
    String price = "";
    String discount = "";
    String user_name = "";
    String document_type = "";
    String document_number = "";
    String mobile_phone = "";
    String code = "";
    String user_email = "";

    //Prueba
    String comentarioUno = "";
    String comentarioDos = "";

    // Constructor
    public OfertasRedimidasVO(String id, String status, String redemption_date, String offer_name, String price, String discount,
                              String user_name, String document_type, String document_number, String mobile_phone, String code,
                              String user_email, String comentarioUno, String comentarioDos) {
        this.id = id;
        this.status = status;
        this.redemption_date = redemption_date;
        this.offer_name = offer_name;
        this.price = price;
        this.discount = discount;
        this.user_name = user_name;
        this.document_type = document_type;
        this.document_number = document_number;
        this.mobile_phone = mobile_phone;
        this.code = code;
        this.user_email = user_email;
        this.comentarioUno = comentarioUno;
        this.comentarioDos = comentarioDos;
    }

    // Parcelling part
    public OfertasRedimidasVO(Parcel in) {
        this.id = in.readString();
        this.status = in.readString();
        this.redemption_date = in.readString();
        this.offer_name = in.readString();
        this.price = in.readString();
        this.discount = in.readString();
        this.user_name = in.readString();
        this.document_type = in.readString();
        this.document_number = in.readString();
        this.mobile_phone = in.readString();
        this.code = in.readString();
        this.user_email = in.readString();
        this.comentarioUno = in.readString();
        this.comentarioDos = in.readString();
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getComentarioUno() {
        return comentarioUno;
    }

    public void setComentarioUno(String comentarioUno) {
        this.comentarioUno = comentarioUno;
    }

    public String getComentarioDos() {
        return comentarioDos;
    }

    public void setComentarioDos(String comentarioDos) {
        this.comentarioDos = comentarioDos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRedemption_date() {
        return redemption_date;
    }

    public void setRedemption_date(String redemption_date) {
        this.redemption_date = redemption_date;
    }

    public String getOffer_name() {
        return offer_name;
    }

    public void setOffer_name(String offer_name) {
        this.offer_name = offer_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.status);
        dest.writeString(this.redemption_date);
        dest.writeString(this.offer_name);
        dest.writeString(this.price);
        dest.writeString(this.discount);
        dest.writeString(this.user_name);
        dest.writeString(this.document_type);
        dest.writeString(this.document_number);
        dest.writeString(this.mobile_phone);
        dest.writeString(this.code);
        dest.writeString(this.user_email);
        dest.writeString(this.comentarioUno);
        dest.writeString(this.comentarioDos);
    }
}
