package com.civiconegocios.app.VO;

import android.os.Parcel;
import android.os.Parcelable;

public class FiltroOfertasVO implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public FiltroOfertasVO createFromParcel(Parcel in) {
            return new FiltroOfertasVO(in);
        }

        public FiltroOfertasVO[] newArray(int size) {
            return new FiltroOfertasVO[size];
        }
    };

    String id;
    String name;
    boolean checkFiltro;

    // Constructor
    public FiltroOfertasVO(String id, String name, Boolean checkFiltro) {
        this.id = id;
        this.name = name;
        this.checkFiltro = checkFiltro;
    }

    // Parcelling part
    public FiltroOfertasVO(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.checkFiltro = in.readByte() != 0;
    }

    public boolean isCheckFiltro() {
        return checkFiltro;
    }

    public void setCheckFiltro(boolean checkFiltro) {
        this.checkFiltro = checkFiltro;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeByte((byte) (checkFiltro ? 1 : 0));
    }
}
