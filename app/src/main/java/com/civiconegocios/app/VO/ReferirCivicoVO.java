package com.civiconegocios.app.VO;


public class ReferirCivicoVO {

    private String referral_code;
    private String referral_code_plain;
    private String main_title;
    private String main_text;
    private String dynamic_text;
    private String button_invite;
    private String modal_title;
    private String modal_text;

    public String getReferral_code() {
        return referral_code;
    }

    public void setReferral_code(String referral_code) {
        this.referral_code = referral_code;
    }

    public String getReferral_code_plain() {
        return referral_code_plain;
    }

    public void setReferral_code_plain(String referral_code_plain) {
        this.referral_code_plain = referral_code_plain;
    }

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getMain_text() {
        return main_text;
    }

    public void setMain_text(String main_text) {
        this.main_text = main_text;
    }

    public String getDynamic_text() {
        return dynamic_text;
    }

    public void setDynamic_text(String dynamic_text) {
        this.dynamic_text = dynamic_text;
    }

    public String getButton_invite() {
        return button_invite;
    }

    public void setButton_invite(String button_invite) {
        this.button_invite = button_invite;
    }

    public String getModal_title() {
        return modal_title;
    }

    public void setModal_title(String modal_title) {
        this.modal_title = modal_title;
    }

    public String getModal_text() {
        return modal_text;
    }

    public void setModal_text(String modal_text) {
        this.modal_text = modal_text;
    }
}
