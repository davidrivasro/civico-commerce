package com.civiconegocios.app.VO;

import android.os.Parcel;
import android.os.Parcelable;

public class OfertasVO implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public OfertasVO createFromParcel(Parcel in) {
            return new OfertasVO(in);
        }

        public OfertasVO[] newArray(int size) {
            return new OfertasVO[size];
        }
    };

    String id = "";
    String name = "";
    String uri = "";
    String price = "";
    String conditions = "";
    String information = "";
    String image = "";
    String discount = "";
    String start_date = "";
    String end_date = "";
    String soon_expiration = "";
    String redeem_coupons = "";
    String available_coupons = "";
    String coupons_per_user = "";
    String end_date_text = "";
    String total_coupons = "";

    // Constructor
    public OfertasVO(String id, String name, String uri, String price, String conditions, String information, String image, String discount,
                     String start_date, String end_date, String soon_expiration, String redeem_coupons, String available_coupons, String coupons_per_user,
                     String end_date_text, String total_coupons) {
        this.id = id;
        this.name = name;
        this.uri = uri;
        this.price = price;
        this.conditions = conditions;
        this.information = information;
        this.image = image;
        this.discount = discount;
        this.start_date = start_date;
        this.end_date = end_date;
        this.soon_expiration = soon_expiration;
        this.redeem_coupons = redeem_coupons;
        this.available_coupons = available_coupons;
        this.coupons_per_user = coupons_per_user;
        this.end_date_text = end_date_text;
        this.total_coupons = total_coupons;
    }

    // Parcelling part
    public OfertasVO(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.uri = in.readString();
        this.price = in.readString();
        this.conditions = in.readString();
        this.information = in.readString();
        this.image = in.readString();
        this.discount = in.readString();
        this.start_date = in.readString();
        this.end_date = in.readString();
        this.soon_expiration = in.readString();
        this.redeem_coupons = in.readString();
        this.available_coupons = in.readString();
        this.coupons_per_user = in.readString();
        this.end_date_text = in.readString();
        this.total_coupons = in.readString();

    }

    public String getTotal_coupons() {
        return total_coupons;
    }

    public void setTotal_coupons(String total_coupons) {
        this.total_coupons = total_coupons;
    }

    public String getEnd_date_text() {
        return end_date_text;
    }

    public void setEnd_date_text(String end_date_text) {
        this.end_date_text = end_date_text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getSoon_expiration() {
        return soon_expiration;
    }

    public void setSoon_expiration(String soon_expiration) {
        this.soon_expiration = soon_expiration;
    }

    public String getRedeem_coupons() {
        return redeem_coupons;
    }

    public void setRedeem_coupons(String redeem_coupons) {
        this.redeem_coupons = redeem_coupons;
    }

    public String getAvailable_coupons() {
        return available_coupons;
    }

    public void setAvailable_coupons(String available_coupons) {
        this.available_coupons = available_coupons;
    }

    public String getCoupons_per_user() {
        return coupons_per_user;
    }

    public void setCoupons_per_user(String coupons_per_user) {
        this.coupons_per_user = coupons_per_user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.uri);
        dest.writeString(this.price);
        dest.writeString(this.conditions);
        dest.writeString(this.information);
        dest.writeString(this.image);
        dest.writeString(this.discount);
        dest.writeString(this.start_date);
        dest.writeString(this.end_date);
        dest.writeString(this.soon_expiration);
        dest.writeString(this.redeem_coupons);
        dest.writeString(this.available_coupons);
        dest.writeString(this.coupons_per_user);
        dest.writeString(this.end_date_text);
        dest.writeString(this.total_coupons);
    }
}
