package com.civiconegocios.app.VO;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestionsVO implements Parcelable {

    public static final Parcelable.Creator<QuestionsVO> CREATOR = new Parcelable.Creator<QuestionsVO>() {
        @Override
        public QuestionsVO createFromParcel(Parcel source) {
            return new QuestionsVO(source);
        }

        @Override
        public QuestionsVO[] newArray(int size) {
            return new QuestionsVO[size];
        }
    };
    private String question;
    private String answer;
    private boolean visible;

    public QuestionsVO() {
    }

    public QuestionsVO(String question, String answer, String categoria) {
        this.question = question;
        this.answer = answer;
    }

    protected QuestionsVO(Parcel in) {
        this.question = in.readString();
        this.answer = in.readString();
        this.visible = in.readByte() != 0;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.question);
        dest.writeString(this.answer);
        dest.writeByte(this.visible ? (byte) 1 : (byte) 0);
    }
}
