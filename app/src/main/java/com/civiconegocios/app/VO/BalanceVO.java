package com.civiconegocios.app.VO;

import java.util.List;

public class BalanceVO {

    String amount;
    String currency;
    String updated_at;
    String reteiva;
    String reterenta;
    String reteica;
    String total_transfer;
    List<BalanceVO> listBalanceTemp;
    String pending_to_transfer_amount;
    String pending_to_transfer_currency;
    String pending_to_transfer_reteiva;
    String pending_to_transfer_reterenta;
    String pending_to_transfer_reteica;
    String pending_to_transfer_total_transfer;
    String pending_to_transfer_discounts;
    String total_transfered_amount;
    String total_transfered_currency;
    String discounts;

    public String getPending_to_transfer_discounts() {
        return pending_to_transfer_discounts;
    }

    public void setPending_to_transfer_discounts(String pending_to_transfer_discounts) {
        this.pending_to_transfer_discounts = pending_to_transfer_discounts;
    }

    public String getDiscounts() {
        return discounts;
    }

    public void setDiscounts(String discounts) {
        this.discounts = discounts;
    }

    public String getReteiva() {
        return reteiva;
    }

    public void setReteiva(String reteiva) {
        this.reteiva = reteiva;
    }

    public String getReterenta() {
        return reterenta;
    }

    public void setReterenta(String reterenta) {
        this.reterenta = reterenta;
    }

    public String getReteica() {
        return reteica;
    }

    public void setReteica(String reteica) {
        this.reteica = reteica;
    }

    public String getTotal_transfer() {
        return total_transfer;
    }

    public void setTotal_transfer(String total_transfer) {
        this.total_transfer = total_transfer;
    }

    public String getPending_to_transfer_reteiva() {
        return pending_to_transfer_reteiva;
    }

    public void setPending_to_transfer_reteiva(String pending_to_transfer_reteiva) {
        this.pending_to_transfer_reteiva = pending_to_transfer_reteiva;
    }

    public String getPending_to_transfer_reterenta() {
        return pending_to_transfer_reterenta;
    }

    public void setPending_to_transfer_reterenta(String pending_to_transfer_reterenta) {
        this.pending_to_transfer_reterenta = pending_to_transfer_reterenta;
    }

    public String getPending_to_transfer_reteica() {
        return pending_to_transfer_reteica;
    }

    public void setPending_to_transfer_reteica(String pending_to_transfer_reteica) {
        this.pending_to_transfer_reteica = pending_to_transfer_reteica;
    }

    public String getPending_to_transfer_total_transfer() {
        return pending_to_transfer_total_transfer;
    }

    public void setPending_to_transfer_total_transfer(String pending_to_transfer_total_transfer) {
        this.pending_to_transfer_total_transfer = pending_to_transfer_total_transfer;
    }

    public List<BalanceVO> getListBalanceTemp() {
        return listBalanceTemp;
    }

    public void setListBalanceTemp(List<BalanceVO> listBalanceTemp) {
        this.listBalanceTemp = listBalanceTemp;
    }

    public String getPending_to_transfer_amount() {
        return pending_to_transfer_amount;
    }

    public void setPending_to_transfer_amount(String pending_to_transfer_amount) {
        this.pending_to_transfer_amount = pending_to_transfer_amount;
    }

    public String getPending_to_transfer_currency() {
        return pending_to_transfer_currency;
    }

    public void setPending_to_transfer_currency(String pending_to_transfer_currency) {
        this.pending_to_transfer_currency = pending_to_transfer_currency;
    }

    public String getTotal_transfered_amount() {
        return total_transfered_amount;
    }

    public void setTotal_transfered_amount(String total_transfered_amount) {
        this.total_transfered_amount = total_transfered_amount;
    }

    public String getTotal_transfered_currency() {
        return total_transfered_currency;
    }

    public void setTotal_transfered_currency(String total_transfered_currency) {
        this.total_transfered_currency = total_transfered_currency;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


}
