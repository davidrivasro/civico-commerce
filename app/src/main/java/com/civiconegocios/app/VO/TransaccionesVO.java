package com.civiconegocios.app.VO;

public class TransaccionesVO {

    String id;
    String amount;
    String amountBase;
    String currency;
    String created_at;
    String status;
    String description;
    String transaction_id;
    String updated_at;
    String trazability_id; //ojo puede venir  null
    String reason; //ojo puede venir  null  316 6613930
    String pinUser;
    // nuevos paramatros
    String tax_amount;
    String authorization_code;
    String origin;
    String iac_amount;
    String tip_amount;

    public String getIac_amount() {
        return iac_amount;
    }

    public void setIac_amount(String iac_amount) {
        this.iac_amount = iac_amount;
    }

    public String getTip_amount() {
        return tip_amount;
    }

    public void setTip_amount(String tip_amount) {
        this.tip_amount = tip_amount;
    }

    public String getAmountBase() {
        return amountBase;
    }

    public void setAmountBase(String amountBase) {
        this.amountBase = amountBase;
    }

    public String getTax_amount() {
        return tax_amount;
    }

    public void setTax_amount(String tax_amount) {
        this.tax_amount = tax_amount;
    }

    public String getAuthorization_code() {
        return authorization_code;
    }

    public void setAuthorization_code(String authorization_code) {
        this.authorization_code = authorization_code;
    }

    public String getPinUser() {
        return pinUser;
    }

    public void setPinUser(String pinUser) {
        this.pinUser = pinUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getTrazability_id() {
        return trazability_id;
    }

    public void setTrazability_id(String trazability_id) {
        this.trazability_id = trazability_id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}
