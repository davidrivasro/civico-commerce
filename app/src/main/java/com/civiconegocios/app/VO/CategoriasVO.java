package com.civiconegocios.app.VO;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class CategoriasVO implements Parcelable {

    public static final Parcelable.Creator<CategoriasVO> CREATOR = new Parcelable.Creator<CategoriasVO>() {
        @Override
        public CategoriasVO createFromParcel(Parcel source) {
            return new CategoriasVO(source);
        }

        @Override
        public CategoriasVO[] newArray(int size) {
            return new CategoriasVO[size];
        }
    };
    private String title;
    private List<QuestionsVO> questions;

    public CategoriasVO() {
    }

    protected CategoriasVO(Parcel in) {
        this.title = in.readString();
        this.questions = new ArrayList<QuestionsVO>();
        in.readList(this.questions, QuestionsVO.class.getClassLoader());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<QuestionsVO> getQuestions() {
        return questions;
    }

    public void setQuestions(QuestionsVO question) {
        if (questions == null) questions = new ArrayList<>();
        this.questions.add(question);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeList(this.questions);
    }
}
