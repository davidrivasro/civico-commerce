package com.civiconegocios.app.services;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

import com.civiconegocios.app.VO.OfertasVO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DealsListDataSerializer extends AsyncTaskLoader<List<OfertasVO>> {

    private Bundle args;

    public DealsListDataSerializer(Context context, Bundle args) {
        super(context);
        this.args = args;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<OfertasVO> loadInBackground() {

        List<OfertasVO> listBalanceTemp = new ArrayList<>();

        try {
            List<OfertasVO> hoyList = new ArrayList<>();
            JSONObject catObj = new JSONObject(this.args.getString("data"));
            JSONArray arrayOffers = catObj.getJSONArray("offers");
            if (arrayOffers.length() > 0) {
                for (int i = 0; i < arrayOffers.length(); i++) {

                    JSONObject teamObj = arrayOffers.getJSONObject(i);
                    String strIncluye = "";
                    JSONArray conditionsArray = teamObj.getJSONArray("conditions");
                    if (conditionsArray.length() > 0) {

                        for (int a = 0; a < conditionsArray.length(); a++) {
                            strIncluye += conditionsArray.get(a) + "SEPARADOR";
                        }
                    }

                    String strInfo = "";
                    JSONArray includedArray = teamObj.getJSONArray("information");
                    if (includedArray.length() > 0) {

                        for (int a = 0; a < includedArray.length(); a++) {
                            strInfo += includedArray.get(a) + "SEPARADOR";
                        }
                    }

                    OfertasVO categoriasInfo = new OfertasVO(teamObj.getString("id"), teamObj.getString("name"), teamObj.getString("uri"),
                            teamObj.getInt("price") + "", strIncluye, strInfo, teamObj.getString("image"), teamObj.getInt("discount") + "",
                            teamObj.getString("start_date"), teamObj.getString("end_date"), teamObj.getBoolean("soon_expiration") + "",
                            teamObj.getInt("redeemed_coupons") + "", teamObj.getInt("available_coupons") + "", teamObj.getInt("coupons_per_user") + "",
                            teamObj.getString("end_date_text"), teamObj.getString("total_coupons"));
                    hoyList.add(categoriasInfo);
                }
                listBalanceTemp.addAll(hoyList);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            cancelLoad();
        }
        return listBalanceTemp;
    }

    @Override
    public void deliverResult(List<OfertasVO> data) {
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void onCanceled(@Nullable List<OfertasVO> data) {
        deliverResult(null);
        super.onCanceled(data);
    }

    @Override
    protected boolean onCancelLoad() {
        return super.onCancelLoad();
    }
}
