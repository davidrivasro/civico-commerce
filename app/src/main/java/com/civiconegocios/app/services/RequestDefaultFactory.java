package com.civiconegocios.app.services;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.content.AsyncTaskLoader;

import com.civiconegocios.app.CreateDealActivity;
import com.civiconegocios.app.servicios.ObjectHttp;
import com.civiconegocios.app.utils.Constants;

import static com.civiconegocios.app.SplashActivity.APP_VERSION;


public class RequestDefaultFactory extends AsyncTaskLoader<String> {

    private final String TAG = CreateDealActivity.class.getSimpleName();

    String resultFromHttp;
    private Bundle args;

    public RequestDefaultFactory(Context context, Bundle args) {
        super(context);
        this.args = args;
    }

    @Override
    protected void onStartLoading() {
        if (resultFromHttp != null) {
            deliverResult(resultFromHttp);
        } else {
            forceLoad();
        }
    }

    @Override
    public String loadInBackground() {

        String output = "";

        try {
            ObjectHttp httResponse = new ObjectHttp(getContext());
            switch (this.args.getString(Constants.OPERATION_TYPE)) {
                case "POST":
                    if (this.args.getString(Constants.OPERATION_BODY) != null) {
                        if (this.args.getString(Constants.OPERATION_PARAMETERS) != null) {
                            output = httResponse.obtenerHttpConDatosPostBasicAutorization(this.args.getString(Constants.OPERATION_URL),
                                    this.args.getString(Constants.OPERATION_BODY),
                                    this.args.getString(Constants.OPERATION_PARAMETERS),
                                    this.args.getString(Constants.OPERATION_CITY),
                                    APP_VERSION);
                        } else {
                            output = httResponse.obtenerHttpConDatos(this.args.getString(Constants.OPERATION_URL),
                                    this.args.getString(Constants.OPERATION_BODY));
                        }
                    } else {
                        cancelLoad();
                    }
                    break;
                case "GET":
                    if (this.args.getString(Constants.OPERATION_ENDPOINT) != null && this.args.getString(Constants.OPERATION_PARAMETERS) != null) {
                        output = httResponse.obtenerHttpGetAutorizacion(this.args.getString(Constants.OPERATION_URL),
                                this.args.getString(Constants.OPERATION_ENDPOINT),
                                this.args.getString(Constants.OPERATION_PARAMETERS));
                    } else {
                        cancelLoad();
                    }
                    break;
                case "PUT":
                    if (this.args.getString(Constants.OPERATION_BODY) != null && this.args.getString(Constants.OPERATION_PARAMETERS) != null) {
                        output = httResponse.obtenerHttpConDatosPutBasicAutorization(this.args.getString(Constants.OPERATION_URL),
                                this.args.getString(Constants.OPERATION_BODY), this.args.getString(Constants.OPERATION_PARAMETERS));
                    } else {
                        cancelLoad();
                    }
                    break;
                case "PATCH":
                    if (this.args.getString(Constants.OPERATION_BODY) != null && this.args.getString(Constants.OPERATION_PARAMETERS) != null) {
                        output = httResponse.obtenerHttpConDatosPatchBasicAutorization(this.args.getString(Constants.OPERATION_URL),
                                this.args.getString(Constants.OPERATION_BODY), this.args.getString(Constants.OPERATION_PARAMETERS));
                    } else {
                        cancelLoad();
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            cancelLoad();
        }
        /*
        } catch (ProtocolException e) {
            e.printStackTrace();
            cancelLoad();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            cancelLoad();
        } catch (IOException e) {
            e.printStackTrace();
            cancelLoad();
        } catch (NullPointerException e) {
            e.printStackTrace();
            cancelLoad();
        }
        */

        return output;
    }

    @Override
    public void deliverResult(String data) {
        resultFromHttp = data;
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void onCanceled(String data) {
        deliverResult(null);
        super.onCanceled(data);
    }

    @Override
    protected boolean onCancelLoad() {
        return super.onCancelLoad();
    }

}
