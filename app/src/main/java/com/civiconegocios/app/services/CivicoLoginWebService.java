package com.civiconegocios.app.services;

import com.civiconegocios.app.BuildConfig;
import com.civiconegocios.app.models.DefaultCivicoResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CivicoLoginWebService {

    String RECOVER_PASSWORD = "forgot_password";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.RETROFIT_LOGIN_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();


    @POST(RECOVER_PASSWORD)
    @FormUrlEncoded
    Call<DefaultCivicoResponse> recoverPassword(@Field("email") String email);
}
