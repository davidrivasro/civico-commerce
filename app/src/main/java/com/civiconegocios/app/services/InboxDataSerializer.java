package com.civiconegocios.app.services;

import android.content.Context;
import android.os.Bundle;
import androidx.loader.content.AsyncTaskLoader;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.Message;
import com.civiconegocios.app.models.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmList;


public class InboxDataSerializer extends AsyncTaskLoader<String> {

    String result;
    private Bundle args;
    private Realm realm;

    public InboxDataSerializer(Context context, Bundle args) {
        super(context);
        this.args = args;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public String loadInBackground() {
        try {
            realm = Realm.getInstance(((TrackingOps) getContext().getApplicationContext()).getConfiguration());
            if (this.result == null || !this.result.equals(this.args.getString("data"))) {
                this.result = this.args.getString("data");
                JSONObject dataObject = new JSONObject(this.result);

                JSONArray arrayResults = dataObject.getJSONArray("results");

                if (arrayResults.length() > 0) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            try {
                                for (int i = 0; i < arrayResults.length(); i++) {
                                    JSONObject resultObject = arrayResults.getJSONObject(i);
                                    JSONArray responsesArray = resultObject.getJSONArray("responses");
                                    final Message message = new Message();
                                    message.setId(resultObject.getString("id"));
                                    message.setName(resultObject.getString("name"));
                                    message.setLastname(resultObject.getString("lastname"));
                                    message.setMessage(resultObject.getString("message"));
                                    message.setPlace(resultObject.getString("place"));
                                    message.setCreated_at(resultObject.getString("created_at"));
                                    message.setEmail(resultObject.getString("email"));
                                    message.setType(resultObject.getString("type"));
                                    message.setRelated(resultObject.getBoolean("related"));
                                    message.setStatus(resultObject.getString("status"));
                                    message.setTotal_responses(resultObject.getInt("total_responses"));

                                    if (responsesArray.length() > 0) {
                                        RealmList<Response> responses = new RealmList<Response>();
                                        for (int io = 0; io < responsesArray.length(); io++) {
                                            JSONObject responseObject = responsesArray.getJSONObject(io);
                                            final Response response = new Response();
                                            response.setId(responseObject.getString("id"));
                                            response.setCreated_at(responseObject.getString("created_at"));
                                            response.setUser_id(responseObject.getString("user_id"));
                                            response.setText(responseObject.getString("text"));
                                            responses.add(response);
                                        }
                                        message.setResponses(responses);
                                    }

                                    realm.copyToRealmOrUpdate(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void deliverResult(String data) {
        result = data;
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void onCanceled(String data) {
        deliverResult(null);
        super.onCanceled(data);
    }

    @Override
    protected boolean onCancelLoad() {
        return super.onCancelLoad();
    }

}
