package com.civiconegocios.app.services;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

import com.civiconegocios.app.VO.FiltroOfertasVO;
import com.civiconegocios.app.VO.OfertasRedimidasVO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RedeemedDealsDataSerializer extends AsyncTaskLoader<ArrayList<List<?>>> {

    List<OfertasRedimidasVO> listBalanceTemp;
    List<FiltroOfertasVO> arrayFiltro;
    private String data;

    public RedeemedDealsDataSerializer(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setArrayFiltro(List<FiltroOfertasVO> arrayFiltro) {
        this.arrayFiltro = arrayFiltro;
    }

    public void setListBalanceTemp(List<OfertasRedimidasVO> listBalanceTemp) {
        this.listBalanceTemp = listBalanceTemp;
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
    }

    @Override
    public ArrayList<List<?>> loadInBackground() {

        ArrayList<List<?>> output = new ArrayList<List<?>>();

        try {
            JSONObject catObj = new JSONObject(data);

            List<OfertasRedimidasVO> hoyList = new ArrayList<>();
            JSONArray arrayOffers = catObj.getJSONArray("offer_coupons");
            if (arrayOffers.length() > 0) {
                for (int i = 0; i < arrayOffers.length(); i++) {
                    JSONObject teamObj = arrayOffers.getJSONObject(i);
                    JSONArray arrayComments = teamObj.getJSONArray("comments");
                    String firstComment = "";
                    String secondsComment = "";
                    if (arrayComments.length() > 0) {
                        if (arrayComments.length() > 1) {
                            firstComment = arrayComments.getJSONObject(0).getString("text");
                            secondsComment = arrayComments.getJSONObject(1).getString("text");
                        } else {
                            firstComment = arrayComments.getJSONObject(0).getString("text");
                        }
                    }

                    OfertasRedimidasVO RedeemedDeal = new OfertasRedimidasVO(teamObj.getString("id"), teamObj.getString("status"),
                            teamObj.getString("redemption_date"), teamObj.getString("offer_name"), teamObj.getInt("price") + "", teamObj.getInt("discount") + "",
                            teamObj.getString("user_name"), teamObj.getString("document_type"), teamObj.getString("document_number"), teamObj.getString("mobile_phone"), teamObj.getString("code"),
                            teamObj.getString("user_email"), firstComment, secondsComment);

                    hoyList.add(RedeemedDeal);
                }

                listBalanceTemp.addAll(hoyList);

                List<FiltroOfertasVO> arrayFiiltroTemp = new ArrayList<>();
                JSONArray arrayOffersFiltro = catObj.getJSONArray("offers");
                if (arrayOffersFiltro.length() > 0) {
                    for (int a = 0; a < arrayOffersFiltro.length(); a++) {
                        JSONObject teamObj = arrayOffersFiltro.getJSONObject(a);
                        FiltroOfertasVO categoriasInfo = new FiltroOfertasVO(teamObj.getString("id"), teamObj.getString("name"), false);
//                        categoriasInfo.setId(teamObj.getString("id"));
//                        categoriasInfo.setName(teamObj.getString("name"));
//                        categoriasInfo.setCheckFiltro(false);
                        arrayFiiltroTemp.add(categoriasInfo);
                    }
                    arrayFiltro.clear();
                    arrayFiltro.addAll(arrayFiiltroTemp);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (listBalanceTemp != null) {
            output.add(listBalanceTemp);
        }
        if (arrayFiltro != null) {
            output.add(arrayFiltro);
        }

        return output;
    }

    @Override
    public void deliverResult(ArrayList<List<?>> data) {
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void onCanceled(@Nullable ArrayList<List<?>> data) {
        deliverResult(null);
        super.onCanceled(data);
    }

    @Override
    protected boolean onCancelLoad() {
        return super.onCancelLoad();
    }
}
