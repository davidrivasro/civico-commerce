package com.civiconegocios.app.services;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

import com.civiconegocios.app.VO.BalanceVO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BalanceDataSerializer extends AsyncTaskLoader<List<BalanceVO>> {

    private Bundle args;

    public BalanceDataSerializer(Context context, Bundle args) {
        super(context);
        this.args = args;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<BalanceVO> loadInBackground() {
        List<BalanceVO> balanceList = new ArrayList<BalanceVO>();
        try {

            String data = this.args.getString("data");
            if (data != null && "".equals(data)) {
                return null;
            }

            JSONObject catObj = new JSONObject(this.args.getString("data"));

            JSONObject pending_to_transfer = catObj.getJSONObject("pending_to_transfer");
            JSONObject total_transfered = catObj.getJSONObject("total_transfered");

            BalanceVO objetoBalance = new BalanceVO();

            objetoBalance.setPending_to_transfer_amount(pending_to_transfer.getString("total_amount"));
            objetoBalance.setPending_to_transfer_currency(pending_to_transfer.getString("currency"));
            objetoBalance.setPending_to_transfer_reteiva(pending_to_transfer.getString("reteiva"));
            objetoBalance.setPending_to_transfer_reterenta(pending_to_transfer.getString("reterenta"));
            objetoBalance.setPending_to_transfer_reteica(pending_to_transfer.getString("reteica"));
            objetoBalance.setPending_to_transfer_total_transfer(pending_to_transfer.getString("total_transfer"));
            objetoBalance.setPending_to_transfer_discounts(pending_to_transfer.getString("discounts"));

            // no cambia
            objetoBalance.setTotal_transfered_amount(total_transfered.getString("amount"));
            objetoBalance.setTotal_transfered_currency(total_transfered.getString("currency"));

            List<BalanceVO> last_transfersList = new ArrayList<BalanceVO>();

            JSONArray last_transfers = catObj.getJSONArray("last_transfers");
            for (int i = 0; i < last_transfers.length(); i++) {
                BalanceVO objetoLast_transfers = new BalanceVO();
                JSONObject teamObj = last_transfers.getJSONObject(i);
                objetoLast_transfers.setAmount(teamObj.getString("total_amount"));
                objetoLast_transfers.setCurrency(teamObj.getString("currency"));
                objetoLast_transfers.setUpdated_at(teamObj.getString("updated_at"));
                objetoLast_transfers.setReteiva(teamObj.getString("reteiva") + "");
                objetoLast_transfers.setReterenta(teamObj.getString("reterenta") + "");
                objetoLast_transfers.setReteica(teamObj.getString("reteica") + "");
                objetoLast_transfers.setTotal_transfer(teamObj.getString("total_transfer") + "");
                objetoLast_transfers.setDiscounts(teamObj.getString("discounts") + "");
                last_transfersList.add(objetoLast_transfers);
            }
            objetoBalance.setListBalanceTemp(last_transfersList);
            balanceList.add(objetoBalance);
        } catch (JSONException e) {
            e.printStackTrace();
            cancelLoad();
        }

        return balanceList;
    }

    @Override
    public void deliverResult(List<BalanceVO> data) {
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void onCanceled(@Nullable List<BalanceVO> data) {
        deliverResult(null);
        super.onCanceled(data);
    }

    @Override
    protected boolean onCancelLoad() {
        return super.onCancelLoad();
    }
}
