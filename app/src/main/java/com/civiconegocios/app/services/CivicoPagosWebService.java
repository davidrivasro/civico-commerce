package com.civiconegocios.app.services;

import com.civiconegocios.app.BuildConfig;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CivicoPagosWebService {

    String CUSTOMER_TRANSACTIONS = "customers/transactions";
    String CUSTOMER_MPOS_TRANSACTIONS = "customers/transactions_mpos/{id_transaction}";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.RETROFIT_PAGOS_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Headers({
            "Content-Type: application/json",
            "Cache-Control: no-cache"
    })
    @POST(CUSTOMER_TRANSACTIONS)
    Call<JSONObject> customerTransactions(@Header("Authorization") String authorization, @Body String body);

    @PUT(CUSTOMER_MPOS_TRANSACTIONS)
    Call<ResponseBody> mposTransaction(
            @Path("id_transaction") String idTransaction,
            @HeaderMap Map<String, String> headers,
            @Body RequestBody params
    );
}
