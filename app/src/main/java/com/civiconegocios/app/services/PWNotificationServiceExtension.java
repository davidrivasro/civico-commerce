package com.civiconegocios.app.services;

import android.content.Intent;
import android.os.Handler;
import androidx.annotation.Keep;
import androidx.annotation.MainThread;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.SplashActivity;
import com.pushwoosh.Pushwoosh;
import com.pushwoosh.notification.NotificationServiceExtension;
import com.pushwoosh.notification.PushMessage;

import org.json.JSONObject;

@Keep
public class PWNotificationServiceExtension extends NotificationServiceExtension {

    public static final String LTAG = "PWNotificationService";

    @Override
    public boolean onMessageReceived(final PushMessage message) {

        // automatic foreground push handling
        if (isAppOnForeground()) {
            Handler mainHandler = new Handler(getApplicationContext().getMainLooper());
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    handlePush(message);
                }
            });

            // this indicates that notification should not be displayed
            return true;
        }
        return false;
    }

    @Override
    protected void startActivityForPushMessage(PushMessage message) {

        String type = "";
        String id_message = "";

        try {

            JSONObject pushData = message.toJson().getJSONObject("userdata");
            type = pushData.getString("type");
            id_message = pushData.getString("id_message");

        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent launchIntent = new Intent(getApplicationContext(), SplashActivity.class);
        launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        launchIntent.putExtra(Pushwoosh.PUSH_RECEIVE_EVENT, message.toJson().toString());
        launchIntent.putExtra("typePush", type);

        if (type.equalsIgnoreCase("payment_update")) {
            ((TrackingOps) getApplicationContext()).setJsonProcesoPago(message.toJson().toString());
        }

        if (!id_message.equals("")) {
            launchIntent.putExtra("id_message", id_message);
        }

        getApplicationContext().startActivity(launchIntent);

    }

    @MainThread
    private void handlePush(PushMessage message) {
        startActivityForPushMessage(message);
    }
}