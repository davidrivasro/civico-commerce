package com.civiconegocios.app.services;


import android.content.Context;
import android.os.Bundle;
import androidx.loader.content.AsyncTaskLoader;

import com.civiconegocios.app.VO.TransaccionesVO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyTransactionsDataSerializer extends AsyncTaskLoader<ArrayList<List<TransaccionesVO>>> {

    private Bundle args;
    private List<TransaccionesVO> hoyListTemp = new ArrayList<TransaccionesVO>();
    private List<TransaccionesVO> ayerListTemp = new ArrayList<TransaccionesVO>();
    private List<TransaccionesVO> lastSevenTemp = new ArrayList<TransaccionesVO>();
    private List<TransaccionesVO> lastOnesTemp = new ArrayList<TransaccionesVO>();

    public MyTransactionsDataSerializer(Context context, Bundle args) {
        super(context);
        this.args = args;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public ArrayList<List<TransaccionesVO>> loadInBackground() {

        ArrayList<List<TransaccionesVO>> arrayLists = new ArrayList<List<TransaccionesVO>>();

        try {
            List<TransaccionesVO> hoyList = new ArrayList<TransaccionesVO>();
            List<TransaccionesVO> ayerList = new ArrayList<TransaccionesVO>();
            List<TransaccionesVO> lastSeven = new ArrayList<TransaccionesVO>();
            List<TransaccionesVO> lastOnes = new ArrayList<TransaccionesVO>();

            JSONObject catObj = new JSONObject(this.args.getString("data"));
            JSONObject todayArray = catObj.getJSONObject("today");

            JSONArray todayTrans = todayArray.getJSONArray("transactions");
            if (todayTrans.length() > 0) {
                for (int i = 0; i < todayTrans.length(); i++) {
                    TransaccionesVO categoriasInfo = createTransactionObject(todayTrans.getJSONObject(i));
                    hoyList.add(categoriasInfo);
                }
                hoyListTemp.addAll(hoyList);
            }

            JSONObject Objyesterday = catObj.getJSONObject("yesterday");
            JSONArray yesterdayTrans = Objyesterday.getJSONArray("transactions");
            if (yesterdayTrans.length() > 0) {

                for (int i = 0; i < yesterdayTrans.length(); i++) {
                    TransaccionesVO categoriasInfoyes = createTransactionObject(yesterdayTrans.getJSONObject(i));
                    ayerList.add(categoriasInfoyes);
                }
                ayerListTemp.addAll(ayerList);
            }

            JSONObject lastSeventObj = catObj.getJSONObject("last-seven");
            JSONArray lastSevenTrans = lastSeventObj.getJSONArray("transactions");
            if (lastSevenTrans.length() > 0) {

                for (int i = 0; i < lastSevenTrans.length(); i++) {
                    TransaccionesVO categoriasInfoLast = createTransactionObject(lastSevenTrans.getJSONObject(i));
                    lastSeven.add(categoriasInfoLast);
                }
                lastSevenTemp.addAll(lastSeven);
            }

            JSONObject lastTransactionsObj = catObj.getJSONObject("last-transactions");
            JSONArray lastTrans = lastTransactionsObj.getJSONArray("transactions");
            if (lastTrans.length() > 0) {

                for (int i = 0; i < lastTrans.length(); i++) {
                    TransaccionesVO categoriasInfoLast = createTransactionObject(lastTrans.getJSONObject(i));
                    lastOnes.add(categoriasInfoLast);
                }
                lastOnesTemp.addAll(lastOnes);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        arrayLists.add(hoyListTemp);
        arrayLists.add(ayerListTemp);
        arrayLists.add(lastSevenTemp);
        arrayLists.add(lastOnesTemp);

        return arrayLists;
    }

    @Override
    public void deliverResult(ArrayList<List<TransaccionesVO>> data) {
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
    }

    @Override
    public void onCanceled(ArrayList<List<TransaccionesVO>> data) {
        deliverResult(null);
        super.onCanceled(data);
    }

    @Override
    protected boolean onCancelLoad() {
        return super.onCancelLoad();
    }

    protected TransaccionesVO createTransactionObject(JSONObject teamObj) {
        TransaccionesVO categoriasInfoLast = new TransaccionesVO();
        try {
            categoriasInfoLast.setId(teamObj.getString("id"));
            categoriasInfoLast.setAmount(teamObj.getInt("amount") + ""); // valor total
            categoriasInfoLast.setAmountBase(teamObj.getInt("amount_base") + ""); // valor Base
            categoriasInfoLast.setTax_amount(teamObj.getInt("tax_amount") + ""); // iva
            categoriasInfoLast.setIac_amount(teamObj.getInt("iac_amount") + ""); // impoconsumo
            categoriasInfoLast.setTip_amount(teamObj.getInt("tip_amount") + ""); // propina
            categoriasInfoLast.setAuthorization_code(teamObj.getString("authorization_code"));
            categoriasInfoLast.setCurrency(teamObj.getString("currency"));
            categoriasInfoLast.setCreated_at(teamObj.getString("created_at"));
            categoriasInfoLast.setStatus(teamObj.getInt("status") + "");
            categoriasInfoLast.setDescription(teamObj.getString("description"));
            categoriasInfoLast.setOrigin(teamObj.getString("origin"));
            if (!teamObj.isNull("transaction_id")) {
                categoriasInfoLast.setTransaction_id(teamObj.getString("transaction_id") + "");
            }
            categoriasInfoLast.setUpdated_at(teamObj.getString("updated_at"));
            categoriasInfoLast.setOrigin(teamObj.getString("origin"));

            if (teamObj.getString("trazability_id") == null) {
                categoriasInfoLast.setTrazability_id(teamObj.getString("trazability_id"));
            } else {
                categoriasInfoLast.setTrazability_id("");
            }
            if (teamObj.getString("reason") != null) {
                categoriasInfoLast.setReason(teamObj.getString("reason"));
            } else {
                categoriasInfoLast.setReason("");
            }
            JSONObject userObj = teamObj.getJSONObject("user");
            categoriasInfoLast.setPinUser(userObj.getString("pin"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return categoriasInfoLast;
    }
}

