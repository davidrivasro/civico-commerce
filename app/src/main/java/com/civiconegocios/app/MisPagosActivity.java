package com.civiconegocios.app;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.net.ParseException;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.TransaccionesVO;
import com.civiconegocios.app.item.EntryAdapter;
import com.civiconegocios.app.item.EntryItem;
import com.civiconegocios.app.item.Item;
import com.civiconegocios.app.item.SectionItem;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.MyTransactionsDataSerializer;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MisPagosActivity extends BaseActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks {

    private static final String LBL_TODAY = "lbl_today";
    private static final String LBL_YESTERDAY = "lbl_yesterday";
    private static final String LBL_LAST_SEVEN_DAYS = "lbl_last_seven_days";
    private static final String LBL_LATEST_TRANSACTIONS = "lbl_latest_transactions";
    private static final String TOOLBAR_MENU_TRANSACTIONS = "toolbar_menu_transactions";
    private static final String FILTER = "filter";
    private static final String LBL_BTN_SEND_REPORT = "lbl_btn_send_report";
    private static final String LBL_GENERATE_REPORT = "lbl_generate_report";
    private static final String TODAS_FECHAS = "todas_fechas";
    private static final String FALLO_REPORTE = "fallo_reporte";
    private static final String REPORT = "lbl_btn_send_report";
    private static final String PAYMENTS_NOT_FOUND = "payments_not_found";
    private static final String BEGIN_PAYMENTS = "begin_payments";
    private static final String APPROVED_ONES = "approved_ones";
    private static final String CANCELED_ONES = "canceled_ones";
    private static final String ONHOLD_ONES = "onhold_ones";
    private static final String ALL = "all";
    private static final String FROM = "from";
    private static final String TO = "to";

    //    Loaders IDS
    private final int DATA_REQUEST_LOADER = 10;
    private final int DATA_SERIALIZER_LOADER = 11;
    private final int REPORT_LOADER = 12;
    //    UI
    Toolbar defaultToolbar;
    LinearLayout lnBtnFiltrar;
    LinearLayout lnBtnEnviarReporte;
    LinearLayout lnContFiltros;
    LinearLayout lnContReporte;
    int flagFiltro;
    int flagReporte;
    RecyclerView listFiltros;
    ArrayList<Item> items = new ArrayList<Item>();
    // creamos tres tipos de array
    List<TransaccionesVO> hoyList;
    List<TransaccionesVO> ayerList;
    List<TransaccionesVO> lastSeven, lastOnes;
    EntryAdapter adapter;
    SwipeRefreshLayout swipeRefresh;
    Utils util = new Utils();
    LinearLayout lnDesdeReporte;
    LinearLayout lnHastaReporte;
    LinearLayout lnBtnEnviarReprte;
    TextView txtTitFiltrar, txtFiltroAprobadas, txtFiltroCanceladas, txtFiltroEspera,
            txtFiltroTodos, txtTitReporte, txtTitDesde, txtTitHasta, toolbar_title, txtReportelnBtnEnviarReprte,
            txtAunPagos, txtEmpiezarealizar;
    boolean desdeBol = false;
    boolean hastaBol = false;
    String strDesde = "";
    String strHasta = "";
    LinearLayout lnNotienesPagos;
    RelativeLayout relMistransacciones;
    //    Utils
    boolean doubleBackToExitPressedOnce = false;
    String serviceEndpoint = "/customers/transactions";
    String reportsEndpoint = "/customers/transactions/report?";
    String transactionsData;
    TrackingOps trackingOps;
    User user;
    private DatePicker datePicker;
    private TextView dateView;
    private int year, month, day;
    private TextView txDesde;
    private TextView txHasta;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            // arg1 = year
            // arg2 = month
            // arg3 = day
            arg2 = arg2 + 1;
            if (hastaBol) {
                strHasta = arg1 + "-" + arg2 + "-" + arg3;
                txHasta.setText(StringToDate(arg1 + "-" + arg2 + "-" + arg3));
            } else if (desdeBol) {
                strDesde = arg1 + "-" + arg2 + "-" + arg3;
                txDesde.setText(StringToDate(arg1 + "-" + arg2 + "-" + arg3));
            }
        }
    };

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mis_pagos_activity);

        initDrawer();
        user = ((TrackingOps) getApplication()).getUser();
        trackingOps = (TrackingOps) getApplication();

        lnNotienesPagos = findViewById(R.id.lnNotienesPagos);
        relMistransacciones = findViewById(R.id.relMistransacciones);

        // Picasso.with(getApplicationContext()).load(R.drawable.mascotas).transform(new CircleTransform()).into(imgMenuLateralMisPagos);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_TRANSACTIONS));
        lnBtnFiltrar = findViewById(R.id.lnBtnFiltrar);
        lnDesdeReporte = findViewById(R.id.lnDesdeReporte);
        lnHastaReporte = findViewById(R.id.lnHastaReporte);
        lnBtnEnviarReprte = findViewById(R.id.lnBtnEnviarReprte);
        lnDesdeReporte.setOnClickListener(this);
        lnHastaReporte.setOnClickListener(this);
        lnBtnEnviarReprte.setOnClickListener(this);

        lnBtnEnviarReporte = findViewById(R.id.lnBtnEnviarReporte);
        lnContFiltros = findViewById(R.id.lnContFiltros);
        lnContReporte = findViewById(R.id.lnContReporte);
        lnBtnFiltrar.setOnClickListener(this);
        lnBtnEnviarReporte.setOnClickListener(this);
        flagFiltro = 0;
        flagReporte = 0; //

        txtTitFiltrar = findViewById(R.id.txtTitFiltrar);
        txtTitFiltrar.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(FILTER).toUpperCase());
        txtTitFiltrar.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_filtrar), null, null, null);
        //txtTitFiltrar.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtTitReporte = findViewById(R.id.txtTitReporte);
        txtTitReporte.setText(trackingOps.getmFirebaseRemoteConfig().getString(REPORT).toUpperCase());
        //txtTitReporte.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));

        txtTitDesde = findViewById(R.id.txtTitDesde);
        txtTitDesde.setText(trackingOps.getmFirebaseRemoteConfig().getString(FROM));
        //txtTitDesde.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtTitHasta = findViewById(R.id.txtTitHasta);
        txtTitHasta.setText(trackingOps.getmFirebaseRemoteConfig().getString(TO));
        //txtTitHasta.setTypeface(util.fuenteSubTituHint(getApplicationContext()));

        txDesde = findViewById(R.id.txDesde);
        txDesde.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txHasta = findViewById(R.id.txHasta);
        txHasta.setTypeface(util.fuenteSubTituHint(getApplicationContext()));

        txtAunPagos = findViewById(R.id.txtAunPagos);
        txtAunPagos.setText(trackingOps.getmFirebaseRemoteConfig().getString(PAYMENTS_NOT_FOUND));
        txtEmpiezarealizar = findViewById(R.id.txtEmpiezarealizar);
        txtEmpiezarealizar.setText(trackingOps.getmFirebaseRemoteConfig().getString(BEGIN_PAYMENTS));

        txtFiltroAprobadas = findViewById(R.id.txtFiltroAprobadas);
        txtFiltroAprobadas.setText("  " + trackingOps.getmFirebaseRemoteConfig().getString(APPROVED_ONES));
        //txtFiltroAprobadas.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtFiltroCanceladas = findViewById(R.id.txtFiltroCanceladas);
        txtFiltroCanceladas.setText("  " + trackingOps.getmFirebaseRemoteConfig().getString(CANCELED_ONES));
        //txtFiltroCanceladas.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtFiltroEspera = findViewById(R.id.txtFiltroEspera);
        txtFiltroEspera.setText("  " + trackingOps.getmFirebaseRemoteConfig().getString(ONHOLD_ONES));
        //txtFiltroEspera.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtFiltroTodos = findViewById(R.id.txtFiltroTodos);
        txtFiltroTodos.setText("  " + trackingOps.getmFirebaseRemoteConfig().getString(ALL));
        //txtFiltroTodos.setTypeface(util.fuenteSubTituHint(getApplicationContext()));

        txtReportelnBtnEnviarReprte = findViewById(R.id.txtReportelnBtnEnviarReprte);
        txtReportelnBtnEnviarReprte.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_GENERATE_REPORT).toUpperCase());

        txtFiltroAprobadas.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_aprobado), null, null, null);
        txtFiltroAprobadas.setOnClickListener(this);
        txtFiltroCanceladas.setOnClickListener(this);
        txtFiltroCanceladas.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_cancelado), null, null, null);
        txtFiltroEspera.setOnClickListener(this);
        txtFiltroEspera.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_espera), null, null, null);
        txtFiltroTodos.setOnClickListener(this);
        txtFiltroTodos.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_filtrar), null, null, null);
        swipeRefresh = findViewById(R.id.swipeRefresh);


        listFiltros = findViewById(R.id.listFiltros);
        adapter = new EntryAdapter(this, items);
        listFiltros.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listFiltros.setLayoutManager(linearLayoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        listFiltros.addItemDecoration(itemDecorator);
        listFiltros.setHasFixedSize(true);

        hoyList = new ArrayList<TransaccionesVO>();
        ayerList = new ArrayList<TransaccionesVO>();
        lastSeven = new ArrayList<TransaccionesVO>();
        lastOnes = new ArrayList<TransaccionesVO>();


        if (util.haveInternet(getApplicationContext())) {
            makeDataRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(false);
            }
        });

        trackingOps.registerScreenName("Mis transacciones | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    public void llenarList(List<TransaccionesVO> hoyListTemp, List<TransaccionesVO> ayerListTemp, List<TransaccionesVO> lastSevenTemp, List<TransaccionesVO> lastOnesTemp) {
        hoyList = hoyListTemp;
        ayerList = ayerListTemp;
        lastSeven = lastSevenTemp;
        lastOnes = lastOnesTemp;

        if (hoyListTemp.isEmpty() && ayerListTemp.isEmpty() && lastSevenTemp.isEmpty() && lastOnesTemp.isEmpty()) {
            lnNotienesPagos.setVisibility(View.VISIBLE);
            relMistransacciones.setVisibility(View.GONE);
        } else {
            lnNotienesPagos.setVisibility(View.GONE);
            relMistransacciones.setVisibility(View.VISIBLE);
        }

        if (hoyListTemp.size() != 0) {
            items.add(new SectionItem(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TODAY)));
            for (int h = 0; h < hoyListTemp.size(); h++) {
                TransaccionesVO hoyObj = hoyListTemp.get(h);
                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
            }
        }
        if (ayerListTemp.size() != 0) {
            items.add(new SectionItem(trackingOps.getmFirebaseRemoteConfig().getString(LBL_YESTERDAY)));
            for (int h = 0; h < ayerListTemp.size(); h++) {
                TransaccionesVO hoyObj = ayerListTemp.get(h);
                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
            }
        }
        if (lastSevenTemp.size() != 0) {
            items.add(new SectionItem(trackingOps.getmFirebaseRemoteConfig().getString(LBL_LAST_SEVEN_DAYS)));
            for (int h = 0; h < lastSevenTemp.size(); h++) {
                TransaccionesVO hoyObj = lastSevenTemp.get(h);
                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
            }
        }

        if (lastOnesTemp.size() != 0) {
            items.add(new SectionItem(trackingOps.getmFirebaseRemoteConfig().getString(LBL_LATEST_TRANSACTIONS)));
            for (int h = 0; h < lastOnesTemp.size(); h++) {
                TransaccionesVO hoyObj = lastOnesTemp.get(h);
                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
            }
        }
        adapter.notifyDataSetChanged();
        cerrarRefesh();
    }

    public void llenarListFiltros(List<TransaccionesVO> hoyListTemp, List<TransaccionesVO> ayerListTemp, List<TransaccionesVO> lastSevenTemp, List<TransaccionesVO> lastOnesTemp, String filtro) {

        items.clear();
        adapter.notifyDataSetChanged();

        if (hoyListTemp.size() != 0) {
            items.add(new SectionItem(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TODAY)));
            for (int h = 0; h < hoyListTemp.size(); h++) {
                TransaccionesVO hoyObj = hoyListTemp.get(h);
                switch (filtro) {
                    case "0":  // TRANSACCIONES EN ESPERA
                        if (hoyObj.getStatus().equals("0") || hoyObj.getStatus().equals("1")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;

                    case "2":  // TRANSACCIONES APROBADAS
                        if (hoyObj.getStatus().equals("2") || hoyObj.getStatus().equals("7")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;

                    case "3":  // TRANSACCIONES CANCELADAS
                        if (hoyObj.getStatus().equals("3") || hoyObj.getStatus().equals("4") || hoyObj.getStatus().equals("5") || hoyObj.getStatus().equals("6") || hoyObj.getStatus().equals("8") || hoyObj.getStatus().equals("9")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;
                }
            }
        }

        if (ayerListTemp.size() != 0) {
            items.add(new SectionItem(trackingOps.getmFirebaseRemoteConfig().getString(LBL_YESTERDAY)));
            for (int h = 0; h < ayerListTemp.size(); h++) {
                TransaccionesVO hoyObj = ayerListTemp.get(h);
                switch (filtro) {
                    case "0":  // TRANSACCIONES EN ESPERA
                        if (hoyObj.getStatus().equals("0") || hoyObj.getStatus().equals("1")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;

                    case "2":  // TRANSACCIONES APROBADAS
                        if (hoyObj.getStatus().equals("2") || hoyObj.getStatus().equals("7")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;

                    case "3":  // TRANSACCIONES CANCELADAS
                        if (hoyObj.getStatus().equals("3") || hoyObj.getStatus().equals("4") || hoyObj.getStatus().equals("5") || hoyObj.getStatus().equals("6") || hoyObj.getStatus().equals("8") || hoyObj.getStatus().equals("9")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;
                }
            }
        }

        if (lastSevenTemp.size() != 0) {
            items.add(new SectionItem(trackingOps.getmFirebaseRemoteConfig().getString(LBL_LAST_SEVEN_DAYS)));
            for (int h = 0; h < lastSevenTemp.size(); h++) {
                TransaccionesVO hoyObj = lastSevenTemp.get(h);
                switch (filtro) {
                    case "0":  // TRANSACCIONES EN ESPERA
                        if (hoyObj.getStatus().equals("0") || hoyObj.getStatus().equals("1")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;

                    case "2":  // TRANSACCIONES APROBADAS
                        if (hoyObj.getStatus().equals("2") || hoyObj.getStatus().equals("7")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;

                    case "3":  // TRANSACCIONES CANCELADAS
                        if (hoyObj.getStatus().equals("3") || hoyObj.getStatus().equals("4") || hoyObj.getStatus().equals("5") || hoyObj.getStatus().equals("6") || hoyObj.getStatus().equals("8") || hoyObj.getStatus().equals("9")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;
                }
            }
        }

        if (lastOnesTemp.size() != 0) {
            items.add(new SectionItem(trackingOps.getmFirebaseRemoteConfig().getString(LBL_LATEST_TRANSACTIONS)));
            for (int h = 0; h < lastOnesTemp.size(); h++) {
                TransaccionesVO hoyObj = lastOnesTemp.get(h);
                switch (filtro) {
                    case "0":  // TRANSACCIONES EN ESPERA
                        if (hoyObj.getStatus().equals("0") || hoyObj.getStatus().equals("1")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;

                    case "2":  // TRANSACCIONES APROBADAS
                        if (hoyObj.getStatus().equals("2") || hoyObj.getStatus().equals("7")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;

                    case "3":  // TRANSACCIONES CANCELADAS
                        if (hoyObj.getStatus().equals("3") || hoyObj.getStatus().equals("4") || hoyObj.getStatus().equals("5") || hoyObj.getStatus().equals("6") || hoyObj.getStatus().equals("8") || hoyObj.getStatus().equals("9")) {
                            if (validarTransaccionId(hoyObj.getTransaction_id(), hoyObj.getAmount()))
                                items.add(new EntryItem(hoyObj.getAmount(), hoyObj.getStatus(), hoyObj.getCreated_at().substring(hoyObj.getCreated_at().indexOf("T") + 1, hoyObj.getCreated_at().length()), hoyObj.getCreated_at(), hoyObj.getPinUser(), hoyObj.getTax_amount(), hoyObj.getAuthorization_code(), hoyObj.getTransaction_id(), hoyObj.getAmountBase(), hoyObj.getIac_amount(), hoyObj.getTip_amount(), hoyObj.getOrigin(), hoyObj.getReason(), hoyObj.getDescription()));
                        }
                        break;
                }
            }
        }

        adapter.notifyDataSetChanged();
    }

    private boolean validarTransaccionId(String transaction_id, String amount) {
        boolean flag = true;
        for (int i = 0; i < items.size(); i++) {
            try {
                EntryItem item = (EntryItem) items.get(i);
                try {
                    if (item.transId.equalsIgnoreCase(transaction_id))
                        flag = false;
                } catch (NullPointerException ignore) {
                    if (item.title.equalsIgnoreCase(amount))
                        flag = false;
                }
            } catch (ClassCastException ignore) {
            }
        }
        return flag;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnBtnFiltrar:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_TRANSACTIONS), trackingOps.getmFirebaseRemoteConfig().getString(FILTER));
                if (flagFiltro == 0) {
                    lnContFiltros.setVisibility(View.VISIBLE);
                    lnContReporte.setVisibility(View.GONE);
                    flagFiltro = 1;
                    txtTitFiltrar.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_filtrar1), null, null, null);
                } else {
                    lnContFiltros.setVisibility(View.GONE);
                    lnContReporte.setVisibility(View.GONE);
                    flagFiltro = 0;
                    txtTitFiltrar.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_filtrar), null, null, null);
                }

                break;
            case R.id.lnBtnEnviarReporte: // muestra o no el popup para seleccionar las fechas
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_TRANSACTIONS), trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_SEND_REPORT));
                if (flagReporte == 0) {
                    lnContFiltros.setVisibility(View.GONE);
                    lnContReporte.setVisibility(View.VISIBLE);
                    flagReporte = 1;
                } else {
                    lnContFiltros.setVisibility(View.GONE);
                    lnContReporte.setVisibility(View.GONE);
                    flagReporte = 0;
                }
                break;
            case R.id.txtFiltroAprobadas:
                llenarListFiltros(hoyList, ayerList, lastSeven, lastOnes, "2");
                lnContFiltros.setVisibility(View.GONE);
                flagFiltro = 0;
                txtTitFiltrar.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_filtrar), null, null, null);
                break;
            case R.id.txtFiltroCanceladas:
                llenarListFiltros(hoyList, ayerList, lastSeven, lastOnes, "3");
                lnContFiltros.setVisibility(View.GONE);
                flagFiltro = 0;
                txtTitFiltrar.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_filtrar), null, null, null);
                break;
            case R.id.txtFiltroEspera:
                llenarListFiltros(hoyList, ayerList, lastSeven, lastOnes, "0");
                lnContFiltros.setVisibility(View.GONE);
                flagFiltro = 0;
                txtTitFiltrar.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_filtrar), null, null, null);
                break;
            case R.id.lnDesdeReporte:
                showDialog(999);
                desdeBol = true;
                hastaBol = false;
                break;
            case R.id.lnHastaReporte:
                showDialog(999);
                desdeBol = false;
                hastaBol = true;
                break;
            case R.id.lnBtnEnviarReprte:
                if (txDesde.getText().toString().length() > 0 && txHasta.getText().toString().length() > 0) {
                    lnContFiltros.setVisibility(View.GONE);
                    lnContReporte.setVisibility(View.GONE);
                    flagReporte = 0;
                    if (util.haveInternet(getApplicationContext())) {
                        trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_TRANSACTIONS), trackingOps.getmFirebaseRemoteConfig().getString(LBL_GENERATE_REPORT));
                        makeReportRequest();
                    } else {
                        util.mensajeNoInternet(getApplicationContext());
                    }
                } else {
                    Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(TODAS_FECHAS),
                            Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.txtFiltroTodos:
                items.clear();
                adapter.notifyDataSetChanged();
                llenarList(hoyList, ayerList, lastSeven, lastOnes);
                lnContFiltros.setVisibility(View.GONE);
                flagFiltro = 0;
                txtTitFiltrar.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_pagos_ico_filtrar), null, null, null);
                break;
            default:
                break;
        }
    }

    public void cerrarRefesh() {
        if (swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(false);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            Calendar calendar = Calendar.getInstance();
            int thisYear = calendar.get(Calendar.YEAR);
            int thisMonth = calendar.get(Calendar.MONTH);
            int thisDay = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(this, myDateListener, thisYear, thisMonth + 1, thisDay);
        }
        return null;
    }

    public void reporteExitoso(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje,
                Toast.LENGTH_SHORT).show();
    }

    public void reporteFallido() {
        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(FALLO_REPORTE),
                Toast.LENGTH_SHORT).show();
    }

    public String StringToDate(String fecha) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try {
            date = format.parse(fecha);

        } catch (ParseException e) {

            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return DateToString(date);
    }

    public String DateToString(java.util.Date date) {

        SimpleDateFormat dateformat = new SimpleDateFormat("EE yyyy-MM-dd");
        String datetime = "";
        try {
            datetime = dateformat.format(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return datetime;
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, this.serviceEndpoint);
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(DATA_REQUEST_LOADER, queryBundle, this);
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    public void makeReportRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, reportsEndpoint + "date_from=" + strDesde.replace(" ", "") + "&date_to=" + strHasta.replace(" ", ""));
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(REPORT_LOADER);

            if (loader == null) {
                loaderManager.initLoader(REPORT_LOADER, queryBundle, this);
            }
//            else {
//                loaderManager.restartLoader(REPORT_LOADER, queryBundle, this);
//            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    public void makeDataSerialization() {
        Bundle queryBundle = new Bundle();
        queryBundle.putString("data", this.transactionsData);

        LoaderManager loaderManager = getSupportLoaderManager();
        Loader loader = loaderManager.getLoader(DATA_SERIALIZER_LOADER);

        if (loader == null) {
            loaderManager.initLoader(DATA_SERIALIZER_LOADER, queryBundle, this);
        } else {
            loaderManager.restartLoader(DATA_SERIALIZER_LOADER, queryBundle, this);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        switch (id) {
            case DATA_REQUEST_LOADER:
                return new RequestDefaultFactory(this, args);
            case DATA_SERIALIZER_LOADER:
                return new MyTransactionsDataSerializer(this, args);
            case REPORT_LOADER:
                return new RequestDefaultFactory(this, args);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        toggleProgressDialog(false);
        if (data == null) {
            util.alertErrorTimeOut(this);
        } else if (items.isEmpty()) {
            switch (loader.getId()) {
                case DATA_REQUEST_LOADER:
                    this.transactionsData = String.valueOf(data);
                    makeDataSerialization();
                    break;
                case DATA_SERIALIZER_LOADER:
                    ArrayList<List<TransaccionesVO>> arrayLists = (ArrayList<List<TransaccionesVO>>) data;
                    llenarList(arrayLists.get(0), arrayLists.get(1), arrayLists.get(2), arrayLists.get(3));
                    break;
                case REPORT_LOADER:
                    if (data instanceof String) {
                        try {
                            JSONObject catObj = new JSONObject(String.valueOf(data));
                            this.reporteExitoso(catObj.getString("userMessage"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            util.alertError500(this);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void onBackPressed() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if (bundle.getBoolean("mpos")) {
                return;
            }
        } else {
            super.onBackPressed();
        }

    }


    @Override
    public void onLoaderReset(Loader loader) {
        toggleProgressDialog(false);
    }
}
