package com.civiconegocios.app;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brayancamilovillateleon on 10/02/17.
 */

public class ActivityContactoPay extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<String> {

    private static final String INFORMATION_REQUEST = "information_request";
    private static final String COMMENTS_COMPLAINTS = "comments_complaints";
    private static final String SUPPORT_OFFERS = "support_offers";
    private static final String PAY_SUPPORT = "pay_support";
    private static final String MSJ_ENTER_INFORMATION = "msj_enter_information";
    private static final String MSJ_MAIL_NOT_VALID = "msj_mail_not_valid";
    private static final String MSJ_INFORMATION_SENT_CORRECTLY = "msj_information_sent_correctly";
    private static final String CONTACT_SUBTITLE = "contact_subtitle";
    private static final String FULL_NAME = "full_name";
    private static final String MAIL = "mail";
    private static final String MESSAGE = "message";
    private static final String LBL_BTN_SEND_MESSAGE = "lbl_btn_send_message";
    private static final String URL_CIVICO_CONTACT_US = "url_civico_contact_us";

    TextView txtSubtituloContacto;
    EditText editNombreContacto;
    EditText editCorreoContacto;
    EditText editMensajeContacto;
    TextView txtEnviarMensaje;
    LinearLayout lnBtnEnviarMensaje;
    Spinner spinnerTipoDoc;
    Utils util = new Utils();
    User user;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto_civico_pay);

        user = ((TrackingOps) getApplication()).getUser();
        trackingOps = (TrackingOps) getApplicationContext();

        txtSubtituloContacto = findViewById(R.id.txtSubtituloContacto);
        txtSubtituloContacto.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONTACT_SUBTITLE));
        editNombreContacto = findViewById(R.id.editNombreContacto);
        editNombreContacto.setHint(trackingOps.getmFirebaseRemoteConfig().getString(FULL_NAME));
        editCorreoContacto = findViewById(R.id.editCorreoContacto);
        editCorreoContacto.setHint(trackingOps.getmFirebaseRemoteConfig().getString(MAIL));
        editMensajeContacto = findViewById(R.id.editMensajeContacto);
        editMensajeContacto.setHint(trackingOps.getmFirebaseRemoteConfig().getString(MESSAGE));
        txtEnviarMensaje = findViewById(R.id.txtEnviarMensaje);
        txtEnviarMensaje.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_SEND_MESSAGE));
        lnBtnEnviarMensaje = findViewById(R.id.lnBtnEnviarMensaje);
        spinnerTipoDoc = findViewById(R.id.spinnerTipoDoc);

        editNombreContacto.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        editCorreoContacto.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        editMensajeContacto.setTypeface(util.fuenteSubTituHint(getApplicationContext()));

        editNombreContacto.setText(user.getFirstName() + " " + user.getLastName());
        editCorreoContacto.setText(user.getEmail());

        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(INFORMATION_REQUEST));
        spinnerArray.add(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(COMMENTS_COMPLAINTS));
        spinnerArray.add(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(SUPPORT_OFFERS));
        spinnerArray.add(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(PAY_SUPPORT));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item_dos, spinnerArray) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                // Typeface externalFont=Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTCom-Lt.ttf");
                ((TextView) v).setTypeface(util.fuenteSubTituHint(getApplicationContext()));

                return v;
            }


            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                //  Typeface externalFont=Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTCom-Lt.ttf");
                ((TextView) v).setTypeface(util.fuenteSubTituHint(getApplicationContext()));


                return v;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoDoc.setAdapter(adapter);

        lnBtnEnviarMensaje.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnBtnEnviarMensaje:
                if (Utils.isValidEmail(editCorreoContacto.getText().toString())) {
                    if (editNombreContacto.getText().toString().equals("") || editMensajeContacto.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(MSJ_ENTER_INFORMATION),
                                Toast.LENGTH_SHORT).show();
                    } else {
                        makeRequestOperation();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(MSJ_MAIL_NOT_VALID),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    public void makeRequestOperation() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();
        List<String> spinnerArrayEnviar = new ArrayList<String>();
        spinnerArrayEnviar.add("pay_request_information");
        spinnerArrayEnviar.add("pay_suggestions_requests");
        spinnerArrayEnviar.add("pay_offer_support");
        spinnerArrayEnviar.add("pay_support");
        int spinner_pos = spinnerTipoDoc.getSelectedItemPosition();

        try {
            body.put("category", spinnerArrayEnviar.get(spinner_pos));
            body.put("name", editNombreContacto.getText().toString());
            body.put("email", editCorreoContacto.getText().toString());
            body.put("message", editMensajeContacto.getText().toString());

            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, trackingOps.getmFirebaseRemoteConfig().getString(URL_CIVICO_CONTACT_US));
            queryBundle.putString(Constants.OPERATION_BODY, body.toString());

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(0);

            if (loader == null) {
                loaderManager.initLoader(0, queryBundle, this);
            } else {
                loaderManager.restartLoader(0, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        toggleProgressDialog(false);
        if (data == null) {
            util.alertErrorTimeOut(this);
        } else {
            try {
                JSONObject responseObject = new JSONObject(data);
                if (responseObject.getString("email").equals("sended")) {
                    Toast.makeText(this, ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(MSJ_INFORMATION_SENT_CORRECTLY),
                            Toast.LENGTH_LONG).show();
                } else {
                    util.alertError500(this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                util.alertErrorTimeOut(this);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }
}
