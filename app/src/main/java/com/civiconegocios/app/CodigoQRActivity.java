package com.civiconegocios.app;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.UserDataSerializer;
import com.civiconegocios.app.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class CodigoQRActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    private final String TAG = CodigoQRActivity.class.getSimpleName();

    private final int USER_DATA_REQUEST_LOADER = 11;

    private User user;
    private String urlUser = "/users/edit";
    private TrackingOps trackingOps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(null);
        setContentView(R.layout.activity_codigo_qr);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_flecha_atras_azul);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //https://pagosbeta.civico.com/api/v1/customers/5a3a963c9496f9a394000001/qrcode?size=300

        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();
        if (user != null && !user.isValid()) {
            user = null;
        }

        trackingOps.registerScreenName("QR | Cívico PAY Comercios | " + user.getAnalyticsCity());

        if(user.getCustomer() != null && user.getCustomer().getId() != null) {
            ImageView imageView4 = findViewById(R.id.imageView4);
            Picasso.with(this).load("https://pagosbeta.civico.com/api/v1/customers/" + user.getCustomer().getId() + "/qrcode?size=300").into(imageView4);
        } else {
            makeDataRequest();
        }
    }

    public void makeDataRequest() {
        Utils util = new Utils();
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, this.urlUser);
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(USER_DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(USER_DATA_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(USER_DATA_REQUEST_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (Exception e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    @NonNull
    @Override
    public Loader<String> onCreateLoader(int id, @Nullable Bundle args) {
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<String> loader, String data) {
        try {
            switch (loader.getId()) {
                case USER_DATA_REQUEST_LOADER:
                    if (data == null) {
                        (new Utils()).alertError500(this);
                    } else {
                        try {
                            (new UserDataSerializer()).processData(data, user.getAccessToken(), user.getPassword(), this);
                            ((TrackingOps) this.getApplication()).setUser();

                            ImageView imageView4 = findViewById(R.id.imageView4);
                            Picasso.with(this).load("https://pagosbeta.civico.com/api/v1/customers/" + user.getCustomer().getId() + "/qrcode?size=300").into(imageView4);

                        } catch (IOException e) {
                            e.printStackTrace();
                            (new Utils()).alertError500(this);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            (new Utils()).alertError500(this);
                        } catch (Exception e) {
                            e.printStackTrace();
                            (new Utils()).alertError500(this);
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            (new Utils()).alertError500(this);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<String> loader) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
