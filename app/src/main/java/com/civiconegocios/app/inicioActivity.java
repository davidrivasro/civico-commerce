package com.civiconegocios.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.OfertasRedimidasVO;
import com.civiconegocios.app.VO.SubMenuVO;
import com.civiconegocios.app.adapters.AdapterGridInicio;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.firebase.perf.metrics.AddTrace;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

public class inicioActivity extends BaseActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<String> {

    private static final String TOTAL_VALUE_EQUAL = "total_value_equal";
    private static final String NEXT = "next";
    private static final String OFFER_SAVED = "offer_saved";
    private static final String TRANSACTION_STATUS = "transaction_status";
    private static final String TRANSACTION = "transaction";
    private static final String NO_VALOR_MAXIMO = "no_valor_maximo";
    private static final String TAX = "tax";
    private static final String NO_VALOR_MINIMO = "no_valor_minimo";
    private static final String LBL_BTN_CONTINUAR = "lbl_btn_continuar";
    private static final String NO_VALOR = "no_valor";
    private static final String CONSUMPTION_TAX = "consumption_tax";
    private static final String ERROR_VALOR_IVA = "error_valor_iva";
    private static final String TIP = "tip";
    private static final String CHARGE = "charge";
    private static final String ERROR_VALOR_IMPOCON = "error_valor_impocon";
    private static final String TOOLBAR_MENU_ACTUAL_SALE = "toolbar_menu_actual_sale";
    private static final String CLOSE_APP_CONFIRMATION = "close_app_confirmation";
    private static final String TOTAL_VALUE = "total_value";
    private static final String BASE_VALUE = "base_value";
    private static final String TERMS_AND_CONDITIONS = "terminos_condiciones_negocios";
    private static final String TERMS_AND_CONDITIONS_PAY = "terminos_condiciones_pay_negocios";
    static List<SubMenuVO> listSubMenu;
    private final String TAG = inicioActivity.class.getSimpleName();
    //    Loaders IDS
    private final int GET_ID_LOADER = 10;
    private final int REGISTER_PUSHWOOSH_LOADER = 11;
    private final int TERMS_ACCEPTANCE_CHECK_LOADER = 12;
    private final int TERMS_ACCEPTANCE_UPDATE_LOADER = 13;
    public AdapterGridInicio adapter;
    //    UI
    LinearLayout lnBtnCobrar;
    GridView gridview;
    ImageView imgMenuLateral;
    TextView txtValorVenta;
    String numeroVenta;
    String numeroIva, deviceID;
    // nuevos valores
    String numeroImpoconsumo;
    String numeroPropina;
    Utils util = new Utils();
    TextView txtHeaderVentaActual;
    TextView txtSignoPesos;
    TextView txtlnBtnCobrar;
    // nuevos textview
    TextView txtTituloValorBase;
    TextView txtTituloValorTotalNuevo;
    TextView txtValorTotalNuevo;
    TextView txtValorIva;
    TextView toolbar_title;
    TextView txtBack;
    // nuevo
    TextView txtValorImpoconsumo;
    TextView txtValorPropina;
    TextView txtAtrasIva;
    boolean valorBaseIngresado = false;
    boolean valorIvaIngresado = false;
    boolean valorimpoconsumoIngresado = false;
    com.google.android.gms.analytics.Tracker mTracker;
    //    Utils
    boolean doubleBackToExitPressedOnce = false;
    String tokenServiceEndpoint = "/users/push_token";
    String urlUserTos = "/users/tos";
    User user;
    TrackingOps trackingOps;
    //    Ops
    private String[] idsSubArrays = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "C", "0", "DEL"};
    private Realm realm;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicioactivity);

        try {
            initDrawer();
            trackingOps = (TrackingOps) getApplication();
            realm = trackingOps.getRealm();
            user = trackingOps.getUser();
        } catch (NullPointerException ignore) {
        }

        if (user.getAcceptingPayments()) {
            OneSignal.setEmail(user.getEmail());

            JSONObject tags = new JSONObject();
            try {
                tags.put("user_name", user.getFirstName());
                tags.put("brand", user.getBrand().getName());
                tags.put("accepting_payments", user.getAcceptingPayments());
                tags.put("mpos_enable", user.getMposEnabled());
                tags.put("city", user.getCity());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            OneSignal.sendTags(tags);
        }

        lnBtnCobrar = findViewById(R.id.lnBtnCobrar);
        lnBtnCobrar.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_ACTUAL_SALE));
        txtValorVenta = findViewById(R.id.txtValorVenta);
//        txtHeaderVentaActual = findViewById(R.id.txtHeaderVentaActual);
        txtSignoPesos = findViewById(R.id.txtSignoPesos);
        txtlnBtnCobrar = findViewById(R.id.txtlnBtnCobrar);
        txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(CHARGE));
        txtValorIva = findViewById(R.id.txtValorIva);
        txtValorImpoconsumo = findViewById(R.id.txtValorImpoconsumo);
        txtValorPropina = findViewById(R.id.txtValorPropina);
//        txtAtrasIva = findViewById(R.id.txtTituloValorBase);
//        txtAtrasIva.setOnClickListener(this);

        txtTituloValorBase = findViewById(R.id.txtTituloValorBase);
        txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(BASE_VALUE));
        txtTituloValorTotalNuevo = findViewById(R.id.txtTituloValorTotalNuevo);
        txtTituloValorTotalNuevo.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE_EQUAL));
        txtValorTotalNuevo = findViewById(R.id.txtValorTotalNuevo);

        txtBack = findViewById(R.id.txtBack);
        txtBack.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);
        txtBack.setOnClickListener(this);

        numeroVenta = "";
        numeroIva = "";
        numeroImpoconsumo = "";
        numeroPropina = "";
        //  Picasso.with(getApplicationContext()).load(R.drawable.mascotas).transform(new CircleTransform()).into(imgMenuLateral);
        listSubMenu = new ArrayList<SubMenuVO>();
        gridview = findViewById(R.id.gridview);
        for (int a = 0; a < idsSubArrays.length; a++) {
            SubMenuVO submenu = new SubMenuVO();
            submenu.setNumero(idsSubArrays[a]);
            listSubMenu.add(submenu);
        }

        adapter = new AdapterGridInicio(getApplicationContext(), listSubMenu);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                try {
                    if (valorimpoconsumoIngresado) { // ya ingreso impoconsumo ahora va a ingresar propina
                        // Toast.makeText(getApplicationContext(), "agregando propina", Toast.LENGTH_SHORT).show();
                        if (position == 9) {
                            numeroPropina = "";
                            txtValorPropina.setText("0.0");
                            txtValorTotalNuevo.setText(numeroVenta); // texto en verde
                        } else if (position == 11) {
                            if (numeroPropina.length() > 0) {
                                if (numeroPropina.length() == 1) {
                                    numeroPropina = "";
                                    txtValorPropina.setText("0.0");
                                    txtValorTotalNuevo.setText(numeroVenta); // texto en verde
                                } else {
                                    String borrarStr = borrar(numeroPropina);
                                    int valorBase = Integer.parseInt(numeroVenta.replace(".", ""));
                                    int valorPropina = Integer.parseInt(borrarStr.replace(".", ""));
                                    int totalTodo = valorBase + valorPropina;
                                    numeroPropina = borrarStr;
                                    txtValorPropina.setText(miles(borrarStr).replace(",", "."));
                                    txtValorTotalNuevo.setText(miles(totalTodo + "").replace(",", ".")); // texto en verde

                                }
                            }
                        } else {
                            numeroPropina += idsSubArrays[position];
                            int valorBase = Integer.parseInt(numeroVenta.replace(".", ""));
                            int valorIva = Integer.parseInt(numeroPropina.replace(".", ""));
                            int totalTodo = valorBase + valorIva;
                            numeroPropina = miles(numeroPropina).replace(",", ".");
                            txtValorPropina.setText(numeroPropina);
                            txtValorTotalNuevo.setText(miles(totalTodo + "").replace(",", ".")); // texto en verde

                        }
                    } else if (valorIvaIngresado) { // ya ingreso iva ahora va a ingresar impoconsumo
                        // Toast.makeText(getApplicationContext(), "agregando impocon", Toast.LENGTH_SHORT).show();
                        if (position == 9) {
                            numeroImpoconsumo = "";
                            txtValorImpoconsumo.setText("0.0");
                            txtValorTotalNuevo.setText(numeroVenta); // texto en verde
                        } else if (position == 11) {
                            if (numeroImpoconsumo.length() > 0) {
                                if (numeroImpoconsumo.length() == 1) {
                                    numeroImpoconsumo = "";
                                    txtValorImpoconsumo.setText("0.0");
                                    txtValorTotalNuevo.setText(numeroVenta); // texto en verde
                                } else {
                                    String borrarStr = borrar(numeroImpoconsumo);
                                    numeroImpoconsumo = borrarStr;
                                    txtValorImpoconsumo.setText(miles(borrarStr).replace(",", "."));

                                }
                            }
                        } else {
                            numeroImpoconsumo += idsSubArrays[position];
                            numeroImpoconsumo = miles(numeroImpoconsumo).replace(",", ".");
                            txtValorImpoconsumo.setText(numeroImpoconsumo);

                        }
                    } else if (valorBaseIngresado == true) { // ingresando iva
                        if (position == 9) { // borrar el iva
                            numeroIva = "";
                            txtValorIva.setText("0.0");
                            txtValorTotalNuevo.setText(numeroVenta); // texto en verde
                        } else if (position == 11) {
                            if (numeroIva.length() > 0) {
                                if (numeroIva.length() == 1) {
                                    numeroIva = "";
                                    txtValorIva.setText("0.0");
                                    txtValorTotalNuevo.setText(numeroVenta); // texto en verde
                                } else {
                                    String borrarStr = borrar(numeroIva);
                                    numeroIva = borrarStr;
                                    txtValorIva.setText(miles(borrarStr).replace(",", "."));
                                    //txtValorTotalNuevo.setText(miles(totalTodo+"").replace(",", ".")); // texto en verde
                                }
                            }
                        } else {
                            numeroIva += idsSubArrays[position];
                            //  int valorBase = Integer.parseInt(numeroVenta.replace(".",""));
                            //   int valorIva = Integer.parseInt(numeroIva.replace(".",""));
                            //   int totalTodo = valorBase+valorIva;
                            numeroIva = miles(numeroIva).replace(",", ".");
                            txtValorIva.setText(numeroIva);
                            //txtValorTotalNuevo.setText(miles(totalTodo+"").replace(",", ".")); // texto en verde
                        }


                    } else {// ingresando valor base
                        if (position == 9) {
                            numeroVenta = "";
                            txtValorVenta.setText("0.0");
                            txtValorTotalNuevo.setText("0.0"); // texto en verde
                        } else if (position == 11) {
                            if (numeroVenta.length() > 0) {
                                if (numeroVenta.length() == 1) {
                                    numeroVenta = "";
                                    txtValorVenta.setText("0.0");
                                    txtValorTotalNuevo.setText("0.0"); // texto en verde
                                } else {
                                    String borrarStr = borrar(numeroVenta);
                                    numeroVenta = borrarStr;
                                    txtValorVenta.setText(miles(borrarStr).replace(",", "."));
                                    txtValorTotalNuevo.setText(miles(borrarStr).replace(",", ".")); // texto en verde
                                }

                            }
                        } else {

                            int valor = Integer.parseInt(miles(numeroVenta + idsSubArrays[position]).replace(",", ""));
                            numeroVenta += idsSubArrays[position];
                            numeroVenta = miles(numeroVenta).replace(",", ".");
                            txtValorVenta.setText(numeroVenta);
                            txtValorTotalNuevo.setText(numeroVenta); // texto en verde
                        }
                    }

                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(NO_VALOR_MAXIMO),
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        if (user.getDeviceID() == null) {
            if (user.getPushToken() != null && !user.getPushToken().equals("")) {
                if (getSupportLoaderManager().getLoader(GET_ID_LOADER) == null) {
                    getSupportLoaderManager().initLoader(GET_ID_LOADER, null, this).forceLoad();
                } else {
                    getSupportLoaderManager().restartLoader(GET_ID_LOADER, null, this).forceLoad();
                }
            } else {
                validateTermsDates();
            }
        } else {
            validateTermsDates();
        }

        txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE).toUpperCase());
        txtValorTotalNuevo.setText("0.0");
        txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEXT));

        if (!((TrackingOps) getApplication()).getJsonProcesoPago().equals("no")) {
            try {
                JSONObject pushBody = new JSONObject(((TrackingOps) getApplication()).getJsonProcesoPago());
                JSONObject catObj = pushBody.getJSONObject("userdata");
                if (catObj.getString("type").equals("coupon_redeemed")) {
                    trackingOps.registerEvent("Pushwoosh", trackingOps.getmFirebaseRemoteConfig().getString(OFFER_SAVED), catObj.getString("offer_name") + " | " + user.getCustomer().getName());


                    OfertasRedimidasVO RedeemedDeal = new OfertasRedimidasVO(catObj.getString("id"), catObj.getString("status"),
                            catObj.getString("redemption_date"), catObj.getString("offer_name"), catObj.getInt("price") + "", catObj.getInt("discount") + "",
                            catObj.getString("user_name"), catObj.getString("document_type"), catObj.getString("document_number"), "", catObj.getString("code"),
                            catObj.getString("user_email"), "", "");
                    ((TrackingOps) getApplication()).setJsonProcesoPago("no");
                    Intent i = new Intent(getApplicationContext(), ActivityEstadoCodigoDetalle.class);
                    i.putExtra("Object", RedeemedDeal);
                    startActivity(i);
                } else {
                    JSONObject userObj = catObj.getJSONObject("user");
                    trackingOps.registerEvent(trackingOps.getmFirebaseRemoteConfig().getString(TRANSACTION_STATUS), trackingOps.getmFirebaseRemoteConfig().getString(TRANSACTION), catObj.getInt("status") + "");

                    Intent i = new Intent(getApplicationContext(), ActivityDetalleTransaccion.class);
                    if (catObj.isNull("created_at")) {
                        i.putExtra("fechaTotal", "");
                        i.putExtra("hora", "");
                    } else {
                        i.putExtra("fechaTotal", catObj.getString("created_at")); // no
                        i.putExtra("hora", catObj.getString("created_at")); // NO
                    }

                    if (catObj.isNull("status"))
                        i.putExtra("status", "");
                    else
                        i.putExtra("status", catObj.getInt("status") + "");


                    if (catObj.isNull("amount"))
                        i.putExtra("monto", "");
                    else
                        i.putExtra("monto", catObj.getInt("amount") + ""); // no

                    if (catObj.isNull("pin"))
                        i.putExtra("pin", "");
                    else
                        i.putExtra("pin", userObj.getString("pin")); // no

                    if (catObj.isNull("tax_amount"))
                        i.putExtra("taxAmount", "");
                    else
                        i.putExtra("taxAmount", catObj.getInt("tax_amount") + ""); // no

                    if (catObj.isNull("authorization_code"))
                        i.putExtra("numAprobacion", "");
                    else
                        i.putExtra("numAprobacion", catObj.getString("authorization_code") + ""); // no

                    if (catObj.isNull("transaction_id"))
                        i.putExtra("transId", "");
                    else
                        i.putExtra("transId", catObj.getString("transaction_id") + "");

                    if (catObj.isNull("amount_base"))
                        i.putExtra("valorBase", "");
                    else
                        i.putExtra("valorBase", catObj.getInt("amount_base") + "");

                    if (catObj.isNull("iac_amount"))
                        i.putExtra("iac", "");
                    else
                        i.putExtra("iac", catObj.getInt("iac_amount") + "");

                    if (catObj.isNull("tip_amount"))
                        i.putExtra("tip", "");
                    else
                        i.putExtra("tip", catObj.getInt("tip_amount") + "");

                    if (catObj.isNull("origin"))
                        i.putExtra("origin", "");
                    else
                        i.putExtra("origin", catObj.getString("origin"));


                    ((TrackingOps) getApplication()).setJsonProcesoPago("no");
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        trackingOps.registerScreenName("Venta actual | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtBack:
                if (valorimpoconsumoIngresado) { // pasar de propina a impoconsumo
                    valorimpoconsumoIngresado = false;
                    ocultarTodo();
                    numeroPropina = "";
                    txtValorPropina.setText("0.0");
                    txtValorTotalNuevo.setText(numeroVenta);
                    txtBack.setVisibility(View.VISIBLE);
                    txtValorImpoconsumo.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONSUMPTION_TAX).toUpperCase());
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));
                } else if (valorIvaIngresado) { // pasar de impoconsumo a iva
                    valorIvaIngresado = false;
                    ocultarTodo();
                    txtValorIva.setVisibility(View.VISIBLE);
                    txtBack.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TAX).toUpperCase());
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));
                } else if (valorBaseIngresado) {// pasar de iva a valor total
                    valorBaseIngresado = false;
                    ocultarTodo();
                    txtValorVenta.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE));
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));
                }
                break;
            case R.id.lnBtnCobrar:
                if (util.haveInternet(getApplicationContext())) {
                    if (valorBaseIngresado == false) { // ya digito el valor base
                        if (txtValorVenta.getText().toString().length() > 7) {
                            Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(NO_VALOR_MAXIMO),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            if (!txtValorVenta.getText().toString().equals("")) {
                                // con login

                                int valorMinimo = user.getMinAmount();

                                int valorMaximo = user.getMaxAmount();

                                String monto = txtValorVenta.getText().toString().replace(".", "");
                                int montoInt = Integer.parseInt(monto);
                                if (montoInt >= valorMinimo && montoInt <= valorMaximo) {
                                    //singleton.setMonto(txtValorVenta.getText().toString());
                                    // Intent detalle = new Intent(getApplicationContext(), PinUsuarioActivity.class);
                                    //  startActivity(detalle);
                                    valorBaseIngresado = true;
                                    ocultarTodo();
                                    txtValorIva.setVisibility(View.VISIBLE);
                                    txtBack.setVisibility(View.VISIBLE);
                                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TAX));
                                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));
                                    // registarAnalitics("Civico Pay Comercios","Venta actual","Valor Base"); // ojo revisar si va en cobrar o en siguiente
                                } else {
                                    if (montoInt < valorMinimo) {
                                        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(NO_VALOR_MINIMO),
                                                Toast.LENGTH_SHORT).show();
                                    } else if (montoInt > valorMaximo) {
                                        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(NO_VALOR_MAXIMO),
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(NO_VALOR),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else if (valorIvaIngresado == false) { // ingresar iva
                        if (!txtValorIva.getText().toString().equals("")) {
                            String montoBase = txtValorVenta.getText().toString().replace(".", "");
                            int montoBaseInt = Integer.parseInt(montoBase);
                            String monto = txtValorIva.getText().toString().replace(".", "");
                            int montoIvaInt = Integer.parseInt(monto);
                            int porcentaje = 19;
                            double rpta = montoBaseInt * porcentaje / 100.0;

                            if (montoIvaInt <= rpta) { // ok se resetean todos los componentes por que ya se va a enviar el pago
                                valorIvaIngresado = true;
                                ocultarTodo();
                                txtBack.setVisibility(View.VISIBLE);

                                txtValorImpoconsumo.setVisibility(View.VISIBLE);
                                txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONSUMPTION_TAX).toUpperCase());
                                txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));

                            } else {
                                // mensaje de error por que no puede ser mayor igual a la base
                                Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(ERROR_VALOR_IVA),
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(NO_VALOR),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else if (valorimpoconsumoIngresado == false) {// ingresar impoconsumo aca valido valor no superoir al 8%
                        String montoBase = txtValorVenta.getText().toString().replace(".", "");
                        int montoBaseInt = Integer.parseInt(montoBase);
                        String monto = txtValorImpoconsumo.getText().toString().replace(".", "");
                        int montoIvaInt = Integer.parseInt(monto);
                        int porcentaje = 8;
                        double rpta = montoBaseInt * porcentaje / 100.0;
                        if (montoIvaInt <= rpta) { // ok se resetean todos los componentes por que ya se va a enviar el pago
                            valorimpoconsumoIngresado = true;
                            ocultarTodo();
                            txtBack.setVisibility(View.VISIBLE);
                            txtValorPropina.setVisibility(View.VISIBLE);
                            txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TIP).toUpperCase());
                            txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(CHARGE).toUpperCase());
                        } else {
                            // mensaje de error por que no puede ser mayor igual a la base
                            Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(ERROR_VALOR_IMPOCON),
                                    Toast.LENGTH_SHORT).show();
                        }

                    } else {// ingresar propina y cobrar

                        if (numeroVenta.equals("")) {
                            numeroVenta = "0";
                        }
                        if (numeroIva.equals("")) {
                            numeroIva = "0";
                        }
                        if (numeroImpoconsumo.equals("")) {
                            numeroImpoconsumo = "0";
                        }
                        if (numeroPropina.equals("")) {
                            numeroPropina = "0";
                        }

                        Intent detalle = new Intent(getApplicationContext(), PinUsuarioActivity.class);
                        detalle.putExtra("montoValorBase", miles(numeroVenta).replace(",", "."));
                        detalle.putExtra("montoValorIva", miles(numeroIva).replace(",", "."));
                        detalle.putExtra("montoValorImpoConsumo", miles(numeroImpoconsumo).replace(",", "."));
                        detalle.putExtra("montoValorPropina", miles(numeroPropina).replace(",", "."));
                        detalle.putExtra("monto", miles(txtValorTotalNuevo.getText().toString()).replace(",", "."));

                        valorBaseIngresado = false;
                        valorIvaIngresado = false;
                        valorimpoconsumoIngresado = false;
                        ocultarTodo();
                        txtValorVenta.setVisibility(View.VISIBLE);

                        txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE).toUpperCase());
                        txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEXT).toUpperCase());
                        trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_ACTUAL_SALE), trackingOps.getmFirebaseRemoteConfig().getString(CHARGE)); // ojo revisar si va en cobrar o en siguiente

                        resetValores();
                        startActivity(detalle);
                    }
                } else {
                    util.mensajeNoInternet(getApplicationContext());
                }

                break;
            case R.id.txtTituloValorBase:
                // sin impoconsumo y propina
                /*valorBaseIngresado=false;
                numeroIva = "";
                //txtValorVenta.setText("0.0");
                txtValorIva.setText("0.0");
                txtValorVenta.setVisibility(View.VISIBLE);
                txtValorIva.setVisibility(View.GONE);
                txtAtrasIva.setVisibility(View.GONE);
                txtTituloValorBase.setText("Valor Total");
                txtlnBtnCobrar.setText("CONTINUAR");*/


                if (valorimpoconsumoIngresado == true) { // pasar de propina a impoconsumo
                    valorimpoconsumoIngresado = false;
                    ocultarTodo();
                    numeroPropina = "";
                    txtValorPropina.setText("0.0");
                    txtValorTotalNuevo.setText(numeroVenta);
                    txtBack.setVisibility(View.VISIBLE);
                    txtValorImpoconsumo.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONSUMPTION_TAX).toUpperCase());
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR).toUpperCase());
                } else if (valorIvaIngresado == true) { // pasar de impoconsumo a iva
                    valorIvaIngresado = false;
                    ocultarTodo();
                    txtValorIva.setVisibility(View.VISIBLE);
                    txtBack.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TAX).toUpperCase());
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR).toUpperCase());
                } else if (valorBaseIngresado == true) {// pasar de iva a valor total
                    valorBaseIngresado = false;
                    ocultarTodo();
                    txtValorVenta.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE_EQUAL));
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR).toUpperCase());
                }


            default:
                break;
        }
    }

    public void resetValores() {
        numeroPropina = "";
        txtValorPropina.setText("0.0");
        numeroImpoconsumo = "";
        txtValorImpoconsumo.setText("0.0");
        numeroIva = "";
        txtValorIva.setText("0.0");
        numeroVenta = "";
        txtValorVenta.setText("0.0");
        txtValorTotalNuevo.setText("0.0"); // texto en verde
    }

    public void ocultarTodo() {
        txtValorVenta.setVisibility(View.GONE);
        txtValorIva.setVisibility(View.GONE);
        txtValorImpoconsumo.setVisibility(View.GONE);
        txtValorPropina.setVisibility(View.GONE);
        txtBack.setVisibility(View.GONE);
    }

    public String borrar(String str) {
        if (str.length() > 0) {
            str = str.substring(0, str.length() - 1);
        }

        return str;
    }

    public String miles(String no) {
        no = no.replace(".", "");
        no = no.replace(",", "");
        BigDecimal sPrice = new BigDecimal(no);
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(',');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String str = formatter.format(sPrice.longValue());
        return str;
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(CLOSE_APP_CONFIRMATION), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.go_back_toolbar:
                if (valorimpoconsumoIngresado) { // pasar de propina a impoconsumo
                    valorimpoconsumoIngresado = false;
                    ocultarTodo();
                    numeroPropina = "";
                    txtValorPropina.setText("0.0");
                    txtValorTotalNuevo.setText(numeroVenta);
                    txtBack.setVisibility(View.VISIBLE);
                    txtValorImpoconsumo.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONSUMPTION_TAX).toUpperCase());
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));
                } else if (valorIvaIngresado) { // pasar de impoconsumo a iva
                    valorIvaIngresado = false;
                    ocultarTodo();
                    txtValorIva.setVisibility(View.VISIBLE);
                    txtBack.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TAX).toUpperCase());
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));
                } else if (valorBaseIngresado) {// pasar de iva a valor total
                    valorBaseIngresado = false;
                    ocultarTodo();
                    txtValorVenta.setVisibility(View.VISIBLE);
                    txtTituloValorBase.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE));
                    txtlnBtnCobrar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        RequestDefaultFactory requestDefaultFactory = new RequestDefaultFactory(this, args);
        switch (id) {
            case GET_ID_LOADER:
                return new getIdTaskLoader(this);
            case TERMS_ACCEPTANCE_CHECK_LOADER:
                toggleProgressDialog(false);
                return requestDefaultFactory;
            default:
                return requestDefaultFactory;
        }


    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        toggleProgressDialog(false);
        switch (loader.getId()) {
            case GET_ID_LOADER:
                if (util.haveInternet(getApplicationContext())) {
                    deviceID = data;
                    realm.beginTransaction();
                    user.setDeviceID(data);
                    realm.commitTransaction();

                    makePushWooshRegistration();
                } else {
                    util.mensajeNoInternet(getApplicationContext());
                }
                break;
            case REGISTER_PUSHWOOSH_LOADER:
                makeTermsAcceptanceCheckRequest();
                break;
            case TERMS_ACCEPTANCE_CHECK_LOADER:
                if (data != null) {
                    try {
                        JSONObject responseObject = new JSONObject(data);
                        String tos_change_confirmation = responseObject.getString("tos_change_confirmation");
                        String tos_pay_change_confirmation = responseObject.getString("tos_pay_change_confirmation");
                        Boolean tos_confirmation = responseObject.getBoolean("tos_confirmation");
                        Boolean tos_pay_confirmation = responseObject.getBoolean("tos_pay_confirmation");

                        realm.beginTransaction();
                        user.setTosDate(tos_change_confirmation);
                        user.setTosPayDate(tos_pay_change_confirmation);
                        realm.commitTransaction();

                        if (!tos_confirmation || !tos_pay_confirmation) {
                            DialogTerminosCondiciones termConditions = new DialogTerminosCondiciones(this);
                            termConditions.show();
                        } else {
                            validateTermsDates();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        DialogTerminosCondiciones termConditions = new DialogTerminosCondiciones(this);
                        termConditions.show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        util.alertError500(this);
                    }
                } else {
                    DialogTerminosCondiciones termConditions = new DialogTerminosCondiciones(this);
                    termConditions.show();
                }
                break;
            case TERMS_ACCEPTANCE_UPDATE_LOADER:
                try {
                    JSONObject responseObject = new JSONObject(data);
                    String tos_change_confirmation = responseObject.getString("tos_change_confirmation");
                    String tos_pay_change_confirmation = responseObject.getString("tos_pay_change_confirmation");


                    realm.beginTransaction();
                    user.setTosDate(tos_change_confirmation);
                    user.setTosPayDate(tos_pay_change_confirmation);
                    realm.commitTransaction();


                } catch (JSONException e) {
                    e.printStackTrace();
                    util.alertError500(this);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    util.alertError500(this);
                }
                break;
            default:
                break;
        }

    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }

    public void validateTermsDates() {
        if (user.getTosDate() == null) {
            makeTermsAcceptanceCheckRequest();
        } else {
            try {
                DateFormat readFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                String firTerms = trackingOps.getmFirebaseRemoteConfig().getString(TERMS_AND_CONDITIONS);
                Date firTermsDate = readFormat.parse(firTerms);
                String firTermsPay = trackingOps.getmFirebaseRemoteConfig().getString(TERMS_AND_CONDITIONS_PAY);
                Date firTermsPayDate = readFormat.parse(firTermsPay);
                DateFormat userReadFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

                Date tosDate = userReadFormat.parse(user.getTosDate());
                Date tosPayDate = userReadFormat.parse(user.getTosPayDate());

                if (firTermsDate.after(tosDate) || firTermsPayDate.after(tosPayDate)) {
                    DialogTerminosCondiciones termConditions = new DialogTerminosCondiciones(this);
                    termConditions.show();
                }
            } catch (ParseException e) {
                e.printStackTrace();

                DialogTerminosCondiciones termConditions = new DialogTerminosCondiciones(this);
                termConditions.show();
            }
        }
    }

    public void makePushWooshRegistration() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {

            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            body.put("token", user.getPushToken());
            body.put("device_id", deviceID);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL + tokenServiceEndpoint);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(REGISTER_PUSHWOOSH_LOADER);

            if (loader == null) {
                loaderManager.initLoader(REGISTER_PUSHWOOSH_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(REGISTER_PUSHWOOSH_LOADER, queryBundle, this);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void makeTermsAcceptanceCheckRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, this.urlUserTos);
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(TERMS_ACCEPTANCE_CHECK_LOADER);

            if (loader == null) {
                loaderManager.initLoader(TERMS_ACCEPTANCE_CHECK_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(TERMS_ACCEPTANCE_CHECK_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    public void makeTermsAcceptanceUpdateRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            body.put("tos_confirmation", true);
            body.put("tos_pay_confirmation", true);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + urlUserTos);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(TERMS_ACCEPTANCE_UPDATE_LOADER);

            if (loader == null) {
                loaderManager.initLoader(TERMS_ACCEPTANCE_UPDATE_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(TERMS_ACCEPTANCE_UPDATE_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void onObtenerQr(View view) {
        trackingOps.registerEvent("Civico Pay Comercios", "Venta Actual", "Cobrar | QR");
        startActivity(new Intent(this, CodigoQRActivity.class));
    }

    private static class getIdTaskLoader extends AsyncTaskLoader<String> {

        public getIdTaskLoader(Context context) {
            super(context);
        }

        @Override
        public String loadInBackground() {
            Info adInfo = null;

            try {
                adInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.getContext());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }

            return adInfo.getId();
        }

        @Override
        public void deliverResult(String data) {
            super.deliverResult(data);
        }
    }

}
