package com.civiconegocios.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

public class EditTextCustom extends LinearPerfil {


    private static final String PASSWORD = "password";
    EditText editTextCustom;

    public EditTextCustom(Context context) {
        super(context);
    }

    public EditTextCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public EditTextCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attrs) {
        View.inflate(context, R.layout.linear_formulario, this);
        setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);

        editTextCustom = findViewById(R.id.editTextCustom);
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.LinearCustom,
                    0, 0);

            String titleText = "";
            String detailText = "";
            try {
                titleText = a.getString(R.styleable.LinearCustom_mirar);
            } catch (Exception e) {

            } finally {
                a.recycle();
            }
        }
    }

    public String getTextCustom() {
        return editTextCustom.getText().toString();
    }
}
