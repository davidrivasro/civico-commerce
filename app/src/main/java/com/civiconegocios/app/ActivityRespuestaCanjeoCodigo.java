package com.civiconegocios.app;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class ActivityRespuestaCanjeoCodigo extends AppCompatActivity implements View.OnClickListener {

    private static final String COMUNICATE_WITH_US = "comunicate_with_us";
    private static final String INVALID_CODE = "invalid_code";
    private static final String PERMISSION_PHONE_RATIONALE = "permission_phone_rationale";
    private static final String BUTTON_ALLOW = "button_allow";
    private static final String BUTTON_DENY = "button_deny";
    private static final String PERMISSION_CALL_DENIED = "permission_call_denied";
    private static final String PERMISSION_CALL_NEVERASK = "permission_call_neverask";
    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String CODE_IS_NOT_VALID = "code_is_not_valid";
    private static final String QUESTIONS_ABOUT_CODE = "questions_about_code";
    private static final String VALID_COUPON_TITLE = "valid_coupon_title";
    private static final String VALID_COUPON_SUBTITLE = "valid_coupon_subtitle";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    LinearLayout lnCuponInvalido;
    LinearLayout lnCuponValido;
    TextView respuestaServe, validCouponTitle, validCouponSubtitle, textOpenSansRegular, textOpenSansRegular1,
            txtAtrasRespuestaCanjeo, txtBtnComunicateCanjeo;
    String value = "";
    Utils util = new Utils();
    //    Ops
    TrackingOps trackingOps;
    User user;
    Toolbar defaultToolbar;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respuesta_canjeo_codigo);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("KEY");
        }

        defaultToolbar = findViewById(R.id.toolbar);
        defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        setSupportActionBar(defaultToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        lnCuponInvalido = findViewById(R.id.lnCuponInvalido);
        lnCuponValido = findViewById(R.id.lnCuponValido);

        textOpenSansRegular = findViewById(R.id.tv_fecha);
        textOpenSansRegular.setText(trackingOps.getmFirebaseRemoteConfig().getString(CODE_IS_NOT_VALID));

        textOpenSansRegular1 = findViewById(R.id.textOpenSansRegular1);
        textOpenSansRegular1.setText(trackingOps.getmFirebaseRemoteConfig().getString(QUESTIONS_ABOUT_CODE));

        validCouponTitle = findViewById(R.id.validCouponTitle);
        validCouponTitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(VALID_COUPON_TITLE));

        validCouponSubtitle = findViewById(R.id.validCouponSubtitle);
        validCouponSubtitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(VALID_COUPON_SUBTITLE));

        txtAtrasRespuestaCanjeo = findViewById(R.id.txtAtrasRespuestaCanjeo);
        txtAtrasRespuestaCanjeo.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        txtAtrasRespuestaCanjeo.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);
        txtAtrasRespuestaCanjeo.setOnClickListener(this);
        txtBtnComunicateCanjeo = findViewById(R.id.txtBtnComunicateCanjeo);
        txtBtnComunicateCanjeo.setText(trackingOps.getmFirebaseRemoteConfig().getString(COMUNICATE_WITH_US).toUpperCase());
        respuestaServe = findViewById(R.id.respuestaServe);
        txtBtnComunicateCanjeo.setOnClickListener(this);

        if (value.equals("valido")) {
            lnCuponValido.setVisibility(View.VISIBLE);
        } else {
            lnCuponInvalido.setVisibility(View.VISIBLE);
            respuestaServe.setText(value);
        }

        trackingOps.registerScreenName("Código no valido | " + user.getCustomer().getName() + " | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtAtrasRespuestaCanjeo:
                trackingOps.setReloadData(true);
                finish();
                break;
            case R.id.txtBtnComunicateCanjeo:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(COMUNICATE_WITH_US), trackingOps.getmFirebaseRemoteConfig().getString(INVALID_CODE));
                if (util.mostrarPopUp()) {
                    ActivityRespuestaCanjeoCodigoPermissionsDispatcher.callWithPermissionCheck(this, "2 36 31 11");
                } else {
                    DialogComunicate cdd = new DialogComunicate(this);
                    cdd.show();
                }
                break;
            default:
                break;
        }

    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    public void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + "031" + phone));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ActivityRespuestaCanjeoCodigoPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.CALL_PHONE)
    void showRationaleForRegistarToken(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_PHONE_RATIONALE))
                .setPositiveButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_ALLOW), (dialog, button) -> request.proceed())
                .setNegativeButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_DENY), (dialog, button) -> request.cancel())
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(Manifest.permission.CALL_PHONE)
    void showDeniedForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_DENIED), Toast.LENGTH_SHORT).show();
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.CALL_PHONE)
    void showNeverAskForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_NEVERASK), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        trackingOps.setReloadData(true);
        super.onBackPressed();
    }
}
