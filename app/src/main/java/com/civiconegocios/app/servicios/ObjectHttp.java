package com.civiconegocios.app.servicios;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class ObjectHttp {

    private final String TAG = ObjectHttp.class.getSimpleName();

    Context mContext;

    public ObjectHttp(Context mContext) {
        this.mContext = mContext;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String obtenerHttpConDatos(String urlStr, String parametros) throws IOException {

        byte[] postData = parametros.getBytes("utf-8");
        int postDataLength = postData.length;
        String request = urlStr;
        URL url = new URL(request);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

       /* HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        ObtenerCertificado obCert = new ObtenerCertificado(mContext);
        conn.setSSLSocketFactory(obCert.obtenerCert().getSocketFactory());*/

        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setUseCaches(false);
        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }
        int responseCode = conn.getResponseCode();
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }

        br.close();
        return responseOutPut.toString();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String servicePostConCiudad(String urlStr, String parametros) throws IOException {

        String urlParameters = parametros;
        byte[] postData = urlParameters.getBytes("utf-8");
        int postDataLength = postData.length;
        String request = urlStr;
        URL url = new URL(request);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        /*HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        ObtenerCertificado obCert = new ObtenerCertificado(mContext);
        conn.setSSLSocketFactory(obCert.obtenerCert().getSocketFactory());*/

        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("x-civico-city", "bogota");
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setUseCaches(false);
        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }
        br.close();

        return responseOutPut.toString();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String obtenerHttpConDatosPostBasicAutorization(String urlStr, String parametros, String Autorizacion, String ciudad, String appVersion) throws IOException {

       /* String urlParameters = parametros;
        byte[] postData       = urlParameters.getBytes("utf-8");
        int    postDataLength = postData.length;*/
        String request = urlStr;
        URL url = new URL(request);
        // HttpURLConnection conn= (HttpURLConnection) url.openConnection();

        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        ObtenerCertificado obCert = new ObtenerCertificado(mContext);
        conn.setSSLSocketFactory(obCert.obtenerCert().getSocketFactory());

        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("X-civico-city", ciudad);
        conn.setRequestProperty("X-app-version", appVersion);
        conn.setRequestProperty("Authorization", "Basic " + Autorizacion.replace("\n", ""));
        conn.setRequestProperty("Accept", "*/*");


        DataOutputStream dStream = new DataOutputStream(conn.getOutputStream());
        dStream.writeBytes(parametros.toString());
        dStream.flush();
        dStream.close();


        int responseCode = conn.getResponseCode();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }
        br.close();
        return responseOutPut.toString();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String obtenerHttpConDatosPutBasicAutorizationNuevo(String urlStr, String parametros, String Autorizacion) throws IOException {

       /* String urlParameters = parametros;
        byte[] postData       = urlParameters.getBytes("utf-8");
        int    postDataLength = postData.length;*/
        String request = urlStr;
        URL url = new URL(request);
        // HttpURLConnection conn= (HttpURLConnection) url.openConnection();

        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        ObtenerCertificado obCert = new ObtenerCertificado(mContext);
        conn.setSSLSocketFactory(obCert.obtenerCert().getSocketFactory());

        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("PUT");
        conn.setRequestProperty("Content-Type", "application/json");

        conn.setRequestProperty("Authorization", "Basic " + Autorizacion.replace("\n", ""));
        conn.setRequestProperty("Accept", "*/*");


        DataOutputStream dStream = new DataOutputStream(conn.getOutputStream());
        dStream.writeBytes(parametros.toString());
        dStream.flush();
        dStream.close();


        int responseCode = conn.getResponseCode();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }
        br.close();
        return responseOutPut.toString();
    }

    public String obtenerHttpGet(String urlStr, String parametros, String ciudad) throws IOException {

        URL url = new URL(urlStr + parametros);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }
        br.close();
        return responseOutPut.toString();
    }

    public String obtenerHttpGetAutorizacion(String urlStr, String parametros, String Autorizacion) throws IOException {

        URL url = new URL(urlStr + parametros);

        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        ObtenerCertificado obCert = new ObtenerCertificado(mContext);
        conn.setSSLSocketFactory(obCert.obtenerCert().getSocketFactory());

        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Basic " + Autorizacion.replace("\n", ""));
        conn.setRequestProperty("Accept", "*/*");
        conn.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }
        br.close();
        return responseOutPut.toString();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String obtenerHttpConDatosPutBasicAutorization(String urlStr, String parametros, String Autorizacion) throws IOException {

        String request = urlStr;
        URL url = new URL(request);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        /*HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        ObtenerCertificado obCert = new ObtenerCertificado(mContext);
        conn.setSSLSocketFactory(obCert.obtenerCert().getSocketFactory());*/

        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("PUT");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Basic " + Autorizacion.replace("\n", ""));
        conn.setRequestProperty("Accept", "*/*");
        DataOutputStream dStream = new DataOutputStream(conn.getOutputStream());
        dStream.writeBytes(parametros.toString());
        dStream.flush();
        dStream.close();


        int responseCode = conn.getResponseCode();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }
        br.close();
        return responseOutPut.toString();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String obtenerHttpConDatosPatchBasicAutorization(String urlStr, String parametros, String Autorizacion) throws IOException {

        String request = urlStr;
        URL url = new URL(request);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Basic " + Autorizacion.replace("\n", ""));
        conn.setRequestProperty("Accept", "*/*");
        DataOutputStream dStream = new DataOutputStream(conn.getOutputStream());
        dStream.writeBytes(parametros.toString());
        dStream.flush();
        dStream.close();


        int responseCode = conn.getResponseCode();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }
        br.close();

        return responseOutPut.toString();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String obtenerHttpConDatosPutBasicAutorizationCambioPass(String urlStr, String parametros, String Autorizacion) throws IOException {

        String request = urlStr;
        URL url = new URL(request);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(false);
        conn.setRequestMethod("PUT");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("authorization", "Basic " + Autorizacion.replace("\n", ""));
        conn.setRequestProperty("Accept", "*/*");
        DataOutputStream dStream = new DataOutputStream(conn.getOutputStream());
        dStream.writeBytes(parametros.toString());
        dStream.flush();
        dStream.close();

        int responseCode = conn.getResponseCode();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuilder responseOutPut = new StringBuilder();

        while ((line = br.readLine()) != null) {
            responseOutPut.append(line);
        }
        br.close();
        return responseCode + "";
    }
}
