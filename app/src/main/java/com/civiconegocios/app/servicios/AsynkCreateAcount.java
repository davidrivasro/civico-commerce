package com.civiconegocios.app.servicios;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;

import com.civiconegocios.app.BuildConfig;
import com.civiconegocios.app.CrearCuentaActivity;
import com.civiconegocios.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

public class AsynkCreateAcount extends AsyncTask<Void, Void, Boolean> {

    ProgressDialog progressDialog;
    String urlServicio = "/api/v1/user";
    String tocs;
    String email;
    String password;
    String password_confirmation;
    String first_name;
    String last_name;
    String registry_source; //"Mobile Android"
    String city;
    CrearCuentaActivity parent;
    boolean payFlag;

    public AsynkCreateAcount(CrearCuentaActivity parent, String tocs, String email, String password,
                             String password_confirmation, String first_name, String last_name,
                             String registry_source, String city, boolean payFlag) {
        this.parent = parent;
        this.tocs = tocs;
        this.email = email;
        this.password = password;
        this.password_confirmation = password_confirmation;
        this.first_name = first_name;
        this.last_name = last_name;
        this.registry_source = registry_source;
        this.city = city;
        this.payFlag = payFlag;
    }

    @Override
    protected void onPreExecute() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(parent);
            progressDialog.setMessage("Consultando, por favor espere");
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected Boolean doInBackground(Void... params) {


        try {

            URL url = new URL(BuildConfig.OAuth_URL + urlServicio);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            //String urlParameters  = "uid=f0955949f0c4c11aae0b3cec5460183b&estado=si";
            //String urlParameters = "tocs=true&email="+encodeUTF(email)+"&password="+encodeUTF(password)+"&password_confirmation="+encodeUTF(password_confirmation) +"&first_name=" + encodeUTF(first_name)+"&last_name="+encodeUTF(last_name)+"&registry_source=AndroidMobile&city="+encodeUTF(city);

            JSONObject general = new JSONObject();
            try {
                general.put("tocs", "true");
                general.put("tos_pay_confirmation", String.valueOf(this.payFlag));
                general.put("email", encodeUTF(email));
                general.put("password", encodeUTF(password));
                general.put("password_confirmation", encodeUTF(password_confirmation));
                general.put("first_name", encodeUTF(first_name));
                general.put("last_name", encodeUTF(last_name));
                general.put("registry_source", "AndroidMobile");
                general.put("city", encodeUTF(city));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            // convertimos el json en string
            String urlParameters = general.toString();
            connection.setRequestMethod("POST");
            //   connection.setRequestProperty("Content-Type", "application/json");
            //  connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //   connection.setRequestProperty("charset", "utf-8");


            connection.setDoOutput(true);

            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            dStream.writeBytes(urlParameters);
            dStream.flush();
            dStream.close();

            int responseCode = connection.getResponseCode();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = "";
            StringBuilder responseOutPut = new StringBuilder();

            while ((line = br.readLine()) != null) {
                responseOutPut.append(line);
            }
            br.close();

            JSONObject catObj = new JSONObject(responseOutPut.toString());
            // accesToken = catObj.getString("access_token");
            return responseOutPut.toString().length() > 0;

        } catch (ProtocolException e) {
            e.printStackTrace();
            return false;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        // return false;
    }

    @Override
    protected void onCancelled(Boolean aBoolean) {
        super.onCancelled(aBoolean);
        if (aBoolean) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            Utils util = new Utils();
            util.alertErrorTimeOut(parent);
        }
    }

    protected void onProgressUpdate(Void... values) {

    }

    public String encodeUTF(String strEncode) throws UnsupportedEncodingException {
        return URLEncoder.encode(strEncode, "utf-8");
    }


}
