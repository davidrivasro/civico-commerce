package com.civiconegocios.app;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.OfertasRedimidasVO;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class ActivityEstadoCodigoDetalle extends AppCompatActivity implements View.OnClickListener {

    private static final String LBL_REDEEMED_CODE = "lbl_redeemed_code";
    private static final String LBL_EXPIRED_CODE = "lbl_expired_code";
    private static final String NEW_COMMENT = "new_comment";
    private static final String COMUNICATE_WITH_US = "comunicate_with_us";
    private static final String REDEEM_CODE = "redeem_code";
    private static final String REDEEM = "redeem";
    private static final String CALL_USER = "call_user";
    private static final String TOOLBAR_MENU_REDEEMED_DEALS = "toolbar_menu_redeemed_deals";
    private static final String PERMISSION_PHONE_RATIONALE = "permission_phone_rationale";
    private static final String BUTTON_ALLOW = "button_allow";
    private static final String BUTTON_DENY = "button_deny";
    private static final String PERMISSION_CALL_DENIED = "permission_call_denied";
    private static final String PERMISSION_CALL_NEVERASK = "permission_call_neverask";
    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String AVAILABLE_CODE = "available_code";
    private static final String CODE_NOT_AVAILABLE = "code_not_available";
    private static final String DEADLINE = "deadline";
    private static final String DEAL = "deal";
    private static final String PRICE = "price";
    private static final String DISCOUNT = "discount";
    private static final String USER = "user";
    private static final String MAIL = "mail";
    private static final String CODE = "code";
    private static final String USE_FIELD_COMPETANT_HAVE = "use_field_competant_have";
    private static final String COMMENTS = "comments";
    private static final String HAVE_NOT_COMMENTED = "have_not_commented";
    private static final String SEE_ALL_COMMENTS = "see_all_comments";
    private static final String HAVE_QUESTIONS_COUPONS = "have_questions_coupons";
    private static final String LBL_BTN_COMUNICATE_WITH_US = "lbl_btn_comunicate_with_us";
    private static final String EXCHANGE_CODE = "exchange_code";


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    //    UI
    Toolbar defaultToolbar;
    TextView txtCodValido;
    TextView txtCodNoValido;
    TextView txtFechaLimite;
    TextView txtDescripCionOferta;
    TextView txtPrecioEstCodigo;
    TextView txtDescuentoEstCodigo;
    TextView txtUsarioEstCodigo;
    TextView txtTitCorreo;
    TextView txtCorreoEstCodigo;
    TextView txtCodigoEstCodigo;
    TextView txtTitLugar;
    TextView txtLugarEstCodigo;
    TextView txtBtnCanjearCodDisponible;
    TextView txtBtnComunicateCodDisponible;
    TextView txtAtrasEstadoCodigo;
    TextView textOpenSansLight3;
    // otros textviews
    OfertasRedimidasVO ofertasRedimididas;
    TextView txtRojoCodDisponible;
    TextView txtTitComentariosCodDisponible;
    TextView txtComentariosCodDisponible;
    TextView txtBtnVerComentarios;
    TextView txtBtnNuevoComentario;
    TextView txtLlamarDetalle, barcodeIcon, codeDetailDiscountIcon,
            codeStatePercentageIcon, codeStatePriceIcon, codeStateUserIcon,
            codeStateTrustIcon;
    Utils util = new Utils();
    //    Ops
    TrackingOps trackingOps;
    User user;
    Boolean needsValidation = false;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado_codigo_detalle);


        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            needsValidation = extras.getBoolean("needsValidation");
            ofertasRedimididas = (OfertasRedimidasVO) extras.getParcelable("Object");
        }

        defaultToolbar = findViewById(R.id.toolbar);
        defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        setSupportActionBar(defaultToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtCodValido = findViewById(R.id.txtCodValido);
        txtCodValido.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(AVAILABLE_CODE));
        txtCodNoValido = findViewById(R.id.txtCodNoValido);
        txtCodNoValido.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(CODE_NOT_AVAILABLE));
        textOpenSansLight3 = findViewById(R.id.createDealTitle);
        textOpenSansLight3.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(HAVE_QUESTIONS_COUPONS));
        txtFechaLimite = findViewById(R.id.txtFechaLimite);
        txtDescripCionOferta = findViewById(R.id.txtDescripCionOferta);
        txtPrecioEstCodigo = findViewById(R.id.txtPrecioEstCodigo);
        txtDescuentoEstCodigo = findViewById(R.id.txtDescuentoEstCodigo);
        txtUsarioEstCodigo = findViewById(R.id.txtUsarioEstCodigo);
        txtTitCorreo = findViewById(R.id.txtTitCorreo);
        txtTitCorreo.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(MAIL));
        txtCorreoEstCodigo = findViewById(R.id.txtCorreoEstCodigo);
        txtCodigoEstCodigo = findViewById(R.id.txtCodigoEstCodigo);
        txtTitLugar = findViewById(R.id.txtTitLugar);
        txtLugarEstCodigo = findViewById(R.id.txtLugarEstCodigo);
        txtBtnNuevoComentario = findViewById(R.id.txtBtnNuevoComentario);
        txtBtnNuevoComentario.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(NEW_COMMENT).toUpperCase());
        txtBtnCanjearCodDisponible = findViewById(R.id.txtBtnCanjearCodDisponible);
        txtBtnCanjearCodDisponible.setText(trackingOps.getmFirebaseRemoteConfig().getString(EXCHANGE_CODE));
        txtBtnComunicateCodDisponible = findViewById(R.id.createDealBt);
        txtBtnComunicateCodDisponible.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_COMUNICATE_WITH_US));
        txtAtrasEstadoCodigo = findViewById(R.id.txtAtrasEstadoCodigo);
        txtAtrasEstadoCodigo.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        codeStatePercentageIcon = findViewById(R.id.codeStatePercentageIcon);
        codeStatePercentageIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(DEAL));
        codeStatePriceIcon = findViewById(R.id.codeStatePriceIcon);
        codeStatePriceIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(PRICE));
        codeStateUserIcon = findViewById(R.id.codeStateUserIcon);
        codeStateUserIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(USER));
        codeStateTrustIcon = findViewById(R.id.codeStateTrustIcon);
        codeStateTrustIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(DEADLINE));
        // otros
        txtRojoCodDisponible = findViewById(R.id.txtRojoCodDisponible);
        txtRojoCodDisponible.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(USE_FIELD_COMPETANT_HAVE));
        txtTitComentariosCodDisponible = findViewById(R.id.txtTitComentariosCodDisponible);
        txtTitComentariosCodDisponible.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(COMMENTS));
        txtComentariosCodDisponible = findViewById(R.id.txtComentariosCodDisponible);
        txtComentariosCodDisponible.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(HAVE_NOT_COMMENTED));
        txtLlamarDetalle = findViewById(R.id.txtLlamarDetalle);
        txtLlamarDetalle.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(CALL_USER));
        txtComentariosCodDisponible.setText(" " + ofertasRedimididas.getComentarioUno());

        barcodeIcon = findViewById(R.id.barcodeIcon);
        barcodeIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(CODE));
        barcodeIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_codigo), null, null, null);

        txtTitCorreo.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_correo), null, null, null);

        codeDetailDiscountIcon = findViewById(R.id.codeDetailDiscountIcon);
        codeDetailDiscountIcon.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(DISCOUNT));
        codeDetailDiscountIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_descuento), null, null, null);

        txtLlamarDetalle.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_llamar), null, null, null);
        txtTitLugar.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_lugar), null, null, null);
        txtCodNoValido.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_nodisponible), null, null, null);
        codeStatePercentageIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_porcentaje), null, null, null);
        codeStatePriceIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_precio), null, null, null);
        codeStateUserIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_usuarios), null, null, null);
        txtCodValido.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_valido), null, null, null);
        codeStateTrustIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_vigencia), null, null, null);
        txtAtrasEstadoCodigo.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);

        txtBtnVerComentarios = findViewById(R.id.txtBtnVerComentarios);
        txtBtnVerComentarios.setText(trackingOps.getmFirebaseRemoteConfig().getString(SEE_ALL_COMMENTS) + " ");
        txtBtnVerComentarios.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this, R.drawable.ic_pagos_flecha), null);
        txtBtnVerComentarios.setOnClickListener(this);
        txtBtnNuevoComentario.setOnClickListener(this);
        txtBtnComunicateCodDisponible.setOnClickListener(this);
        txtBtnCanjearCodDisponible.setOnClickListener(this);
        txtLlamarDetalle.setOnClickListener(this);
        txtAtrasEstadoCodigo.setOnClickListener(this);
        txtCorreoEstCodigo.setOnClickListener(this);

        if (ofertasRedimididas.getStatus().equals("acquirable")) {
            txtCodValido.setVisibility(View.VISIBLE);
            txtCodNoValido.setVisibility(View.GONE);
            txtBtnCanjearCodDisponible.setVisibility(View.VISIBLE);
            txtBtnNuevoComentario.setVisibility(View.GONE);
        } else if (ofertasRedimididas.getStatus().equals("redeemed")) {
            txtCodValido.setVisibility(View.GONE);
            txtCodNoValido.setVisibility(View.VISIBLE);
            txtCodNoValido.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(LBL_REDEEMED_CODE));
        } else if (ofertasRedimididas.getStatus().equals("expired")) {
            txtCodValido.setVisibility(View.GONE);
            txtCodNoValido.setVisibility(View.VISIBLE);
            txtCodNoValido.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(LBL_REDEEMED_CODE));
        } else {
            txtCodValido.setVisibility(View.GONE);
            txtCodNoValido.setVisibility(View.GONE);
        }

        //if (ofertasRedimididas.getComentarioUno().equals("")){
        txtRojoCodDisponible.setVisibility(View.GONE);
        txtTitComentariosCodDisponible.setVisibility(View.GONE);
        txtComentariosCodDisponible.setVisibility(View.GONE);
        txtBtnVerComentarios.setVisibility(View.VISIBLE);
        //}

        String[] parts = ofertasRedimididas.getRedemption_date().split("T");
        txtFechaLimite.setText(" " + parts[0]);
        txtDescripCionOferta.setText(" " + ofertasRedimididas.getOffer_name());
        txtPrecioEstCodigo.setText(" " + "$" + ofertasRedimididas.getPrice());
        txtDescuentoEstCodigo.setText(" " + "$" + ofertasRedimididas.getDiscount());
        txtUsarioEstCodigo.setText(" " + ofertasRedimididas.getUser_name());
        txtCodigoEstCodigo.setText(" " + ofertasRedimididas.getCode());
        txtCorreoEstCodigo.setText(" " + ofertasRedimididas.getUser_email());

        if (needsValidation) {
            txtRojoCodDisponible.setVisibility(View.GONE);
            txtTitComentariosCodDisponible.setVisibility(View.GONE);
            txtComentariosCodDisponible.setVisibility(View.GONE);
            txtBtnVerComentarios.setVisibility(View.GONE);
            txtBtnNuevoComentario.setVisibility(View.GONE);
        }

        String valido = "";
        if (ofertasRedimididas.getStatus().equals("acquirable")) {
            valido = "Código disponible";
        } else {
            valido = "Código no disponible";
        }

        trackingOps.registerScreenName(valido + " | " + ofertasRedimididas.getOffer_name() + " | " + user.getCustomer().getName() + " | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.txtBtnVerComentarios:
                Intent i = new Intent(this, ActivityComentarios.class);
                i.putExtra("KEY", ofertasRedimididas.getId());
                startActivity(i);
                break;
            case R.id.txtBtnNuevoComentario:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_REDEEMED_DEALS), trackingOps.getmFirebaseRemoteConfig().getString(NEW_COMMENT));
                Intent iN = new Intent(this, ActivityRealizarComentario.class);
                iN.putExtra("KEY", ofertasRedimididas.getId());
                startActivity(iN);
                break;
            case R.id.createDealBt:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(COMUNICATE_WITH_US), trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_REDEEMED_DEALS));
                if (util.mostrarPopUp()) {
                    ActivityEstadoCodigoDetallePermissionsDispatcher.callWithPermissionCheck(this, "2 36 31 11");
                } else {
                    DialogComunicate cdd = new DialogComunicate(this);
                    cdd.show();
                }
                break;
            case R.id.txtBtnCanjearCodDisponible:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(REDEEM_CODE), trackingOps.getmFirebaseRemoteConfig().getString(REDEEM));
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(REDEEM_CODE), ofertasRedimididas.getCode());
                finish();
                Intent iCan = new Intent(this, ActivityCanjearCodigo.class);
                iCan.putExtra("KEY", ofertasRedimididas.getId());
                startActivity(iCan);
                break;
            case R.id.txtLlamarDetalle:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_REDEEMED_DEALS), trackingOps.getmFirebaseRemoteConfig().getString(CALL_USER));
                Intent iT = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + ofertasRedimididas.getMobile_phone()));
                iT.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(iT);
                break;
            case R.id.txtAtrasEstadoCodigo:
                finish();
                break;
            case R.id.txtCorreoEstCodigo:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_REDEEMED_DEALS), "Email");
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", txtCorreoEstCodigo.getText().toString(), null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;
            default:
                break;
        }

    }

    @NeedsPermission(android.Manifest.permission.CALL_PHONE)
    public void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + "031" + phone));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ActivityEstadoCodigoDetallePermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(android.Manifest.permission.CALL_PHONE)
    void showRationaleForRegistarToken(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_PHONE_RATIONALE))
                .setPositiveButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_ALLOW), (dialog, button) -> request.proceed())
                .setNegativeButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_DENY), (dialog, button) -> request.cancel())
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(android.Manifest.permission.CALL_PHONE)
    void showDeniedForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_DENIED), Toast.LENGTH_SHORT).show();
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(android.Manifest.permission.CALL_PHONE)
    void showNeverAskForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_NEVERASK), Toast.LENGTH_SHORT).show();
    }
}
