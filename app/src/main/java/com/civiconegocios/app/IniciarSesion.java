package com.civiconegocios.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.UserDataSerializer;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import io.realm.Realm;

/**
 * Class IniciarSesion
 * <p>
 * <p>Opera la funcionalidad del login de la aplicacion</p>
 *
 * @author Roger Perlaza
 * @version 1.4.4
 * @date update 2018/03/21
 * @implNote implementacion de javadoc
 */
public class IniciarSesion extends BaseActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<String> {

    private static final String AUTENTICACION_FALLIDA = "autenticacion_fallida";
    private static final String ACEPTAR_TERMINOS_CONDICIONES = "aceptar_terminos_condiciones";
    private static final String FORGOT_PASSWORD = "forgot_password";
    private static final String LOGIN = "login";
    private static final String LBL_TERMINOS_INICIAL_PAY = "lbl_terminos_inicial_pay";
    private static final String LBL_ACEPTAS = "lbl_aceptas";
    private static final String LBL_TITLLE_LOGIN_CIVICO = "lbl_titlle_login_civico";
    private static final String LBL_CORREO_INICIAL = "lbl_correo_inicial";
    private static final String PASSWORD = "password";
    private static final String LBL_LNK_FORGET_PASSWORD = "lbl_lnk_forget_password";
    private static final String LBL_BTN_LOGIN = "lbl_btn_login";
    private static final String URL_TERMS_CONDITIONS_CIVIC_PAY = "url_terms_conditions_civic_pay";


    private final String TAG = IniciarSesion.class.getSimpleName();
    //    Loaders IDS
    private final int SIGN_IN_LOADER = 10;
    private final int USER_DATA_REQUEST_LOADER = 11;
    private final int PAY_ACCEPTANCE_LOADER = 12;
    private final int MESSAGES_DATA_LOADER = 13;
    //      Objetos
    LinearLayout btnLogin;
    TextView txtOlvidoContrase;
    TextView txtTituloIngresarCivico;
    TextView txtbtnLogin;
    EditText editTextCorreoInicio;
    EditText editTextPassInicio;
    Utils util = new Utils();
    boolean checkOnPay = true;
    //    Ops
    TrackingOps trackingOps;
    User user;
    String accessToken;
    String urlLogin = "/oauth/token";
    String urlUser = "/users/edit";
    boolean payFlag = false;
    Realm realm;
    private String userData, nameComercio;
    private int sign_in_count;

    /**
     * <p>Llamado cuando la actividad esta comenzando</p>
     *
     * @param savedInstanceState Datos proporcionados mas recientes
     */
    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iniciarsesionactivity);

        trackingOps = (TrackingOps) getApplication();
        realm = trackingOps.getRealm();

        (findViewById(R.id.btnCheckPay)).setOnClickListener(this);

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        txtOlvidoContrase = findViewById(R.id.txtOlvidoContrase);
        txtOlvidoContrase.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_LNK_FORGET_PASSWORD));

        txtTituloIngresarCivico = findViewById(R.id.txtTituloIngresarCivico);
        txtTituloIngresarCivico.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TITLLE_LOGIN_CIVICO));

        txtbtnLogin = findViewById(R.id.txtbtnLogin);
        txtbtnLogin.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_LOGIN));

        editTextCorreoInicio = findViewById(R.id.editTextCorreoInicio);
        editTextCorreoInicio.setHint(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CORREO_INICIAL));
        editTextPassInicio = findViewById(R.id.editTextPassInicio);
        editTextPassInicio.setHint(trackingOps.getmFirebaseRemoteConfig().getString(PASSWORD));
        txtOlvidoContrase.setOnClickListener(this);

        editTextCorreoInicio.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        editTextPassInicio.setTypeface(util.fuenteSubTituHint(getApplicationContext()));


        trackingOps.registerScreenName("Iniciar sesión | Cívico PAY Comercios");
    }

    /**
     * <p>Accion de un llamado que se ejecuta cuando se hace tap en un objeto.</p>
     *
     * @param v Objeto al que se le realiza el tap
     */
    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btnLogin:
                if (checkOnPay) {
                    if (Utils.isValidEmail(editTextCorreoInicio.getText().toString())) {
                        if (util.haveInternet(getApplicationContext())) {
                            makeSignInRequest();
                        } else {
                            util.mensajeNoInternet(getApplicationContext());
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(AUTENTICACION_FALLIDA),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(ACEPTAR_TERMINOS_CONDICIONES),
                            Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.txtOlvidoContrase:

                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(LOGIN), trackingOps.getmFirebaseRemoteConfig().getString(FORGOT_PASSWORD));
                Intent olvido = new Intent(getApplicationContext(), OlvidoPassActivty.class);
                startActivity(olvido);
                break;

            case R.id.btnCheckPay:
                if (checkOnPay) {
                    (findViewById(R.id.btnCheckPay)).setBackgroundResource(R.drawable.ic_btn_check);
                    checkOnPay = false;
                } else {
                    (findViewById(R.id.btnCheckPay)).setBackgroundResource(R.drawable.ic_btn_checkon);
                    checkOnPay = true;
                }
                break;

            case R.id.txtTerminosInicialPay:
                Intent detallePay = new Intent(getApplicationContext(), WebSobreCivicoActivity.class);
                detallePay.putExtra("KEY", URL_TERMS_CONDITIONS_CIVIC_PAY + user.getCity() + "&layout=false");
                startActivity(detallePay);
                break;

            default:
                break;
        }


    }

    /**
     * <p>Se obtiene informacion de usuario y se verifica conexion a internet</p>
     *
     * @param access_token numero de inicio de sesiones
     * @param sign_count   token de inicio de sesion
     */
    public void callGetDataUser(String access_token, int sign_count) {
        this.sign_in_count = sign_count;
        this.accessToken = access_token;

        if (util.haveInternet(getApplicationContext())) {
            makeDataRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }
    }

    /**
     * <p>Verificar el primer inicio de session y solicita porsterior cambio de contrasena</p>
     */
    public void loginOk() {
        if (user.getAcceptingPayments()) {
            payFlag = true;
        }

        if (this.sign_in_count == 1) {
            Intent forgot = new Intent(getApplicationContext(), ActivityCambiarPassInicial.class);
            forgot.putExtra("pass", editTextPassInicio.getText().toString());
            startActivity(forgot);
            editTextCorreoInicio.setText("");
            editTextPassInicio.setText("");
//            finish();
        } else {
            if (user.getAcceptingPayments()) {
                makePayAcceptanceRequest();
            } else {
                startNewActivity();
            }
        }
    }

    /**
     * <p>Inicia el activity inicioActivity si el cliente tiene pagos por la aplicacion activados de lo contrario inicia el activity ActivityListarOfertas</p>
     */
    public void startNewActivity() {

        trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(LOGIN), nameComercio);
        user = trackingOps.getUser();

        OneSignal.setEmail(user.getEmail());

        JSONObject tags = new JSONObject();
        try {
            tags.put("user_name", user.getFirstName());
            tags.put("brand", user.getBrand().getName());
            tags.put("accepting_payments", user.getAcceptingPayments());
            tags.put("mpos_enable", user.getMposEnabled());
            tags.put("city", user.getCity());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OneSignal.sendTags(tags);

        if (user.getAcceptingPayments()) {
            Intent detalle = new Intent(getApplicationContext(), inicioActivity.class);
            startActivity(detalle);
            finish();
        } else {
            Intent detalle = new Intent(getApplicationContext(), ActivityListarOfertas.class);
            startActivity(detalle);
            finish();
        }
    }

    public void makeSignInRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            body.put("grant_type", "password");
            body.put("client_id", Constants.CLIENT_ID);
            body.put("client_secret", Constants.CLIENT_SECRET);
            body.put("username", editTextCorreoInicio.getText().toString());
            body.put("password", editTextPassInicio.getText().toString());

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.OAuth_URL + urlLogin);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(SIGN_IN_LOADER);

            if (loader == null) {
                loaderManager.initLoader(SIGN_IN_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(SIGN_IN_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (Exception e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(this.accessToken);
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, this.urlUser);
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(USER_DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(USER_DATA_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(USER_DATA_REQUEST_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (Exception e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void makePayAcceptanceRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(this.accessToken);
            encoded = encoded.replaceAll("\n", "");
            body.put("tos_pay_confirmation", String.valueOf(this.payFlag));

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "PUT");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + "/users");

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(PAY_ACCEPTANCE_LOADER);

            if (loader == null) {
                loaderManager.initLoader(PAY_ACCEPTANCE_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(PAY_ACCEPTANCE_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (Exception e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void makeMessagesDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, "/business/customers/places/" + user.getBrand().getId() + "/messages?page=1&limit=20&auth_token=" + user.getAccessToken());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader loader = loaderManager.getLoader(MESSAGES_DATA_LOADER);

            if (loader == null) {
                loaderManager.initLoader(MESSAGES_DATA_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(MESSAGES_DATA_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (Exception e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    /**
     * <p>Crea una instancia y devuelve un nuevo cargador para la identificacion dada.</p>
     *
     * @param id   El ID del cargador que se va a crear
     * @param args Argunmento adicionales
     * @return Retorna un nuevo cargador para la identificacion dada.
     */
    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        return new RequestDefaultFactory(this, args);
    }

    /**
     * <p>Acciones que se ejecutan una vez se termine la carga de una activity</p>
     *
     * @param loader Objeto que termino su carga
     * @param data   Datos generados por la carga
     */
    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        toggleProgressDialog(false);
        try {
            switch (loader.getId()) {
                case SIGN_IN_LOADER:
                    if (data == null) {
                        util.alertErrorContrasena(this);
                    } else {
                        try {
                            JSONObject responseObject = new JSONObject(data);
                            accessToken = responseObject.getString("access_token");
                            JSONObject responseObjectUser = responseObject.getJSONObject("user");
                            if (accessToken.length() == 0) {
                                util.alertError500(this);
                            } else {
                                callGetDataUser(accessToken, responseObjectUser.getInt("sign_in_count"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            util.alertError500(this);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            util.alertError500(this);
                        } catch (Exception e) {
                            e.printStackTrace();
                            util.alertError500(this);
                        }
                    }
                    break;
                case USER_DATA_REQUEST_LOADER:
                    if (data == null) {
                        util.alertError500(this);
                    } else {
                        try {
                            UserDataSerializer userDataSerializer = new UserDataSerializer();
                            JSONObject object = userDataSerializer.processData(data, accessToken, util.encode(editTextPassInicio.getText().toString()).replaceAll("\n", ""), this);
                            ((TrackingOps) this.getApplication()).setUser();
                            user = trackingOps.getUser();
                            makeMessagesDataRequest();
                        } catch (IOException e) {
                            e.printStackTrace();
                            util.alertError500(this);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            util.alertError500(this);
                        } catch (Exception e) {
                            e.printStackTrace();
                            util.alertError500(this);
                        }
                    }
                    break;
                case PAY_ACCEPTANCE_LOADER:
                    try {
                        if (data == null) {
                            util.alertError500(this);
                        } else {
                            this.startNewActivity();
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        this.loginOk();
                    } catch (Exception e) {
                        e.printStackTrace();
                        util.alertError500(this);
                    }
                    break;
                case MESSAGES_DATA_LOADER:
                    try {
                        JSONObject dataObject = new JSONObject(data);
                        JSONObject metadata = dataObject.getJSONObject("metadata");
                        trackingOps.setUnreadMessagesCount(metadata.getInt("total_unread_messages"));
                        this.loginOk();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        this.loginOk();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        this.loginOk();
                    } catch (Exception e) {
                        e.printStackTrace();
                        util.alertError500(this);
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }


    /**
     * <p>Se invoca cuando se restablece un cargador</p>
     *
     * @param loader cargador que se restablece
     */
    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }
}
