package com.civiconegocios.app.Ops;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.civiconegocios.app.BuildConfig;
import com.civiconegocios.app.R;
import com.civiconegocios.app.SplashActivity;
import com.civiconegocios.app.models.User;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;

//import com.appspector.sdk.AppSpector;

public class TrackingOps extends MultiDexApplication {

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker, dTracker;
    private final String TAG = TrackingOps.class.getSimpleName();
    public RealmConfiguration configuration;
    public Boolean reloadData = false;
    public Boolean inboxReloadData = false;
    private String jsonProcesoPago = "no";
    private Realm realm;
    private User user;
    private Integer unreadMessagesCount = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        /*
        AppSpector
                .build(this)
                .withDefaultMonitors()
                .run("YzlkNGQ5NmItMTlhNy00ZGMzLTkzNjgtNTQ3ZjgxODE4MWU1");

        AppspectorLogger.AndroidLogger.enableDebugLogging(true);
        */

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignalNotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        Realm.init(this);
        sAnalytics = GoogleAnalytics.getInstance(this);
        setRealm();
        setUser();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            if (BuildConfig.IS_DEBUG) {
                sTracker = sAnalytics.newTracker(R.xml.dev_tracker);
            } else {
                if (user != null) {
                    switch (user.getCity()) {
                        case "mexico":
                            sTracker = sAnalytics.newTracker(R.xml.mexico_tracker);
                            break;
                        case "bogota":
                            sTracker = sAnalytics.newTracker(R.xml.bogota_tracker);
                            break;
                        case "santiago":
                            sTracker = sAnalytics.newTracker(R.xml.santiago_tracker);
                        default:
                            sTracker = sAnalytics.newTracker(R.xml.bogota_tracker);
                            break;
                    }
                } else {
                    sTracker = sAnalytics.newTracker(R.xml.bogota_tracker);
                }

                dTracker = sAnalytics.newTracker(R.xml.default_prod_tracker);
            }
        }
        return sTracker;
    }


    public void registerScreenName(String screenName) {
        if (sTracker == null) {
            sTracker = this.getDefaultTracker();
        }

        if (sTracker != null) {
            sTracker.setScreenName(screenName);
            sTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }

        if (!BuildConfig.IS_DEBUG) {
            if (dTracker == null) {
                dTracker = sAnalytics.newTracker(R.xml.default_prod_tracker);
            }
            dTracker.setScreenName(screenName);
            dTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    public void registerEvent(String category, String action, String labelStr) {
        if (sTracker == null) {
            sTracker = this.getDefaultTracker();
        }

        if (sTracker != null) {
            sTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(labelStr)
                    .build());
        }

        if (!BuildConfig.IS_DEBUG) {
            if (dTracker == null) {
                dTracker = sAnalytics.newTracker(R.xml.default_prod_tracker);
            }

            dTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(labelStr)
                    .build());
        }
    }

    public Realm getRealm() {
        return realm;
    }

    public void setRealm() {
        configuration = new RealmConfiguration.Builder()
                .schemaVersion(4)
                .migration(new RLMTosMigration())
                .build();
        this.realm = Realm.getInstance(configuration);
    }

    public void setUser() {
        RealmQuery<User> query = realm.where(User.class);
        if (query.findFirst() != null) {
            this.user = query.findFirst();
        }

    }

    public User getUser() {
        return this.user;
    }

    public FirebaseRemoteConfig getmFirebaseRemoteConfig() {
        return FirebaseRemoteConfig.getInstance();
    }

    public String getJsonProcesoPago() {
        return jsonProcesoPago;
    }

    public void setJsonProcesoPago(String jsonProcesoPago) {
        this.jsonProcesoPago = jsonProcesoPago;
    }

    public Boolean getReloadData() {
        return reloadData;
    }

    public void setReloadData(Boolean reloadData) {
        this.reloadData = reloadData;
    }

    public RealmConfiguration getConfiguration() {
        return configuration;
    }

    public Boolean getInboxReloadData() {
        return inboxReloadData;
    }

    public void setInboxReloadData(Boolean inboxReloadData) {
        this.inboxReloadData = inboxReloadData;
    }

    public Integer getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public void setUnreadMessagesCount(Integer unreadMessagesCount) {
        this.unreadMessagesCount = unreadMessagesCount;
    }

    private class OneSignalNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;

            Object activityToLaunch = SplashActivity.class;

            Intent launchIntent = new Intent(getApplicationContext(), (Class<?>) activityToLaunch);
            try {
                launchIntent.putExtra("typePush", data.getString("type"));
                String id_message = data.getString("id_message");
                if (!id_message.equals("")) {
                    launchIntent.putExtra("id_message", id_message);
                }
            } catch (Exception ignore) {
            }

            try {
                JSONObject jsonObject = new JSONObject(data.getString("json"));
                launchIntent.putExtra("typePush", jsonObject.getString("type"));
                String id_message = jsonObject.getString("id_message");
                if (!id_message.equals("")) {
                    launchIntent.putExtra("id_message", id_message);
                }
            } catch (Exception ignore) {
            }

            try {
                launchIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(launchIntent);
            } catch (Exception ignore) {
            }
        }
    }
}
