package com.civiconegocios.app.Ops;


import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class RLMTosMigration implements RealmMigration {

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        // DynamicRealm exposes an editable schema
        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            schema.get("User")
                    .addField("tosDate", String.class)
                    .addField("tosPayDate", String.class);
            oldVersion++;
        }

        if (oldVersion == 1) {

            RealmObjectSchema ResponseSchema = schema.create("Response")
                    .addField("id", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("created_at", String.class)
                    .addField("text", String.class)
                    .addField("user_id", String.class);

            schema.create("Message")
                    .addField("id", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("name", String.class)
                    .addField("lastname", String.class)
                    .addField("message", String.class)
                    .addField("place", String.class)
                    .addField("created_at", String.class)
                    .addField("email", String.class)
                    .addField("type", String.class)
                    .addField("related", Boolean.class)
                    .addField("status", String.class)
                    .addField("total_responses", Integer.class)
                    .addRealmListField("responses", ResponseSchema);
            oldVersion++;
        }

        if (oldVersion == 3) {
            schema.get("Customer")
                    .addField("id", String.class)
                    .addField("slug", String.class);
            oldVersion++;
        }
    }
}
