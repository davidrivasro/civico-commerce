package com.civiconegocios.app.item;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.civiconegocios.app.ActivityDetalleTransaccion;
import com.civiconegocios.app.R;
import com.civiconegocios.app.utils.Utils;

import java.util.ArrayList;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;


public class EntryAdapter extends RecyclerView.Adapter<EntryAdapter.ViewHolder> {

    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    Utils util = new Utils();
    private ArrayList<Item> mItems;
    private Context mContext;
    public int icon_espera;
    public int icon_cancelado;
    public int icon_aprobado;

    // Pass in the contact array into the constructor
    public EntryAdapter(Context context, ArrayList<Item> items) {
        mItems = items;
        mContext = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }

    @Override
    public EntryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        int layoutRes = 0;
        switch (viewType) {
            case HEADER_VIEW:
                layoutRes = R.layout.list_item_section;
                break;
            case CONTENT_VIEW:
                layoutRes = R.layout.list_item_entry;
                break;
        }

        View transactionView = inflater.inflate(layoutRes, parent, false);
        return new EntryAdapter.ViewHolder(transactionView);
    }

    @Override
    public void onBindViewHolder(EntryAdapter.ViewHolder holder, int position) {
        final Item itemData = mItems.get(position);

        int viewType = getItemViewType(position);

        final TextView mTitleTextView = holder.titleTextView;
        final TextView subtitle = holder.subtitleTextView;
        final AppCompatImageView imageSection = holder.drawable;
        final TextView goBackEntry = holder.gobackTexView;

        if (itemData != null) {

            if (itemData.isSection()) {
                SectionItem si = (SectionItem) itemData;
                final TextView headerText = holder.headerTextView;
                headerText.setText(si.getTitle());
            } else {
                EntryItem ei = (EntryItem) itemData;
                goBackEntry.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(getContext(), R.drawable.ic_pagos_flecha), null);


                if(ei.origin.equals("mpos")){
                    icon_cancelado = R.drawable.ic_pagos_icon_cancelado;
                    icon_aprobado = R.drawable.ic_pagos_icon_aprobado;
                }else{
                    icon_cancelado = R.drawable.ic_pagos_icon_cancelado_app2app;
                    icon_aprobado = R.drawable.ic_pagos_icon_aprobado_app2app;
                }

                switch (ei.subtitle) {
                    case "0":
                        imageSection.setImageResource(R.drawable.ic_pagos_icon_espera);
                        break;

                    case "1":
                        imageSection.setImageResource(R.drawable.ic_pagos_icon_espera);
                        break;

                    case "2":
                        imageSection.setImageResource(icon_aprobado);
                        break;

                    case "3":
                        imageSection.setImageResource(icon_cancelado);
                        break;

                    case "4":
                        imageSection.setImageResource(icon_cancelado);
                        break;

                    case "5":
                        imageSection.setImageResource(icon_cancelado);
                        break;

                    case "6":
                        imageSection.setImageResource(icon_cancelado);
                        break;

                    case "7":
                        imageSection.setImageResource(icon_aprobado);
                        break;

                    case "8":
                        imageSection.setImageResource(icon_cancelado);
                        break;

                    case "9":
                        imageSection.setImageResource(icon_cancelado);
                        break;
                }

                if (mTitleTextView != null)
                    mTitleTextView.setText("$" + util.miles(ei.title).replaceAll(",", "."));
                if (subtitle != null)
                    subtitle.setText(util.StringToDate(ei.fecha));
            }
        }

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        final Item itemData = mItems.get(position);

        if (itemData.isSection()) {
            return HEADER_VIEW;
        } else {
            return CONTENT_VIEW;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView titleTextView, subtitleTextView, gobackTexView, headerTextView;
        public AppCompatImageView drawable;

        public ViewHolder(View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.list_item_entry_title);
            drawable = itemView.findViewById(R.id.list_item_entry_drawable);
            subtitleTextView = itemView.findViewById(R.id.list_item_entry_summary);
            gobackTexView = itemView.findViewById(R.id.goBackEntry);
            headerTextView = itemView.findViewById(R.id.list_item_section_text);
            itemView.setOnClickListener(this);
        }

        // Handles the row being being clicked
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) {
                final Item itemData = mItems.get(position);
                if (!itemData.isSection()) {
                    EntryItem item = (EntryItem) mItems.get(position);
                    Intent i = new Intent(getContext(), ActivityDetalleTransaccion.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("fechaTotal", item.fechaTotal);
                    i.putExtra("hora", item.fecha);
                    i.putExtra("status", item.subtitle);
                    i.putExtra("monto", item.title); // monto total
                    i.putExtra("pin", item.pin);
                    i.putExtra("taxAmount", item.taxAmount); // iva
                    i.putExtra("numAprobacion", item.numAprobacion);
                    i.putExtra("transId", item.transId);
                    i.putExtra("valorBase", item.amount_base); // monto base
                    i.putExtra("iac", item.iac_amount); // monto base
                    i.putExtra("tip", item.tip_amount); // monto base
                    i.putExtra("origin", item.origin);
                    i.putExtra("reason", item.reason);
                    i.putExtra("description", item.description);
                    getContext().startActivity(i);
                }
            }
        }
    }

}
