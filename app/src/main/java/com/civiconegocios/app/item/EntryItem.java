package com.civiconegocios.app.item;


public class EntryItem implements Item {

    public final String title;
    public final String subtitle;
    public final String fecha;
    public final String fechaTotal;
    public final String pin;
    public final String taxAmount;
    public final String numAprobacion;
    public final String transId;
    public final String amount_base;
    public final String iac_amount;
    public final String tip_amount;
    public final String origin;
    public final String reason;
    public final String description;

    public EntryItem(String title, String subtitle, String fecha, String fechaTotal, String pin,
                     String taxAmount, String numAprobacion, String transId, String amount_base,
                     String iac_amount, String tip_amount, String origin, String reason, String description) {
        this.title = title; // valor total
        this.subtitle = subtitle;
        this.fecha = fecha;
        this.fechaTotal = fechaTotal;
        this.pin = pin;
        this.taxAmount = taxAmount; // valor del iva
        this.numAprobacion = numAprobacion;
        this.transId = transId;
        this.amount_base = amount_base;
        this.iac_amount = iac_amount;
        this.tip_amount = tip_amount;
        this.origin = origin;
        this.reason = reason;
        this.description = description;
    }

    @Override
    public boolean isSection() {
        return false;
    }

}
