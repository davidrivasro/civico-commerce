package com.civiconegocios.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

public class WebSobreCivicoActivity extends BaseActivity implements View.OnClickListener {

    private static final String MSJ_LOADING = "msj_loading";
    private static final String URL_ACADEMY_CIVICO = "url_academy_civico";
    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    //    UI
    Toolbar defaultToolbar;
    WebView webView;
    String value;
    TextView txtBack, txtAtrasComentarios;
    Utils util = new Utils();
    TrackingOps trackingOps;
    private ProgressDialog progressDialog;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_sobre_civico_activity);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("KEY");
        }

        trackingOps = (TrackingOps) getApplication();

        txtBack = findViewById(R.id.txtAtrasComentarios);

        if (value.equals(trackingOps.getmFirebaseRemoteConfig().getString(URL_ACADEMY_CIVICO))) {
            initDrawer();
            txtBack.setVisibility(View.GONE);
        }

        txtBack.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        txtBack.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);

        txtAtrasComentarios = findViewById(R.id.txtAtrasComentarios);
        txtAtrasComentarios.setOnClickListener(this);

        webView = findViewById(R.id.webSobre);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setWebViewClient(new WebViewClient());
        /* String postData = "token_mobile="+token.getToken()+data.getData();
        webview.postUrl(url,EncodingUtils.getBytes(postData, "utf-8"));*/
        //   webview.loadUrl(url_enlace.getUrl());
        webView.loadUrl(value);
        progressDialog = ProgressDialog.show(WebSobreCivicoActivity.this, "", trackingOps.getmFirebaseRemoteConfig().getString(MSJ_LOADING));
        webView.setWebViewClient(new WebViewClient() {


            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                // http://autoniza.com/promociones?promo=F-I
                //  http://www.asistenciachat.com/chat_dian/beginchat.aspx

                progressDialog.dismiss();

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txtAtrasComentarios) {
            finish();
        }
    }

}
