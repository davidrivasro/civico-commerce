package com.civiconegocios.app;

import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.core.content.ContextCompat;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;


public class ActivityRealizarComentario extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<String> {

    private static final String MSJ_COMMENT_ADDED_CORRECTLY = "msj_comment_added_correctly";
    private static final String MSJ_TRY_AGAIN = "msj_try_again";
    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String MAKE_YOUR_COMMENT = "make_your_comment";
    private static final String START_WRITING = "start_writing";
    private static final String SAVE_COMMMENT = "save_commment";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    EditText editRealizarComentario;
    TextView txtBtnGuardarComentario;
    TextView txtBtnGuardarComentarioInactivo;
    TextView txtAtrasRealizarComentario;
    TextView textOpenSansRegular6;
    String value;
    Utils util = new Utils();
    Toolbar defaultToolbar;
    User user;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realizar_comentario);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("KEY");
        }

        trackingOps = (TrackingOps) getApplication();
        user = ((TrackingOps) getApplication()).getUser();

        defaultToolbar = findViewById(R.id.toolbar);

        defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        setSupportActionBar(defaultToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        textOpenSansRegular6 = findViewById(R.id.textOpenSansRegular6);
        textOpenSansRegular6.setText(trackingOps.getmFirebaseRemoteConfig().getString(MAKE_YOUR_COMMENT));

        editRealizarComentario = findViewById(R.id.editRealizarComentario);
        editRealizarComentario.setHint(trackingOps.getmFirebaseRemoteConfig().getString(START_WRITING));
        editRealizarComentario.setTypeface(util.fuenteHeader(getApplicationContext()));
        txtBtnGuardarComentario = findViewById(R.id.txtBtnGuardarComentario);
        txtBtnGuardarComentario.setText(trackingOps.getmFirebaseRemoteConfig().getString(SAVE_COMMMENT).toUpperCase());
        txtBtnGuardarComentarioInactivo = findViewById(R.id.txtBtnGuardarComentarioInactivo);
        txtBtnGuardarComentarioInactivo.setText(trackingOps.getmFirebaseRemoteConfig().getString(SAVE_COMMMENT).toUpperCase());
        txtAtrasRealizarComentario = findViewById(R.id.txtAtrasRealizarComentario);
        txtAtrasRealizarComentario.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        txtBtnGuardarComentario.setOnClickListener(this);
        txtAtrasRealizarComentario.setOnClickListener(this);
        txtAtrasRealizarComentario.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);

        editRealizarComentario.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (s.length() > 0) {
                    txtBtnGuardarComentario.setVisibility(View.VISIBLE);
                    txtBtnGuardarComentarioInactivo.setVisibility(View.GONE);
                } else if (s.length() == 0) {
                    txtBtnGuardarComentario.setVisibility(View.GONE);
                    txtBtnGuardarComentarioInactivo.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtBtnGuardarComentario:
                JSONObject manJson = new JSONObject();
                try {
                    manJson.put("text", editRealizarComentario.getText().toString());
                    if (util.haveInternet(getApplicationContext())) {
                        makeDataRequest();
                    } else {
                        util.mensajeNoInternet(getApplicationContext());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.txtAtrasRealizarComentario:
                finish();
                break;
            default:
                break;
        }
    }

    public void respuestaCorrecta() {
        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(MSJ_COMMENT_ADDED_CORRECTLY),
                Toast.LENGTH_SHORT).show();
        finish();
    }

    public void respuestaInCorrecta() {
        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(MSJ_TRY_AGAIN),
                Toast.LENGTH_SHORT).show();
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            body.put("text", editRealizarComentario.getText().toString());

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + "/brand/" + user.getBrand().getId() + "/offer_coupons/" + value + "/comments");

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(0);

            if (loader == null) {
                loaderManager.initLoader(0, queryBundle, this);
            } else {
                loaderManager.restartLoader(0, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        toggleProgressDialog(false);
        if (data == null) {
            util.alertErrorTimeOut(this);
        } else {
            try {
                JSONObject catObj = new JSONObject(data);
                String response = catObj.get("success") + "";
                if (response.equals("true")) {
                    respuestaCorrecta();
                } else {
                    respuestaInCorrecta();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                respuestaInCorrecta();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }
}
