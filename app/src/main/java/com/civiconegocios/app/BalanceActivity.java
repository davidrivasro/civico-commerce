package com.civiconegocios.app;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.BalanceVO;
import com.civiconegocios.app.adapters.AdapterBalance;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.BalanceDataSerializer;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class BalanceActivity extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks {

    private static final String PERMISSION_PHONE_RATIONALE = "permission_phone_rationale";
    private static final String BUTTON_ALLOW = "button_allow";
    private static final String BUTTON_DENY = "button_deny";
    private static final String PERMISSION_CALL_DENIED = "permission_call_denied";
    private static final String PERMISSION_CALL_NEVERASK = "permission_call_neverask";
    private static final String TOOLBAR_MENU_BALANCE = "toolbar_menu_balance";
    private static final String PENDING_TRANSFER = "pending_transfer";
    private static final String NEXT_COURT = "next_court";
    private static final String TOTAL_VALUE = "total_value";
    private static final String TAX_RETENTION = "tax_retention";
    private static final String RENT_RETENTION = "rent_retention";
    private static final String ICA_RETENTION = "ica_retention";
    private static final String OTHER_DISCOUNTS = "other_discounts";
    private static final String VALUE_TRANSFER = "value_transfer";
    private static final String COPY_BALANCE = "copy_balance";
    private static final String LAST_TRANSFERS = "last_transfers";
    private static final String TOTAL_TRANSFERRED = "total_transferred";
    private static final String COPY_BALANCE_AYUDA = "copy_balance_ayuda";
    private static final String COPY_BALANCE_AYUDA_NUMERO = "copy_balance_ayuda_numero";

    //    Loaders IDS
    private final int DATA_REQUEST_LOADER = 10;
    private final int DATA_SERIALIZER_LOADER = 11;
    //    UI
    Toolbar defaultToolbar;
    List<BalanceVO> balanceList;
    Utils util = new Utils();
    TextView txtTituloPendiente;
    TextView txtTituloProximo;
    TextView txtPendienteBalance;
    TextView txtTituloTotal;
    TextView txtTituloFecha;
    TextView txtTotalTransfer;
    TextView txtUltimasTransfer;
    TextView txtTitValorTotalBalance;
    TextView txtValorTotalBalance;
    // nuevos texview
    TextView txtTitReteIvaBalance;
    TextView txtValorReteIvaBalance;
    TextView txtTitReterentaBalance;
    TextView txtValorReterentaBalance;
    TextView txtTitReteICA;
    TextView txtValorReteICA;
    TextView txtTituloValorTotalDef;
    TextView txtAunPagos;
    TextView txtEmpiezarealizar;
    TextView txtValorOtrosDescuentos;
    TextView txtBalanceNumLlamar;
    TextView toolbar_title;
    TextView txtTitOtrosDescuentos;
    TextView textOpenSansLight9;
    TextView textOpenSansLight8;
    RecyclerView listUltimasTransferencias;
    AdapterBalance adapterbalance;
    //    Ops
    TrackingOps trackingOps;
    User user;
    String balanceData;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.balance_activity);

        initDrawer();
        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BALANCE));

        txtTituloPendiente = findViewById(R.id.txtTituloPendiente);
        txtTituloPendiente.setText(trackingOps.getmFirebaseRemoteConfig().getString(PENDING_TRANSFER));
        txtTituloProximo = findViewById(R.id.txtTituloProximo);
        txtTituloProximo.setText(trackingOps.getmFirebaseRemoteConfig().getString(NEXT_COURT));
        txtPendienteBalance = findViewById(R.id.txtPendienteBalance);
        txtTituloTotal = findViewById(R.id.txtTituloTotal);
        txtTituloTotal.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_TRANSFERRED));
        txtTituloFecha = findViewById(R.id.txtTituloFecha);
        txtTotalTransfer = findViewById(R.id.txtTotalTransfer);
        txtUltimasTransfer = findViewById(R.id.txtUltimasTransfer);
        txtUltimasTransfer.setText(trackingOps.getmFirebaseRemoteConfig().getString(LAST_TRANSFERS));

        // nuevos text

        txtTitValorTotalBalance = findViewById(R.id.txtTitValorTotalBalance);
        txtTitValorTotalBalance.setText(trackingOps.getmFirebaseRemoteConfig().getString(VALUE_TRANSFER));
        txtValorTotalBalance = findViewById(R.id.txtValorTotalBalance);
        txtTituloValorTotalDef = findViewById(R.id.txtTituloValorTotalDef);
        txtTituloValorTotalDef.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE));

        txtTitReteIvaBalance = findViewById(R.id.txtTitReteIvaBalance);
        txtTitReteIvaBalance.setText(trackingOps.getmFirebaseRemoteConfig().getString(TAX_RETENTION));
        txtValorReteIvaBalance = findViewById(R.id.txtValorReteIvaBalance);
        txtTitReterentaBalance = findViewById(R.id.txtTitReterentaBalance);
        txtTitReterentaBalance.setText(trackingOps.getmFirebaseRemoteConfig().getString(RENT_RETENTION));
        txtValorReterentaBalance = findViewById(R.id.txtValorReterentaBalance);
        txtTitReteICA = findViewById(R.id.txtTitReteICA);
        txtTitReteICA.setText(trackingOps.getmFirebaseRemoteConfig().getString(ICA_RETENTION));
        txtValorReteICA = findViewById(R.id.txtValorReteICA);
        txtTitOtrosDescuentos = findViewById(R.id.txtTitOtrosDescuentos);
        txtTitOtrosDescuentos.setText(trackingOps.getmFirebaseRemoteConfig().getString(OTHER_DISCOUNTS));
        txtValorOtrosDescuentos = findViewById(R.id.txtValorOtrosDescuentos);
        txtBalanceNumLlamar = findViewById(R.id.txtBalanceNumLlamar);
        txtBalanceNumLlamar.setText(trackingOps.getmFirebaseRemoteConfig().getString(COPY_BALANCE_AYUDA_NUMERO));
        txtBalanceNumLlamar.setOnClickListener(this);

        textOpenSansLight9 = findViewById(R.id.textOpenSansLight9);
        textOpenSansLight9.setText(trackingOps.getmFirebaseRemoteConfig().getString(COPY_BALANCE));

        textOpenSansLight8 = findViewById(R.id.textOpenSansLight8);
        textOpenSansLight8.setText(trackingOps.getmFirebaseRemoteConfig().getString(COPY_BALANCE_AYUDA));

        txtTituloFecha.setText("Al " + DateToString().replace(" ", " de "));

        listUltimasTransferencias = findViewById(R.id.listUltimasTransferencias);
        balanceList = new ArrayList<BalanceVO>();
        adapterbalance = new AdapterBalance(this, balanceList);
        listUltimasTransferencias.setAdapter(adapterbalance);
        listUltimasTransferencias.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        listUltimasTransferencias.addItemDecoration(itemDecorator);


        if (util.haveInternet(getApplicationContext())) {
            makeDataRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }
        BitmapFactory.Options dimensions = new BitmapFactory.Options();
        dimensions.inJustDecodeBounds = true;

        trackingOps.registerScreenName("Balance | Cívico PAY Comercios | " + user.getAnalyticsCity());

    }

    public void recibirBalance(List<BalanceVO> balanceList) {
        txtPendienteBalance.setText("$" + balanceList.get(0).getPending_to_transfer_amount());
        txtValorTotalBalance.setText("$" + balanceList.get(0).getPending_to_transfer_total_transfer());
        //txtValorTotalBalance.setText("$"+balanceList.get(0).getTotal_transfered_amount());
        txtValorReteIvaBalance.setText("$" + balanceList.get(0).getPending_to_transfer_reteiva());
        txtValorReteICA.setText("$" + balanceList.get(0).getPending_to_transfer_reteica());
        txtValorOtrosDescuentos.setText("$" + balanceList.get(0).getPending_to_transfer_discounts());
        txtValorReterentaBalance.setText("$" + balanceList.get(0).getPending_to_transfer_reterenta());

        txtTotalTransfer.setText("$" + balanceList.get(0).getTotal_transfered_amount());

        final List<BalanceVO> lastTranfers = balanceList.get(0).getListBalanceTemp();
        this.balanceList.addAll(lastTranfers);


//        listUltimasTransferencias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View v,
//                                    int position, long id) {
//                BalanceVO voBalance = lastTranfers.get(position);
//                Intent i = new Intent(getApplicationContext(), ActivityDetalleBalance.class);
//                i.putExtra("fechaTotal", voBalance.getUpdated_at());
//                i.putExtra("valorTotal", voBalance.getAmount());
//                i.putExtra("reteIva", voBalance.getReteiva());
//                i.putExtra("reteRenta", voBalance.getReterenta());
//                i.putExtra("reteIca", voBalance.getReteica());
//                i.putExtra("valorTranferido", voBalance.getTotal_transfer());
//                i.putExtra("otrosDescuentos", voBalance.getDiscounts());
//                startActivity(i);
//            }
//        });

        adapterbalance.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtBalanceNumLlamar:
                BalanceActivityPermissionsDispatcher.callWithPermissionCheck(this, "031 " + txtBalanceNumLlamar.getText().toString());
                break;
            default:
                break;
        }

    }

    public String DateToString() {
        Date d = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd MMMM yyyy");
        String datetime = "";
        try {
            datetime = dateformat.format(d);
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return datetime;
    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    public void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        BalanceActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.CALL_PHONE)
    void showRationaleForRegistarToken(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_PHONE_RATIONALE))
                .setPositiveButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_ALLOW), (dialog, button) -> request.proceed())
                .setNegativeButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_DENY), (dialog, button) -> request.cancel())
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(Manifest.permission.CALL_PHONE)
    void showDeniedForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_DENIED), Toast.LENGTH_SHORT).show();
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.CALL_PHONE)
    void showNeverAskForPhoneCall() {
        Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_CALL_NEVERASK), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, "/customers/balance");
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(DATA_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(DATA_REQUEST_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    public void makeDataSerialization() {
        Bundle queryBundle = new Bundle();
        queryBundle.putString("data", this.balanceData);
        LoaderManager loaderManager = getSupportLoaderManager();
        Loader loader = loaderManager.getLoader(DATA_SERIALIZER_LOADER);

        if (loader == null) {
            loaderManager.initLoader(DATA_SERIALIZER_LOADER, queryBundle, this);
        } else {
            loaderManager.restartLoader(DATA_SERIALIZER_LOADER, queryBundle, this);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        switch (id) {
            case DATA_REQUEST_LOADER:
                return new RequestDefaultFactory(this, args);
            case DATA_SERIALIZER_LOADER:
                return new BalanceDataSerializer(this, args);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        toggleProgressDialog(false);
        switch (loader.getId()) {
            case DATA_REQUEST_LOADER:
                this.balanceData = String.valueOf(data);
                makeDataSerialization();
                break;
            case DATA_SERIALIZER_LOADER:
                if (data instanceof List<?>) {
                    List<BalanceVO> balanceList = (List<BalanceVO>) data;
                    recibirBalance(balanceList);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        toggleProgressDialog(false);
    }
}
