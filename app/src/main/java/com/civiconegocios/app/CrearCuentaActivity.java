package com.civiconegocios.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.servicios.AsynkCreateAcount;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import java.util.ArrayList;
import java.util.List;

public class CrearCuentaActivity extends Activity implements View.OnClickListener {

    private static final String MSJ_VALID_EMAIL = "msj_valid_email";
    private static final String PASS_DESIGUALES = "pass_desiguales";
    private static final String MSJ_EMPTY_FIELDS = "msj_empty_fields";
    private static final String MSJ_ACCOUNT_CREATED_CORRECTLY = "msj_account_created_correctly";
    private static final String MSJ_INVALID_EMAIL = "msj_invalid_email";
    private static final String MSJ_WRONG_ACCOUNT = "msj_wrong_account";
    private static final String LBL_TITLLE_START_USING_CIVICO = "lbl_titlle_start_using_civico";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String CITY = "city";
    private static final String LBL_CORREO_INICIAL = "lbl_correo_inicial";
    private static final String PASSWORD = "password";
    private static final String CONFIRM_PASSWORD = "confirm_password";
    private static final String DATA_TRADE = "data_trade";
    private static final String NAME_BUSINESS = "name_business";
    private static final String ADDRESS = "address";
    private static final String CREATING_ACCOUNT_ACCEPT = "creating_account_accept";
    private static final String LBL_TERMINOS_INICIAL = "lbl_terminos_inicial";
    private static final String LBL_TERMINOS_INICIAL_PAY = "lbl_terminos_inicial_pay";
    private static final String LBL_BTN_CREATE_ACCOUNT = "lbl_btn_create_account";
    private static final String URL_TERMS_CONDITIONS = "url_terms_conditions";
    private static final String URL_BETA_TERMS_CONDITIONS_CIVIC_PAY = "url_beta_terms_conditions_civic_pay";
    TextView txtTerminosCrearCuenta, txtTerminosCrearCuentaPay, textOpenSansRegular, txtTitleCeldaPerfil, titleCrearCuenta, titleCrearCuentaPay;
    TextView btnCreateAccount;
    EditText editTextNombreCrear;
    EditText editTextApellidoCrear;
    EditText editTextCiudadCrear;
    EditText editTextCorreoCrear;
    EditText editTexPswCrear;
    EditText editTexPswComCrear;
    EditText editTexNombreNegocioCrear;
    EditText editTexDireccionCrear;
    List<EditText> editaArray;
    LinearLayout lnBtnCrearCuentaUser;
    Utils util = new Utils();
    TrackingOps trackingOps;

    public static String Trim(EditText v) {
        return v.getText().toString().trim();
    }

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crear_cuenta_activity);

        trackingOps = (TrackingOps) getApplication();

        txtTerminosCrearCuenta = findViewById(R.id.txtTerminosCrearCuenta);
        txtTerminosCrearCuenta.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
        titleCrearCuentaPay = findViewById(R.id.titleCrearCuentaPay);
        titleCrearCuentaPay.setText(trackingOps.getmFirebaseRemoteConfig().getString(CREATING_ACCOUNT_ACCEPT));
        txtTerminosCrearCuenta.setOnClickListener(this);
        txtTerminosCrearCuentaPay = findViewById(R.id.txtTerminosCrearCuentaPay);
        txtTerminosCrearCuentaPay.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL_PAY));
        txtTerminosCrearCuentaPay.setOnClickListener(this);

        textOpenSansRegular = findViewById(R.id.tv_fecha);
        textOpenSansRegular.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TITLLE_START_USING_CIVICO));

        editTextNombreCrear = findViewById(R.id.editTextNombreCrear);
        editTextNombreCrear.setText(trackingOps.getmFirebaseRemoteConfig().getString(FIRST_NAME));
        editTextApellidoCrear = findViewById(R.id.editTextApellidoCrear);
        editTextApellidoCrear.setText(trackingOps.getmFirebaseRemoteConfig().getString(LAST_NAME));
        editTextCiudadCrear = findViewById(R.id.editTextCiudadCrear);
        editTextCiudadCrear.setText(trackingOps.getmFirebaseRemoteConfig().getString(CITY));
        editTextCorreoCrear = findViewById(R.id.editTextCorreoCrear);
        editTextCorreoCrear.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CORREO_INICIAL));
        editTexPswCrear = findViewById(R.id.editTexPswCrear);
        editTexPswCrear.setText(trackingOps.getmFirebaseRemoteConfig().getString(PASSWORD));
        editTexPswComCrear = findViewById(R.id.editTexPswComCrear);
        editTexPswComCrear.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONFIRM_PASSWORD));
        editTexNombreNegocioCrear = findViewById(R.id.editTexNombreNegocioCrear);
        editTexNombreNegocioCrear.setText(trackingOps.getmFirebaseRemoteConfig().getString(NAME_BUSINESS));
        editTexDireccionCrear = findViewById(R.id.editTexDireccionCrear);
        editTexDireccionCrear.setText(trackingOps.getmFirebaseRemoteConfig().getString(ADDRESS));
        txtTitleCeldaPerfil = findViewById(R.id.txtTitleCeldaPerfil);
        txtTitleCeldaPerfil.setText(trackingOps.getmFirebaseRemoteConfig().getString(DATA_TRADE));
        titleCrearCuenta = findViewById(R.id.titleCrearCuenta);
        titleCrearCuenta.setText(trackingOps.getmFirebaseRemoteConfig().getString(CREATING_ACCOUNT_ACCEPT));

        btnCreateAccount = findViewById(R.id.btnCreateAccount);
        btnCreateAccount.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CREATE_ACCOUNT));

        editaArray = new ArrayList<EditText>();
        editaArray.add(editTextNombreCrear);
        editaArray.add(editTextApellidoCrear);
        editaArray.add(editTextCiudadCrear);
        editaArray.add(editTextCorreoCrear);
        editaArray.add(editTexPswCrear);
        editaArray.add(editTexPswComCrear);

        lnBtnCrearCuentaUser = findViewById(R.id.lnBtnCrearCuentaUser);
        lnBtnCrearCuentaUser.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtTerminosCrearCuenta:
                Intent detalle = new Intent(getApplicationContext(), WebSobreCivicoActivity.class);
                detalle.putExtra("KEY", trackingOps.getmFirebaseRemoteConfig().getString(URL_TERMS_CONDITIONS) + "bogota&layout=false");
                startActivity(detalle);
                break;

            case R.id.txtTerminosCrearCuentaPay:
                Intent detallePay = new Intent(getApplicationContext(), WebSobreCivicoActivity.class);
                detallePay.putExtra("KEY", trackingOps.getmFirebaseRemoteConfig().getString(URL_BETA_TERMS_CONDITIONS_CIVIC_PAY) + "bogota&layout=false");
                startActivity(detallePay);
                break;

            case R.id.lnBtnCrearCuentaUser:
                if (Validar()) {
                    if (Utils.isValidEmail(editTextCorreoCrear.getText().toString())) {
                        Toast.makeText(getApplicationContext(), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(MSJ_VALID_EMAIL),
                                Toast.LENGTH_SHORT).show();
                        if (editTexPswCrear.getText().toString().equals(editTexPswComCrear.getText().toString())) {
                            //String tocs, String email,String password, String password_confirmation,String first_name,String last_name,String registry_source,String city
                            if (util.haveInternet(getApplicationContext())) {
                                AsynkCreateAcount asynk = new AsynkCreateAcount(this, "true", editTextCorreoCrear.getText().toString(), editTexPswCrear.getText().toString(), editTexPswCrear.getText().toString(), editTextNombreCrear.getText().toString(), editTextApellidoCrear.getText().toString(), "mobiile", editTextCiudadCrear.getText().toString(), true);
                                asynk.execute();
                            } else {
                                util.mensajeNoInternet(getApplicationContext());
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(PASS_DESIGUALES),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(MSJ_INVALID_EMAIL),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(MSJ_EMPTY_FIELDS),
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    public boolean Validar() {
        boolean validar = true;
        for (int a = 0; a < editaArray.size(); a++) {
            if (editaArray.get(a).getText().toString().equals("")) {
                validar = false;
            }
        }
        return validar;
    }

    public void cuentaExitosa() {
        Toast.makeText(getApplicationContext(), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(MSJ_ACCOUNT_CREATED_CORRECTLY),
                Toast.LENGTH_SHORT).show();
    }

    public void cuentaNoExitosa() {
        Toast.makeText(getApplicationContext(), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(MSJ_WRONG_ACCOUNT),
                Toast.LENGTH_SHORT).show();
    }
}
