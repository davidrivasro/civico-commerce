package com.civiconegocios.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.adapters.SnapMessageItemAdapter;
import com.civiconegocios.app.models.Message;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.InboxDataSerializer;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class InboxActivity extends BaseActivity implements LoaderManager.LoaderCallbacks {

    private static final String INBOX_TITLE = "inbox_title";
    private static final String EMPTY_INBOX = "empty_inbox";

    //    Loaders IDS
    private final int DATA_REQUEST_LOADER = 10;
    private final int DATA_SERIALIZER_LOADER = 20;

    TextView toolbarTitle, notMessagesTxt;
    RecyclerView inboxRecyclerView;
    SnapMessageItemAdapter snapAdapter;
    RealmResults<Message> messages;
    LinearLayout notMessagesContainer;
    Integer total_pages = 0;
    Integer actualPage = 1;
    User user;
    Realm realm;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        initDrawer();
        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();
        realm = trackingOps.getRealm();

        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarTitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(INBOX_TITLE));

        notMessagesTxt = findViewById(R.id.notMessagesTxt);
        notMessagesTxt.setText(trackingOps.getmFirebaseRemoteConfig().getString(EMPTY_INBOX));

        notMessagesContainer = findViewById(R.id.notMessagesContainer);

        messages = realm.where(Message.class)
                .equalTo("status", "unread")
                .or()
                .equalTo("status", "read")
                .sort("created_at", Sort.DESCENDING)
                .findAll();

        inboxRecyclerView = findViewById(R.id.inboxRecyclerView);
        snapAdapter = new SnapMessageItemAdapter(this, messages);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        inboxRecyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        inboxRecyclerView.addItemDecoration(itemDecorator);
        inboxRecyclerView.setHasFixedSize(true);
        inboxRecyclerView.setAdapter(snapAdapter);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (actualPage < total_pages) {
                    actualPage += 1;
                    makeDataRequest();
                }
            }
        };

        inboxRecyclerView.addOnScrollListener(scrollListener);

        if (messages.isEmpty()) {
            if (util.haveInternet(getApplicationContext())) {
                toggleProgressDialog(true);
                makeDataRequest();
            } else {
                util.mensajeNoInternet(getApplicationContext());
            }

        } else {
            snapAdapter.notifyDataSetChanged();
            makeDataRequest();
        }


        try {

            String id_message = getIntent().getExtras().getString("id_message");


            if (id_message != null) {

                Intent i = new Intent(getApplicationContext(), MessageDetailActivity.class);
                i.putExtra("id", id_message);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //util.alertError500(this);
        }


    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, "/business/customers/places/" + user.getBrand().getId() + "/messages?page=" + actualPage + "&limit=20&auth_token=" + user.getAccessToken());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader loader = loaderManager.getLoader(DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(DATA_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(DATA_REQUEST_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        switch (id) {
            case DATA_REQUEST_LOADER:
                return new RequestDefaultFactory(this, args);
            case DATA_SERIALIZER_LOADER:
                return new InboxDataSerializer(this, args);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        switch (loader.getId()) {
            case DATA_REQUEST_LOADER:
                Bundle queryBundle = new Bundle();
                queryBundle.putString("data", String.valueOf(data));

                if (String.valueOf(data).equals("")) {
                    notMessagesContainer.setVisibility(View.VISIBLE);
                    inboxRecyclerView.setVisibility(View.GONE);
                } else {
                    notMessagesContainer.setVisibility(View.GONE);
                    inboxRecyclerView.setVisibility(View.VISIBLE);
                }

                try {
                    JSONObject dataObject = new JSONObject(String.valueOf(data));
                    JSONObject metadata = dataObject.getJSONObject("metadata");
                    actualPage = metadata.getInt("page");
                    total_pages = metadata.getInt("total_pages");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                LoaderManager loaderManager = getSupportLoaderManager();
                Loader serializerLoader = loaderManager.getLoader(DATA_SERIALIZER_LOADER);

                if (serializerLoader == null) {
                    loaderManager.initLoader(DATA_SERIALIZER_LOADER, queryBundle, this);
                } else {
                    loaderManager.restartLoader(DATA_SERIALIZER_LOADER, queryBundle, this);
                }
                break;
            case DATA_SERIALIZER_LOADER:
                messages = realm.where(Message.class)
                        .equalTo("status", "unread")
                        .or()
                        .equalTo("status", "read")
                        .sort("created_at", Sort.DESCENDING)
                        .findAll();
                if (messages.isEmpty()) {
                    notMessagesContainer.setVisibility(View.VISIBLE);
                    inboxRecyclerView.setVisibility(View.GONE);
                } else {
                    snapAdapter.notifyDataSetChanged();
                }
                updateUnreadMessagesBadge();
                toggleProgressDialog(false);
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        toggleProgressDialog(false);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus && trackingOps.getInboxReloadData()) {
            trackingOps.setInboxReloadData(false);
            messages = realm.where(Message.class)
                    .equalTo("status", "unread")
                    .or()
                    .equalTo("status", "read")
                    .sort("created_at", Sort.DESCENDING)
                    .findAll();
            snapAdapter.notifyDataSetChanged();
            updateUnreadMessagesBadge();
            makeDataRequest();

        }
    }
}
