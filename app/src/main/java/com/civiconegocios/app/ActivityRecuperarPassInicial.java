package com.civiconegocios.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

public class ActivityRecuperarPassInicial extends Activity implements View.OnClickListener {

    private static final String LBL_RECUPERAR_PASS_INICIAL = "lbl_recuperar_pass_inicial";
    private static final String LBL_CORREO_INICIAL = "lbl_correo_inicial";
    private static final String LBL_BTN_RECUPERAR_PASS_INICIAL = "lbl_btn_recuperar_pass_inicial";
    TextView txtRecuperarPassInicial;
    EditText editCorreoPassInicial;
    TextView txtbtnRecuperarPassInicial;
    Utils util = new Utils();
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_pass_inicial);

        trackingOps = (TrackingOps) getApplicationContext();

        txtRecuperarPassInicial = findViewById(R.id.txtRecuperarPassInicial);
        txtRecuperarPassInicial.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_RECUPERAR_PASS_INICIAL));
        editCorreoPassInicial = findViewById(R.id.editCorreoPassInicial);
        editCorreoPassInicial.setHint(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CORREO_INICIAL));
        txtbtnRecuperarPassInicial = findViewById(R.id.txtbtnRecuperarPassInicial);
        txtbtnRecuperarPassInicial.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_RECUPERAR_PASS_INICIAL));

        txtRecuperarPassInicial.setTypeface(util.fuenteHeader(getApplicationContext()));
        editCorreoPassInicial.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtbtnRecuperarPassInicial.setTypeface(util.fuenteHeader(getApplicationContext()));

        txtbtnRecuperarPassInicial.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtbtnRecuperarPassInicial:
                finish();
                break;
            default:
                break;
        }

    }
}
