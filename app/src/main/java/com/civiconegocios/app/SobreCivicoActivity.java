package com.civiconegocios.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

public class SobreCivicoActivity extends BaseActivity implements View.OnClickListener {

    private static final String ABOUT_CIVICO_PAY = "about_civico_pay";
    private static final String LBL_TERMINOS_INICIAL = "lbl_terminos_inicial";
    private static final String LBL_TERMINOS_INICIAL_PAY = "lbl_terminos_inicial_pay";
    private static final String TOOLBAR_MENU_ABOUT_CIVICO = "toolbar_menu_about_civico";
    private static final String PRIVACY_POLICIES = "privacy_policies";
    private static final String LBL_CREDITS = "lbl_credits";
    private static final String LBL_CONTACT = "lbl_contact";
    private static final String TOOLBAR_ABOUT_US = "toolbar_about_us";
    private static final String CIVIC_SHARE = "civic_share";
    private static final String URL_TERMS_CONDITIONS = "url_terms_conditions";
    private static final String URL_TERMS_CONDITIONS_CIVIC_PAY = "url_terms_conditions_civic_pay";
    private static final String URL_PRIVACY_POLICIES = "url_privacy_policies";
    private static final String URL_CREDITS = "url_credits";
    private final String TAG = SobreCivicoActivity.class.getSimpleName();
    //    UI
    Toolbar defaultToolbar;
    RelativeLayout relTerminos, relTerminosPay;
    RelativeLayout relPoliticas;
    RelativeLayout relCreditos;
    RelativeLayout relContacto, relShare;
    double latitude;
    double longitude;
    TextView txtSobreCivico, txtTerminosCondiciones, txtTerminosCondicionesPay, txtPoliticas,
            txtCreditos, txtContacto, txtVersion, txtShare;
    Utils util = new Utils();
    User user;
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sobre_civico_activity);

        initDrawer();
        user = ((TrackingOps) getApplication()).getUser();
        trackingOps = (TrackingOps) getApplicationContext();


        //  Picasso.with(getApplicationContext()).load(R.drawable.mascotas).transform(new CircleTransform()).into(imgMenuLateralSobreCivico);
        relTerminos = findViewById(R.id.relTerminos);

        relTerminosPay = findViewById(R.id.relTerminosPay);
        if (user.getAcceptingPayments())
            relTerminosPay.setVisibility(View.VISIBLE);

        relPoliticas = findViewById(R.id.relPoliticas);
        relCreditos = findViewById(R.id.relCreditos);
        relContacto = findViewById(R.id.relContacto);
        relShare = findViewById(R.id.relShare);

        txtSobreCivico = findViewById(R.id.txtSobreCivico);
        txtSobreCivico.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_ABOUT_US));
        txtTerminosCondiciones = findViewById(R.id.txtTerminosCondiciones);
        txtTerminosCondiciones.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
        txtTerminosCondicionesPay = findViewById(R.id.txtTerminosCondicionesPay);
        txtTerminosCondicionesPay.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL_PAY));
        txtPoliticas = findViewById(R.id.txtPoliticas);
        txtPoliticas.setText(trackingOps.getmFirebaseRemoteConfig().getString(PRIVACY_POLICIES));
        txtCreditos = findViewById(R.id.txtCreditos);
        txtCreditos.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CREDITS));
        txtContacto = findViewById(R.id.txtContacto);
        txtContacto.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CONTACT));
        txtVersion = findViewById(R.id.txtVersion);
        txtShare = findViewById(R.id.txtShare);
        txtShare.setText(trackingOps.getmFirebaseRemoteConfig().getString(CIVIC_SHARE));

        txtSobreCivico.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtTerminosCondiciones.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtTerminosCondicionesPay.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtPoliticas.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtCreditos.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtShare.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtContacto.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtVersion.setTypeface(util.fuenteSubTituHint(getApplicationContext()));

        relTerminos.setOnClickListener(this);
        relTerminosPay.setOnClickListener(this);
        relPoliticas.setOnClickListener(this);
        relCreditos.setOnClickListener(this);
        relContacto.setOnClickListener(this);
        relShare.setOnClickListener(this);

        trackingOps = (TrackingOps) getApplication();
        trackingOps.registerScreenName("Sobre Cívico | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relTerminos:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_ABOUT_CIVICO), trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_TERMS_CONDITIONS) + user.getCity() + "&layout=false");
                break;
            case R.id.relTerminosPay:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_TERMS_CONDITIONS_CIVIC_PAY) + user.getCity() + "&layout=false");
                break;
            case R.id.relPoliticas:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(PRIVACY_POLICIES));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_PRIVACY_POLICIES) + user.getCity() + "&layout=false");
                break;
            case R.id.relCreditos:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(LBL_CREDITS));
                goToWebView(trackingOps.getmFirebaseRemoteConfig().getString(URL_CREDITS));
                break;
            case R.id.relContacto:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(LBL_CONTACT));
                Intent detalle = new Intent(getApplicationContext(), ActivityContactoPay.class);
                startActivity(detalle);
                break;
            case R.id.relShare:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(ABOUT_CIVICO_PAY), trackingOps.getmFirebaseRemoteConfig().getString(CIVIC_SHARE));
                Intent shareIntent = new Intent(getApplicationContext(), ShareCivicoActivity.class);
                startActivity(shareIntent);
                break;
            default:
                break;
        }
    }

    public void goToWebView(String url) {
        Intent detalle = new Intent(getApplicationContext(), WebSobreCivicoActivity.class);
        detalle.putExtra("KEY", url);
        startActivity(detalle);
    }
}
