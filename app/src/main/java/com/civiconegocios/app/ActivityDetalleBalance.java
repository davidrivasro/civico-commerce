package com.civiconegocios.app;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

public class ActivityDetalleBalance extends Activity {

    private static final String LBL_TITLLE_TRANSFERRED_DETAIL = "lbl_titlle_transferred_detail";
    private static final String TOTAL_VALUE = "total_value";
    private static final String TAX_RETENTION = "tax_retention";
    private static final String RENT_RETENTION = "rent_retention";
    private static final String ICA_RETENTION = "ica_retention";
    private static final String OTHER_DISCOUNTS = "other_discounts";
    private static final String PRICE_TRANSFERRED = "price_transferred";
    TextView txtTituloPendienteDetalle;
    TextView txtTituloProximoFechaDetalle;
    TextView txtTitValorTotalDetalleBalance;
    TextView txtValorTotalDetalleBalance;
    TextView txtTitReteIvaBalanceDetalle;
    TextView txtValorReteIvaBalanceDetalle;
    TextView txtTitReterentaBalanceDetalle;
    TextView txtValorReterentaBalanceDetalle;
    TextView txtTitReteICADetalle;
    TextView txtValorReteICADetalle;
    TextView txtTitValorTotalBalanceDetalle;
    TextView txtValorTotalBalanceDetalle;
    TextView txtTitOtrosDescuentosDetalle;
    TextView txtValorOtrosDescuentosDetalle;
    Utils util = new Utils();
    String fechaTotal;
    String valorTotal;
    String reteIva;
    String reteRenta;
    String reteIca;
    String valorTranferido;
    String otrosDescuentos;
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_detalle);

        trackingOps = (TrackingOps) getApplicationContext();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fechaTotal = extras.getString("fechaTotal");
            valorTotal = extras.getString("valorTotal");
            reteIva = extras.getString("reteIva");
            reteRenta = extras.getString("reteRenta");
            reteIca = extras.getString("reteIca");
            valorTranferido = extras.getString("valorTranferido");
            otrosDescuentos = extras.getString("otrosDescuentos");
        }

        txtTituloPendienteDetalle = findViewById(R.id.tv_titulo_pendiente_balance_detalle);
        txtTituloPendienteDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TITLLE_TRANSFERRED_DETAIL));
        txtTituloProximoFechaDetalle = findViewById(R.id.tv_proximo_fecha_balance_detalle);
        txtTitValorTotalDetalleBalance = findViewById(R.id.tv_titulo_valor_total_balance_detalle);
        txtTitValorTotalDetalleBalance.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE));
        txtValorTotalDetalleBalance = findViewById(R.id.tv_valor_total_balance_detalle);
        txtTitReteIvaBalanceDetalle = findViewById(R.id.tv_reteiva_balance_detalle);
        txtTitReteIvaBalanceDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(TAX_RETENTION));
        txtValorReteIvaBalanceDetalle = findViewById(R.id.tv_valor_reteiva_balance_detalle);
        txtTitReterentaBalanceDetalle = findViewById(R.id.tv_reterenta_balance_detalle);
        txtTitReterentaBalanceDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(RENT_RETENTION));
        txtValorReterentaBalanceDetalle = findViewById(R.id.tv_valor_reterenta_balance_detalle);
        txtTitReteICADetalle = findViewById(R.id.tv_reteica_balance_detalle);
        txtTitReteICADetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(ICA_RETENTION));
        txtValorReteICADetalle = findViewById(R.id.tv_valor_reteica_balance_detalle);
        txtTitValorTotalBalanceDetalle = findViewById(R.id.tv_total_balance_detalle);
        txtTitValorTotalBalanceDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(PRICE_TRANSFERRED));
        txtValorTotalBalanceDetalle = findViewById(R.id.tv_valor_balance_detalle);
        txtTitOtrosDescuentosDetalle = findViewById(R.id.tv_otros_descuentos_balance_detalle);
        txtTitOtrosDescuentosDetalle.setText(trackingOps.getmFirebaseRemoteConfig().getString(OTHER_DISCOUNTS));
        txtValorOtrosDescuentosDetalle = findViewById(R.id.tv_valor_otros_descuentos_balance_detalle);

        txtTituloPendienteDetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtTituloProximoFechaDetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtTitValorTotalDetalleBalance.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtValorTotalDetalleBalance.setTypeface(util.fuenteSubTituHint(getApplicationContext()), Typeface.BOLD);
        txtTitReteIvaBalanceDetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtValorReteIvaBalanceDetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()), Typeface.BOLD);
        txtTitReterentaBalanceDetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtValorReterentaBalanceDetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()), Typeface.BOLD);
        txtTitReteICADetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtValorReteICADetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()), Typeface.BOLD);
        txtTitValorTotalBalanceDetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtValorTotalBalanceDetalle.setTypeface(util.fuenteSubTituHint(getApplicationContext()), Typeface.BOLD);

        txtTituloProximoFechaDetalle.setText(fechaTotal);
        txtValorTotalDetalleBalance.setText("$ " + valorTotal);
        txtValorReteIvaBalanceDetalle.setText("$ " + reteIva);
        txtValorReterentaBalanceDetalle.setText("$ " + reteRenta);
        txtValorReteICADetalle.setText("$ " + reteIca);
        txtValorTotalBalanceDetalle.setText("$ " + valorTranferido);
        txtValorOtrosDescuentosDetalle.setText("$ " + otrosDescuentos);
    }
}
