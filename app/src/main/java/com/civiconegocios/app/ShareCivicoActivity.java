package com.civiconegocios.app;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.constraintlayout.widget.Group;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import androidx.loader.app.LoaderManager;
import androidx.core.content.ContextCompat;
import androidx.loader.content.Loader;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.ReferirCivicoVO;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ShareCivicoActivity extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks {

    private static final String CIVIC_SHARE = "civic_share";
    private static final String INVITE_FRIENDS = "invite_friends";
    private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
    private static final String SHARE_QR_SUBTITLE = "share_qr_subtitle";
    //    Loaders IDS
    private final int DATA_REQUEST_LOADER = 10;
    TextView txtCivicoShareBack, txtreferral_code_plain, txtMain_title, txtMain_text,
            txtDynamic_text, txtButton_invite, txtmodal_text, txtmodal_text_cerrar, toolbar_title, shareQRSubtitle;
    LinearLayout lnAlphaCompartir;
    Utils util = new Utils();
    TrackingOps trackingOps;
    User user;
    Group dynamicsGroup;
    ImageView qrCodeImageView;
    private ReferirCivicoVO referirVo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_civico);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        if (util.haveInternet(getApplicationContext())) {
            makeDataRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(CIVIC_SHARE));

        txtreferral_code_plain = findViewById(R.id.txtreferral_code_plain);
        txtMain_title = findViewById(R.id.txtMain_title);
        txtMain_text = findViewById(R.id.txtMain_text);
        txtDynamic_text = findViewById(R.id.txtDynamic_text);
        txtButton_invite = findViewById(R.id.txtButton_invite);
        txtButton_invite.setText(trackingOps.getmFirebaseRemoteConfig().getString(INVITE_FRIENDS));
        txtmodal_text = findViewById(R.id.txtmodal_text);
        dynamicsGroup = findViewById(R.id.dynamicsGroup);
        txtmodal_text_cerrar = findViewById(R.id.txtmodal_text_cerrar);
        lnAlphaCompartir = findViewById(R.id.lnAlphaCompartir);

        txtCivicoShareBack = findViewById(R.id.txtCivicoShareBack);
        txtCivicoShareBack.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
        txtCivicoShareBack.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null, null, null);
        txtCivicoShareBack.setOnClickListener(this);

        txtmodal_text_cerrar.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                VectorDrawableCompat.create(getResources(), R.drawable.ic_clear_grey, getTheme()),
                null
        );

        txtDynamic_text.setOnClickListener(this);
        txtmodal_text_cerrar.setOnClickListener(this);
        lnAlphaCompartir.setOnClickListener(this);
        txtreferral_code_plain.setOnClickListener(this);
        txtButton_invite.setOnClickListener(this);

        shareQRSubtitle = findViewById(R.id.shareQRSubtitle);
        shareQRSubtitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(SHARE_QR_SUBTITLE));
        qrCodeImageView = this.findViewById(R.id.qrCodeImageView);

        trackingOps.registerScreenName("Compartir | Mi Cívico | " + user.getAnalyticsCity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtCivicoShareBack:
                finish();
                break;
            case R.id.txtreferral_code_plain:
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Compartir Cívico", referirVo.getReferral_code_plain());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getBaseContext(), getBaseContext().getResources().getString(R.string.copiado_portapapeles),
                        Toast.LENGTH_LONG).show();
                break;
            case R.id.txtDynamic_text:
                dynamicsGroup.setVisibility(View.VISIBLE);
                break;
            case R.id.txtmodal_text_cerrar:
            case R.id.lnAlphaCompartir:
                dynamicsGroup.setVisibility(View.GONE);
                break;
            case R.id.txtButton_invite:
                trackingOps.registerEvent("Compartir Cívico", "Perfil", "Invitar Amigos");
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, user.getBrand().getName() + " te invita a que descargues CÍVICO en tu móvil para tener toda tu ciudad en una app: " + referirVo.getReferral_code());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Seleccione una opción"));
                break;
            default:
                break;
        }

    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            body.put("tos_confirmation", true);
            body.put("tos_pay_confirmation", true);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + "/users/referral");

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(DATA_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(DATA_REQUEST_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        switch (id) {
            case DATA_REQUEST_LOADER:
                return new RequestDefaultFactory(this, args);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        toggleProgressDialog(false);
        if (data == null) {
            util.alertError500(this);
        } else {
            switch (loader.getId()) {
                case DATA_REQUEST_LOADER:
                    try {
                        JSONObject responseObject = new JSONObject(String.valueOf(data));
                        String referralCode = responseObject.getString("referral_code_plain");
                        if (referralCode.length() == 0) {
                            util.alertError500(this);
                        } else {


                            referirVo = new ReferirCivicoVO();
                            referirVo.setReferral_code(responseObject.getString("referral_code"));
                            referirVo.setReferral_code_plain(responseObject.getString("referral_code_plain"));
                            JSONObject textsObj = responseObject.getJSONObject("texts");
                            referirVo.setMain_title(textsObj.getString("main_title"));
                            referirVo.setMain_text(textsObj.getString("main_text"));
                            referirVo.setDynamic_text(textsObj.getString("dynamic_text"));
                            referirVo.setButton_invite(textsObj.getString("button_invite"));
                            referirVo.setModal_title(textsObj.getString("modal_title"));
                            referirVo.setModal_text(textsObj.getString("modal_text"));

                            txtreferral_code_plain.setText(referirVo.getReferral_code_plain());
                            txtMain_title.setText(referirVo.getMain_title());

                            txtMain_text.setText(referirVo.getMain_text());
                            txtDynamic_text.setText(referirVo.getDynamic_text());
                            txtButton_invite.setText(referirVo.getButton_invite());
                            txtmodal_text.setText(referirVo.getModal_title() + "\n\n" + referirVo.getModal_text());

                            Picasso.with(getApplicationContext()).load("https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=" + referirVo.getReferral_code()).into(qrCodeImageView);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        util.alertError500(this);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        toggleProgressDialog(false);
    }
}
