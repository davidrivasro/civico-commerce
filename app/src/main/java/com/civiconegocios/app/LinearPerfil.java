package com.civiconegocios.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.civiconegocios.app.utils.Utils;

public class LinearPerfil extends RelativeLayout {

    TextView txtTitleCeldaPerfil;
    TextView txtDetailCeldaPerfil;
    Utils util = new Utils();
    Typeface fontUse;

    public LinearPerfil(Context context) {
        super(context);
        fontUse = util.fuenteSubTituHint(context);
    }

    public LinearPerfil(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        fontUse = util.fuenteSubTituHint(context);
    }

    public LinearPerfil(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        View.inflate(context, R.layout.linear_perfil, this);
        setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
        txtTitleCeldaPerfil = findViewById(R.id.txtTitleCeldaPerfil);
        txtDetailCeldaPerfil = findViewById(R.id.txtDetailCeldaPerfil);

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.LinearPerfil,
                    0, 0);

            String titleText = "";
            String detailText = "";
            boolean checked = false;
            try {
                titleText = a.getString(R.styleable.LinearPerfil_titleText);
                detailText = a.getString(R.styleable.LinearPerfil_detailtext);
            } catch (Exception e) {
            } finally {
                a.recycle();
            }
            setTitleText(titleText);
            setDetailText(detailText);

        }
    }

    public void setTitleText(String text) {
        txtTitleCeldaPerfil.setText(text);
        //   txtTitleCeldaPerfil.setTypeface(fontUse);
    }

    public void setDetailText(String text) {
        txtDetailCeldaPerfil.setText(text);
        //  txtDetailCeldaPerfil.setTypeface(fontUse);
    }
}
