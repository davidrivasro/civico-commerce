package com.civiconegocios.app;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.OfertasRedimidasVO;
import com.civiconegocios.app.VO.OfertasVO;
import com.civiconegocios.app.adapters.AdapterItemOfertas;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.DealsListDataSerializer;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class ActivityListarOfertas extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks {

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    private static final String PERMISSION_PHONE_RATIONALE = "permission_phone_rationale";
    private static final String BUTTON_ALLOW = "button_allow";
    private static final String BUTTON_DENY = "button_deny";
    private static final String PERMISSION_CALL_DENIED = "permission_call_denied";
    private static final String PERMISSION_CALL_NEVERASK = "permission_call_neverask";
    private static final String CLOSE_APP_CONFIRMATION = "close_app_confirmation";
    private static final String LBL_BTN_COMUNICATE_WITH_US = "lbl_btn_comunicate_with_us";
    private static final String NOT_DEALS_FOUNDED = "not_deals_founded";
    private static final String CREATE_DEAL_TEXT = "create_deal_text";
    private static final String CREATE_DEAL_BUTTON = "create_deal_button";
    private final String TAG = ActivityListarOfertas.class.getSimpleName();
    //    Loaders IDS
    private final int DATA_REQUEST_LOADER = 10;
    private final int DATA_SERIALIZER_LOADER = 11;
    //    UI
    Toolbar defaultToolbar;
    RecyclerView lvOfertas;
    AdapterItemOfertas adapter;
    List<OfertasVO> ofertasArray;
    Utils util = new Utils();
    TextView txtComunicateLisOferta;
    LinearLayout lnNoResultadosOfertas;
    boolean doubleBackToExitPressedOnce = false;
    //    Ops
    String serviceEndpoint = "/brand/";
    String dealsData;
    User user;
    TextView noResultsFoundedTxt, createDealTitle, createDealBt;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_ofertas);

        initDrawer();
        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        if (!user.getAcceptingPayments()) {
            OneSignal.setEmail(user.getEmail());

            JSONObject tags = new JSONObject();
            try {
                tags.put("user_name", user.getFirstName());
                tags.put("brand", user.getBrand().getName());
                tags.put("accepting_payments", user.getAcceptingPayments());
                tags.put("mpos_enable", user.getMposEnabled());
                tags.put("city", user.getCity());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            OneSignal.sendTags(tags);
        }

        txtComunicateLisOferta = findViewById(R.id.txtComunicateLisOferta);
        txtComunicateLisOferta.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_COMUNICATE_WITH_US));
        txtComunicateLisOferta.setOnClickListener(this);

        noResultsFoundedTxt = findViewById(R.id.noResultsFoundedTxt);
        noResultsFoundedTxt.setText(trackingOps.getmFirebaseRemoteConfig().getString(NOT_DEALS_FOUNDED));

        createDealTitle = findViewById(R.id.createDealTitle);
        createDealTitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(CREATE_DEAL_TEXT));

        createDealBt = findViewById(R.id.createDealBt);
        createDealBt.setText(trackingOps.getmFirebaseRemoteConfig().getString(CREATE_DEAL_BUTTON));
        createDealBt.setOnClickListener(this);

        lnNoResultadosOfertas = findViewById(R.id.lnNoResultadosOfertas);

        lvOfertas = findViewById(R.id.lvOfertas);
        ofertasArray = new ArrayList<>();
        adapter = new AdapterItemOfertas(getApplicationContext(), ofertasArray, trackingOps);
        lvOfertas.setAdapter(adapter);
        lvOfertas.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(AppCompatResources.getDrawable(this, R.drawable.transparent_divider));
        lvOfertas.addItemDecoration(itemDecorator);

        makeDataRequest();

        if (!((TrackingOps) getApplication()).getJsonProcesoPago().equals("no")) {
            try {
                JSONObject catObj = new JSONObject(((TrackingOps) getApplication()).getJsonProcesoPago());
                if (catObj.getString("type").equals("coupon_redeemed")) {
                    // nuevo parseo
                    trackingOps.registerEvent("Pushwoosh", "Oferta guardada", catObj.getString("offer_name") + " | " + user.getCustomer().getName());
                    OfertasRedimidasVO RedeemedDeal = new OfertasRedimidasVO(catObj.getString("id"), catObj.getString("status"),
                            catObj.getString("redemption_date"), catObj.getString("offer_name"), catObj.getInt("price") + "", catObj.getInt("discount") + "",
                            catObj.getString("user_name"), catObj.getString("document_type"), catObj.getString("document_number"), "", catObj.getString("code"),
                            catObj.getString("user_email"), "", "");
                    ((TrackingOps) getApplication()).setJsonProcesoPago("no");
                    Intent i = new Intent(getApplicationContext(), ActivityEstadoCodigoDetalle.class);
                    i.putExtra("Object", RedeemedDeal);
                    startActivity(i);
                } else {
                    JSONObject userObj = catObj.getJSONObject("user");
                    trackingOps.registerEvent("Estado transacción", "Transacción", catObj.getInt("status") + "");
                    Intent i = new Intent(getApplicationContext(), ActivityDetalleTransaccion.class);
                    i.putExtra("fechaTotal", catObj.getString("created_at")); // no
                    i.putExtra("hora", catObj.getString("created_at")); // NO
                    i.putExtra("status", catObj.getInt("status") + "");
                    i.putExtra("monto", catObj.getInt("amount") + ""); // no
                    //i.putExtra("monto", catObj.getInt("amount_base")+"");
                    i.putExtra("pin", userObj.getString("pin")); // no
                    i.putExtra("taxAmount", catObj.getInt("tax_amount") + ""); // no
                    i.putExtra("numAprobacion", catObj.getString("authorization_code") + ""); // no
                    i.putExtra("transId", catObj.getString("transaction_id") + "");
                    i.putExtra("valorBase", catObj.getInt("amount_base") + "");
                    i.putExtra("iac", catObj.getInt("iac_amount") + "");
                    i.putExtra("tip", catObj.getInt("tip_amount") + "");
                    i.putExtra("origin", catObj.getString("origin"));
                    ((TrackingOps) getApplication()).setJsonProcesoPago("no");
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        trackingOps.registerScreenName("Lista de ofertas | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    public void mensajeNoOfertas() {
        lnNoResultadosOfertas.setVisibility(View.VISIBLE);
        lvOfertas.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtComunicateLisOferta:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Comunicate con nosotros", "Lista de ofertas");
                if (util.mostrarPopUp()) {
                    ActivityListarOfertasPermissionsDispatcher.callWithPermissionCheck(this, "2 36 31 11");
                } else {
                    DialogComunicate cdd = new DialogComunicate(this);
                    cdd.show();
                }
                break;
            case R.id.createDealBt:
                Intent intent = new Intent(this, CreateDealActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            default:
                break;
        }
    }

    @NeedsPermission(android.Manifest.permission.CALL_PHONE)
    public void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + "031" + phone));

        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.CALL_PHONE},
                REQUEST_PERMISSIONS_REQUEST_CODE);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ActivityListarOfertasPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(android.Manifest.permission.CALL_PHONE)
    void showRationaleForRegistarToken(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(PERMISSION_PHONE_RATIONALE))
                .setPositiveButton(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(BUTTON_ALLOW), (dialog, button) -> request.proceed())
                .setNegativeButton(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(BUTTON_DENY), (dialog, button) -> request.cancel())
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(android.Manifest.permission.CALL_PHONE)
    void showDeniedForPhoneCall() {
        Toast.makeText(this, ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(PERMISSION_CALL_DENIED), Toast.LENGTH_SHORT).show();
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.CALL_PHONE)
    void showNeverAskForPhoneCall() {
        Toast.makeText(this, ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(PERMISSION_CALL_NEVERASK), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(CLOSE_APP_CONFIRMATION), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, serviceEndpoint + user.getBrand().getId() + "/offers");
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader loader = loaderManager.getLoader(DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(DATA_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(DATA_REQUEST_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    public void makeDataSerialization() {
        Bundle queryBundle = new Bundle();
        queryBundle.putString("data", this.dealsData);

        LoaderManager loaderManager = getSupportLoaderManager();
        Loader loader = loaderManager.getLoader(DATA_SERIALIZER_LOADER);

        if (loader == null) {
            loaderManager.initLoader(DATA_SERIALIZER_LOADER, queryBundle, this);
        } else {
            loaderManager.restartLoader(DATA_SERIALIZER_LOADER, queryBundle, this);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        switch (id) {
            case DATA_REQUEST_LOADER:
                return new RequestDefaultFactory(this, args);
            case DATA_SERIALIZER_LOADER:
                return new DealsListDataSerializer(this, args);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        toggleProgressDialog(false);
        switch (loader.getId()) {
            case DATA_REQUEST_LOADER:
                this.dealsData = String.valueOf(data);
                if (data != null) {
                    makeDataSerialization();
                } else {
                    mensajeNoOfertas();
                }
                break;
            case DATA_SERIALIZER_LOADER:
                if (data instanceof List<?>) {
                    List<OfertasVO> dealsList = (List<OfertasVO>) data;
                    if (!dealsList.isEmpty()) {
                        ofertasArray.clear();
                        ofertasArray.addAll(dealsList);
                        adapter.notifyDataSetChanged();
                    } else {
                        mensajeNoOfertas();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        toggleProgressDialog(false);
    }
}
