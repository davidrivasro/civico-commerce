package com.civiconegocios.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.CategoriasVO;
import com.civiconegocios.app.adapters.list.PreguntasFrecuentesListAdapter;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.sync.LoaderPreguntasFrecuentes;
import com.google.firebase.perf.metrics.AddTrace;

import java.util.ArrayList;

public class PreguntasFrecuentesActivity extends BaseActivity implements
        LoaderManager.LoaderCallbacks<ArrayList<CategoriasVO>>,
        PreguntasFrecuentesListAdapter.PreguntasFrecuentesListener {

    private static final int CURSOR_PF_LIST = 40;
    private static final String PROCESS_DIALOG_MESSAGE = "process_dialog_message";
    private static final String TOOLBAR_MENU_FAQ = "toolbar_menu_faq";
    //    UI

    TextView toolbar_title;
    Toolbar defaultToolbar;
    PreguntasFrecuentesListAdapter pfListAdapter;
    User user;
    TrackingOps trackingOps;
    //    Ops
    private ProgressDialog progressDialog;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preguntas_frecuentes);

        initDrawer();


        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_FAQ));

        RecyclerView recyclerView = findViewById(R.id.list_categories);
        recyclerView.setHasFixedSize(true);
        pfListAdapter = new PreguntasFrecuentesListAdapter(this);
        pfListAdapter.setPreguntasFrecuentesCursor(null);
        recyclerView.setAdapter(pfListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (getSupportLoaderManager().getLoader(CURSOR_PF_LIST) == null)
            getSupportLoaderManager().initLoader(CURSOR_PF_LIST, null, this);
        else
            getSupportLoaderManager().restartLoader(CURSOR_PF_LIST, null, this);

        trackingOps.registerScreenName(" Preguntas frecuentes | " + user.getCustomer().getName() + " | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    @Override
    public Loader<ArrayList<CategoriasVO>> onCreateLoader(int id, Bundle args) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PROCESS_DIALOG_MESSAGE));
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        return new LoaderPreguntasFrecuentes(this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<CategoriasVO>> loader, ArrayList<CategoriasVO> data) {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        if (data != null && data.size() > 0) pfListAdapter.setPreguntasFrecuentesCursor(data);
        else pfListAdapter.setPreguntasFrecuentesCursor(null);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<CategoriasVO>> loader) {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        pfListAdapter.setPreguntasFrecuentesCursor(null);
    }

    @Override
    public void onListItemClick(long clickedItemIndex) {
        CategoriasVO categoria = pfListAdapter.getCategoria(clickedItemIndex);
        Intent intent = new Intent(this, PreguntasCategoria.class);
        intent.putExtra("Categoria", categoria);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
