package com.civiconegocios.app;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;


public class ActivityFiltrarOfertas extends AppCompatActivity {

    private static final String TOOLBAR_MENU_REDEEMED_DEALS = "toolbar_menu_redeemed_deals";
    private static final String FILTER = "filter";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    Toolbar defaultToolbar;
    Utils util = new Utils();
    TextView filterDealsIcon;
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtrar_ofertas);

        trackingOps = (TrackingOps) getApplicationContext();

        defaultToolbar = findViewById(R.id.toolbar);
        defaultToolbar.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.fondoHeader));
        TextView mTitle = defaultToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_REDEEMED_DEALS));
        mTitle.setTypeface(util.fuenteHeader(getApplicationContext()));
        setSupportActionBar(defaultToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        filterDealsIcon = findViewById(R.id.filterDealsIcon);
        filterDealsIcon.setText(trackingOps.getmFirebaseRemoteConfig().getString(FILTER).toUpperCase());
        filterDealsIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_comercio_filtrar), null, null, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.default_toolbar, menu);
        util.toolbarOrderItems(defaultToolbar);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
