package com.civiconegocios.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DefaultCivicoResponse {

    @SerializedName("developerMessage")
    @Expose
    private String developerMessage;
    @SerializedName("userMessage")
    @Expose
    private String userMessage;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
