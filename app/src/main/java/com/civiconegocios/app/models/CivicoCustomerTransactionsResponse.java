package com.civiconegocios.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CivicoCustomerTransactionsResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("transaction")
    @Expose
    private CivicoTransactionResponse civicoTransactionResponse;
    @SerializedName("errors")
    @Expose
    private CivicoErrorsResponse civicoErrorsResponse;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public CivicoTransactionResponse getCivicoTransactionResponse() {
        return civicoTransactionResponse;
    }

    public void setCivicoTransactionResponse(CivicoTransactionResponse civicoTransactionResponse) {
        this.civicoTransactionResponse = civicoTransactionResponse;
    }

    public CivicoErrorsResponse getCivicoErrorsResponse() {
        return civicoErrorsResponse;
    }

    public void setCivicoErrorsResponse(CivicoErrorsResponse civicoErrorsResponse) {
        this.civicoErrorsResponse = civicoErrorsResponse;
    }

}
