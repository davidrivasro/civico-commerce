package com.civiconegocios.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CivicoTransactionResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("authorization_code")
    @Expose
    private String authorizationCode;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("iac_amount")
    @Expose
    private Integer iacAmount;
    @SerializedName("installments")
    @Expose
    private Integer installments;
    @SerializedName("payment_method_id")
    @Expose
    private String paymentMethodId;
    @SerializedName("pin_id")
    @Expose
    private String pinId;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("status_log")
    @Expose
    private List<Object> statusLog = null;
    @SerializedName("tax_amount")
    @Expose
    private Integer taxAmount;
    @SerializedName("tip_amount")
    @Expose
    private Integer tipAmount;
    @SerializedName("transaction_id")
    @Expose
    private Integer transactionId;
    @SerializedName("transfer_amount")
    @Expose
    private Integer transferAmount;
    @SerializedName("transfer_id")
    @Expose
    private String transferId;
    @SerializedName("transferred")
    @Expose
    private Boolean transferred;
    @SerializedName("trazability_id")
    @Expose
    private String trazabilityId;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("remote_ip")
    @Expose
    private String remoteIp;
    @SerializedName("app_version")
    @Expose
    private String appVersion;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("coordinates")
    @Expose
    private CivicoCoordinatesResponse coordinates;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIacAmount() {
        return iacAmount;
    }

    public void setIacAmount(Integer iacAmount) {
        this.iacAmount = iacAmount;
    }

    public Integer getInstallments() {
        return installments;
    }

    public void setInstallments(Integer installments) {
        this.installments = installments;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPinId() {
        return pinId;
    }

    public void setPinId(String pinId) {
        this.pinId = pinId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Object> getStatusLog() {
        return statusLog;
    }

    public void setStatusLog(List<Object> statusLog) {
        this.statusLog = statusLog;
    }

    public Integer getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Integer taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Integer getTipAmount() {
        return tipAmount;
    }

    public void setTipAmount(Integer tipAmount) {
        this.tipAmount = tipAmount;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(Integer transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public Boolean getTransferred() {
        return transferred;
    }

    public void setTransferred(Boolean transferred) {
        this.transferred = transferred;
    }

    public String getTrazabilityId() {
        return trazabilityId;
    }

    public void setTrazabilityId(String trazabilityId) {
        this.trazabilityId = trazabilityId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public CivicoCoordinatesResponse getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(CivicoCoordinatesResponse coordinates) {
        this.coordinates = coordinates;
    }
}
