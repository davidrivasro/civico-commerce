package com.civiconegocios.app.models;

import android.content.Context;

import com.civiconegocios.app.Ops.TrackingOps;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class User extends RealmObject {

    @PrimaryKey
    @Index
    private String id;

    @Required
    @Index
    private String email;

    @Required
    private String password;
    @Required
    private String accessToken;

    private String avatar;
    private String firstName;
    private String lastName;

    @Index
    private String city;
    private String analyticsCity;

    private Boolean hasPassword;
    private String referralCode;
    private Boolean isAcceptingPayments;
    private Boolean isMposEnabled;
    private int minAmount;
    private int maxAmount;
    private String deviceID;
    private Double latitude;
    private Double longitude;
    private String pushToken;
    private Brand brand;
    private Customer customer;
    private String tosDate;
    private String tosPayDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAnalyticsCity() {
        return analyticsCity;
    }

    public void setAnalyticsCity(String analyticsCity) {
        this.analyticsCity = analyticsCity;
    }

    public Boolean getHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(Boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public Boolean getAcceptingPayments() {
        return isAcceptingPayments;
    }

    public void setAcceptingPayments(Boolean acceptingPayments) {
        isAcceptingPayments = acceptingPayments;
    }

    public Boolean getMposEnabled() {
        return isMposEnabled;
    }

    public void setMposEnabled(Boolean mposEnabled) {
        isMposEnabled = mposEnabled;
    }

    public int getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(int minAmount) {
        this.minAmount = minAmount;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(int maxAmount) {
        this.maxAmount = maxAmount;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void deleteAll(Context context) {
        Realm realm = ((TrackingOps) context.getApplicationContext()).getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<User> result = realm.where(User.class).findAll();
                result.deleteAllFromRealm();

                RealmResults<Response> responseResult = realm.where(Response.class).findAll();
                responseResult.deleteAllFromRealm();

                RealmResults<Message> messagesResult = realm.where(Message.class).findAll();
                messagesResult.deleteAllFromRealm();
            }
        });
    }

    public String getTosDate() {
        return tosDate;
    }

    public void setTosDate(String tosDate) {
        this.tosDate = tosDate;
    }

    public String getTosPayDate() {
        return tosPayDate;
    }

    public void setTosPayDate(String tosPayDate) {
        this.tosPayDate = tosPayDate;
    }
}
