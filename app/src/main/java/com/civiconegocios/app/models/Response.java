package com.civiconegocios.app.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Response extends RealmObject {

    @PrimaryKey
    private String id;

    private String created_at;
    private String text;
    private String user_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
