package com.civiconegocios.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CivicoErrorsResponse {

    @SerializedName("errors")
    @Expose
    private CivicoErrors_Response errors;

    public CivicoErrors_Response getErrors() {
        return errors;
    }

    public void setErrors(CivicoErrors_Response errors) {
        this.errors = errors;
    }

}
