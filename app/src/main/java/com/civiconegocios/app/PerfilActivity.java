package com.civiconegocios.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import io.realm.Realm;

public class PerfilActivity extends BaseActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<String> {

    private static final String TOOLBAR_MENU_PROFILE = "toolbar_menu_profile";
    private static final String LBL_EDIT_PROFILE = "lbl_edit_profile";
    private static final String LBL_CAMBIAR_PASS_INICIAL = "lbl_cambiar_pass_inicial";
    private static final String LBL_DISCONNECT = "lbl_btn_disconnect";
    private static final String LBL_UPDATE_PICTURE = "lbl_update_picture";
    private static final String MSJ_SELECT_OPTION = "msj_select_option";
    private static final String LBL_CAMERA = "lbl_camera";
    private static final String LBL_GALLERY = "lbl_gallery";
    private static final String ACTUA_CONTRA = "actua_contra";
    private static final String TRANSA_ERRONEA = "transa_erronea";
    private static final String LBL_BTN_CHANGE = "lbl_btn_change";
    private static final String LBL_BTN_EDIT = "lbl_btn_edit";
    private static final String LBL_BASIC_DATA = "lbl_basic_data";
    private static final String LBL_BTN_CHANGE_PASSWORD = "lbl_btn_change_password";
    private static final String LBL_BTN_REGISTERED_TRADING = "lbl_btn_registered_trading";
    private static final String NAME_TITLE = "first_name";
    private static final String LAST_NAME_TITLE = "last_name";
    private static final String EMAIL_TITLE = "mail";
    private static final String ADRESS_TITLE = "address";


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    //    Loaders IDS
    private final int CHANGE_PASSWORD_REQUEST_LOADER = 10;
    private final int CHANGE_PROFILE_DATA_REQUEST_LOADER = 11;
    private final int UPLOAD_PICTURE_REQUEST_LOADER = 12;
    //    UI
    Toolbar defaultToolbar;
    ImageView imgPerfilIUsuario;
    LinearPerfil linearNombre;
    LinearPerfil linearApellido;
    LinearPerfil linearCorreo;
    LinearPerfil linearNombreComercio;
    LinearPerfil linearDireccion;
    LinearLayout lnBtnCambiarPassPerfil;
    LinearLayout lnBtnDesconectCivico;
    LinearLayout lnBtnCambiarFotoPerfil;
    TextView txtTitleCeldaPerfil;
    TextView txtEditarDatos;
    TextView txtCambiarPassPerfil;
    TextView txtTitleCeldaPerfilComercio;
    TextView toolbar_title;
    TextView txtlnBtnDesconectCivico, profileEditIcon;
    String correoPerfil, currentPassword, newPassword, userName, lastName, mail;
    Utils util = new Utils();
    String encoded = "";
    Bitmap bitmapFoto;
    TextView txtTitBtnCambiar;
    String uploadPictureEndpoint = "/api/v1/image";
    User user;
    TrackingOps trackingOps;
    private Realm realm;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil_activity);

        initDrawer();

        trackingOps = (TrackingOps) getApplication();
        realm = trackingOps.getRealm();
        user = trackingOps.getUser();

        imgPerfilIUsuario = findViewById(R.id.imgPerfilIUsuario);
        txtTitBtnCambiar = findViewById(R.id.txtTitBtnCambiar);
        txtTitBtnCambiar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CHANGE));
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_PROFILE));
        linearNombre = findViewById(R.id.linearNombre);
        linearNombre.setTitleText(trackingOps.getmFirebaseRemoteConfig().getString(NAME_TITLE) + ":");
        linearApellido = findViewById(R.id.linearApellido);
        linearApellido.setTitleText(trackingOps.getmFirebaseRemoteConfig().getString(LAST_NAME_TITLE) + ":");
        linearCorreo = findViewById(R.id.linearCorreo);
        linearCorreo.setTitleText(trackingOps.getmFirebaseRemoteConfig().getString(EMAIL_TITLE) + ":");
        linearNombreComercio = findViewById(R.id.linearNombreComercio);
        linearNombreComercio.setTitleText(trackingOps.getmFirebaseRemoteConfig().getString(NAME_TITLE) + ":");
        linearDireccion = findViewById(R.id.linearDireccion);
        linearDireccion.setTitleText(trackingOps.getmFirebaseRemoteConfig().getString(ADRESS_TITLE) + ":");
        lnBtnDesconectCivico = findViewById(R.id.lnBtnDesconectCivico);
        lnBtnCambiarFotoPerfil = findViewById(R.id.lnBtnCambiarFotoPerfil);
        profileEditIcon = findViewById(R.id.profileEditIcon);
        profileEditIcon.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_EDIT));

        lnBtnCambiarPassPerfil = findViewById(R.id.lnBtnCambiarPassPerfil);
        //   Picasso.with(getApplicationContext()).load(R.drawable.mascotas).transform(new CircleTransform()).into(imgMenuLateralPerfil);

        // prueba.setTitleText("yanyie");
        txtTitleCeldaPerfil = findViewById(R.id.txtTitleCeldaPerfil);
        txtTitleCeldaPerfil.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BASIC_DATA));
        txtEditarDatos = findViewById(R.id.txtEditarDatos);
        txtEditarDatos.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_EDIT));
        txtEditarDatos.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_perfil_editar), null, null, null);
        txtEditarDatos.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtCambiarPassPerfil = findViewById(R.id.txtCambiarPassPerfil);
        txtCambiarPassPerfil.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CHANGE_PASSWORD));
        txtCambiarPassPerfil.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtTitleCeldaPerfilComercio = findViewById(R.id.txtTitleCeldaPerfilComercio);
        txtTitleCeldaPerfilComercio.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_REGISTERED_TRADING));
        txtTitleCeldaPerfil.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtTitleCeldaPerfilComercio.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtlnBtnDesconectCivico = findViewById(R.id.txtlnBtnDesconectCivico);
        txtlnBtnDesconectCivico.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_DISCONNECT));
        txtlnBtnDesconectCivico.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        txtTitBtnCambiar.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));
        profileEditIcon.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_perfil_editar), null, null, null);


        txtEditarDatos.setOnClickListener(this);
        lnBtnCambiarPassPerfil.setOnClickListener(this);
        lnBtnDesconectCivico.setOnClickListener(this);
        lnBtnCambiarFotoPerfil.setOnClickListener(this);
        correoPerfil = user.getEmail();
        linearNombre.setDetailText(user.getFirstName());
        linearApellido.setDetailText(user.getLastName());
        linearCorreo.setDetailText(correoPerfil);
        linearNombreComercio.setDetailText(user.getCustomer().getName());
        linearDireccion.setDetailText(user.getCustomer().getAddress());
        if (user.getAvatar() != null && !user.getAvatar().equals("")) {
            Picasso.with(getApplicationContext()).load(user.getAvatar()).resize(200, 200).transform(new CircleTransform()).into(imgPerfilIUsuario);
        }

        trackingOps.registerScreenName("Mi perfil | Cívico PAY Comercios | " + user.getAnalyticsCity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtEditarDatos:

                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_PROFILE), trackingOps.getmFirebaseRemoteConfig().getString(LBL_EDIT_PROFILE));
                DialogDatosBasicos cdd = new DialogDatosBasicos(this, user.getFirstName(), user.getLastName(), correoPerfil);

                cdd.show();
                break;
            case R.id.lnBtnCambiarPassPerfil:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_PROFILE), trackingOps.getmFirebaseRemoteConfig().getString(LBL_CAMBIAR_PASS_INICIAL));
                DialogCambiarPass cdd1 = new DialogCambiarPass(this, 1);
                cdd1.show();
                break;
            case R.id.lnBtnDesconectCivico:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_PROFILE), trackingOps.getmFirebaseRemoteConfig().getString(LBL_DISCONNECT));
                ((TrackingOps) getApplication()).setJsonProcesoPago("no");
                user.deleteAll(this);
                trackingOps.setUnreadMessagesCount(0);
                finish();
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                break;
            case R.id.lnBtnCambiarFotoPerfil:
                new AlertDialog.Builder(PerfilActivity.this)
                        .setTitle(trackingOps.getmFirebaseRemoteConfig().getString(LBL_UPDATE_PICTURE))
                        .setMessage(trackingOps.getmFirebaseRemoteConfig().getString(MSJ_SELECT_OPTION))
                        .setPositiveButton(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CAMERA).toUpperCase(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                abrirCamara();
                            }
                        })
                        .setNegativeButton(trackingOps.getmFirebaseRemoteConfig().getString(LBL_GALLERY).toUpperCase(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                abrirGaleria();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();
                break;
            default:
                break;
        }

    }

    public void lanzarAsynkEditar(String nombre, String apellido, String correo) {

        userName = nombre;
        lastName = apellido;
        mail = correo;
        if (util.haveInternet(getApplicationContext())) {
            makeChangeProfileDataRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }
    }

    public void lanzarAsynkCambiarPass(String current_password, String password) {

        if (util.haveInternet(getApplicationContext())) {
            currentPassword = current_password;
            newPassword = password;
            makeChangePasswordRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }
    }

    public void editoPerfil(String nombreCambiado, String apellidoCambiado) {
        linearNombre.setDetailText(nombreCambiado);
        linearApellido.setDetailText(apellidoCambiado);

        realm.beginTransaction();
        user.setFirstName(nombreCambiado);
        user.setLastName(apellidoCambiado);
        realm.commitTransaction();
    }

    public void cambioPass() {
        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(ACTUA_CONTRA),
                Toast.LENGTH_SHORT).show();
    }

    public void noCambioPass() {
        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(TRANSA_ERRONEA),
                Toast.LENGTH_SHORT).show();
    }

    // acutalizacion de fotos
    public void abrirGaleria() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        final int ACTIVITY_SELECT_IMAGE = 1234;
        startActivityForResult(i, ACTIVITY_SELECT_IMAGE);

    }

    public void abrirCamara() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, 1);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1234:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();

                    Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath);
                    try {
                        //  saveImage(yourSelectedImage);
                        // imgPerfilIUsuario.setImageBitmap(yourSelectedImage);
                        Picasso.with(this).load(getImageUri(getApplicationContext(), yourSelectedImage)).resize(200, 200).transform(new CircleTransform()).into(imgPerfilIUsuario);
                        convertBase64(yourSelectedImage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    /* Now you have choosen image in Bitmap format in object "yourSelectedImage". You can use it in way you want! */
                }
            case 1:
                if (requestCode == 1 && resultCode == RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    try {
                        // imgPerfilIUsuario.setImageBitmap(imageBitmap);
                        Picasso.with(this).load(getImageUri(getApplicationContext(), imageBitmap)).resize(200, 200).transform(new CircleTransform()).into(imgPerfilIUsuario);
                        convertBase64(imageBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void convertBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        makeUploadPictureRequest();
    }

    public void respuestFoto(String respuesta) {
        Toast.makeText(getApplicationContext(), respuesta,
                Toast.LENGTH_SHORT).show();
    }

    public void makeChangePasswordRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            body.put("current_password", currentPassword);
            body.put("password", newPassword);
            body.put("password_confirmation", newPassword);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "PUT");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL + "/users");

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(CHANGE_PASSWORD_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(CHANGE_PASSWORD_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(CHANGE_PASSWORD_REQUEST_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void makeChangeProfileDataRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            body.put("first_name", userName);
            body.put("last_name", lastName);
            body.put("email", mail);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "PUT");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL + "/users");

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(CHANGE_PROFILE_DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(CHANGE_PROFILE_DATA_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(CHANGE_PROFILE_DATA_REQUEST_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void makeUploadPictureRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("url", encoded);

            body.put("image", jsonObject);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.OAuth_URL + uploadPictureEndpoint);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(UPLOAD_PICTURE_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(UPLOAD_PICTURE_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(UPLOAD_PICTURE_REQUEST_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        toggleProgressDialog(true);
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        toggleProgressDialog(false);
        if (data == null) {
            util.alertErrorTimeOut(this);
        } else {
            switch (loader.getId()) {
                case CHANGE_PASSWORD_REQUEST_LOADER:
                    cambioPass();
                    break;
                case CHANGE_PROFILE_DATA_REQUEST_LOADER:
                    try {
                        JSONObject responseObject = new JSONObject(data);
                        JSONObject profile = responseObject.getJSONObject("profile");
                        editoPerfil(profile.getString("first_name"), profile.getString("last_name"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        util.alertError500(this);
                    }
                    break;
                case UPLOAD_PICTURE_REQUEST_LOADER:
                    try {
                        JSONObject catObj = new JSONObject(data);
                        realm.beginTransaction();
                        user.setAvatar(catObj.getString("avatar"));
                        realm.commitTransaction();
                        respuestFoto(catObj.getString("userMessage"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        util.alertError500(this);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }
}
