package com.civiconegocios.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.Group;
import androidx.core.app.ActivityCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.CivicoPagosWebService;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.perf.metrics.AddTrace;
import io.realm.Realm;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
import com.smartpesa.intent.SpConnect;
import com.smartpesa.intent.TransactionArgument;
import com.smartpesa.intent.TransactionType;
import com.smartpesa.intent.result.TransactionError;
import com.smartpesa.intent.result.TransactionResult;
*/

@RuntimePermissions
public class PinUsuarioActivity extends BaseActivity implements View.OnClickListener, OnSuccessListener<Location>,
		LoaderManager.LoaderCallbacks<String> {

		private static final int TRANSACTION_REQUEST_CODE = 1001;
		private static final int LAST_TRANSACTION_REQUEST_CODE = 1002;

		public static final String TAX_1_TYPE = "IVA";
		public static final String TAX_2_TYPE = "IAC";

		private static final String CODE_INCOM = "code_incom";
		private static final String MSJ_CHOOSE_MEANS_PAYMENT = "msj_choose_means_payment";
		private static final String MPOS_ERROR_MESSAGE = "mpos_error_message";
		private static final String MSJ_PLEASE_TRY_AGAIN = "msj_please_try_again";
		private static final String MSJ_TRANSACTION_COMPLETED_SUCCESSFULLY = "msj_transaction_completed_successfully";
		private static final String PERMISSION_PHONE_RATIONALE = "permission_phone_rationale";
		private static final String BUTTON_ALLOW = "button_allow";
		private static final String BUTTON_DENY = "button_deny";
		private static final String TOOLBAR_MENU_BACK = "toolbar_menu_back";
		private static final String LBL_CHOOSE_MEANS = "lbl_choose_means";
		private static final String LBL_MEANS_CONTINUE_TRANSACTION = "lbl_means_continue_transaction";
		private static final String CHARGE_WITHOUT_CARD = "charge_without_card";
		private static final String CHARGE_WITH_CARD = "charge_with_card";
		private static final String LBL_USER_PIN = "lbl_user_pin";
		private static final String PAYMENT_WITHOUT_CARD_SUBTITLE = "payment_without_card_subtitle";
		private final int MPOS_TRANSACTION_INITIALIZATION = 10;
		private final String TAG = PinUsuarioActivity.class.getSimpleName();
		LinearLayout lnBtnEnviarCobro, pinBoxContainer;
		ImageView imgPin;
		EditText txtPinUno;
		EditText txtPinDos;
		EditText txtPinTres;
		boolean pinUno = false;
		boolean pinDos = false;
		boolean pinTres = false;
		Utils util = new Utils();
		TextView txtTituloPinUsuario;
		TextView txtParrafoUnoPin;
		TextView txtSelectChannel;
		TextView txtSelectChannelSubtitle;
		TextView txtlnBtnPostPayment, txtlnBtnEnviarCobro, goBackPinUsuario, txtlnBtnMPos;
		//    UI
		Group pinValidationGroup, paymentTypeGroup;
		LinearLayout lnMPos;
		Toolbar defaultToolbar;
		//    Ops
		TrackingOps trackingOps;
		Realm realm;
		User user;
		FusedLocationProviderClient fusedLocationProviderClient;
		// Transaction
		String montoValorBase = "";
		String montoValorIva = "";
		String montoValorImpoConsumo = "";
		String montoValorPropina = "";
		String monto = "";
		String mposTransactionEndpoint = "/customers/transactions_mpos";
		String id = "";

		@Override
		@AddTrace(name = "onCreateTrace")
		public void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				setContentView(R.layout.pin_usuario);

				trackingOps = (TrackingOps) getApplication();
				realm = trackingOps.getRealm();
				user = trackingOps.getUser();

				Bundle extras = getIntent().getExtras();
				if (extras != null) {
						montoValorBase = extras.getString("montoValorBase");
						montoValorIva = extras.getString("montoValorIva");
						montoValorImpoConsumo = extras.getString("montoValorImpoConsumo");
						montoValorPropina = extras.getString("montoValorPropina");
						monto = extras.getString("monto");
				}

				lnBtnEnviarCobro = findViewById(R.id.lnBtnEnviarCobro);
				imgPin = findViewById(R.id.imgPin);

				fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

				txtSelectChannel = findViewById(R.id.txtSelectChannel);
				txtSelectChannel.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CHOOSE_MEANS));
				txtSelectChannelSubtitle = findViewById(R.id.txtSelectChannelSubtitle);
				txtSelectChannelSubtitle.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_MEANS_CONTINUE_TRANSACTION));
				txtPinUno = findViewById(R.id.txtPinUno);
				txtPinDos = findViewById(R.id.txtPinDos);
				txtPinTres = findViewById(R.id.txtPinTres);
				txtPinUno.setImeOptions(EditorInfo.IME_ACTION_DONE);
				txtPinDos.setImeOptions(EditorInfo.IME_ACTION_DONE);
				txtPinTres.setImeOptions(EditorInfo.IME_ACTION_DONE);
				goBackPinUsuario = findViewById(R.id.goBackPinUsuario);
				goBackPinUsuario.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_BACK));
				lnMPos = findViewById(R.id.lnMPos);
				pinValidationGroup = findViewById(R.id.pinValidationGroup);
				paymentTypeGroup = findViewById(R.id.paymentTypeGroup);

				txtlnBtnPostPayment = findViewById(R.id.txtlnBtnPostPayment);
				txtlnBtnPostPayment.setText(trackingOps.getmFirebaseRemoteConfig().getString(CHARGE_WITHOUT_CARD));

				txtTituloPinUsuario = findViewById(R.id.txtTituloPinUsuario);
				txtTituloPinUsuario.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_USER_PIN));
				txtParrafoUnoPin = findViewById(R.id.txtParrafoUnoPin);
				txtParrafoUnoPin.setText(trackingOps.getmFirebaseRemoteConfig().getString(PAYMENT_WITHOUT_CARD_SUBTITLE));
				txtlnBtnEnviarCobro = findViewById(R.id.txtlnBtnEnviarCobro);
				txtlnBtnEnviarCobro.setText(trackingOps.getmFirebaseRemoteConfig().getString(CHARGE_WITHOUT_CARD));

				txtlnBtnMPos = findViewById(R.id.txtlnBtnMPos);
				txtlnBtnMPos.setText(trackingOps.getmFirebaseRemoteConfig().getString(CHARGE_WITH_CARD));

				txtlnBtnPostPayment = findViewById(R.id.txtlnBtnPostPayment);
				txtlnBtnPostPayment.setText(trackingOps.getmFirebaseRemoteConfig().getString(CHARGE_WITHOUT_CARD));

				pinBoxContainer = findViewById(R.id.pinBoxContainer);
				pinBoxContainer.setOnClickListener(this);
				goBackPinUsuario.setOnClickListener(this);

				txtPinUno.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
				txtPinDos.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
				txtPinTres.setTypeface(util.fuenteSubTituHint(getApplicationContext()));

				goBackPinUsuario
						.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_flecha_atras_a), null,
								null, null);

				txtPinUno.setOnClickListener(this);
				txtPinDos.setOnClickListener(this);
				txtPinTres.setOnClickListener(this);
				if (user.getMposEnabled()) {
						lnMPos.setVisibility(View.VISIBLE);
				}

				txtPinUno.addTextChangedListener(new TextWatcher() {
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count,
								int after) {

						}

						@Override
						public void onTextChanged(final CharSequence s, int start, int before,
								int count) {
								imgPin.setVisibility(View.GONE);

						}

						@Override
						public void afterTextChanged(final Editable s) {
								//avoid triggering event when text is too short
								if (s.length() == 2) {
										txtPinDos.requestFocus();
										pinUno = true;
										txtPinDos.setEnabled(true);
								}

						}
				});
				txtPinDos.addTextChangedListener(new TextWatcher() {
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count,
								int after) {

						}

						@Override
						public void onTextChanged(final CharSequence s, int start, int before,
								int count) {

								imgPin.setVisibility(View.GONE);
						}

						@Override
						public void afterTextChanged(final Editable s) {
								//avoid triggering event when text is too short
								if (s.length() == 3) {
										txtPinTres.requestFocus();
										pinDos = true;
										txtPinTres.setEnabled(true);
								} else if (s.length() == 0) {
										txtPinUno.requestFocus();
										txtPinDos.setEnabled(false);
								}
						}
				});
				txtPinTres.addTextChangedListener(new TextWatcher() {
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {

						}

						@Override
						public void onTextChanged(final CharSequence s, int start, int before, int count) {
								imgPin.setVisibility(View.GONE);
						}

						@Override
						public void afterTextChanged(final Editable s) {
								//avoid triggering event when text is too short
								if (s.length() == 3) {
										imgPin.setVisibility(View.VISIBLE);
										pinTres = true;
										hideKeyboard();
								} else if (s.length() == 0) {
										txtPinDos.requestFocus();
										txtPinTres.setEnabled(false);
								}
						}
				});

				txtPinUno.setOnEditorActionListener(
						new EditText.OnEditorActionListener() {
								@Override
								public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
										if (actionId == EditorInfo.IME_ACTION_SEARCH ||
												actionId == EditorInfo.IME_ACTION_DONE ||
												event.getAction() == KeyEvent.ACTION_DOWN ||
												event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
												hideKeyboard();

												return true;
										}
										return false;
								}
						});
				txtPinDos.setOnEditorActionListener(
						new EditText.OnEditorActionListener() {
								@Override
								public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
										if (actionId == EditorInfo.IME_ACTION_SEARCH ||
												actionId == EditorInfo.IME_ACTION_DONE ||
												event.getAction() == KeyEvent.ACTION_DOWN ||
												event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
												hideKeyboard();

												return true;
										}
										return false;
								}
						});
				txtPinTres.setOnEditorActionListener(
						new EditText.OnEditorActionListener() {
								@Override
								public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
										if (actionId == EditorInfo.IME_ACTION_SEARCH ||
												actionId == EditorInfo.IME_ACTION_DONE ||
												event.getAction() == KeyEvent.ACTION_DOWN ||
												event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
												hideKeyboard();
												return true;
										}
										return false;
								}
						});

				trackingOps.registerScreenName("Escoger medio de cobro | Cívico PAY Comercios | " + user.getAnalyticsCity());

		}

		@Override
		public void onClick(View v) {
				InputMethodManager inputMethodManager;
				switch (v.getId()) {
						case R.id.pinBoxContainer:
								v.clearFocus();
								txtPinUno.requestFocus();
								inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
								inputMethodManager.showSoftInput(txtPinUno, InputMethodManager.SHOW_IMPLICIT);
								break;
						case R.id.goBackPinUsuario:
								if (pinValidationGroup.getVisibility() == View.VISIBLE && user.getMposEnabled()) {
										pinValidationGroup.setVisibility(View.GONE);
										paymentTypeGroup.setVisibility(View.VISIBLE);
								} else {
										finish();
								}
								break;
						default:
								inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
								inputMethodManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
								break;

				}
		}

		public void cargando() {
				trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Escoger medio de cobro",
						"Cobrar sin tarjeta | " + user.getBrand().getName());
				Intent detalleCrear = new Intent(getApplicationContext(), CargandoCobroActivty.class);
				detalleCrear.putExtra("montoValorBase", montoValorBase);
				detalleCrear.putExtra("montoValorIva", montoValorIva);
				detalleCrear.putExtra("montoValorImpoConsumo", montoValorImpoConsumo);
				detalleCrear.putExtra("montoValorPropina", montoValorPropina);
				detalleCrear.putExtra("monto", monto);
				detalleCrear.putExtra("userPin",
						txtPinUno.getText().toString() + txtPinDos.getText().toString() + txtPinTres.getText().toString());
				startActivity(detalleCrear);
				finish();
		}

		@Override
		public void onBackPressed() {
				super.onBackPressed();
				if (pinValidationGroup.getVisibility() == View.VISIBLE && user.getMposEnabled()) {
						pinValidationGroup.setVisibility(View.GONE);
						lnMPos.setVisibility(View.VISIBLE);
						paymentTypeGroup.setVisibility(View.VISIBLE);
				} else {
						super.onBackPressed();
				}
		}

		private void hideKeyboard() {
				imgPin.setVisibility(View.VISIBLE);
				View view = this.getCurrentFocus();
				if (view != null) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
				}
		}

		public void onEnviarCobro(View view) {
				if ((txtPinUno.getText().toString().length() == 2) &&
						(txtPinDos.getText().toString().length() == 3) &&
						(txtPinTres.getText().toString().length() == 3)) {
						if (pinUno && pinDos && pinTres) {
								trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Enviar cobro", "Cobro Ok");
								DialogConfirmarCobro cdd = new DialogConfirmarCobro(this, 1,
										txtPinUno.getText().toString() + " " + txtPinDos.getText().toString() + " " + txtPinTres.getText().toString(),
										monto, montoValorBase, montoValorIva, montoValorImpoConsumo, montoValorPropina);
								cdd.show();
						} else {
								Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(CODE_INCOM),
										Toast.LENGTH_SHORT).show();
						}
				} else {
						Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(CODE_INCOM),
								Toast.LENGTH_SHORT).show();
				}
		}

		public void processWithCivicoPay(View v) {
				if (pinValidationGroup.getVisibility() == View.VISIBLE) {
						if (pinUno && pinDos && pinTres) {
								DialogConfirmarCobro cdd = new DialogConfirmarCobro(this, 1,
										txtPinUno.getText().toString() + " " + txtPinDos.getText().toString() + " " + txtPinTres.getText().toString(),
										monto, montoValorBase, montoValorIva, montoValorImpoConsumo, montoValorPropina);
								cdd.show();
						} else {
								Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(CODE_INCOM),
										Toast.LENGTH_LONG).show();
						}
				} else {
						trackingOps.registerScreenName("Ingresar Pin | Cívico PAY Comercios | " + user.getAnalyticsCity());
						pinValidationGroup.setVisibility(View.VISIBLE);
						getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
						txtPinUno.setFocusableInTouchMode(true);
						txtPinUno.requestFocus();
						lnMPos.setVisibility(View.GONE);
						paymentTypeGroup.setVisibility(View.GONE);
						lnBtnEnviarCobro.setVisibility(View.GONE);

						TimerTask tt = new TimerTask() {
								@Override
								public void run() {
										InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
										imm.showSoftInput(txtPinUno, InputMethodManager.SHOW_IMPLICIT);
								}
						};
						final Timer timer = new Timer();
						timer.schedule(tt, 200);
				}
		}

		@NeedsPermission({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
		public void processWithMPos(View v) {

				trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY,
						trackingOps.getmFirebaseRemoteConfig().getString(MSJ_CHOOSE_MEANS_PAYMENT),
						"Cobrar con tarjeta | " + user.getBrand().getName());

				if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
						== PackageManager.PERMISSION_GRANTED
						&& ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
						== PackageManager.PERMISSION_GRANTED) {
						fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, this);
				}
		}

		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				super.onActivityResult(requestCode, resultCode, data);

        /*
        if (requestCode == TRANSACTION_REQUEST_CODE) {
            if (data == null) {
                // This can happen if SmartPesa was uninstalled or crashed while we're waiting for a
                // result.
                onNull();
            } else if (resultCode == RESULT_OK) {
                trackingOps.registerScreenName("Pago aprobado Tarjeta | Cívico PAY | " + user.getAnalyticsCity());
                // Parse successful transaction data.
                TransactionResult result = SpConnect.parseSuccessTransaction(data);
                onSuccess(result);
            } else {
                trackingOps.registerScreenName("Pago rechazado Tarjeta | Cívico PAY | " + user.getAnalyticsCity());
                // Parse failed transaction data.
                try {
                    TransactionError error = SpConnect.parseErrorTransaction(data);
                    onFail(error);
                } catch (Exception e) {
                    Toast.makeText(this, "Por favor revisa la versión de la aplicación Cajero instalada, verifica que sea la última en Play Store", Toast.LENGTH_LONG).show();
                    onNull();
                }
            }
        } else if (requestCode == LAST_TRANSACTION_REQUEST_CODE) {
            if (data == null) {
                // This can happen if SmartPesa was uninstalled or crashed while we're waiting for a
                // result.
                onNull();
            } else if (resultCode == RESULT_OK) {
                // Parse successful transaction data.
                TransactionResult result = SpConnect.parseSuccessTransaction(data);
                onSuccess(result);
            } else {
                // Parse failed transaction data.
                try {
                    TransactionError error = SpConnect.parseErrorTransaction(data);
                    onFail(error);
                } catch (Exception e) {
                    Toast.makeText(this, "Por favor revisa la versión de la aplicación Cajero instalada, verifica que sea la última en Play Store", Toast.LENGTH_LONG).show();
                    onNull();
                }
            }
        }
        */
		}

		private void onNull() {
				CivicoPagosWebService civicoPagosWebService = null;
				try {
						civicoPagosWebService = CivicoPagosWebService.retrofit.create(CivicoPagosWebService.class);
				} catch (Exception e) {
						Crashlytics.logException(e);
				}

				HashMap<String, String> headerMap = new HashMap<>();
				JSONObject jsonObject = new JSONObject();

				try {
						String encoded = util.encode(user.getAccessToken());
						encoded = encoded.replaceAll("\n", "");

						headerMap.put("Authorization", encoded);
						headerMap.put("Content-Type", "application/json");
						headerMap.put("Cache-Control", "no-cache");

						jsonObject.put("transaction_id", "");
						jsonObject.put("authorization_code", "");
						jsonObject.put("created_at", "");
						jsonObject.put("currency", "");
						jsonObject.put("amount", "");
						jsonObject.put("response_code", 1);
						jsonObject.put("response", "Rechazada");
						jsonObject.put("description", "No se pudo estrablecer comunicación con el MPos");
						jsonObject.put("authorization_id", "");
						jsonObject.put("authorization_response_code", "");
						jsonObject.put("authorization_response", "");
						jsonObject.put("reversed", false);
						jsonObject.put("type", "SALES");
						jsonObject.put("pan", "");
						jsonObject.put("holder", "");
						jsonObject.put("device_id", user.getDeviceID());
						jsonObject.put("device_token", user.getPushToken());

				} catch (JSONException e) {
						e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
				} catch (NullPointerException e) {
						e.printStackTrace();
				}

				ProgressDialog progressDialog = new ProgressDialog(this);
				progressDialog.setMessage("Consultando, por favor espere");
				progressDialog.show();
				progressDialog.setCanceledOnTouchOutside(false);
				progressDialog.setCancelable(false);

				RequestBody body = RequestBody
						.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject).toString());

				try {
						if (civicoPagosWebService == null) {
								try {
										civicoPagosWebService = CivicoPagosWebService.retrofit.create(CivicoPagosWebService.class);
								} catch (Exception ignore) {
								}
						}

						Call<ResponseBody> call = civicoPagosWebService.mposTransaction(id, headerMap, body);
						call.enqueue(new Callback<ResponseBody>() {
								@Override
								public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rawResponse) {
										try {
												progressDialog.dismiss();
												Toast.makeText(getApplicationContext(),
														trackingOps.getmFirebaseRemoteConfig().getString(MSJ_PLEASE_TRY_AGAIN), Toast.LENGTH_LONG).show();
												Intent intent = new Intent(getApplicationContext(), MisPagosActivity.class);
												intent.putExtra("mpos", false);
												startActivity(intent);
												finish();
										} catch (NullPointerException e) {
												e.printStackTrace();
										}
								}

								@Override
								public void onFailure(Call<ResponseBody> call, Throwable throwable) {
										Toast.makeText(getApplicationContext(), throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
								}
						});
				} catch (Exception ignore) {
				}
		}

    /*
    private void onFail(TransactionError error) {

        CivicoPagosWebService civicoPagosWebService = CivicoPagosWebService.retrofit.create(CivicoPagosWebService.class);

        HashMap<String, String> headerMap = new HashMap<String, String>();

        if (error.transactionResult() != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                String encoded = util.encode(user.getAccessToken());
                encoded = encoded.replaceAll("\n", "");

                headerMap.put("Authorization", encoded);
                headerMap.put("Content-Type", "application/json");
                headerMap.put("Cache-Control", "no-cache");

                TransactionResult r = error.transactionResult();

                jsonObject.put("transaction_id", r.id());
                jsonObject.put("authorization_code", r.reference());
                jsonObject.put("created_at", r.datetime().toString());
                jsonObject.put("currency", r.currency().symbol());
                jsonObject.put("amount", r.amount());
                jsonObject.put("response_code", r.responseCode());
                jsonObject.put("response", r.responseDescription());
                jsonObject.put("description", r.description());
                jsonObject.put("authorization_id", r.authorisationId());
                jsonObject.put("authorization_response_code", r.authorisationResponseCode());
                jsonObject.put("authorization_response", r.authorisationResponse());
                jsonObject.put("reversed", r.isReversed());
                jsonObject.put("type", r.type().name());
                jsonObject.put("pan", r.card().pan());
                jsonObject.put("holder", r.card().holderName());
                jsonObject.put("device_id", user.getDeviceID());
                jsonObject.put("device_token", user.getPushToken());

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject).toString());

            Call<ResponseBody> call = civicoPagosWebService.mposTransaction(id, headerMap, body);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rawResponse) {
                    try {
                        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(MSJ_PLEASE_TRY_AGAIN), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MisPagosActivity.class);
                        intent.putExtra("mpos", true);
                        startActivity(intent);
                        finish();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                    Toast.makeText(getApplicationContext(), throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }

        //Toast.makeText(this, trackingOps.getmFirebaseRemoteConfig().getString(MSJ_PLEASE_TRY_AGAIN), Toast.LENGTH_LONG).show();
    }

    private void onSuccess(TransactionResult result) {

        CivicoPagosWebService civicoPagosWebService = CivicoPagosWebService.retrofit.create(CivicoPagosWebService.class);
        HashMap<String, String> headerMap = new HashMap<String, String>();

        JSONObject jsonObject = new JSONObject();
        try {

            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            headerMap.put("Authorization", encoded);
            headerMap.put("Content-Type", "application/json");
            headerMap.put("Cache-Control", "no-cache");

            jsonObject.put("transaction_id", result.id());
            jsonObject.put("authorization_code", result.reference());
            jsonObject.put("created_at", result.datetime().toString());
            jsonObject.put("currency", result.currency().symbol());
            jsonObject.put("amount", result.amount());
            jsonObject.put("response_code", result.responseCode());
            jsonObject.put("response", result.responseDescription());
            jsonObject.put("description", result.description());
            jsonObject.put("authorization_id", result.authorisationId());
            jsonObject.put("authorization_response_code", result.authorisationResponseCode());
            jsonObject.put("authorization_response", result.authorisationResponse());
            jsonObject.put("reversed", result.isReversed());
            jsonObject.put("type", result.type().name());
            jsonObject.put("pan", result.card().pan());
            jsonObject.put("holder", result.card().holderName());
            jsonObject.put("device_id", user.getDeviceID());
            jsonObject.put("device_token", user.getPushToken());

            if (user.getLatitude() == null) {
                jsonObject.put("coordinates", "0.0,0.0");
            } else {
                jsonObject.put("coordinates", String.valueOf(user.getLongitude()) + "," + String.valueOf(user.getLatitude()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject).toString());

        Call<ResponseBody> call = civicoPagosWebService.mposTransaction(id, headerMap, body);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rawResponse) {
                try {
                    Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(MSJ_TRANSACTION_COMPLETED_SUCCESSFULLY), Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), MisPagosActivity.class);
                    intent.putExtra("mpos", true);
                    startActivity(intent);
                    finish();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Toast.makeText(getApplicationContext(), throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    */

		public void initMposProcess(View v) {
        /*
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            processWithMPos(v);
        } else {
            try {
                PinUsuarioActivityPermissionsDispatcher.processWithMPosWithPermissionCheck(this, lnMPos);
            } catch (Exception e) {
                processWithMPos(v);
            }
        }
        */
		}

		@Override
		public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				PinUsuarioActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
		}

		@OnShowRationale({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
		void showRationaleForLocation(PermissionRequest request) {
				new AlertDialog.Builder(this)
						.setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_PHONE_RATIONALE))
						.setPositiveButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_ALLOW),
								(dialog, button) -> request.proceed())
						.setNegativeButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_DENY),
								(dialog, button) -> request.cancel())
						.show();
		}

		// Annotate a method which is invoked if the user doesn't grant the permissions
		@OnPermissionDenied({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
		void showDeniedForLocation() {
				processWithMPos(lnMPos);
		}

		// Annotates a method which is invoked if the user
		// chose to have the device "never ask again" about a permission
		@OnNeverAskAgain({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
		void showNeverAskForLocation() {
				processWithMPos(lnMPos);
		}

		@Override
		public void onSuccess(Location location) {
				if (location != null) {
						realm.beginTransaction();
						user.setLatitude(location.getLatitude());
						user.setLongitude(location.getLongitude());
						realm.commitTransaction();
				}

				/*
				if (SpConnect.isSmartPesaInstalled(getApplicationContext())) {
						makeMPosRequest();
				} else {
						Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(MPOS_ERROR_MESSAGE), Toast.LENGTH_LONG).show();
				}
				*/
		}

		public void makeMPosRequest() {
				Bundle queryBundle = new Bundle();
				JSONObject body = new JSONObject();

				try {

						int valorBaseInt = 0;

						int tipValue = Integer.parseInt(montoValorPropina.replace(".", ""));
						BigDecimal tip = new BigDecimal(tipValue);

						int tax1Value = Integer.parseInt(montoValorIva.replace(".", ""));
						BigDecimal tax1 = new BigDecimal(tax1Value);

						int tax2Value = Integer.parseInt(montoValorImpoConsumo.replace(".", ""));
						BigDecimal tax2 = new BigDecimal(tax2Value);

						if (!montoValorBase.equals("")) {
								valorBaseInt = Integer.parseInt(montoValorBase.replace(".", ""));
						}

						valorBaseInt = valorBaseInt - (tax1Value + tax2Value);

						BigDecimal valorBaseBigDecimal = new BigDecimal(valorBaseInt);

						String longitude = "0.0";
						String latitude = "0.0";

						if (user.getLongitude() != null) {
								longitude = String.valueOf(user.getLongitude());
								latitude = String.valueOf(user.getLatitude());
						}

						body.put("amount", valorBaseBigDecimal);
						body.put("tax_amount", tax1);
						body.put("iac_amount", tax2);
						body.put("tip_amount", tip);
						body.put("device_id", user.getDeviceID());
						body.put("device_token", user.getPushToken());
						body.put("coordinates", longitude + "," + latitude);

						String encoded = util.encode(user.getAccessToken());
						encoded = encoded.replaceAll("\n", "");

						queryBundle.putString(Constants.OPERATION_BODY, body.toString());
						queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
						queryBundle.putString(Constants.OPERATION_TYPE, "POST");
						queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
						queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL + mposTransactionEndpoint);

						LoaderManager loaderManager = getSupportLoaderManager();
						Loader<String> loader = loaderManager.getLoader(MPOS_TRANSACTION_INITIALIZATION);

						if (loader == null) {
								loaderManager.initLoader(MPOS_TRANSACTION_INITIALIZATION, queryBundle, this);
						} else {
								loaderManager.restartLoader(MPOS_TRANSACTION_INITIALIZATION, queryBundle, this);
						}
				} catch (JSONException e) {
						e.printStackTrace();
						util.alertError500(this);
				} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						util.alertError500(this);
				} catch (NullPointerException e) {
						e.printStackTrace();
				}
		}

		@NonNull
		@Override
		public Loader<String> onCreateLoader(int id, @Nullable Bundle args) {
				toggleProgressDialog(true);
				return new RequestDefaultFactory(this, args);
		}

		@Override
		public void onLoadFinished(@NonNull Loader<String> loader, String data) {
				toggleProgressDialog(false);
				switch (loader.getId()) {
						case MPOS_TRANSACTION_INITIALIZATION:
								try {
										JSONObject responseObject = new JSONObject(data);
										id = responseObject.getString("id");
								} catch (JSONException e) {
										e.printStackTrace();
										util.alertError500(this);
								} catch (NullPointerException e) {
										e.printStackTrace();
										util.alertError500(this);
								}

								//sendTransaction(this);
								break;
						default:
								break;
				}
		}

    /*
    public void sendTransaction(Activity activity) {

        // First, check if there's any instance of SmartPesa application that is installed
        // in this device.
        if (SpConnect.isSmartPesaInstalled(this)) {

            // Specify the amount that will be used for the transaction.
            BigDecimal amount = new BigDecimal("100.00");

            // Specify the transaction type which will take place.
            // Currently it only supports SALES, VOID, and REFUND.


            int valorBaseInt = 0;

            int tipValue = Integer.parseInt(montoValorPropina.replace(".", ""));
            BigDecimal tip = new BigDecimal(tipValue);

            int tax1Value = Integer.parseInt(montoValorIva.replace(".", ""));
            BigDecimal tax1 = new BigDecimal(tax1Value);

            int tax2Value = Integer.parseInt(montoValorImpoConsumo.replace(".", ""));
            BigDecimal tax2 = new BigDecimal(tax2Value);

            if (!montoValorBase.equals("")) {
                valorBaseInt = Integer.parseInt(montoValorBase.replace(".", ""));
            }

            valorBaseInt = valorBaseInt - (tax1Value + tax2Value);

            // Specify the transaction type which will take place.
            // Currently it only supports SALES, VOID, and REFUND.
            TransactionType transactionType = TransactionType.SALES;

            BigDecimal valorBaseBigDecimal = new BigDecimal(valorBaseInt);

            // Create TransactionArgument object using the provided builder method.
            TransactionArgument argument = TransactionArgument.builder()
                    .transactionType(transactionType)
                    .amount(valorBaseBigDecimal)
                    .tip(tip)
                    .tax1Amount(tax1)
                    .tax1Type(TAX_1_TYPE)
                    .tax2Amount(tax2)
                    .tax2Type(TAX_2_TYPE)
                    .printReceipt(true)
                    .externalReference(id)
                    .build();

            // Create intent from TransactionArgument which will include all the necessary
            // details of the transaction.
            Intent intent = SpConnect.createTransactionIntent(argument, true);

            try {
                // This will starts SmartPesa activity which will handle your payment request
                // based on the argument you provided in TransactionArgument object.
                activity.startActivityForResult(intent, TRANSACTION_REQUEST_CODE);

            } catch (ActivityNotFoundException e) {
                // Unable to find the activity to handle your transaction.
                // SmartPesa mPos app might has just been recently uninstalled.
                //showMessage("SmartPesa was recently uninstalled.");
            }
        } else {
            // SmartPesa mPos app is not installed in this device.
            // Unable to start transaction.
            Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(MPOS_ERROR_MESSAGE), Toast.LENGTH_LONG).show();
            //showMessage("SmartPesa is not installed.");
        }
    }
    */

		@Override
		public void onLoaderReset(@NonNull Loader<String> loader) {
				toggleProgressDialog(false);
		}

}
