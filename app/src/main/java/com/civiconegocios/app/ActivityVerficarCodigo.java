package com.civiconegocios.app;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;


public class ActivityVerficarCodigo extends BaseActivity implements View.OnClickListener {

    private static final String TOOLBAR_MENU_VERIFY_CODE = "toolbar_menu_verify_code";
    private static final String CHECK = "check";
    private static final String ENTER_THE_USER_CODE = "enter_the_user_code";
    private static final String REQUEST_CODE_CLIENT_STATUS_OFFER = "request_code_client_status_offer";
    //    UI
    Toolbar defaultToolbar;
    TextView txtBtnVerificar;
    TextView toolbar_title;
    TextView textOpenSansLight6;
    TextView textOpenSansLight7;
    EditText editCodigoAVerificar;
    Utils util = new Utils();
    //    Ops
    User user;
    TrackingOps trackingOps;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verificar_codigo);

        initDrawer();

        trackingOps = (TrackingOps) getApplicationContext();

        user = ((TrackingOps) this.getApplication()).getUser();
        user = ((TrackingOps) this.getApplication()).getUser();

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_VERIFY_CODE));

        textOpenSansLight6 = findViewById(R.id.textOpenSansLight6);
        textOpenSansLight6.setText(trackingOps.getmFirebaseRemoteConfig().getString(ENTER_THE_USER_CODE));

        textOpenSansLight7 = findViewById(R.id.textOpenSansLight7);
        textOpenSansLight7.setText(trackingOps.getmFirebaseRemoteConfig().getString(REQUEST_CODE_CLIENT_STATUS_OFFER));

        editCodigoAVerificar = findViewById(R.id.editCodigoAVerificar);

        txtBtnVerificar = findViewById(R.id.txtBtnVerificar);
        txtBtnVerificar.setText(trackingOps.getmFirebaseRemoteConfig().getString(CHECK).toUpperCase());
        txtBtnVerificar.setOnClickListener(this);

        editCodigoAVerificar.setTypeface(util.fuenteParrafos(getApplicationContext()));
        editCodigoAVerificar.requestFocus();

        trackingOps = (TrackingOps) getApplication();
        trackingOps.registerScreenName("Verificar código | " + user.getCustomer().getName() + " | Cívico PAY Comercios | " + user.getAnalyticsCity());

        editCodigoAVerificar.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(editCodigoAVerificar, InputMethodManager.SHOW_IMPLICIT);
            }
        }, 500);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtBtnVerificar:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(TOOLBAR_MENU_VERIFY_CODE), ((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(CHECK));
                Intent i = new Intent(getApplicationContext(), CargandoVerificacion.class);
                i.putExtra("KEY", editCodigoAVerificar.getText().toString());
                startActivity(i);
                editCodigoAVerificar.setText("");
                break;
            default:
                break;
        }
    }

}
