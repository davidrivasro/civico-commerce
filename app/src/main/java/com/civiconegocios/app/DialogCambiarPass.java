package com.civiconegocios.app;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.utils.Utils;

import static com.pushwoosh.internal.platform.AndroidPlatformModule.getApplicationContext;

public class DialogCambiarPass extends Dialog implements View.OnClickListener {

    private static final String CAMPOS_VACIOS = "campos_vacios";
    private static final String PASS_DESIGUALES = "pass_desiguales";
    private static final String LBL_BTN_CHANGE_PASSWORD = "lbl_btn_change_password";
    private static final String CURRENT_PASSWORD = "current_password";
    private static final String LBL_NUEVA_PASS = "lbl_nueva_pass";
    private static final String CONFIRM_PASSWORD = "confirm_password";
    private static final String LBL_BTN_SAVE_CHANGES = "lbl_btn_save_changes";
    PerfilActivity c;
    LinearLayout lnCambiarPassDialog;
    EditText editPassCambiar;
    EditText editPassNuevaCambiar;
    EditText editPassNuevaConfirCambiarCambiar;
    TextView txtTituloDialogPass;
    TextView txtTituloDialogPassActual;
    TextView txtTituloDialogPassNueva;
    TextView txtTituloDialogPassConfirmar;
    TextView txtTituloDialogGuardarPass;
    Utils util = new Utils();
    TrackingOps trackingOps;


    public DialogCambiarPass(PerfilActivity a, int formaspago) {
        super(a);

        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_cambiar_pass);

        trackingOps = (TrackingOps) getApplicationContext();

        editPassCambiar = findViewById(R.id.editPassCambiar);
        editPassNuevaCambiar = findViewById(R.id.editPassNuevaCambiar);
        editPassNuevaConfirCambiarCambiar = findViewById(R.id.editPassNuevaConfirCambiarCambiar);
        lnCambiarPassDialog = findViewById(R.id.lnCambiarPassDialog);

        lnCambiarPassDialog.setOnClickListener(this);

        txtTituloDialogPass = findViewById(R.id.txtTituloDialogPass);
        txtTituloDialogPass.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CHANGE_PASSWORD));
        txtTituloDialogPassActual = findViewById(R.id.txtTituloDialogPassActual);
        txtTituloDialogPassActual.setText(trackingOps.getmFirebaseRemoteConfig().getString(CURRENT_PASSWORD));
        txtTituloDialogPassNueva = findViewById(R.id.txtTituloDialogPassNueva);
        txtTituloDialogPassNueva.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_NUEVA_PASS));
        txtTituloDialogPassConfirmar = findViewById(R.id.txtTituloDialogPassConfirmar);
        txtTituloDialogPassConfirmar.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONFIRM_PASSWORD));
        txtTituloDialogGuardarPass = findViewById(R.id.txtTituloDialogGuardarPass);
        txtTituloDialogGuardarPass.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_SAVE_CHANGES));

        txtTituloDialogPass.setTypeface(util.fuenteSubTituHint(c));
        txtTituloDialogPassActual.setTypeface(util.fuenteSubTituHint(c));
        txtTituloDialogPassNueva.setTypeface(util.fuenteSubTituHint(c));
        txtTituloDialogPassConfirmar.setTypeface(util.fuenteSubTituHint(c));
        txtTituloDialogGuardarPass.setTypeface(util.fuenteMenuLatBotones(c));

        editPassCambiar.setTypeface(util.fuenteSubTituHint(c));
        editPassNuevaCambiar.setTypeface(util.fuenteSubTituHint(c));
        editPassNuevaConfirCambiarCambiar.setTypeface(util.fuenteSubTituHint(c));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnCambiarPassDialog:
                if (editPassCambiar.getText().toString().equals("") || editPassNuevaCambiar.getText().toString().equals("") || editPassNuevaConfirCambiarCambiar.getText().toString().equals("")) {
                    Toast.makeText(c, trackingOps.getmFirebaseRemoteConfig().getString(CAMPOS_VACIOS),
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (editPassNuevaCambiar.getText().toString().equals(editPassNuevaConfirCambiarCambiar.getText().toString())) {
                        c.lanzarAsynkCambiarPass(editPassCambiar.getText().toString(), editPassNuevaCambiar.getText().toString());
                        dismiss();
                    } else {
                        Toast.makeText(c, trackingOps.getmFirebaseRemoteConfig().getString(PASS_DESIGUALES),
                                Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            default:
                break;
        }

    }
}
