package com.civiconegocios.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.UserDataSerializer;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import io.realm.Realm;

public class ActivityCambiarPassInicial extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<String> {

    private static final String LBL_TERMINOS_INICIAL = "lbl_terminos_inicial";
    private static final String LBL_TERMINOS_INICIAL_PAY = "lbl_terminos_inicial_pay";
    private static final String LBL_ACEPTAS = "lbl_aceptas";
    private static final String PASS_NO_COINCIDEN = "pass_no_coinciden";
    private static final String ACEPTAR_TERMINOS_CONDICIONES = "aceptar_terminos_condiciones";
    private static final String LBL_CAMBIAR_PASS_INICIAL = "lbl_cambiar_pass_inicial";
    private static final String LBL_AL_SER = "lbl_al_ser";
    private static final String LBL_NUEVA_PASS = "lbl_nueva_pass";
    private static final String LBL_NUEVA_PASS_REP = "lbl_nueva_pass_rep";
    private static final String LBL_BTN_CONTINUAR = "lbl_btn_continuar";
    private static final String URL_TERMS_CONDITIONS = "url_terms_conditions";
    private static final String URL_TERMS_CONDITIONS_CIVIC_PAY = "url_terms_conditions_civic_pay";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private final String TAG = ActivityCambiarPassInicial.class.getSimpleName();
    //    Loaders IDS
    private final int DATA_REQUEST_LOADER = 10;
    private final int PAY_CONDITIONS_LOADER = 11;
    private final int PAY_ACCEPT_CONDITIONS_LOADER = 12;
    TextView txtCambiarPassInicial;
    TextView txtAlSerLaPrimera;
    EditText editNuevaPassInicial;
    EditText editNuevaPassInicialRep;
    TextView txtTerminosInicial;
    TextView txtbtnContinuarCambiarPass;
    Utils util = new Utils();
    String value = "";
    AppCompatImageView btnCheck, btnCheckPay;
    boolean checkOn = true;
    boolean checkOnPay = true;
    boolean flag;
    boolean payFlag;
    String urlUser = "/users/edit";
    User user;
    TrackingOps trackingOps;
    private Realm realm;

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_pass_inicial);

        trackingOps = (TrackingOps) getApplicationContext();


        realm = trackingOps.getRealm();
        user = trackingOps.getUser();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("pass");
        }

        if (user.getAcceptingPayments()) {
            (findViewById(R.id.lyTerminosCívicoPay)).setVisibility(View.VISIBLE);
            (findViewById(R.id.txtTerminosInicialPay)).setOnClickListener(this);

            String next = "<font color='#1d96c5'>" + trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL_PAY) + "</font>";
            ((TextView) (findViewById(R.id.txtTerminosInicialPay))).setText(Html.fromHtml(trackingOps.getmFirebaseRemoteConfig().getString(LBL_ACEPTAS) + " " + next));
        }

        flag = false;
        makeDataRequest();

        txtCambiarPassInicial = findViewById(R.id.txtCambiarPassInicial);
        txtCambiarPassInicial.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CAMBIAR_PASS_INICIAL));
        txtAlSerLaPrimera = findViewById(R.id.txtAlSerLaPrimera);
        txtAlSerLaPrimera.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_AL_SER));
        editNuevaPassInicial = findViewById(R.id.editNuevaPassInicial);
        editNuevaPassInicial.setHint(trackingOps.getmFirebaseRemoteConfig().getString(LBL_NUEVA_PASS));
        editNuevaPassInicialRep = findViewById(R.id.editNuevaPassInicialRep);
        editNuevaPassInicialRep.setHint(trackingOps.getmFirebaseRemoteConfig().getString(LBL_NUEVA_PASS_REP));
        txtTerminosInicial = findViewById(R.id.txtTerminosInicial);
        txtTerminosInicial.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL));
        txtbtnContinuarCambiarPass = findViewById(R.id.txtbtnContinuarCambiarPass);
        txtbtnContinuarCambiarPass.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_CONTINUAR));
        btnCheck = findViewById(R.id.btnCheck);
        btnCheckPay = findViewById(R.id.btnCheckPay);

        editNuevaPassInicial.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        editNuevaPassInicialRep.setTypeface(util.fuenteSubTituHint(getApplicationContext()));

        txtbtnContinuarCambiarPass.setOnClickListener(this);
        txtTerminosInicial.setOnClickListener(this);

        String next = "<font color='#1d96c5'>" + trackingOps.getmFirebaseRemoteConfig().getString(LBL_TERMINOS_INICIAL) + "</font>";
        ((TextView) (findViewById(R.id.txtTerminosInicial))).setText(Html.fromHtml(trackingOps.getmFirebaseRemoteConfig().getString(LBL_ACEPTAS) + " " + next));

        btnCheck.setOnClickListener(this);
        btnCheckPay.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtbtnContinuarCambiarPass:
                if (checkOn && checkOnPay) {
                    if (editNuevaPassInicial.getText().toString().equals(editNuevaPassInicialRep.getText().toString())) {
                        if (util.haveInternet(getApplicationContext())) {
                            if (user.getAcceptingPayments()) {
                                makePayAcceptConditionsRequest();
                            } else {
                                updateCondicionesPay();
                            }
                        } else {
                            util.mensajeNoInternet(getApplicationContext());
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(PASS_NO_COINCIDEN),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(ACEPTAR_TERMINOS_CONDICIONES),
                            Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnCheck:
                if (checkOn) {
                    btnCheck.setImageResource(R.drawable.ic_btn_check);
                    checkOn = false;
                } else {
                    btnCheck.setImageResource(R.drawable.ic_btn_checkon);
                    checkOn = true;
                }
                break;

            case R.id.btnCheckPay:
                if (checkOnPay) {
                    btnCheckPay.setImageResource(R.drawable.ic_btn_check);
                    checkOnPay = false;
                } else {
                    btnCheckPay.setImageResource(R.drawable.ic_btn_checkon);
                    checkOnPay = true;
                }
                break;

            case R.id.txtTerminosInicial:
                Intent detalle = new Intent(getApplicationContext(), WebSobreCivicoActivity.class);
                detalle.putExtra("KEY", trackingOps.getmFirebaseRemoteConfig().getString(URL_TERMS_CONDITIONS) + user.getCity() + "&layout=false");
                startActivity(detalle);
                break;

            case R.id.txtTerminosInicialPay:
                Intent detallePay = new Intent(getApplicationContext(), WebSobreCivicoActivity.class);
                detallePay.putExtra("KEY", trackingOps.getmFirebaseRemoteConfig().getString(URL_TERMS_CONDITIONS_CIVIC_PAY) + user.getCity() + "&layout=false");
                startActivity(detallePay);
                break;

            default:
                break;
        }
    }

    public void cambioInicialExitoso() {
        if (util.haveInternet(getApplicationContext())) {
            flag = true;
            makeDataRequest();
        } else {
            util.mensajeNoInternet(getApplicationContext());
        }
    }

    public void loginOk(String userData, boolean flag) {
        if (flag) {
            Intent detalle = new Intent(getApplicationContext(), inicioActivity.class);
            startActivity(detalle);
            finish();
        }
    }

    public void updateCondicionesPay() {
        payFlag = false;
        if (user.getAcceptingPayments()) {
            payFlag = true;
            realm.beginTransaction();
            user.setAcceptingPayments(true);
            realm.commitTransaction();
        }

        makePayConditionsRequest();
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        RequestDefaultFactory requestDefaultFactory = new RequestDefaultFactory(this, args);
        this.toggleProgressDialog(true);
        switch (id) {
            case PAY_CONDITIONS_LOADER:
                txtbtnContinuarCambiarPass.setEnabled(false);
                break;
            default:
                break;
        }

        return requestDefaultFactory;

    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        this.toggleProgressDialog(false);
        if (data == null) {
            util.alertErrorTimeOut(this);
        } else {
            switch (loader.getId()) {
                case DATA_REQUEST_LOADER:
                    try {
                        UserDataSerializer userDataSerializer = new UserDataSerializer();
                        String userData = userDataSerializer.processPasswordUpdateData(data, this.user, this);
                        this.loginOk(userData, this.flag);
                    } catch (IOException e) {
                        e.printStackTrace();
                        util.alertError500(this);
                    }
                    break;
                case PAY_CONDITIONS_LOADER:
                    txtbtnContinuarCambiarPass.setEnabled(true);
                    cambioInicialExitoso();
                case PAY_ACCEPT_CONDITIONS_LOADER:
                    updateCondicionesPay();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        this.toggleProgressDialog(false);
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            queryBundle.putString(Constants.OPERATION_TYPE, "GET");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL);
            queryBundle.putString(Constants.OPERATION_ENDPOINT, this.urlUser);
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(DATA_REQUEST_LOADER);

            if (loader == null) {
                loaderManager.initLoader(DATA_REQUEST_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(DATA_REQUEST_LOADER, queryBundle, this);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    public void makePayConditionsRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            body.put("tocs", "true");
            body.put("tos_pay_confirmation", String.valueOf(this.payFlag));
            body.put("current_password", value);
            body.put("password", editNuevaPassInicial.getText().toString());
            body.put("password_confirmation", editNuevaPassInicial.getText().toString());

            queryBundle.putString(Constants.OPERATION_TYPE, "PUT");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL + "/users");
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_BODY, body.toString());

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(PAY_CONDITIONS_LOADER);

            if (loader == null) {
                loaderManager.initLoader(PAY_CONDITIONS_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(PAY_CONDITIONS_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    public void makePayAcceptConditionsRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            body.put("tos_pay_confirmation", String.valueOf(this.payFlag));

            queryBundle.putString(Constants.OPERATION_TYPE, "PUT");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + "/users");
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_BODY, body.toString());

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(PAY_ACCEPT_CONDITIONS_LOADER);

            if (loader == null) {
                loaderManager.initLoader(PAY_ACCEPT_CONDITIONS_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(PAY_ACCEPT_CONDITIONS_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }
}
