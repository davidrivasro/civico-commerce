package com.civiconegocios.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static com.pushwoosh.internal.platform.AndroidPlatformModule.getApplicationContext;

public class DialogComunicate extends AppCompatDialog implements
        View.OnClickListener, LoaderManager.LoaderCallbacks<String> {

    private static final String CHANGE_INFORMATION = "change_information";
    private static final String HELP = "help";
    private static final String TECHNICAL_SUPPORT = "technical_support";
    private static final String PROCESS_DIALOG_MESSAGE = "process_dialog_message";
    private static final String LBL_TELL_US_CONCERN = "lbl_tell_us_concern";
    private static final String LBL_ATTENTION_SCHEDULE = "lbl_attention_schedule";
    private static final String LBL_FIELDS_REQUIRED = "lbl_fields_required";
    private static final String LBL_CATEGORY = "lbl_category";
    private static final String LBL_COMMENTARY = "lbl_commentary";
    private static final String LBL_I = "lbl_i";
    private static final String LBL_BTN_SEND = "lbl_btn_send";
    Context c;
    int idComunicate = 0;
    Utils util = new Utils();
    String urlServicio = "/brand/contact";
    User user;
    TrackingOps trackingOps;
    private Spinner spinnerComunicate;
    private EditText editComunicate;
    private TextView txtEnviarComunicate;
    private TextView txtTitDatosTitulo;
    private TextView textOpenSansRegular7;
    private TextView textOpenSansRegular8;
    private TextView textOpenSansRegular9;
    private TextView textOpenSansRegular10;
    private ProgressDialog progressDialog;


    public DialogComunicate(Context a) {
        super(a);

        this.c = a;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_comunicate_nosotros);


        user = ((TrackingOps) getApplicationContext()).getUser();
        trackingOps = (TrackingOps) getApplicationContext();

        txtTitDatosTitulo = findViewById(R.id.txtTitDatosTitulo);
        txtTitDatosTitulo.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_TELL_US_CONCERN));
        textOpenSansRegular7 = findViewById(R.id.textOpenSansRegular7);
        textOpenSansRegular7.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_ATTENTION_SCHEDULE));
        textOpenSansRegular8 = findViewById(R.id.textOpenSansRegular8);
        textOpenSansRegular8.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_FIELDS_REQUIRED));
        textOpenSansRegular9 = findViewById(R.id.textOpenSansRegular9);
        textOpenSansRegular9.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CATEGORY));
        textOpenSansRegular10 = findViewById(R.id.textOpenSansRegular10);
        textOpenSansRegular10.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_COMMENTARY));
        spinnerComunicate = findViewById(R.id.spinnerComunicate);
        editComunicate = findViewById(R.id.editComunicate);
        editComunicate.setHint(trackingOps.getmFirebaseRemoteConfig().getString(LBL_I));
        editComunicate.setTypeface(util.fuenteParrafos(c));
        spinnerComunicate.setOnItemSelectedListener(new YourItemSelectedListener());
        txtEnviarComunicate = findViewById(R.id.txtEnviarComunicate);
        txtEnviarComunicate.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_SEND));
        txtEnviarComunicate.setOnClickListener(this);


        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add(trackingOps.getmFirebaseRemoteConfig().getString(CHANGE_INFORMATION));
        spinnerArray.add(trackingOps.getmFirebaseRemoteConfig().getString(HELP));
        spinnerArray.add(trackingOps.getmFirebaseRemoteConfig().getString(TECHNICAL_SUPPORT));


        ArrayAdapter<String> adapterNuevo = new ArrayAdapter<String>(c, R.layout.spinner_item_dos, spinnerArray) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                // Typeface externalFont=Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTCom-Lt.ttf");
                ((TextView) v).setTypeface(util.fuenteParrafos(c));

                return v;
            }


            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                //  Typeface externalFont=Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTCom-Lt.ttf");
                ((TextView) v).setTypeface(util.fuenteParrafos(c));


                return v;
            }
        };

        adapterNuevo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerComunicate.setAdapter(adapterNuevo);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtEnviarComunicate:
                if (util.haveInternet(c)) {
                    dismiss();
                    makeDataRequest();
                } else {
                    util.mensajeNoInternet(c);
                }

                break;
            default:
                break;
        }

    }

    public JSONObject crearJson() {

        JSONObject student1 = new JSONObject();
        try {
            student1.put("text", editComunicate.getHint().toString());
            student1.put("category_id", idComunicate);

        } catch (JSONException e) {

            e.printStackTrace();
        }
        JSONObject studentsObj = new JSONObject();
        try {
            studentsObj.put("contact", student1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return studentsObj;
    }

    public void makeDataRequest() {

        AppCompatActivity parent = (AppCompatActivity) c;
        if (parent != null) {
            Bundle queryBundle = new Bundle();

            try {
                String encoded = util.encode(user.getAccessToken());
                encoded = encoded.replaceAll("\n", "");

                queryBundle.putString(Constants.OPERATION_BODY, crearJson().toString());
                queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
                queryBundle.putString(Constants.OPERATION_TYPE, "POST");
                queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
                queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + urlServicio);

                LoaderManager loaderManager = parent.getSupportLoaderManager();
                Loader<String> loader = loaderManager.getLoader(0);

                if (loader == null) {
                    loaderManager.initLoader(0, queryBundle, this);
                } else {
                    loaderManager.restartLoader(0, queryBundle, this);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                util.alertError500(this.getContext());
            }
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        progressDialog = new ProgressDialog(this.getContext());
        progressDialog.setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PROCESS_DIALOG_MESSAGE));
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        return new RequestDefaultFactory(this.getContext(), args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        if (data == null) {
            util.alertErrorTimeOut(c);
        } else {
            try {
                JSONObject catObj = new JSONObject(data);
                Toast.makeText(c, catObj.get("message") + "",
                        Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public class YourItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            idComunicate = pos;
            String selected = parent.getItemAtPosition(pos).toString();
        }

        public void onNothingSelected(AdapterView parent) {
            // Do nothing.
        }
    }
}
