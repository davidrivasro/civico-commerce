package com.civiconegocios.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.appcompat.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.adapters.MessageDetailExpandableListAdapter;
import com.civiconegocios.app.models.Message;
import com.civiconegocios.app.models.Response;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


public class MessageDetailActivity extends BaseActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks {

    private static final String MESSAGE_RESPONSE_HINT = "message_response_hint";
    private static final String PROCESS_DIALOG_SENDING_MESSAGE = "process_dialog_sending_message";
    private static final String PROCESS_DIALOG_ARCHIVE_MESSAGE = "process_dialog_archive_message";
    //    Loaders IDS
    private final int REPLY_MESSAGE_LOADER = 10;
    private final int MESSAGE_STATUS_LOADER = 20;
    MessageDetailExpandableListAdapter messageDetailExpandableListAdapter;
    ExpandableListView responsesListView;
    String messageId;
    Message message;
    Realm realm;
    TrackingOps trackingOps;
    TextView senderNameTextView, senderEmailTextView, toNameTextView,
            messageTypeTextView, messageDateTextView, messageTextView,
            answerTextView, refferedTextView;
    EditText responseEditText;
    ImageView answerImageView, messageDetailGoBack, messageArchiveGoBack;
    FloatingActionButton sendMessage;
    ConstraintLayout answerContainer;
    Utils util = new Utils();
    User user;
    Integer newId = 9090090;
    RealmList<Response> responses;
    ScrollView detailScrollView;
    ProgressDialog replyProgressDialog;
    Boolean isReplying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        trackingOps = (TrackingOps) getApplication();
        user = trackingOps.getUser();
        realm = trackingOps.getRealm();

        responsesListView = findViewById(R.id.responsesListView);
        messageTypeTextView = findViewById(R.id.tv_message_type);
        senderNameTextView = findViewById(R.id.senderNameTextView);
        senderEmailTextView = findViewById(R.id.senderEmailTextView);
        toNameTextView = findViewById(R.id.toNameTextView);
        messageDateTextView = findViewById(R.id.messageDateTextView);
        messageTextView = findViewById(R.id.messageTextView);
        answerContainer = findViewById(R.id.answerContainer);
        refferedTextView = findViewById(R.id.tv_referido);
        detailScrollView = findViewById(R.id.detailScrollView);
        answerContainer.setOnClickListener(this);
        messageDetailGoBack = findViewById(R.id.messageDetailGoBack);
        messageDetailGoBack.setOnClickListener(this);
        messageArchiveGoBack = findViewById(R.id.messageArchiveGoBack);
        messageArchiveGoBack.setOnClickListener(this);

        responseEditText = findViewById(R.id.et_respuesta);
        responseEditText.setHint(trackingOps.getmFirebaseRemoteConfig().getString(MESSAGE_RESPONSE_HINT));
        responseEditText.setTypeface(util.fuenteParrafos(getApplicationContext()));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            messageId = extras.getString("id");

            RealmResults<Message> result = realm.where(Message.class)
                    .equalTo("id", messageId)
                    .findAll();

            if (!result.isEmpty()) {
                message = result.first();
                responses = message.getResponses();
                messageDetailExpandableListAdapter = new MessageDetailExpandableListAdapter(this, responses, user, message);
                responsesListView.setAdapter(messageDetailExpandableListAdapter);

                if (message.getStatus().equals("unread")) {
                    makeStatusRequest("read");
                }

                try {

                    messageTypeTextView.setText(URLDecoder.decode(message.getType(), "UTF-8"));
                    senderNameTextView.setText(URLDecoder.decode(message.getName() + " " + message.getLastname(), "UTF-8"));
                    senderEmailTextView.setText(URLDecoder.decode("de " + message.getEmail(), "UTF-8"));
                    toNameTextView.setText("para " + message.getPlace());

                    messageTextView.setText(message.getMessage());

                    DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
                    Date firTermsDate = readFormat.parse(message.getCreated_at());
                    long timeInMilliseconds = firTermsDate.getTime();

                    messageDateTextView.setText(
                            DateUtils.getRelativeTimeSpanString(
                                    timeInMilliseconds,
                                    System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS,
                                    DateUtils.FORMAT_ABBREV_ALL).toString());
                    if (message.getRelated()) {
                        refferedTextView.setVisibility(View.VISIBLE);
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }

        responsesListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                messageDetailExpandableListAdapter.notifyDataSetChanged();
                setListViewHeight(parent, groupPosition);
                return false;
            }
        });

        responsesListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                parent.collapseGroup(groupPosition);
                setListViewHeight(responsesListView, groupPosition);
                return false;
            }
        });

        updateListSize();

        sendMessage = findViewById(R.id.sendMessage);
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (responseEditText.getText().toString().matches("")) {
                    Snackbar.make(view, "Por favor ingrese su respuesta", Snackbar.LENGTH_LONG)
                            .setAction("Error", null).show();
                } else {
                    if (!isReplying) {
                        isReplying = true;
                        makeReplyRequest();
                    }

                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.answerContainer:
                responseEditText.setVisibility(View.VISIBLE);
                sendMessage.hide();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(responseEditText, InputMethodManager.SHOW_IMPLICIT);
                break;
            case R.id.messageDetailGoBack:
                finish();
                break;
            case R.id.messageArchiveGoBack:
                toggleProgressDialog(true);
                makeStatusRequest("archived");
                break;
            default:
                break;
        }
    }

    public void makeStatusRequest(String status) {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {

            if (status.equals("archived")) {
                toggleProgressSendingMessageDialog(true, PROCESS_DIALOG_ARCHIVE_MESSAGE);
            }

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    message.setStatus(status);
                    realm.copyToRealmOrUpdate(message);
                }
            });

            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            body.put("status", status);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "PATCH");
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + "/business/customers/places/" + user.getBrand().getId() + "/messages/" + message.getId() + "?auth_token=" + user.getAccessToken());

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(MESSAGE_STATUS_LOADER);

            if (loader == null) {
                loaderManager.initLoader(MESSAGE_STATUS_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(MESSAGE_STATUS_LOADER, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        }
    }

    public void makeReplyRequest() {
        Bundle queryBundle = new Bundle();
        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");

            JSONObject body = new JSONObject();
            body.put("text", responseEditText.getText().toString());

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL_OFERTAS + "/business/customers/places/"
                    + user.getBrand().getId() + "/messages/" + message.getId() +
                    "/responses?auth_token=" + user.getAccessToken());

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader loader = loaderManager.getLoader(REPLY_MESSAGE_LOADER);

            if (loader == null) {
                loaderManager.initLoader(REPLY_MESSAGE_LOADER, queryBundle, this);
            } else {
                loaderManager.restartLoader(REPLY_MESSAGE_LOADER, queryBundle, this);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertErrorTimeOut(this);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        switch (id) {
            case REPLY_MESSAGE_LOADER:
                toggleProgressSendingMessageDialog(true, PROCESS_DIALOG_SENDING_MESSAGE);
                return new RequestDefaultFactory(this, args);
            case MESSAGE_STATUS_LOADER:
                return new RequestDefaultFactory(this, args);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object data) {
        toggleProgressDialog(false);
        toggleProgressSendingMessageDialog(false, PROCESS_DIALOG_SENDING_MESSAGE);
        switch (loader.getId()) {
            case REPLY_MESSAGE_LOADER:
                isReplying = false;
                if (data == null) {
                    util.alertError500(this);
                } else {
                    responseEditText.clearFocus();

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            String date = df.format(Calendar.getInstance().getTime());

                            final Response response = new Response();
                            response.setId(String.valueOf(newId));
                            response.setCreated_at(date);
                            response.setUser_id("null");
                            response.setText(responseEditText.getText().toString());
                            responses.add(response);
                            messageDetailExpandableListAdapter.notifyDataSetChanged();
                            newId += 1;
                        }
                    });

                    responseEditText.getText().clear();

                    trackingOps.setInboxReloadData(true);
                    Toast.makeText(this, "Mensaje Enviado", Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < messageDetailExpandableListAdapter.getGroupCount(); ++i) {
                        if (i < messageDetailExpandableListAdapter.getGroupCount()) {
                            responsesListView.collapseGroup(i);
                        }
                    }

                    updateListSize();
                    Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            detailScrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    };

                    handler.postDelayed(runnable, 300);

                }
                break;
            case MESSAGE_STATUS_LOADER:
                if (data == null) {
                    util.alertError500(this);
                } else {
                    trackingOps.setInboxReloadData(true);
                    if (message.getStatus().equals("archived")) {
                        MessageArchivedDialog messageArchivedDialog = new MessageArchivedDialog(this);
                        messageArchivedDialog.show();
                    }
                }
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        toggleProgressDialog(false);
        toggleProgressSendingMessageDialog(false, "");
    }

    private void setListViewHeight(ExpandableListView listView,
                                   int group) {


        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
                int totalHeight = 0;
                int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                        View.MeasureSpec.EXACTLY);
                for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                    View groupItem = listAdapter.getGroupView(i, false, null, listView);
                    groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    if (!listView.isGroupExpanded(i)) {
                        totalHeight += groupItem.getMeasuredHeight();
                    }

                    if (listView.isGroupExpanded(i)) {
                        View listItem = listAdapter.getChildView(i, 0, false, null,
                                listView);
                        listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                        totalHeight += listItem.getMeasuredHeight();
                    }
                }

                ViewGroup.LayoutParams params = listView.getLayoutParams();
                int height = totalHeight
                        + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
                if (height < 10)
                    height = 200;
                params.height = height;
                listView.setLayoutParams(params);
                listView.requestLayout();
            }
        };

        handler.postDelayed(runnable, 10);

    }

    public void updateListSize() {
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                responsesListView.performItemClick(responsesListView.getChildAt(messageDetailExpandableListAdapter.getGroupCount() - 1),
                        messageDetailExpandableListAdapter.getGroupCount() - 1,
                        responsesListView.getItemIdAtPosition(messageDetailExpandableListAdapter.getGroupCount() - 1));
            }
        };

        handler.postDelayed(runnable, 100);
    }

    public void toggleProgressSendingMessageDialog(Boolean show, String message) {

        if (show) {
            if (replyProgressDialog == null) {
                replyProgressDialog = new ProgressDialog(this);
                replyProgressDialog.setCanceledOnTouchOutside(false);
                replyProgressDialog.setCancelable(false);
            }
            replyProgressDialog.setMessage(((TrackingOps) getApplication()).getmFirebaseRemoteConfig().getString(message));
            replyProgressDialog.show();
        } else {
            if (replyProgressDialog != null && replyProgressDialog.isShowing()) {
                replyProgressDialog.dismiss();
            }
        }
    }

}
