package com.civiconegocios.app;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.utils.TextUbuntuLight;
import com.civiconegocios.app.utils.Utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import static com.pushwoosh.internal.platform.AndroidPlatformModule.getApplicationContext;

public class DialogConfirmarCobro extends AppCompatDialog implements View.OnClickListener {

    private static final String CONFIRM_PAYMENT = "confirm_payment";
    private static final String LBL_CANCEL = "lbl_cancel";
    private static final String CIVIC_PAY_TRADES = "civic_pay_trades";
    private static final String LBL_TO_ACCEPT = "lbl_to_accept";
    private static final String LBL_ARE_YOU_SURE = "lbl_are_you_sure";
    private static final String LBL_RECTIFY_PIN = "lbl_rectify_pin";
    private static final String LBL_USER_CORRECT = "lbl_user_correct";
    private static final String USER_CODE = "user_code";
    private static final String BASE_VALUE = "base_value";
    private static final String TAX = "tax";
    private static final String CONSUMPTION_TAX = "consumption_tax";
    private static final String TIP = "tip";
    private static final String TOTAL_VALUE = "total_value";


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    PinUsuarioActivity c;
    LinearLayout lnBtnCancelarCobro;
    LinearLayout lnAceptarCobro;
    TextView txtTituloDialogConfirmar;
    TextView txtParrafoDialogConfirmar;
    TextView txtParrafoDosDialogConfirmar;
    TextView txtCodigoPinDialog;
    TextView txtValorDialog;
    TextView txtAceptarlnAceptarCobro;
    TextView txtCancelarlnBtnCancelarCobro;
    // nuevos txt
    TextView txtLblCodigoPinDialog;
    TextView txtTituloValorBaseDialog;
    TextView txtValorBaseDialog;
    TextView txtTituloValorIvaDialog;
    TextView txtValorIvaDialog;
    TextView txtTituloValorTotalDialog;
    TextView textUbuntuLight2;
    TextView textUbuntuLight;
    TextUbuntuLight txtValorImpoconsumo;
    TextUbuntuLight txtValorPropina;
    Utils util = new Utils();
    String pinDialog;
    String valor;
    String valorBase;
    String valorIva;
    String valorImpoconsumo;
    String valorPropina;
    //    Ops
    TrackingOps trackingOps;
    User user;


    public DialogConfirmarCobro(PinUsuarioActivity a, int formaspago, String pinDialog, String valor, String valorBase, String valorIva, String valorImpoconsumo, String valorPropina) {
        super(a);

        this.c = a;
        this.pinDialog = pinDialog;
        this.valor = valor;
        this.valorBase = valorBase;
        this.valorIva = valorIva;
        this.valorImpoconsumo = valorImpoconsumo;
        this.valorPropina = valorPropina;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirmar_cobro);
        lnBtnCancelarCobro = findViewById(R.id.lnBtnCancelarCobro);
        lnAceptarCobro = findViewById(R.id.lnAceptarCobro);

        trackingOps = (TrackingOps) getApplicationContext();

        txtTituloDialogConfirmar = findViewById(R.id.txtTituloDialogConfirmar);
        txtTituloDialogConfirmar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_ARE_YOU_SURE));
        txtParrafoDialogConfirmar = findViewById(R.id.txtParrafoDialogConfirmar);
        txtParrafoDialogConfirmar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_RECTIFY_PIN));
        txtParrafoDosDialogConfirmar = findViewById(R.id.txtParrafoDosDialogConfirmar);
        txtParrafoDosDialogConfirmar.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_USER_CORRECT));
        txtCodigoPinDialog = findViewById(R.id.txtCodigoPinDialog);
        txtValorDialog = findViewById(R.id.txtValorDialog);
        txtAceptarlnAceptarCobro = findViewById(R.id.txtAceptarlnAceptarCobro);
        txtAceptarlnAceptarCobro.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(LBL_TO_ACCEPT));
        txtCancelarlnBtnCancelarCobro = findViewById(R.id.txtCancelarlnBtnCancelarCobro);
        txtCancelarlnBtnCancelarCobro.setText(" " + trackingOps.getmFirebaseRemoteConfig().getString(LBL_CANCEL));

        txtLblCodigoPinDialog = findViewById(R.id.txtLblCodigoPinDialog);
        txtLblCodigoPinDialog.setText(trackingOps.getmFirebaseRemoteConfig().getString(USER_CODE));
        txtTituloValorBaseDialog = findViewById(R.id.txtTituloValorBaseDialog);
        txtTituloValorBaseDialog.setText(trackingOps.getmFirebaseRemoteConfig().getString(BASE_VALUE));
        txtValorBaseDialog = findViewById(R.id.txtValorBaseDialog);
        txtTituloValorIvaDialog = findViewById(R.id.txtTituloValorIvaDialog);
        txtTituloValorIvaDialog.setText(trackingOps.getmFirebaseRemoteConfig().getString(TAX));
        txtValorIvaDialog = findViewById(R.id.txtValorIvaDialog);
        textUbuntuLight2 = findViewById(R.id.textUbuntuLight2);
        textUbuntuLight2.setText(trackingOps.getmFirebaseRemoteConfig().getString(CONSUMPTION_TAX));
        textUbuntuLight = findViewById(R.id.textUbuntuLight);
        textUbuntuLight.setText(trackingOps.getmFirebaseRemoteConfig().getString(TIP));

        txtTituloValorTotalDialog = findViewById(R.id.txtTituloValorTotalDialog);
        txtTituloValorTotalDialog.setText(trackingOps.getmFirebaseRemoteConfig().getString(TOTAL_VALUE));

        txtValorImpoconsumo = findViewById(R.id.txtValorImpoconsumo);
        txtValorPropina = findViewById(R.id.txtValorPropina);


        txtTituloDialogConfirmar.setTypeface(util.fuenteSubTituHint(c));
        txtParrafoDialogConfirmar.setTypeface(util.fuenteParrafos(c));
        txtParrafoDosDialogConfirmar.setTypeface(util.fuenteParrafos(c));
        txtCodigoPinDialog.setTypeface(util.fuenteSubTituHint(c));
        txtValorDialog.setTypeface(util.fuenteSubTituHint(c));
        txtAceptarlnAceptarCobro.setTypeface(util.fuenteMenuLatBotones(c));
        txtCancelarlnBtnCancelarCobro.setTypeface(util.fuenteMenuLatBotones(c));


        txtLblCodigoPinDialog.setTypeface(util.fuenteSubTituHint(c));
        txtTituloValorBaseDialog.setTypeface(util.fuenteSubTituHint(c));
        txtValorBaseDialog.setTypeface(util.fuenteSubTituHint(c));
        txtTituloValorIvaDialog.setTypeface(util.fuenteSubTituHint(c));
        txtValorIvaDialog.setTypeface(util.fuenteSubTituHint(c));
        txtTituloValorTotalDialog.setTypeface(util.fuenteSubTituHint(c));

        txtAceptarlnAceptarCobro.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(c, R.drawable.ic_codigo_ico_aceptar), null, null, null);
        txtCancelarlnBtnCancelarCobro.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(c, R.drawable.ic_codigo_ico_cancelar), null, null, null);


        /*
                    singleton.setMontoValorBase(numeroVenta); // totall con todos
                    singleton.setMontoValorIva(numeroIva); // esto se debe restar al total
                    singleton.setMontoValorImpoconsumo(numeroImpoconsumo); // esto se debe restar al total
                    singleton.setMontoValorPropina(numeroPropina); // esto se suma a lo que quedo de la resta
         */


        lnBtnCancelarCobro.setOnClickListener(this);
        lnAceptarCobro.setOnClickListener(this);
        txtCodigoPinDialog.setText(pinDialog);


       /* int valorBseInt =0;
        int valorInt = Integer.parseInt(valor.replace(".",""));
        int valorIvaInt = 0;
        if (!valorIva.equals("")){
             valorIvaInt = Integer.parseInt(valorIva.replace(".",""));
        }
        valorBseInt = valorInt-valorIvaInt;
        txtValorDialog.setText("$"+valorBase);

        txtValorBaseDialog.setText("$"+miles(valorBseInt+"").replace(",","."));
        singleton.setMontoValorBase(miles(valorBseInt+"").replace(",",""));
        if (valorIva.equals("")){
            valorIva = "0";
        }
        txtValorIvaDialog.setText("$"+valorIva);


        txtValorImpoconsumo.setText(valorImpoconsumo);
        txtValorPropina.setText(valorPropina);*/


        int valorBaseInt = 0;
        int valorIvaInt = 0;
        int valorImpoconsumoInt = 0;
        int valorPropinaInt = 0;
        if (!valorBase.equals("")) {
            valorBaseInt = Integer.parseInt(valorBase.replace(".", ""));
        }
        if (!valorIva.equals("")) {
            valorIvaInt = Integer.parseInt(valorIva.replace(".", ""));
        }
        if (!valorImpoconsumo.equals("")) {
            valorImpoconsumoInt = Integer.parseInt(valorImpoconsumo.replace(".", ""));
        }
        if (!valorPropina.equals("")) {
            valorPropinaInt = Integer.parseInt(valorPropina.replace(".", ""));
        }

        int valorBaseRealInt = valorBaseInt - valorIvaInt - valorImpoconsumoInt;
        int valorTotalReal = valorBaseInt + valorPropinaInt;

        txtValorBaseDialog.setText("$ " + miles(valorBaseRealInt + "").replace(",", "."));
        txtValorIvaDialog.setText("$ " + valorIva);
        txtValorImpoconsumo.setText("$ " + valorImpoconsumo);
        txtValorPropina.setText("$ " + valorPropina);
        if (valorIva.equals("")) {
            txtValorIvaDialog.setText("$ 0");
        }
        if (valorImpoconsumo.equals("")) {
            txtValorImpoconsumo.setText("$ 0");
        }
        if (valorPropina.equals("")) {
            txtValorPropina.setText("$ 0");
        }

        txtValorDialog.setText("$" + miles(valorTotalReal + "").replace(",", "."));

        trackingOps = (TrackingOps) c.getApplication();
        user = trackingOps.getUser();
        trackingOps.registerScreenName("Ingresar Pin | Cívico PAY Comercios | " + user.getAnalyticsCity());


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnBtnCancelarCobro:
                trackingOps.registerEvent(trackingOps.getmFirebaseRemoteConfig().getString(CIVIC_PAY_TRADES), trackingOps.getmFirebaseRemoteConfig().getString(CONFIRM_PAYMENT), trackingOps.getmFirebaseRemoteConfig().getString(LBL_CANCEL));
                dismiss();
                break;
            case R.id.lnAceptarCobro:
                trackingOps.registerEvent(trackingOps.getmFirebaseRemoteConfig().getString(CIVIC_PAY_TRADES), trackingOps.getmFirebaseRemoteConfig().getString(CONFIRM_PAYMENT), trackingOps.getmFirebaseRemoteConfig().getString(LBL_TO_ACCEPT));
                c.cargando();
                dismiss();
                break;
            default:
                break;
        }

    }

    public String miles(String no) {
        no = no.replace(".", "");
        BigDecimal sPrice = new BigDecimal(no);
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(',');
        DecimalFormat formatter = new DecimalFormat("###,###.##", symbols);
        String str = formatter.format(sPrice.longValue());
        return str;
    }
}
