package com.civiconegocios.app.sync;

import android.content.Context;
import androidx.loader.content.AsyncTaskLoader;

import com.civiconegocios.app.BuildConfig;
import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.VO.CategoriasVO;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.servicios.ObjectHttp;
import com.civiconegocios.app.utils.JsonUtils;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class LoaderPreguntasFrecuentes extends AsyncTaskLoader<ArrayList<CategoriasVO>> {

    private final String TAG = LoaderPreguntasFrecuentes.class.getSimpleName();

    private ArrayList<CategoriasVO> result;
    private String city = "bogota";

    public LoaderPreguntasFrecuentes(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        User user = ((TrackingOps) getContext().getApplicationContext()).getUser();
        this.city = user.getCity();

        if (this.result != null) deliverResult(this.result);
        else forceLoad();
    }

    @Override
    public ArrayList<CategoriasVO> loadInBackground() {

        ObjectHttp httResponse = new ObjectHttp(getContext());
        String ciudad = city;
        ciudad = ciudad.replace("á", "a");
        ciudad = ciudad.replace("é", "e");
        ciudad = ciudad.toLowerCase();

        if (ciudad.equalsIgnoreCase("cdmx"))
            ciudad = "mexico";

        String urlServicio = "/faq/topics/" + ciudad + "/negocios";

        try {
            String responseStr = httResponse.obtenerHttpGet(
                    BuildConfig.API_URL_OFERTAS,
                    urlServicio,
                    city);
            return (new JsonUtils()).getPreguntasFrecuentes(responseStr);
        } catch (IOException | JSONException e) {
        }

        return null;
    }

    @Override
    public void deliverResult(ArrayList<CategoriasVO> data) {
        this.result = data;
        super.deliverResult(data);
    }
}
