package com.civiconegocios.app;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.User;
import com.civiconegocios.app.services.RequestDefaultFactory;
import com.civiconegocios.app.utils.ConnectionLiveData;
import com.civiconegocios.app.utils.ConnectionModel;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import io.realm.Realm;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

@RuntimePermissions
public class CargandoCobroActivty extends BaseActivity implements LoaderManager.LoaderCallbacks<String>,
        OnSuccessListener<Location> {

    private static final String PERMISSION_LOCATION_RATIONALE = "permission_location_rationale";
    private static final String BUTTON_ALLOW = "button_allow";
    private static final String BUTTON_DENY = "button_deny";
    private static final String CONFIRM_PAYMENT = "confirm_payment";
    private static final String PAYMENT_MADE = "payment_made";
    private static final String TRANSA_ERRONEA = "transa_erronea";
    private static final String MSJ_CREATION_PAYMENT_ORDER = "msj_creation_payment_order";
    private static final String PROCESSING = "processing";
    private static final String PERFORMING_TRANSACTION = "performing_transaction";
    private static final String SENDING_TRANSACTION_TEXT_1 = "sending_transaction_text_1";
    private static final String SENDING_TRANSACTION_TEXT_2 = "sending_transaction_text_2";

    private final String TAG = CargandoCobroActivty.class.getSimpleName();
    GifImageView givImageView;
    Utils util = new Utils();
    TextView txtTituloCargando;
    TextView txtTituloEnProceso;
    TextView txtParrafoProcesoUno;
    TextView txtParrafoProcesoDos;
    TextView txtParrafoProcesoTres;
    //    Ops
    TrackingOps trackingOps;
    String coordinates;
    String serviceEndpoint = "/customers/transactions";
    int gpsRetries = 0;
    Realm realm;
    User user;
    FusedLocationProviderClient fusedLocationProviderClient;
    // Transaction
    String montoValorBase = "";
    String montoValorIva = "";
    String montoValorImpoConsumo = "";
    String montoValorPropina = "";
    String monto = "";
    String userPin = "";

    public static String encode(String access_token_str) throws UnsupportedEncodingException {
        // Sending side
        byte[] data = access_token_str.getBytes("UTF-8");
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }

    @Override
    @AddTrace(name = "onCreateTrace")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cargando_cobro_activity);


        givImageView = findViewById(R.id.imgGif);

        initDrawer();
        trackingOps = (TrackingOps) getApplication();
        realm = trackingOps.getRealm();
        user = trackingOps.getUser();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            montoValorBase = extras.getString("montoValorBase");
            montoValorIva = extras.getString("montoValorIva");
            montoValorImpoConsumo = extras.getString("montoValorImpoConsumo");
            montoValorPropina = extras.getString("montoValorPropina");
            monto = extras.getString("monto");
            userPin = extras.getString("userPin");
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        txtTituloCargando = findViewById(R.id.txtTituloCargando);
        txtTituloCargando.setText(trackingOps.getmFirebaseRemoteConfig().getString(PROCESSING));
        txtTituloEnProceso = findViewById(R.id.txtTituloEnProceso);
        txtTituloEnProceso.setText(trackingOps.getmFirebaseRemoteConfig().getString(PERFORMING_TRANSACTION));
        txtParrafoProcesoUno = findViewById(R.id.txtParrafoProcesoUno);
        txtParrafoProcesoUno.setText(trackingOps.getmFirebaseRemoteConfig().getString(SENDING_TRANSACTION_TEXT_1));
        txtParrafoProcesoDos = findViewById(R.id.txtParrafoProcesoDos);
        txtParrafoProcesoDos.setText(trackingOps.getmFirebaseRemoteConfig().getString(SENDING_TRANSACTION_TEXT_2));
        txtParrafoProcesoTres = findViewById(R.id.txtParrafoProcesoTres);

      /*  txtTituloCargando.setTypeface(util.fuenteHeader(getApplicationContext()));
        txtTituloEnProceso.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        txtParrafoProcesoUno.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtParrafoProcesoDos.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtParrafoProcesoTres.setTypeface(util.fuenteParrafos(getApplicationContext()));*/

        try {
            GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.cargando);
            givImageView.setImageDrawable(gifFromResource);

        } catch (IOException e) {
            e.printStackTrace();
        }

        verificacionConexionInternet();
    }

    private void verificacionConexionInternet() {
        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getApplicationContext());
        connectionLiveData.observe(this, new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()) {
                        case 1:
                            callRequestPayment();
                            break;
                        case 2:
                            callRequestPayment();
                            break;
                    }
                } else {
                    showSnackBarMessage("No cuenta con conexión a internet", Snackbar.LENGTH_LONG, "Configuración");
                    findViewById(R.id.txtParrafoProcesoCuatro).setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void callRequestPayment() {
        CargandoCobroActivtyPermissionsDispatcher.processPaymentWithPermissionCheck(this);
    }

    public void showSnackBarMessage(String message, int duration, String action) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.toolbar), message, duration);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.black));
        switch (action) {
            case "Configuración":
                snackbar.setAction(action, InternetNetworkSetting());
                snackbar.setActionTextColor(Color.WHITE);
                break;
        }
        snackbar.show();
    }

    private View.OnClickListener InternetNetworkSetting() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                startActivity(intent);
            }
        };
    }

    @NeedsPermission({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
    public void processPayment() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        CargandoCobroActivtyPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
    void showRationaleForLocation(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PERMISSION_LOCATION_RATIONALE))
                .setPositiveButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_ALLOW), (dialog, button) -> request.proceed())
                .setNegativeButton(trackingOps.getmFirebaseRemoteConfig().getString(BUTTON_DENY), (dialog, button) -> request.cancel())
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
    void showDeniedForLocation() {
        processPaymentAction();
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain({android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION})
    void showNeverAskForLocation() {
        processPaymentAction();
    }

    private void processPaymentAction() {
        coordinates = "0.0,0.0";
        makeDataRequest();
    }

    public void exitoso() {
        trackingOps.registerScreenName("Pago realizado| Cívico PAY Comercios | " + user.getAnalyticsCity());
        trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, trackingOps.getmFirebaseRemoteConfig().getString(CONFIRM_PAYMENT), trackingOps.getmFirebaseRemoteConfig().getString(PAYMENT_MADE));
        finish();
    }

    public void fallido(String stattus) {
        Toast.makeText(getApplicationContext(), "El PIN de usuario ingresado no es correcto, por favor verifica los datos ingresados!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, PinUsuarioActivity.class);
        intent.putExtra("montoValorBase", montoValorBase);
        intent.putExtra("montoValorIva", montoValorIva);
        intent.putExtra("montoValorImpoConsumo", montoValorImpoConsumo);
        intent.putExtra("montoValorPropina", montoValorPropina);
        intent.putExtra("monto", monto);
        startActivity(intent);
        finish();
    }

    public void makeDataRequest() {
        Bundle queryBundle = new Bundle();
        JSONObject body = new JSONObject();

        try {
            String encoded = util.encode(user.getAccessToken());
            encoded = encoded.replaceAll("\n", "");
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            String amount = monto.replace(".", "").replace(".", "");
            amount = URLEncoder.encode(amount, "utf-8");
            String description = trackingOps.getmFirebaseRemoteConfig().getString(MSJ_CREATION_PAYMENT_ORDER) + monto;

            body.put("amount", amount);
            body.put("description", URLEncoder.encode(description, "utf-8"));
            body.put("pin", URLEncoder.encode(userPin, "utf-8"));
            body.put("tax_amount", montoValorIva.replace(".", ""));
            body.put("iac_amount", montoValorImpoConsumo.replace(".", ""));
            body.put("tip_amount", montoValorPropina.replace(".", ""));
            body.put("app_version", pInfo.versionName);
            body.put("device_id", user.getDeviceID());
            body.put("device_token", user.getPushToken());
            body.put("coordinates", coordinates);

            queryBundle.putString(Constants.OPERATION_BODY, body.toString());
            queryBundle.putString(Constants.OPERATION_PARAMETERS, encoded);
            queryBundle.putString(Constants.OPERATION_TYPE, "POST");
            queryBundle.putString(Constants.OPERATION_CITY, user.getCity());
            queryBundle.putString(Constants.OPERATION_URL, BuildConfig.API_URL + serviceEndpoint);

            LoaderManager loaderManager = getSupportLoaderManager();
            Loader<String> loader = loaderManager.getLoader(0);

            if (loader == null) {
                loaderManager.initLoader(0, queryBundle, this);
            } else {
                loaderManager.restartLoader(0, queryBundle, this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            util.alertError500(this);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new RequestDefaultFactory(this, args);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

        Log.i(TAG, "data: " + data);

        if (data == null) {
            fallido(0 + "");
            util.alertError500(this);
        } else {
            try {
                JSONObject catObj = new JSONObject(data);
                if (catObj.getBoolean("success")) {
                    exitoso();
                } else {
                    fallido(0 + "");
                    util.alertError500(this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                fallido(0 + "");
                util.alertError500(this);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        toggleProgressDialog(false);
    }

    @Override
    public void onSuccess(Location location) {
        if (location != null) {
            realm.beginTransaction();
            user.setLatitude(location.getLatitude());
            user.setLongitude(location.getLongitude());
            realm.commitTransaction();

            coordinates = location.getLongitude() + "," + location.getLatitude();
            makeDataRequest();
        } else {
            processPaymentAction();
        }
    }
}
