package com.civiconegocios.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.models.DefaultCivicoResponse;
import com.civiconegocios.app.services.CivicoLoginWebService;
import com.civiconegocios.app.utils.NetworkHelper;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OlvidoPassActivty extends AppCompatActivity implements View.OnClickListener {

    private static final String PROCESS_DIALOG_MESSAGE = "process_dialog_message";
    private static final String ALERT_500 = "alert_500";
    private static final String LBL_RECUPERAR_PASS_INICIAL = "lbl_recuperar_pass_inicial";
    private static final String LBL_CORREO_INICIAL = "lbl_correo_inicial";
    private static final String LBL_BTN_REMEMBER_PASSWORD = "lbl_btn_remember_password";

    LinearLayout btnRecuperarContrasena;
    EditText editTextCorreoRecuperarContra;
    TextView txtTituloOlvide;
    TextView txtbtnRecuperarContrasena;
    Utils util = new Utils();
    ProgressDialog progressDialog;
    TrackingOps trackingOps;
    private boolean networkOk;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.olvido_pass_activity);

        trackingOps = (TrackingOps) getApplication();

        btnRecuperarContrasena = findViewById(R.id.btnRecuperarContrasena);
        btnRecuperarContrasena.setOnClickListener(this);
        editTextCorreoRecuperarContra = findViewById(R.id.editTextCorreoRecuperarContra);
        editTextCorreoRecuperarContra.setHint(trackingOps.getmFirebaseRemoteConfig().getString(LBL_CORREO_INICIAL));
        txtTituloOlvide = findViewById(R.id.txtTituloOlvide);
        txtTituloOlvide.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_RECUPERAR_PASS_INICIAL));
        txtbtnRecuperarContrasena = findViewById(R.id.txtbtnRecuperarContrasena);
        txtbtnRecuperarContrasena.setText(trackingOps.getmFirebaseRemoteConfig().getString(LBL_BTN_REMEMBER_PASSWORD));

        editTextCorreoRecuperarContra.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        // txtTituloOlvide.setTypeface(util.fuenteSubTituHint(getApplicationContext()));
        // txtbtnRecuperarContrasena.setTypeface(util.fuenteSubTituHint(getApplicationContext()));


    }

    public void respuestaServicio(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        networkOk = NetworkHelper.hasNetworkAccess(this);
        switch (v.getId()) {
            case R.id.btnRecuperarContrasena:

                if (networkOk) {
                    if (Utils.isValidEmail(editTextCorreoRecuperarContra.getText().toString())) {

                        if (progressDialog == null) {
                            progressDialog = new ProgressDialog(this);
                            progressDialog.setMessage(trackingOps.getmFirebaseRemoteConfig().getString(PROCESS_DIALOG_MESSAGE));
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.setCancelable(false);
                        }

                        progressDialog.show();

                        CivicoLoginWebService civicoWebService =
                                CivicoLoginWebService.retrofit.create(CivicoLoginWebService.class);

                        Call<DefaultCivicoResponse> call = civicoWebService.recoverPassword(editTextCorreoRecuperarContra.getText().toString());

                        call.enqueue(new Callback<DefaultCivicoResponse>() {
                            @Override
                            public void onResponse(Call<DefaultCivicoResponse> call, Response<DefaultCivicoResponse> response) {

                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }

                                if (response.body() != null) {
                                    DefaultCivicoResponse dataResponse = response.body();

                                    try {
                                        Toast.makeText(getApplicationContext(), dataResponse.getUserMessage(), Toast.LENGTH_SHORT).show();
                                    } catch (NullPointerException ie) {
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<DefaultCivicoResponse> call, Throwable t) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                Toast.makeText(getApplicationContext(), trackingOps.getmFirebaseRemoteConfig().getString(ALERT_500),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } else {
                    util.mensajeNoInternet(getApplicationContext());
                }

                break;
            default:
                break;
        }
    }
}
