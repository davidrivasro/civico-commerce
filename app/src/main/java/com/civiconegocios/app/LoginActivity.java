package com.civiconegocios.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.civiconegocios.app.Ops.TrackingOps;
import com.civiconegocios.app.utils.Constants;
import com.civiconegocios.app.utils.Utils;
import com.google.firebase.perf.metrics.AddTrace;

public class LoginActivity extends Activity implements View.OnClickListener {
    private static final String LOGIN = "login";
    private static final String MENSAJE_BIENVENIDA = "mensaje_bienvenida";
    LinearLayout lnBtnIniciarSesion;
    TextView txtParrafoIniciarSesion;
    TextView txtlnBtnIniciarSesion;
    Utils util = new Utils();
    com.google.android.gms.analytics.Tracker mTracker;
    //    Ops
    TrackingOps trackingOps;
    ;

    @Override
    @AddTrace(name = "onCreateTrace")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginactivity);

        trackingOps = (TrackingOps) getApplication();

        lnBtnIniciarSesion = findViewById(R.id.lnBtnIniciarSesion);
        txtParrafoIniciarSesion = findViewById(R.id.txtParrafoIniciarSesion);
        txtParrafoIniciarSesion.setText(trackingOps.getmFirebaseRemoteConfig().getString(MENSAJE_BIENVENIDA));
        lnBtnIniciarSesion.setOnClickListener(this);
        txtParrafoIniciarSesion.setTypeface(util.fuenteParrafos(getApplicationContext()));
        txtlnBtnIniciarSesion = findViewById(R.id.txtlnBtnIniciarSesion);
        txtlnBtnIniciarSesion.setText(trackingOps.getmFirebaseRemoteConfig().getString(LOGIN).toUpperCase());
        txtlnBtnIniciarSesion.setTypeface(util.fuenteMenuLatBotones(getApplicationContext()));

        trackingOps.registerScreenName("Home | Cívico PAY Comercios | bogota");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnBtnIniciarSesion:
                trackingOps.registerEvent(Constants.CRASHLYTICS_CATEGORY, "Home principal", trackingOps.getmFirebaseRemoteConfig().getString(LOGIN));
                Intent detalle = new Intent(getApplicationContext(), IniciarSesion.class);
                startActivity(detalle);
                finish();
                break;
            default:
                break;
        }
    }
}
